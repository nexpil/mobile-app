//
//  Page.swift
//  VisionAPIClient
//
//  Created by Cagri Sahan on 6/19/18.
//  Copyright © 2018 Cagri Sahan. All rights reserved.
//

public struct Page: Codable {
    public let property: TextProperty
    public let width: Int
    public let height: Int
    public var blocks: [Block]
    public let confidence: Float?        
}
