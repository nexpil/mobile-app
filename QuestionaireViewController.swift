//
//  QuestionaireViewController.swift
//  Nexpil
//
//  Created by CROCODILE on 16.01.2021.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class QuestionaireViewController: UIViewController {
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var txtAnswer: UITextField!
    
    var task = JSON()
    var currentIndex = 0
    var questionCounts = 0
    var questions = [JSON]()
    var stepCompleted = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
        self.initValues()
//        self.lblDescription.text = questions[0]
        // Do any additional setup after loading the view.
    }
    
    private func initUI(){
        self.btnNext.layer.cornerRadius = 24
        self.btnNext.layer.masksToBounds = true
        self.lblQuestion.text = self.task["name"].stringValue
        self.btnYes.layer.cornerRadius = 5
        self.btnNo.layer.cornerRadius = 5
        self.btnYes.layer.borderWidth = 1
        self.btnYes.layer.borderColor = UIColor.gray.cgColor
        self.btnNo.layer.borderWidth = 1
        self.btnNo.layer.borderColor = UIColor.gray.cgColor
        self.btnYes.layer.masksToBounds = true
        self.btnNo.layer.masksToBounds = true
        self.txtAnswer.text = ""
    }
    
    private func initValues(){
        let values = JSON(self.task["details"]["questions"])
        for item in values.arrayValue {
            self.questions.append(item)
//            if (self.stepCompleted.count == 0){
                self.stepCompleted.append("")
//            }
        }
        if (self.questions.count > 0){
            self.progressView.progress = Float(1 / self.questions.count)
        }
        self.lblDescription.text = self.questions[self.currentIndex]["question"].stringValue
        if (self.questions[self.currentIndex]["type"] == "multiple") {
            let answers = JSON(self.questions[0]["answers"])
            var i = 0
            for item in answers.arrayValue {
                if (i == 0){
                    self.btnYes.setTitle(item.stringValue, for: .normal)
                } else if (i == 1) {
                    self.btnNo.setTitle(item.stringValue, for: .normal)
                }
                i += 1
            }
            if (self.stepCompleted.count > 0){
                if (self.stepCompleted[self.currentIndex] == "1"){
                    self.btnYes.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
                    self.btnYes.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
                    self.btnYes.setTitleColor(UIColor.white, for: .normal)
                    self.btnYes.layer.masksToBounds = true
                    self.stepCompleted[self.currentIndex] = "1"
                    self.btnNo.backgroundColor = UIColor.init(hex: "ffffff")
                    self.btnNo.layer.borderWidth = 1
                    self.btnNo.layer.borderColor = UIColor.gray.cgColor
                    self.btnNo.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
                    self.btnNo.layer.masksToBounds = true
                } else if(self.stepCompleted[self.currentIndex] == "2"){
                    self.btnNo.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
                    self.btnNo.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
                    self.btnNo.setTitleColor(UIColor.white, for: .normal)
                    self.btnNo.layer.masksToBounds = true
                    self.stepCompleted[self.currentIndex] = "2"
                    self.btnYes.backgroundColor = UIColor.init(hex: "ffffff")
                    self.btnYes.layer.borderWidth = 1
                    self.btnYes.layer.borderColor = UIColor.gray.cgColor
                    self.btnYes.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
                    self.btnYes.layer.masksToBounds = true
                }
            }
            self.txtAnswer.isHidden = true
            self.btnYes.isHidden = false
            self.btnNo.isHidden = false
        } else {
            if (self.stepCompleted.count > 0){
                self.txtAnswer.text = self.stepCompleted[self.currentIndex]
            }
            self.txtAnswer.isHidden = false
            self.btnYes.isHidden = true
            self.btnNo.isHidden = true
        }
    }
    
    @IBAction func nextQuestion(_ sender: UIButton) {
        if (self.questions[self.currentIndex]["type"] != "multiple") {
            self.stepCompleted[self.currentIndex] = self.txtAnswer.text! as String
//            self.stepCompleted[self.currentIndex] = ""
        }
        if (self.stepCompleted[self.currentIndex] != "-1" && self.stepCompleted[self.currentIndex] != "") {
            if(self.currentIndex < self.questions.count - 1){
                self.currentIndex += 1
                self.progressView.setProgress(Float(Float(1) / Float(self.questions.count) * Float(currentIndex)), animated: true)
                self.lblDescription.text = self.questions[self.currentIndex]["question"].stringValue
                if (self.questions[self.currentIndex]["type"] == "multiple") {
                    let answers = JSON(self.questions[self.currentIndex]["answers"])
                    var i = 0
                    for item in answers.arrayValue {
                        if (i == 0){
                            self.btnYes.setTitle(item.stringValue, for: .normal)
                        } else if (i == 1) {
                            self.btnNo.setTitle(item.stringValue, for: .normal)
                        }
                        i += 1
                    }
                    self.txtAnswer.isHidden = true
                    self.btnYes.isHidden = false
                    self.btnNo.isHidden = false
                    if (self.stepCompleted[self.currentIndex] == "1"){
                        self.btnYes.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
                        self.btnYes.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
                        self.btnYes.setTitleColor(UIColor.white, for: .normal)
                        self.btnYes.layer.masksToBounds = true
                        self.stepCompleted[self.currentIndex] = "1"
                        self.btnNo.backgroundColor = UIColor.init(hex: "ffffff")
                        self.btnNo.layer.borderWidth = 1
                        self.btnNo.layer.borderColor = UIColor.gray.cgColor
                        self.btnNo.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
                        self.btnNo.layer.masksToBounds = true
                    } else if(self.stepCompleted[self.currentIndex] == "2"){
                        self.btnNo.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
                        self.btnNo.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
                        self.btnNo.setTitleColor(UIColor.white, for: .normal)
                        self.btnNo.layer.masksToBounds = true
                        self.stepCompleted[self.currentIndex] = "2"
                        self.btnYes.backgroundColor = UIColor.init(hex: "ffffff")
                        self.btnYes.layer.borderWidth = 1
                        self.btnYes.layer.borderColor = UIColor.gray.cgColor
                        self.btnYes.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
                        self.btnYes.layer.masksToBounds = true
                    } else {
                        self.btnYes.backgroundColor = UIColor.init(hex: "ffffff")
                        self.btnYes.layer.borderWidth = 1
                        self.btnYes.layer.borderColor = UIColor.gray.cgColor
                        self.btnYes.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
                        self.btnYes.layer.masksToBounds = true
                        self.btnNo.backgroundColor = UIColor.init(hex: "ffffff")
                        self.btnNo.layer.borderWidth = 1
                        self.btnNo.layer.borderColor = UIColor.gray.cgColor
                        self.btnNo.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
                        self.btnNo.layer.masksToBounds = true
                    }
                } else {
                    self.txtAnswer.text = self.stepCompleted[self.currentIndex]
                    self.txtAnswer.isHidden = false
                    self.btnYes.isHidden = true
                    self.btnNo.isHidden = true
                }
            } else if(self.currentIndex == self.questions.count - 1) {
                let questionFinalVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionaireFinalViewController") as! QuestionaireFinalViewController
                questionFinalVC.lastIndex = self.currentIndex
                questionFinalVC.task = self.task
                questionFinalVC.stepCompleted = self.stepCompleted
                questionFinalVC.modalPresentationStyle = .overFullScreen
                self.present(questionFinalVC, animated: true)
            }
        }
        
//        self.view.backgroundColor = UIColor(hex: "4939E3")
//        self.imgScan.isHidden = true
//        self.viewScanCodeText.isHidden = true
//        let addManuallyViewController = AddTaskViewUsingController(nibName: "AddTaskViewUsingController", bundle: nil)
//        addManuallyViewController.selectedDate = self.selectedDate
//        addChildViewController(addManuallyViewController)
//
//        addManuallyViewController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
//        addManuallyViewController.view.backgroundColor = .clear
//        view.addSubview(addManuallyViewController.view)
//        addManuallyViewController.didMove(toParentViewController: self)
    }
    
    @IBAction func backScreen(_ sender: UIButton) {
        if (self.currentIndex == 0){
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
            homeVC.modalPresentationStyle = .overFullScreen
            self.present(homeVC, animated: true)
        } else if (self.currentIndex >= 1){
            self.currentIndex -= 1
            self.progressView.progress = Float(1 / self.questions.count * (currentIndex + 1))
            self.lblDescription.text = self.questions[self.currentIndex]["question"].stringValue
            if (self.stepCompleted[self.currentIndex] == "1"){
                self.btnYes.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
                self.btnYes.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
                self.btnYes.layer.masksToBounds = true
                self.btnNo.backgroundColor = UIColor.init(hex: "ffffff")
                self.btnNo.layer.borderWidth = 1
                self.btnNo.layer.borderColor = UIColor.gray.cgColor
                self.btnNo.layer.masksToBounds = true
            } else {
                self.btnNo.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
                self.btnNo.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
                self.btnNo.layer.masksToBounds = true
                self.btnYes.backgroundColor = UIColor.init(hex: "ffffff")
                self.btnYes.layer.borderWidth = 1
                self.btnYes.layer.borderColor = UIColor.gray.cgColor
                self.btnYes.layer.masksToBounds = true
            }
        }
    }
    
    @IBAction func backToHome(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnYesClicked(_ sender: UIButton) {
        if (self.stepCompleted[self.currentIndex] == "" || self.stepCompleted[self.currentIndex] == "2"){
            self.btnYes.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
            self.btnYes.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
            self.btnYes.setTitleColor(UIColor.white, for: .normal)
            self.btnYes.layer.masksToBounds = true
            self.stepCompleted[self.currentIndex] = "1"
            self.btnNo.backgroundColor = UIColor.init(hex: "ffffff")
            self.btnNo.layer.borderWidth = 1
            self.btnNo.layer.borderColor = UIColor.gray.cgColor
            self.btnNo.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
            self.btnNo.layer.masksToBounds = true
        }
    }
    
    @IBAction func btnNoClicked(_ sender: UIButton) {
        if (self.stepCompleted[self.currentIndex] == "" || self.stepCompleted[self.currentIndex] == "1"){
            self.btnNo.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
            self.btnNo.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
            self.btnNo.setTitleColor(UIColor.white, for: .normal)
            self.btnNo.layer.masksToBounds = true
            self.stepCompleted[self.currentIndex] = "2"
            self.btnYes.backgroundColor = UIColor.init(hex: "ffffff")
            self.btnYes.layer.borderWidth = 1
            self.btnYes.layer.borderColor = UIColor.gray.cgColor
            self.btnYes.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
            self.btnYes.layer.masksToBounds = true
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
