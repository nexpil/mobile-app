//
//  NPVisionDataUtils.swift
//  NexpilVision
//
//  Created by Cagri Sahan on 8/23/18.
//  Copyright © 2018 Cagri Sahan. All rights reserved.
//

import Foundation

struct NPVisionDataUtils {
    static let NDC_URL = "https://twilio.nexp.xyz/nexpil/find_drug.php"
    
    private init() {}
}
