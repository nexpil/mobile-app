//
//  AppDelegate.swift
//  Nexpil
//
//  Created by Admin on 4/3/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Whisper
import CoreData
import Alamofire
import UserNotifications
import IQKeyboardManagerSwift

var App: AppDelegate!

//arif@mailcupp.com
//123456
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    var pageViewController: PageViewController?
    var vitaminPageViewController: VitaminPageViewController?
    var bgDate = Date()
    var roomName = ""
    var channelName = ""
    var physicianName = ""
    var physicianId = ""
    //    var bgTask: UIBackgroundTaskIdentifier?
    //    self.bgTask = UIApplication.shared.beginBackgroundTask(expirationHandler: {
    //
    //    })
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        App = self
        window = UIWindow(frame: UIScreen.main.bounds)
        
        if UserDefaults.standard.value(forKey: "id") != nil {
            show(window: window!)
        } else{
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LandingScreenViewController") as! LandingScreenViewController
            window!.rootViewController = viewController
        }
        window?.makeKeyAndVisible()
        
        Global_SetGlassEffect()
        //-----PING-----
        //Global_SetGlassEffect1()
//        SetupPushNotification(application: application)
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        UIPasteboard.general.string = "Nexpil:/eyJpdiI6ImlTdGRpTWEwRFNjN3ArcXRQWUVORGc9PSIsInZhbHVlIjoibGNYMG1KWk01TU1tQ0ZtakV5anVlZz09IiwibWFjIjoiY2IwNGY3ZjZlYzU0OTBjY2Q2ZjAxY2FjOGUxMGVmNGFlNmE2ZjgzZjgyMmJkZjYxMjY3YzZjMGI1NzJhNDExMiJ9"
        
        print("Documents Directory: ", FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last ?? "Not Found!")
        return true
    }
    
    func show(window: UIWindow) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainviewcontroller") as! UITabBarController
        viewController.tabBar.layer.cornerRadius = 10
        viewController.tabBar.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner];
        viewController.tabBar.layer.borderWidth = 1
        viewController.tabBar.layer.borderColor = UIColor(red: (112/255.0), green: (112/255.0), blue: (112/255.0), alpha: 0.2).cgColor
        viewController.tabBar.clipsToBounds = true
        window.rootViewController = viewController
    }
    
    // Setup app delegate for push notifications
    func SetupPushNotification(application: UIApplication) -> () {
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {(granted, error) in
            print("granted: \(granted)")
            if granted {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                    
                    let snoozeAction = UNNotificationAction( identifier: "snooze", title: "Snooze", options: [.foreground])
                    // 2
                    let snoozeCategory = UNNotificationCategory(identifier: "snoozeCategory", actions: [snoozeAction], intentIdentifiers: [], options: [])
                    // 3
                    UNUserNotificationCenter.current().setNotificationCategories([snoozeCategory])
                }
            } else {
                print("User Notification permission denied: \(error?.localizedDescription ?? "error")")
            }
        })
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("deviceToken:  \(tokenString(deviceToken))")
        UserDefaults.standard.set(tokenString(deviceToken), forKey: "DeviceToken")
        
        if UserDefaults.standard.value(forKey: "id") != nil {
            let localTimeZoneName = TimeZone.current.identifier
            let deviceToken = tokenString(deviceToken)
            let params1 = [
                "userid": PreferenceHelper().getId(),
                "timezone": localTimeZoneName,
                "deviceToken": deviceToken,
                "choice": 2
            ] as [String : Any]
            Alamofire.request(DataUtils.APIURL + DataUtils.PATIENT_URL, method: .post, parameters: params1).responseString { response in
                print(response)
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register for rmote notifications: \(error.localizedDescription)")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Message received")
        
        let userInfo = response.notification.request.content.userInfo
        if let aps = userInfo["aps"] as? [String: Any] {
            print(aps)
            if response.actionIdentifier == "snooze" {
                let params = [
                    "snooze": PreferenceHelper().getSnooze(),
                    "userid": PreferenceHelper().getId()
                ]
                Alamofire.request(DataUtils.APIURL + DataUtils.NOTIFICATION_URL, method: .post, parameters: params).responseString { response in
                    print(response)
                }
            } else if let notificationID = aps["notification_id"] as? Int {
                if notificationID == 1 {
                    if let channel_name = aps["channel_name"] as? String {
                        self.channelName = channel_name
                        self.checkChannelAndNavigateAccordingly(channelName)
                    }
                    //                    print(NexpilManager.shared.chatManager)
                } else if notificationID == 2 {
                    if let roomID = aps["room_name"] {
                        if let drname = aps["physician_name"] as? String {
                            self.physicianName = drname
                        }
                        self.extractRoomNameAndNavigateToCommunity(roomID)
                    }
                } else if notificationID == 3 {
                    NotificationCenter.default.post(name: Notification.Name.reloadTasks, object: nil, userInfo: nil)
                }
            }
        }
        completionHandler()
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("didReceiveRemoteNotification")
        if let aps = userInfo["aps"] as? [String: Any] {
            if let notificationID = aps["notification_id"] as? Int {
                print(UIApplication.shared.applicationState)
                if notificationID == 1 {
                    if let channel_name = aps["channel_name"] as? String {
                        self.channelName = channel_name
                        self.checkChannelAndNavigateAccordingly(channelName)
                    }
                } else if notificationID == 2 {
                    if let roomID = aps["room_name"] {
                        guard let alert = aps["alert"] as? [String: String] else {
                            self.extractRoomNameAndNavigateToCommunity(roomID)
                            return
                        }
                        let title = alert["title"]
                        let subtitle = alert["body"]
                        if let drname = aps["physician_name"] as? String {
                            self.physicianName = drname
                        }
                        
                        if let drid = aps["physician_id"] as? Int {
                            self.physicianId = String(drid)
                        } else  if let drid = aps["physician_id"] as? String {
                            self.physicianId = drid
                        }
                        self.extractRoomNameAndNavigateToCommunity(roomID, title: title ?? "", subtitle: subtitle ?? "")
                    }
                } else if notificationID == 3 {
                    NotificationCenter.default.post(name: Notification.Name.reloadTasks, object: nil, userInfo: nil)
                }
            }
        }
        completionHandler(.newData)
    }
    
    func checkChannelAndNavigateAccordingly(_ channel_name: String) {
        if NexpilManager.shared.activeChannel == nil {
            if NexpilManager.shared.chatManager.client != nil {
                if NexpilManager.shared.channels.count > 0 {
                    if let channel = NexpilManager.shared.channels.first(where: { (ch) -> Bool in
                        return ch.uniqueName == channel_name
                    }) {
                        // Navigate to chat Here
                        NexpilManager.shared.activeChannel = channel
                        let chatVC = CommunityChatViewController(nibName: "CommunityChatViewController", bundle: nil)
                        let chatNavVC = UINavigationController(rootViewController: chatVC)
                        chatNavVC.modalPresentationStyle = .fullScreen
                        UIApplication.shared.topMostViewController()!.present(chatNavVC, animated: true)
                    }
                } else {
                    NexpilManager.shared.chatManager.client?.channelsList()?.channel(withSidOrUniqueName: channel_name, completion: { (result, channel) in
                        // Navigate to Chat
                        if result.isSuccessful() {
                            NexpilManager.shared.activeChannel = channel
                            let chatVC = CommunityChatViewController(nibName: "CommunityChatViewController", bundle: nil)
                            let chatNavVC = UINavigationController(rootViewController: chatVC)
                            chatNavVC.modalPresentationStyle = .fullScreen
                            UIApplication.shared.topMostViewController()!.present(chatNavVC, animated: true)
                        }
                    })
                }
            }
        }
    }
    
    func extractRoomNameAndNavigateToCommunity(_ roomID: Any, title: String = "", subtitle: String = "") {
        if let int_room_name = roomID as? Int {
            self.roomName = String(int_room_name)
        } else if let string_room_name = roomID as? String {
            self.roomName = string_room_name
        }
        if let tabController = self.window?.rootViewController as? TabBarViewController {
            if UIApplication.shared.applicationState == .active {
                if title != "" || subtitle != "" {
                    let announcement = Announcement(title: title, subtitle: subtitle, duration: 5) {
                        mTabView.setSelectedIndex(2)
                        runAfter(1) {
                            NotificationCenter.default.post(name: Notification.Name("VideoRequest"), object: nil)
                        }
                    }
                    Whisper.show(shout: announcement, to: tabController)
                    return
                }
            }
            runAfter(1) {
                mTabView.setSelectedIndex(2)
                runAfter(0) {
                    NotificationCenter.default.post(name: Notification.Name("VideoRequest"), object: nil)
                }
            }
            
        }
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        //        print("willPresent")
        if let aps = notification.request.content.userInfo["aps"] as? [String: Any] {
            if let notificationID = aps["notification_id"] as? Int {
                if notificationID == 1 {
                    if let channel_name = aps["channel_name"] as? String {
                        self.channelName = channel_name
                        if NexpilManager.shared.activeChannel != nil && NexpilManager.shared.activeChannel?.uniqueName == channel_name {
                            NotificationCenter.default.post(name: Notification.Name.reloadChats, object: nil, userInfo: nil)
                            completionHandler([])
                            return
                        }
                    }
                } else if notificationID == 2 {
                    if let roomID = aps["room_name"] {
                        if let drname = aps["physician_name"] as? String {
                            self.physicianName = drname
                        }
                        if let drid = aps["physician_id"] as? Int {
                            self.physicianId = String(drid)
                        } else  if let drid = aps["physician_id"] as? String {
                            self.physicianId = drid
                        }
                        self.roomName = (roomID as? String) ?? ""
                        //                        self.extractRoomNameAndNavigateToCommunity(roomID)
                    }
                } else if notificationID == 3 {
                    let taskId:[String: Int?] = ["taskId": aps["group_id"] as! Int]
                    
                    NotificationCenter.default.post(name: Notification.Name.reloadTasks, object: nil, userInfo: nil)
                    NotificationCenter.default.post(name: Notification.Name.createdTask, object: nil, userInfo: taskId)
                    completionHandler([])
                    return
                }
            }
        }
        //        print(notification.request.content.userInfo)
        completionHandler([.alert, .sound])
    }
    
    func tokenString(_ deviceToken: Data) -> String {
        let bytes = [UInt8](deviceToken)
        var token = ""
        for byte in bytes {
            token += String(format: "%02x", byte)
        }
        return token
    }
    
    func applicationWillResignActive(_ application: UIApplication) {        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        self.bgDate = Date()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        let now = Date()
        //        print(now.isSameDay(date: self.bgDate))
        if !self.bgDate.isSameDay(date: now) {
            NotificationCenter.default.post(name: NSNotification.Name.UpdateHomeDates, object: nil)
        }
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if DBManager.getObject().createDatabase() {
            
        }
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    //    lazy var persistentContainer: NSPersistentContainer = {
    @objc var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Nexpil")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate {
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let urlString = url.absoluteString
        //if let taskCode = urlString.components(separatedBy: ":/").last {
        if let taskCode = urlString.components(separatedBy: "://").last {
            if var topController = UIApplication.shared.keyWindow?.rootViewController {
                if let presentedViewController = topController.presentedViewController {
                    topController = presentedViewController
                }
                
                //                print(topController)
                let lastViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
                lastViewController.USER_CODE = taskCode
                if topController is LandingScreenViewController {
                    print("Hey This is Landing View Controller")
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OnboardingOneViewController") as! OnboardingOneViewController
                    let navController = UINavigationController()
                    navController.viewControllers = [viewController, lastViewController]
                    navController.setNavigationBarHidden(true, animated: false)
                    navController.modalPresentationStyle = .overFullScreen
                    //                    present(navController, animated: true, completion: nil)
                    topController.present(navController, animated: false)
                    
                } else if topController is UINavigationController {
//                    print("Hey This is Landing View Controller")
                    (topController as? UINavigationController)?.pushViewController(lastViewController, animated: true)
                }
            }
            // NAVIGATE TO REGISTER SCREEN
            
            //            self.navigationController?.pushViewController(viewController, animated: true)
            print(taskCode)
        }
        return true
    }
}
