//
//  Date.swift
//  Nexpil
//
//  Created by Golder on 3/19/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

public extension Date {
    public func dateByAddingCurrentHourMinuteSecond() -> Date {
        let curDate = Date()
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "GMT")!
        
        let hour = calendar.component(.hour, from: curDate)
        let minutes = calendar.component(.minute, from: curDate)
        let seconds = calendar.component(.second, from: curDate)
        
        return self.addingTimeInterval(TimeInterval(hour * 3600 + minutes * 60 + seconds))
    }
    
    static func today() -> Date {
        let nowUTC = Date()
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: nowUTC) else { return Date() }
        return localDate
    }
    
    
    static func startOfToday()  -> Date {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar.startOfDay(for: Date())
        //        print(startOfDate)
    }
    
    var startOfDate: Date {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar.startOfDay(for: self)
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
    
    var monthNumber: String? {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.string(from: self)
    }
    
    func isBetween(_ date1: Date, and date2: Date) -> Bool {
        return (min(date1, date2) ... max(date1, date2)).contains(self)
    }
    
    func adding(hours: Int) -> Date {
        return Calendar.current.date(byAdding: .hour, value: hours, to: self)!
    }
    
    func adding(days: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: days, to: self)!
    }
    
    func adding(months: Int) -> Date {
        return Calendar.current.date(byAdding: .month, value: months, to: self)!
    }
    
    func adding(years: Int) -> Date {
        return Calendar.current.date(byAdding: .year, value: years, to: self)!
    }
    
    
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)!
    }
    
    var sixMonths: Date {
        return Calendar.current.date(byAdding: .month, value: 6, to: self)!
    }
    
    var twoMonths: Date {
        return Calendar.current.date(byAdding: .month, value: 2, to: self)!
    }
    
    func isSameDay(date : Date ) -> Bool {
        return Date.isSameDay(date: self, as: date)
    }
    
    var year: Int? {
        let calendar = Calendar.current
        let currentComponents = calendar.dateComponents([.year], from: self)
        return currentComponents.year
    }
    
    /**
     *  Returns whether two dates fall on the same day.
     *
     *  - parameter date: First date to compare
     *  - parameter compareDate: Second date to compare
     *
     *  - returns: True if both paramter dates fall on the same day, false otherwise
     */
    static func isSameDay(date: Date, as compareDate: Date) -> Bool {
        let calendar = Calendar.autoupdatingCurrent
        var components = calendar.dateComponents([.era, .year, .month, .day], from: date)
        let dateOne = calendar.date(from: components)
        
        components = calendar.dateComponents([.era, .year, .month, .day], from: compareDate)
        let dateTwo = calendar.date(from: components)
        
        return (dateOne?.equals(dateTwo!))!
    }
    
    /**
     *  Returns a true if receiver is equal to provided comparison date, otherwise returns false
     *
     *  - parameter date: Provided date for comparison
     *
     *  - returns: Bool representing comparison result
     */
    func equals(_ date: Date) -> Bool {
        return self.compare(date) == .orderedSame
        
    }
    
    func taskString() -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "EEEE, MMMM dd, YYYY"
        let dateString = formatter.string(from: self)
        return dateString
    }
    
    func timeString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "hh:mm:ss"
        let dateString = formatter.string(from: self)
        return dateString
    }
    
    func chatTimeString() -> String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "HH:mm a"
        let dateString = formatter.string(from: self)
        return dateString
    }
    
    func dateString() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        let dateString = formatter.string(from: self)
        return dateString
    }
    
    func startOfMonth() -> Date {
        let interval = Calendar.current.dateInterval(of: .month, for: self)
        return (interval?.start.toLocalTime())! // Without toLocalTime it give last months last date
    }
    
    func endOfMonth() -> Date {
        let interval = Calendar.current.dateInterval(of: .month, for: self)
        return interval!.end
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone    = TimeZone.current
        let seconds     = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func date() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.string(from: self)
    }
    
    func monthDay() -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.string(from: self)
    }
    
    func monthName(_ short: Bool = false) -> String {
        let formatter = DateFormatter()
        if short {
            formatter.dateFormat = "MMM"
        } else {
            formatter.dateFormat = "LLLL"
        }
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        formatter.locale = Locale(identifier: "en_US_POSIX")
        return formatter.string(from: self)
    }
    
    static func getWeekOfYear(from week: Int, year: Int = Date().year!) -> Date? {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        calendar.locale = Locale(identifier: "en_US_POSIX")        
        let dateComponents = DateComponents(calendar: calendar, year: year, weekday: 2, weekOfYear: week)
        return calendar.date(from: dateComponents)
    }
    
    var startOfWeek: Date? {
        var calendar = Calendar.current
        calendar.firstWeekday = 2
        return calendar.dateInterval(of: .weekOfMonth, for: self)?.start
    }
    
    var endOfWeek: Date? {
        var calendar = Calendar.current
        calendar.firstWeekday = 2
        return calendar.dateInterval(of: .weekOfMonth, for: self)?.end
    }
    
//    public func next(_ weekday: Weekday, direction: Calendar.SearchDirection = .forward, considerToday: Bool = false) -> Date {
//        let calendar = Calendar(identifier: .gregorian)
//        let components = DateComponents(weekday: weekday.rawValue)
//
//        if considerToday &&
//            calendar.component(.weekday, from: self) == weekday.rawValue {
//            return self
//        }
//
//        return calendar.nextDate(after: self, matching: components, matchingPolicy: .nextTime, direction: direction)!
//    }
}

//public enum Weekday: Int {
//    case sunday = 1, monday, tuesday, wednesday, thursday, friday, saturday
//}
