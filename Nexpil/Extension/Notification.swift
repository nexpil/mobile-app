//
//  Notification.swift
//  Nexpil
//
//  Created by mac on 11/21/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

extension Notification.Name {
    public struct Action {
        //notification name
        static let UpdateTabBarItem = Notification.Name("UpdateTabBarItem")
        static let DoubleTapHomeTab = Notification.Name("DoubleTapHomeTab")
        static let DoubleTapCommunityTab = Notification.Name("DoubleTapCommunityTab")        
        static let FinishAddDrug = Notification.Name("FinishAddDrug")
        
        static let UpdateBloodGlucose = Notification.Name("UpdateBloodGlucose")
        static let UpdateBloodPressure = Notification.Name("UpdateBloodPressure")
        static let UpdateMoodList = Notification.Name("UpdateMoodList")
        static let UpdateOxygenLevels = Notification.Name("UpdateOxygenLevels")
        static let UpdateWeight = Notification.Name("UpdateWeight")
        static let UpdateSteps = Notification.Name("UpdateOxygenLevels")
    }
    
    public struct Field {
        static let PermissionAdded = "PermissionAdded"
    }
    
    static let UpdateHomeDates = Notification.Name("UpdateHomeDates")
    
    static let reloadTasks = Notification.Name("reloadTasks")
    static let reloadChats = Notification.Name("reloadChats")
    static let createdTask = Notification.Name("createTask")
}
