//
//  MLKIT.swift
//  Nexpil
//
//  Created by Arif on 6/26/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import MLKit

extension TextBlock {
    func complete(from page: Text) {
        for i in 0..<self.lines.count {
            for block in page.blocks {
                for rightParagraph in block.lines {
                    lines[i].complete(from: rightParagraph)
                }
            }
        }
    }
}

extension TextLine {            
    func complete(from paragraph: TextLine) {
        let leftLines = self.text.split(separator: "\n")
        let leftLinesTrimmed = leftLines.map { line -> String in
            // Don't take last character - might be distorted
            let index = line.index(before: line.endIndex)
            return String(line[..<index])
            }.filter({$0.count > 1})


        let rightLines = paragraph.text.split(separator: "\n")
        let rightLinesTrimmed = rightLines.map { line -> String in
            // Don't take first character - might be distorted
            let index = line.index(after: line.startIndex)
            return String(line[index...])
            }.filter({$0.count > 1})

        for rightLine in rightLinesTrimmed {
            // Offset these by 1 so it's a match only if the more than the last and first two characters match
            if rightLine.count > 2 {
                for leftLine in leftLinesTrimmed {
                    if leftLine.count > 2 {
                        var startIndex = rightLine.startIndex
                        var endIndex = leftLine.endIndex


                        for _ in 1...(leftLine.count<rightLine.count ? leftLine.count : rightLine.count) {
                            startIndex = rightLine.index(startIndex, offsetBy: 1)
                            endIndex = leftLine.index(endIndex, offsetBy: -1)
                            if rightLine[..<startIndex] == leftLine[endIndex...] && startIndex.encodedOffset > 2 {
                                let newLines = leftLinesTrimmed.map { string -> String in
                                    if leftLine != string {
                                        return String(leftLines[leftLinesTrimmed.index(of: string)!])
                                    }
                                    else {
                                        return String(leftLine[..<endIndex] + rightLine)
                                    }
                                }
//                                textVal = newLines.joined(separator: "\n")
//                                print(newLines.joined(separator: "\n"))
                            }
                        }
                    }
                }
            }
        }
    }
}
