//
//  CGFloatExtension.swift
//  Nexpil
//
//  Created by Shine on 2/14/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

extension CGFloat {
    func between(a: CGFloat, b: CGFloat) -> Bool {
        return self >= Swift.min(a, b) && self <= Swift.max(a, b)
    }
}
