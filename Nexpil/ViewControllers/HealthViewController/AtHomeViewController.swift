//
//  AtHomeViewController.swift
//  Nexpil
//
//  Created by Loyal Lauzier on 2018/05/28.
//  Copyright © 2018 MobileDev. All rights reserved.
//

import UIKit
import XLPagerTabStrip
import SVProgressHUD

protocol AtHomeViewControllerDelegate: class {
    func didTapButtonAtHomeViewController(dic: NSDictionary, index: NSInteger)
    func didTapButtonAtHomeViewControllerRefresh(callback: (()->())?)
}

enum HealthTableTypes {
    case LabsLabel
    case HomeModule
    case LabModule
    case Empty
}

class AtHomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    weak var delegate:AtHomeViewControllerDelegate?
    
    @IBOutlet weak var tableList: UITableView!
    @IBOutlet weak var lbl_pulldown: UILabel!
    
    let tabTitle = "At Home"
    
    var arrayList: NSMutableArray?
    var manager = DataManager.shared
    private var cellTypes: [HealthTableTypes] = []
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(self.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.gray
        
        return refreshControl
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrayList = NSMutableArray.init()
        // Do any additional setup after loading the view.

        lbl_pulldown.isHidden = true
//        self.getSelfData()
        self.initMainView()
    }
    
    private func resetCellTypes() {
        var moduleIds = UserDefaults.standard.array(forKey: "selected_health") as? [Int]
        moduleIds = moduleIds?.sorted()
        cellTypes.removeAll()
        if moduleIds == nil {
            return
        }
        for (idx, id) in moduleIds!.enumerated() {
            if idx == 0 {
                if id > 5 {
                    /////hong
//                    cellTypes.append(.LabsLabel)
                } else {
//                    cellTypes.append(.HomeLabel)
                }
            }
            if id > 5 {
                /////hong
//                if !cellTypes.contains(.LabsLabel) {
//                    cellTypes.append(.LabsLabel)
//                }
            }
            if id >= 5 {
                cellTypes.append(.LabModule)
            } else {
                cellTypes.append(.HomeModule)
            }
        }
        cellTypes.append(.Empty)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // init MainView
    func initMainView() {
        // table
        tableList.delegate = self
        tableList.dataSource = self
        tableList.separatorColor = UIColor.clear
        
        tableList.register(UINib(nibName: "CategoryTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryTableViewCell")
        tableList.register(UINib(nibName: "HealthEmptyTableViewCell", bundle: nil), forCellReuseIdentifier: "HealthEmptyTableViewCell")
        tableList.register(UINib(nibName: "HealthLabelTableViewCell", bundle: nil), forCellReuseIdentifier: "HealthLabelTableViewCell")
        
        tableList.addSubview(refreshControl)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.delegate?.didTapButtonAtHomeViewControllerRefresh {
            DispatchQueue.main.async {
                self.getSelfData()
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func getSelfData() {
        let bloodGlucose = getBloodGlocose()
        let bloodPressure = getBloodPressure()
        let mood = getMood()
        let weight = getWeight()
        let oxygenLevel = getOxygenlevel()
        let steps = getSteps()
        
        let dic0 = NSDictionary.init(objects: ["Blood Glucose", "Blood Glucose", bloodGlucose, "mg/dl", "icon_blood_glucose", "#333333"], forKeys: ["title" as NSString, "image" as NSString, "value" as NSString, "unit" as NSString, "icon" as NSString, "unit_color" as NSString])
        let dic1 = NSDictionary.init(objects: ["Blood Pressure", "Blood Pressure", bloodPressure, "", "icon_blood_pressure", "#333333"], forKeys: ["title" as NSString, "image" as NSString, "value" as NSString, "unit" as NSString, "icon" as NSString, "unit_color" as NSString])
        let dic2 = NSDictionary.init(objects: ["Mood", "Mood", mood, "", "", "#333333"], forKeys: ["title" as NSString, "image" as NSString, "value" as NSString, "unit" as NSString, "icon" as NSString, "unit_color" as NSString])
        let dic3 = NSDictionary.init(objects: ["Weight", "Weight", weight, "lbs", "", "#877CEC"], forKeys: ["title" as NSString, "image" as NSString, "value" as NSString, "unit" as NSString, "icon" as NSString, "unit_color" as NSString])
        let dic4 = NSDictionary.init(objects: ["Oxygen Level", "Oxygen Level", oxygenLevel, "%", "", "#333333"], forKeys: ["title" as NSString, "image" as NSString, "value" as NSString, "unit" as NSString, "icon" as NSString, "unit_color" as NSString])
        let dic5 = NSDictionary.init(objects: ["Steps", "Steps", steps, "", "", "#8495ED"], forKeys: ["title" as NSString, "image" as NSString, "value" as NSString, "unit" as NSString, "icon" as NSString, "unit_color" as NSString])
        let dic6 = NSDictionary.init(objects: ["Labs", "Hemoglobin A1c", "", "", "", "#333333"], forKeys: ["title" as NSString, "image" as NSString, "value" as NSString, "unit" as NSString, "icon" as NSString, "unit_color" as NSString])

        if let tmp = UserDefaults.standard.array(forKey: "selected_health"){
            if arrayList?.count == 0 {
                DispatchQueue.main.async {
                    self.lbl_pulldown.isHidden = false
                    Timer.scheduledTimer(withTimeInterval: 2.0, repeats: false) { timer in
                        UIView.animate(withDuration: 0.5, animations: {
                            self.lbl_pulldown.alpha = 0;
                        })
                        timer.invalidate()
                    }
                }
            }
            arrayList?.removeAllObjects()

            var ary = tmp as! [Int]
            ary = ary.sorted()
            for i in ary {
                switch i {
                case 0: // blood glucose
                    arrayList?.add(dic0)
                    break
                case 1: // blood pressure
                    arrayList?.add(dic1)
                    break
                case 2: // mood
                    arrayList?.add(dic2)
                    break
                case 3: // weight
                    arrayList?.add(dic3)
                    break
                case 4: // oxygen level
                    arrayList?.add(dic4)
                    break
                case 5: // steps
                    arrayList?.add(dic5)
                    break
                case 6:
                    arrayList?.add(dic6)
                    break
                default:
                    break
                }
            }
        } else {
            arrayList?.removeAllObjects()
        }
        
        DispatchQueue.main.async {
            self.resetCellTypes()
            self.tableList.reloadData()
        }
    }

    // table view datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellTypes.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellType = cellTypes[indexPath.row]
        if cellType == .HomeModule || cellType == .LabModule {
            return 110
        } else {
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellType = cellTypes[indexPath.row]
        
        switch cellType {
        case .LabsLabel:
            let cell = tableList.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
            cell.selectionStyle = .none
            if let array = self.arrayList, array.count > indexPath.row {
                cell.setInfo(array: array, index: indexPath.row)
            }
            return cell
        case .HomeModule:
            let cell = tableList.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
            cell.selectionStyle = .none
            if let array = self.arrayList, array.count > indexPath.row {
                cell.setInfo(array: array, index: indexPath.row)
            }
            return cell
        case .LabModule:
            let cell = tableList.dequeueReusableCell(withIdentifier: "CategoryTableViewCell", for: indexPath) as! CategoryTableViewCell
            if let array = self.arrayList, array.count > indexPath.row {
                cell.setInfo(array: array, index: indexPath.row)
            }
            cell.selectionStyle = .none
            return cell
        default:
            let cell = tableList.dequeueReusableCell(withIdentifier: "HealthEmptyTableViewCell", for: indexPath) as! HealthEmptyTableViewCell
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
        var tmp = UserDefaults.standard.array(forKey: "selected_health") as! [Int]
        tmp = tmp.sorted()
        let cellType = cellTypes[indexPath.row]
        if cellType == .Empty {
            return
        }
        
        let dicList = arrayList![indexPath.row] as! NSDictionary
        self.delegate?.didTapButtonAtHomeViewController(dic: dicList, index: tmp[indexPath.row])
    }
}

extension AtHomeViewController : IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: tabTitle)
    }
}
