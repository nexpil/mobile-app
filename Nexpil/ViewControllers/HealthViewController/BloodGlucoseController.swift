//
//  BloodGlucoseController.swift
//  Nexpil
//
//  Created by Nexpil Admin on 1/15/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import Alamofire

public class BloodGlucoseController {
    
    private static let apiURL = DataUtils.APIURL + DataUtils.HEALTH_BLOODGLUCOSE_URL
    
    // Insert BloodGlucose data to the Database
//    public static func addBloodGlucose(measurement: String, timing: Int, date: Date, handler: @escaping (Bool) -> ()) {
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        formatter.timeZone = TimeZone(abbreviation: "GMT")
//
//        let params = [
//            "choice": 0,
//            "measurement": measurement,
//            "timing": timing,
//            "date": formatter.string(from: date),
//            "userid": PreferenceHelper().getId()
//        ] as [String : Any]
//
//        Alamofire.request(apiURL, method: .post, parameters: params)
//            .responseJSON(completionHandler: { response in
//                switch response.result {
//                case .success:
//                    handler(true)
//                case .failure(let error):
//                    print(error.localizedDescription)
//                    handler(false)
//                }
//            })
//    }

    public static func addBloodGlucose(measurement: String, timing: Int, date: Date, callback:((Bool?, Error?)->())?) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let params = [
            "choice": 0,
            "measurement": measurement,
            "timing": timing,
            "date": formatter.string(from: date),
            "userid": PreferenceHelper().getId()
        ] as [String : Any]
        
        Alamofire.request(apiURL, method: .post, parameters: params)
            .responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    DataManager.shared.writeSampleByBloodGlucose(value: Int(measurement) ?? 0, date: date, whenIndex: timing, note: "") { (success, error) in
                        runOnMainThread {
                            callback?(success, error)
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: Notification.Name.Action.UpdateBloodGlucose, object: nil)
                            }
                        }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                    callback?(false, error)
                }
        })
    }

    // Fetch recent BloodGlucose data from HealthKitSDK and Database
    public static func fetchRecentBloodGlucose(handler: @escaping (String) -> ()){
        // Fetch Recent BloodGlucose from Database
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
//        formatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let params = [
            "choice": 1,
            "date": formatter.string(from: Date()),
            "userid": PreferenceHelper().getId()
        ] as [String: Any]
        
        Alamofire.request(apiURL, method: .post, parameters: params).responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                if let data = response.result.value {
                    let json = data as! [String: Any]
                    let value = json["value"] as? String
                    handler(value ?? "")
                }
                break
            case .failure:
                handler("")
                break
            }
        })
    }
    
    // Fetch BloodGlucose history by today
    public static func fetchTodayHistory(handler: @escaping ([ [String: Any]]) -> ()) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
//        formatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let params = [
            "choice": 2,
            "date": formatter.string(from: Date()),
            "userid": PreferenceHelper().getId()
        ] as [String: Any]
                
        Alamofire.request(apiURL, method: .post, parameters: params).responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                if let data = response.result.value {
                    print(data)
                    let json = data as? [String: Any]
                    let historyList = json?["history"] as? [[String: Any]]
                    handler(historyList ?? [])
                }
                break
            case .failure:
                handler([])
                break
            }
        })
    }
    
    // Fetch BloodGlucose history by week
//    public static func fetchWeekHistory(handler: @escaping ([[String: Any]]) -> ()) {
//        // Calculate the start and end of the week
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
//        let today = calendar.startOfDay(for: Date())
//        let dayOfWeek = calendar.component(.weekday, from: today)
//        let startOfWeek = calendar.date(byAdding: .day, value: 1 - dayOfWeek, to: today)
//        let endOfWeek = calendar.date(byAdding: .day, value: 7 - dayOfWeek, to: today)
//        let strStartOfWeek = formatter.string(from: startOfWeek!)
//        let strEndOfWeek = formatter.string(from: endOfWeek!)
//
//        let params = [
//            "choice": 3,
//            "userid": PreferenceHelper().getId(),
//            "startdate": strStartOfWeek,
//            "enddate": strEndOfWeek
//        ] as [String: Any]
//
//        Alamofire.request(apiURL, method: .post, parameters: params).responseJSON(completionHandler: { response in
//            switch response.result {
//            case .success:
//                if let data = response.result.value {
//                    let json = data as? [String: Any]
//                    let historyList = json?["history"] as? [[String:Any]]
//                    handler(historyList ?? [])
//                }
//            case .failure:
//                handler([])
//            }
//        })
//    }
    public static func fetchWeekHistory(handler: (([[String: Any]])->())?) {
        handler?(DataManager.shared.fetchBloodGlucoseGetAllWeekData())
    }

    // Fetch BloodGlucose history by month
//    public static func fetchMonthHistory(handler: @escaping ([[String: Any]]) -> ()) {
//        // Calculate the start and end date of the month
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd"
//        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
//        let today = calendar.startOfDay(for: Date())
//        let components = calendar.dateComponents([.year, .month], from: today)
//        let startOfMonth = calendar.date(from: components)
//        let components2 = NSDateComponents()
//        components2.month = 1
//        components2.day = -1
//        let endOfMonth = calendar.date(byAdding: components2 as DateComponents, to: startOfMonth!)
//
//        let params = [
//            "choice": 3,
//            "userid": PreferenceHelper().getId(),
//            "startdate": formatter.string(from: startOfMonth!),
//            "enddate": formatter.string(from: endOfMonth!)
//        ] as [String: Any]
//
//        Alamofire.request(apiURL, method: .post, parameters: params).responseJSON(completionHandler: { response in
//            switch response.result {
//            case .success:
//                if let data = response.result.value {
//                    let json = data as? [String: Any]
//                    let historyList = json?["history"] as? [[String:Any]]
//                    handler(historyList ?? [])
//                }
//            case .failure:
//                handler([])
//            }
//        })
//    }
    
    public static func fetchMonthHistory(handler: (([[String: Any]])->())?) {
        handler?(DataManager.shared.fetchBloodGlucoseGetAllMonthData())
    }
    
    static func fetchCurrentMonthHistory(forWeeks weeks:[Week], handler: (([Glucose])->())?) {
        handler?(DataManager.shared.fetchCurrentMonthBloodGlucose(forWeeks: weeks))
    }
}
