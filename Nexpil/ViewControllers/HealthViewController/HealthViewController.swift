//
//  HealthViewController.swift
//  Nexpil
//
//  Created by Loyal Lauzier on 2018/05/28.
//  Copyright © 2018 MobileDev. All rights reserved.
//

import UIKit
import SVProgressHUD
import XLPagerTabStrip
import HealthKit
import Alamofire

class HealthViewController: UIViewController, AtHomeViewControllerDelegate, UITabBarControllerDelegate {
    
    @IBOutlet weak var lblDot: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var m_vwRadialRight: UIView!
    @IBOutlet weak var m_vwRadialLeft: UIView!
    @IBOutlet weak var containerView: UIScrollView!
    @IBOutlet weak var addButton: FAButton!
    @IBOutlet weak var vwAddBtn: UIView!
    
    @IBOutlet weak var vwGradient: UIView!
    @IBOutlet weak var premierBackgroundImage: UIImageView!
    @IBOutlet weak var premierModalView: GradientView!
    @IBOutlet weak var blurrView: UIVisualEffectView!
    @IBOutlet weak var premierOverlayView: UIImageView!
    
    @IBOutlet weak var img_defaultOverlay: UIImageView!
    var blurEffectView: UIVisualEffectView!
    
    @IBOutlet weak var contraintVwGradientHeight: NSLayoutConstraint!
    
    var oldCell:ButtonBarViewCell?
    var newCell:ButtonBarViewCell?
    
    var navIVArray: [UIImageView] = []
    var topNavActiveArray: [String] = []
    var topNavDeactiveArray: [String] = []
    
    var pageMenu:CAPSPageMenu?
    var vc_heathData: AtHomeViewController = AtHomeViewController()
    var currentPage = NSInteger()
    var arrayAtHome = NSMutableArray()
    var arrayLabs = NSMutableArray()
    public var manager = DataManager.shared
    var m_originAddBtnPos: CGFloat = 0
    
    var enabledModules:[HealthkitModule] = []
    var formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.view.backgroundColor = UIColor.white//init(hexString: "#F7F7F7")
        self.tabBarController?.delegate = self

        let gesture = UITapGestureRecognizer(target: self, action: #selector(onClickAddButton))
        addButton.addGestureRecognizer(gesture)
        addButton.isUserInteractionEnabled = true
        
        containerView.contentInsetAdjustmentBehavior = .never
        
        //-----PING-----
        //if !UserDefaults.standard.bool(forKey: "CHECKED_HEALTHKIT") {
        //    self.getHealthkitStatus()
        //}
        
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    public func HideDefaultOverlay(){
//        img_defaultOverlay.isHidden = true
        self.updateUI()
    }
    
    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        m_vwRadialLeft.layer.cornerRadius = m_vwRadialLeft.frame.width / 2
        m_vwRadialRight.layer.cornerRadius = m_vwRadialRight.frame.width / 2
        
        if isFirst {
            isFirst = false
 
            containerView.layoutIfNeeded()
            vwGradient.layoutIfNeeded()
            vwAddBtn.layoutIfNeeded()
            
            vc_heathData = AtHomeViewController(nibName: "AtHomeViewController", bundle: nil)
            vc_heathData.delegate = self
            addChildViewController(vc_heathData)
    //        vc_heathData.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            containerView.addSubview(vc_heathData.view)
            vc_heathData.didMove(toParentViewController: self)
            vc_heathData.view.frame = CGRect(x: 0, y: 0, width: containerView.frame.size.width, height: containerView.frame.size.height)
            contraintVwGradientHeight.constant = containerView.frame.size.height
            containerView.clipsToBounds = true

            let gradientMaskLayer = CAGradientLayer()
            gradientMaskLayer.frame = CGRect(x: 0, y: 0, width: vwGradient.frame.size.width, height: vwGradient.frame.size.height)
            let bgColor = UIColor(hex: "ffffff", alpha: 0.8).cgColor
            gradientMaskLayer.colors = [UIColor(hex: "ffffff", alpha: 0).cgColor, bgColor, bgColor]
            gradientMaskLayer.locations = [0, NSNumber(value: Double(80 / vwGradient.frame.size.height)), 1]
            vwGradient.layer.addSublayer(gradientMaskLayer)

            print(containerView.contentOffset.y)
            m_originAddBtnPos = vwAddBtn.frame.origin.y
            
            getNotifications()
            
            //-----PING-----
            self.updateUI(shouldUpdateData: false)
        }
        //self.updateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //self.updateUI()
    }

    // init MainView
    func initMainView() {
//        lblTitle.font = UIFont.init(name: "Montserrat-Bold", size: 40)
    }

    func getNotifications() {
        
        let notificationNameBloodG = Notification.Name.Action.UpdateBloodGlucose
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameBloodG, object: nil)
        
        let notificationNameBloodP = Notification.Name.Action.UpdateBloodPressure
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameBloodP, object: nil)
        
        let notificationNameMood = Notification.Name.Action.UpdateMoodList
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameMood, object: nil)
        
        let notificationNameWeight = Notification.Name.Action.UpdateWeight
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameWeight, object: nil)
        
        let notificationNameOxygenLevels = Notification.Name.Action.UpdateOxygenLevels
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameOxygenLevels, object: nil)
        
        let notificationNameSteps = Notification.Name.Action.UpdateSteps
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameSteps, object: nil)
    }
    
    @objc func updateHealthData(notification:Foundation.Notification) {
        vc_heathData.getSelfData()
    }

    // MARK - AtHomeViewController delegate
    func didTapButtonAtHomeViewController(dic: NSDictionary, index: NSInteger) {

        if index == 0 { // blood glucose
            let bloodGlucoseVC = self.storyboard?.instantiateViewController(withIdentifier: "BloodGlucoseViewController") as! BloodGlucoseViewController
            bloodGlucoseVC.modalPresentationStyle = .overFullScreen
            bloodGlucoseVC.modalTransitionStyle = .crossDissolve
            self.present(bloodGlucoseVC, animated: true, completion: nil)

        } else if index == 1 { // blood pressure
//            let healthDetails01ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails01ViewController") as! HealthDetails01ViewController
//            healthDetails01ViewController.modalPresentationStyle = .overFullScreen
//            self.present(healthDetails01ViewController, animated: true, completion: nil)
            
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "BloodPressureViewController") as! BloodPressureViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
            
        } else if index == 2 { // mood
            
            let moodVC = self.storyboard?.instantiateViewController(withIdentifier: "MoodViewController") as! MoodViewController
            moodVC.modalPresentationStyle = .overFullScreen
            moodVC.modalTransitionStyle = .crossDissolve
            self.present(moodVC, animated: true, completion: nil)

//            let moodVC = self.storyboard?.instantiateViewController(withIdentifier: "NewMoodViewController") as! NewMoodViewController
//            moodVC.modalPresentationStyle = .overFullScreen
//            self.present(moodVC, animated: true, completion: nil)
        } else if index == 3 { // weight
//            let healthDetails05ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails05ViewController") as! HealthDetails05ViewController
//            healthDetails05ViewController.modalPresentationStyle = .overFullScreen
//            self.present(healthDetails05ViewController, animated: true, completion: nil)
            
            let weightVC = self.storyboard?.instantiateViewController(withIdentifier: "WeightViewController") as! WeightViewController
            weightVC.modalPresentationStyle = .overFullScreen
            weightVC.modalTransitionStyle = .crossDissolve
            self.present(weightVC, animated: true, completion: nil)
        }
        else if index == 4 { // oxygen level
//            let healthDetails02ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails02ViewController") as! HealthDetails02ViewController
//            healthDetails02ViewController.modalPresentationStyle = .overFullScreen
//            self.present(healthDetails02ViewController, animated: true, completion: nil)
            
            let oxygenVC = self.storyboard?.instantiateViewController(withIdentifier: "OxygenLevelsViewController") as! OxygenLevelsViewController
            oxygenVC.modalPresentationStyle = .overFullScreen
            oxygenVC.modalTransitionStyle = .crossDissolve
            self.present(oxygenVC, animated: true, completion: nil)
        } else if index == 5 { // steps
//            let healthDetails04ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails04ViewController") as! HealthDetails04ViewController
//            healthDetails04ViewController.modalPresentationStyle = .overFullScreen
//            self.present(healthDetails04ViewController, animated: true, completion: nil)
            let stepsVC = self.storyboard?.instantiateViewController(withIdentifier: "StepsViewController") as! StepsViewController
            stepsVC.modalPresentationStyle = .overFullScreen
            stepsVC.modalTransitionStyle = .crossDissolve
            self.present(stepsVC, animated: true, completion: nil)
        }
        else if index == 6 {
//            let healthDetails10ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails10ViewController") as! HealthDetails10ViewController
//            healthDetails10ViewController.modalPresentationStyle = .overFullScreen
//            self.present(healthDetails10ViewController, animated: true, completion: nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabResultsViewController") as! LabResultsViewController
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            self.present(vc, animated: true, completion: nil)
        } else if index == 7 {
//            let healthDetails11ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails11ViewController") as! HealthDetails11ViewController
//            healthDetails11ViewController.modalPresentationStyle = .overFullScreen
//            self.present(healthDetails11ViewController, animated: true, completion: nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabResultsViewController") as! LabResultsViewController
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)

        } else if index == 8 {
//            let healthDetails12ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails12ViewController") as! HealthDetails12ViewController
//            healthDetails12ViewController.modalPresentationStyle = .overFullScreen
//            self.present(healthDetails12ViewController, animated: true, completion: nil)
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabResultsViewController") as! LabResultsViewController
            vc.modalPresentationStyle = .overFullScreen
            self.present(vc, animated: false, completion: nil)
        }
    }
    
    func activeModule(_ moduleId: Int) {
        let router = Router.activeHealthModule(PreferenceHelper().getId(), moduleId)
        ApiManager.request(router: router) { (response: APIResponse<String>) in
            if response.status == ResponseStatus.success, let data = response.data {
                print(data)
            }
        } failure: { (error) in
            print(error)
        }
    }
    
    func didTapButtonAtHomeViewControllerRefresh(callback: (()->())?) {
        // update
        manager.updateHealthInfo {
            callback?()
        }
    }
    
    // MARK - LabViewController Delegate
    func didTapButtonLabsViewController(dic: NSDictionary, index: NSInteger) {
        
        sleep(UInt32(0.5))

        if index == 0 {
            let healthDetails10ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails10ViewController") as! HealthDetails10ViewController
            self.present(healthDetails10ViewController, animated: true, completion: nil)
            
        } else if index == 1 {
            let healthDetails11ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails11ViewController") as! HealthDetails11ViewController
            self.present(healthDetails11ViewController, animated: true, completion: nil)
            
        } else if index == 2 {
            let healthDetails12ViewController = self.storyboard?.instantiateViewController(withIdentifier: "HealthDetails12ViewController") as! HealthDetails12ViewController
            self.present(healthDetails12ViewController, animated: true, completion: nil)
        }
    }
    
    private func displayAlert(for error: Error) {
        
        let alert = UIAlertController(title: nil,
                                      message: error.localizedDescription,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK",
                                      style: .default,
                                      handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    @objc func onClickAddButton() {
        let viewcontroller = (UIStoryboard(name: "Health", bundle: nil).instantiateViewController(withIdentifier: "AddHealthDataViewController") as! AddHealthDataViewController)
        viewcontroller.delegate = self
        viewcontroller.modalPresentationStyle = .overFullScreen
        viewcontroller.modalTransitionStyle = .crossDissolve
        present(viewcontroller, animated: true, completion: nil)
    }
    
    public func updateUI(shouldUpdateData: Bool = true, callback: (()->())? = nil) {
        DispatchQueue.main.async {
            let tmp = UserDefaults.standard.array(forKey: "selected_health")
            if let _ = tmp {
                if shouldUpdateData {
                    SVProgressHUD.show()
                    self.manager.updateHealthInfo {
                        SVProgressHUD.dismiss()
                        self.vc_heathData.getSelfData()
                        callback?()
                    }
                } else {
                    self.vc_heathData.getSelfData()
                    callback?()
                }
                
                self.blurrView.isHidden = true
                self.premierModalView.isHidden = true
                self.premierBackgroundImage.isHidden = true
//                self.premierOverlayView.isHidden = true
                
            } else {
                self.blurrView.isHidden = false
                self.premierModalView.isHidden = false
                self.premierBackgroundImage.isHidden = false
                
                self.view.bringSubview(toFront: self.lblDot)
                self.view.bringSubview(toFront: self.lblTitle)
//                self.premierOverlayView.isHidden = false
                
                callback?()
            }
            self.containerView.isHidden = !self.premierModalView.isHidden
            self.vwAddBtn.isHidden = !self.premierModalView.isHidden
        }
    }
    
    @IBAction func showAddModuleModal(_ sender: Any) {
        //-----PING-----
        self.getHealthkitStatus()
    }
    
    func showAddHealthDataVC() {
        let viewcontroller = (UIStoryboard(name: "Health", bundle: nil).instantiateViewController(withIdentifier: "AddHealthDataViewController") as! AddHealthDataViewController)
        viewcontroller.delegate = self
        viewcontroller.modalPresentationStyle = .overFullScreen
        viewcontroller.modalTransitionStyle = .crossDissolve
        self.present(viewcontroller, animated: true, completion: nil)
    }
    
    func getHealthkitStatus() {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.getHealthkitModules(String(PreferenceHelper().getId()))
        ApiManager.request(router: router) { (response: APIResponse<[HealthkitModule]>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success, let data = response.data {
                print(data)
                //UserDefaults.standard.set(true, forKey: "CHECKED_HEALTHKIT")
                self.checkEnabledHealthData(data)
            } else {
                self.showAddHealthDataVC()
            }
        } failure: { (error) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
            self.showAddHealthDataVC()
        }
    }
    
    func checkEnabledHealthData(_ modules: [HealthkitModule]) {
        self.enabledModules = []
        for module in modules {
            if module.status == 1 {
                self.enabledModules.append(module)
            }
        }
        
        if self.enabledModules.count > 0 {
            let alert = UIAlertController(title: "", message: "You already have health data. do you want to reload it?", preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "Reload", style: .default) { _ in
                self.checkAuthorization()
            }
            let cancelAction = UIAlertAction(title: "No", style: .cancel) { _ in
                self.showAddHealthDataVC()
            }
            alert.addAction(OKAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            self.showAddHealthDataVC()
        }
    }
    
    func checkAuthorization() {
        var types = Set<HKSampleType>()
        for module in enabledModules {
            switch module.id {
            case 0:
                types.insert(HKObjectType.quantityType(forIdentifier: .bloodGlucose)!)
                break
            case 1:
                types.insert(HKObjectType.quantityType(forIdentifier: .bloodPressureDiastolic)!)
                types.insert(HKObjectType.quantityType(forIdentifier: .bloodPressureSystolic)!)
                break
            case 2:
                types.insert(HKObjectType.quantityType(forIdentifier: .oxygenSaturation)!)
                break
            case 3:
                break
            case 4:
                types.insert(HKObjectType.quantityType(forIdentifier: .bodyMass)!)
                break
            default:
                break
            }
        }
        
        if HKHealthStore.isHealthDataAvailable() {
            DataUtils.customActivityIndicatory(self.view, startAnimate: true)
            HKHealthStore().requestAuthorization(toShare: types, read: types) { (success, error) in
                DispatchQueue.main.async {
                    DataUtils.customActivityIndicatory(self.view, startAnimate: false)
                    if success {
                        self.getEnablbedModules(0)
                    }
                }
            }
        }
    }
    
    func getEnablbedModules(_ index: Int) {
        let module = self.enabledModules[index]
        switch module.id {
        case 0:
            break
        case 1:
            self.getBloodPressuureData(index)
            break
        case 2:
            self.getOxygenData(index)
            break
        case 3:
            self.getMoodData(index)
            break
        case 4:
            self.getWeightData(index)
            break
        default:
            break
        }
    }
    
    func getBloodPressuureData(_ index: Int) {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.getHealthData(PreferenceHelper().getId(), self.enabledModules[index].id)
        ApiManager.request(router: router) { (response: HealthHistoryResponse<[BloodPressureData]>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success, let history = response.history {
                print(history)
                for data in history {
                    let date = self.formatter.date(from: data.date_posted) ?? Date()
                    DataManager.shared.writeSampleByBloodPressure(systolic: data.systolic, diastolic: data.diastolic, date: date) { (success, error) in
                    }
                }
                self.updatedHealthData(1)
                //NotificationCenter.default.post(name: Notification.Name.Action.UpdateBloodPressure, object: nil)
            }
            
            self.checkNextModule(index)
        } failure: { (error) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
        }
    }
    
    func getOxygenData(_ index: Int) {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.getHealthData(PreferenceHelper().getId(), self.enabledModules[index].id)
        ApiManager.request(router: router) { (response: HealthHistoryResponse<[OxygenLevelData]>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success, let history = response.history {
                print(history)
                for data in history {
                    let date = self.formatter.date(from: data.date_posted) ?? Date()
                    DataManager.shared.writeSampleByOxygenLevel(value: data.oxygen_value, date: date) { (success, error) in
                    }
                }
                self.updatedHealthData(4)
                //NotificationCenter.default.post(name: Notification.Name.Action.UpdateOxygenLevels, object: nil)
            }
            self.checkNextModule(index)
        } failure: { (error) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
        }
    }
    
    func getMoodData(_ index: Int) {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.getHealthData(PreferenceHelper().getId(), self.enabledModules[index].id)
        ApiManager.request(router: router) { (response: HealthHistoryResponse<[MoodData]>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success, let history = response.history {
                print(history)
                for data in history {
                    let date = self.formatter.date(from: data.date_posted) ?? Date()
                    _ = DataManager.shared.insertMood(date: date, feeling: data.feeling_type, notes: data.mood_note)
                }
                self.updatedHealthData(2)
                //NotificationCenter.default.post(name: Notification.Name.Action.UpdateMoodList, object: nil)
            }
            self.checkNextModule(index)
        } failure: { (error) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
        }
    }
    
    func getWeightData(_ index: Int) {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.getHealthData(PreferenceHelper().getId(), self.enabledModules[index].id)
        ApiManager.request(router: router) { (response: HealthHistoryResponse<[WeightData]>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success, let history = response.history {
                print(history)
                for data in history {
                    let date = self.formatter.date(from: data.date_posted) ?? Date()
                    DataManager.shared.writeSampleByWeight(weight: data.weight, date: date) { (success, error) in
                        print(success!)
                    }
                }
                self.updatedHealthData(3)
                //NotificationCenter.default.post(name: Notification.Name.Action.UpdateWeight, object: nil)
            }
            self.checkNextModule(index)
        } failure: { (error) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
        }
    }
    
    func checkNextModule(_ index: Int) {
        if index + 1 < self.enabledModules.count {
            self.getEnablbedModules(index + 1)
        } else {
            self.updateUI(shouldUpdateData: false)
            //self.vc_heathData.getSelfData()
        }
    }
    
    func updatedHealthData(_ moduleId: Int) {
        var ary:[Int] = []
        let tmp = UserDefaults.standard.array(forKey: "selected_health")
        //-----PING-----
        if let arry = tmp as? [Int] {
            ary = arry
            if !ary.contains(moduleId){
                ary.append(moduleId)
            }
        } else{
            ary.append(moduleId)
        }
        
        UserDefaults.standard.set(ary, forKey: "selected_health")
        
        //self.updateUI()
    }
}

extension HealthViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
//        vwAddBtn.frame.origin = CGPoint(x: vwAddBtn.frame.origin.x, y: m_originAddBtnPos + scrollView.contentOffset.y + 44)
    }
}

extension HealthViewController: ScanBloodGlucoseViewControllerDelegate {
    private func insertBloodGlucose(date: Date, whenIndex: String, value: NSInteger) {
        SVProgressHUD.show()
        DataManager.shared.writeSampleByBloodGlucose(value: value, date: date, whenIndex: Int(whenIndex) ?? 0, note: "") { (success, error) in
            SVProgressHUD.dismiss()
            NotificationCenter.default.post(name: Notification.Name.Action.UpdateBloodGlucose, object: nil)
        }
    }
    
    func didTapButtonAddGlucoseScanBloodGlucoseViewController(date: Date, whenIndex: String, value: NSInteger) {
        self.insertBloodGlucose(date: date, whenIndex: whenIndex, value: value)
    }
    
    func showShadow() {
        Global_ShowFrostGlass(self.view)
    }
}

extension HealthViewController: ScanBloodPressureViewControllerDelegate {
    func insertBloodPressurel(date: Date, time: String, timeIndex: String, value1: NSInteger, value2: NSInteger) {
        SVProgressHUD.show()
        DataManager.shared.writeSampleByBloodPressure(systolic: value1, diastolic: value2, date: date) { (success, error) in
            SVProgressHUD.dismiss()
            NotificationCenter.default.post(name: Notification.Name.Action.UpdateBloodPressure, object: nil)
        }
    }
    
    func didTapButtonAddScanBloodPressureViewController(date: Date, time: String, timeIndex: String, value1: NSInteger, value2: NSInteger) {
        self.insertBloodPressurel(date: date, time: time, timeIndex: timeIndex, value1: value1, value2: value2)
    }
}

extension HealthViewController: AddHealthDataViewControllerDelegate {
    func didSelectHealthDataType(index: Int) {
//        DispatchQueue.main.async {
//            if ary.count == tmp?.count {
        switch index {
        case 0:
            let scanBloodGlucoseViewController = (self.storyboard?.instantiateViewController(withIdentifier: "ScanBloodGlucoseViewController") as? ScanBloodGlucoseViewController)!
            scanBloodGlucoseViewController.delegate = self
            scanBloodGlucoseViewController.modalPresentationStyle = .overFullScreen
            scanBloodGlucoseViewController.modalTransitionStyle = .crossDissolve
            self.present(scanBloodGlucoseViewController, animated: true, completion: nil)
            
            self.updateUI(shouldUpdateData: true)
            break
            
        case 1:
            self.activeModule(1)
            let scanBloodPressureViewController = (self.storyboard?.instantiateViewController(withIdentifier: "ScanBloodPressureViewController") as? ScanBloodPressureViewController)!
            scanBloodPressureViewController.delegate = self
            scanBloodPressureViewController.modalPresentationStyle = .overFullScreen
            scanBloodPressureViewController.modalTransitionStyle = .crossDissolve
            self.present(scanBloodPressureViewController, animated: true, completion: nil)
            
            self.updateUI(shouldUpdateData: true)
            break
            
        case 2:
            self.activeModule(3)
            self.updateUI(shouldUpdateData: true) {
                DispatchQueue.main.async {
                    let moodVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMoodViewController") as! AddMoodViewController
                    moodVC.modalPresentationStyle = .overFullScreen
                    moodVC.modalTransitionStyle = .crossDissolve
                    self.present(moodVC, animated: true, completion: nil)
                }
            }
            break
            
        case 3:
            self.activeModule(4)
            self.updateUI(shouldUpdateData: true) {
                DispatchQueue.main.async {
                    let addWeightViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddWeightViewController") as! AddWeightViewController
                    addWeightViewController.modalPresentationStyle = .overFullScreen
                    addWeightViewController.modalTransitionStyle = .crossDissolve
                    self.present(addWeightViewController, animated: true, completion: nil)
                }
            }
            break
            
        case 4:
            self.activeModule(2)
            self.updateUI(shouldUpdateData: true) {
                DispatchQueue.main.async {
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "NewBloodPressureAddManuallyViewController") as! NewBloodPressureAddManuallyViewController
                    vc.colorTheme = 2
                    vc.modalPresentationStyle = .overFullScreen
                    vc.modalTransitionStyle = .crossDissolve
                    self.present(vc, animated: true, completion: nil)
                }
            }
            break
            
        case 5:
            self.updateUI(shouldUpdateData: true)
            break
            
        case 6:
            self.updateUI(shouldUpdateData: true)
            break

        default:
            break
        }
//            }
//        }
    }
}
