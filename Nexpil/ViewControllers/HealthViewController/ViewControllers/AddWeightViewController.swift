//
//  AddWeightViewController.swift
//  Nexpil
//
//  Created by Nexpil Admin on 11/5/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class AddWeightViewController: UIViewController {

    @IBOutlet weak var labelBackView: UIView!
    @IBOutlet weak var weightLabel: UITextField!
    
    private let apiURL = DataUtils.APIURL + DataUtils.HEALTH_WEIGHT
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneNextClick(_ sender: Any) {
        let value = (weightLabel.text! as NSString).doubleValue
        
        //-----PING-----
        if value == 0 {
            return
        }
        self.view.endEditing(true)
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let date = Date()
        let params = [
            "choice": 0,
            "weight": value,
            "date_posted": formatter.string(from: date),
            "user_id": PreferenceHelper().getId()
        ] as [String : Any]
        
        SVProgressHUD.show()
        Alamofire.request(apiURL, method: .post, parameters: params)
            .responseJSON(completionHandler: { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success:
                    DataManager.shared.writeSampleByWeight(weight: value, date: date) { (success, error) in
                        DispatchQueue.main.async {
                            self.dismiss(animated: true, completion: nil)
                            NotificationCenter.default.post(name: Notification.Name.Action.UpdateWeight, object: nil)
                        }
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global_ShowFrostGlass(self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Global_HideFrostGlass()
    }
}
