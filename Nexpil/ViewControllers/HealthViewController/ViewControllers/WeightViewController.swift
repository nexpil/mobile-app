//
//  WeightViewController.swift
//  Nexpil
//
//  Created by mac on 12/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Charts
import CareKit
import SVProgressHUD

class WeightViewController: UIViewController {
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet var btnDuration: [UIButton]!
    @IBOutlet weak var vwDurationBar: UIView!
    @IBOutlet weak var tblHistory: UITableView!
    @IBOutlet weak var addButton: FAButton!
    @IBOutlet weak var lblMostRecent: UILabel!
    
    @IBOutlet weak var constraintHeightTblvHistory  : NSLayoutConstraint!
    @IBOutlet weak var viewGraph        : HealthWeightGraphView!
    @IBOutlet var constraintsBarHorz    : [NSLayoutConstraint]!
    
    var currentTab : Int = 0
    var manager = DataManager.shared
    var val : [[String:String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickAdd)))
        addButton.isUserInteractionEnabled = true
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
        
        let strMostRecent = getWeight()
        lblMostRecent.text = strMostRecent.length > 0 ? strMostRecent : "-"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let notificationNameWeight = Notification.Name.Action.UpdateWeight
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameWeight, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateHealthData(notification:Foundation.Notification) {
        fetchWeight()
    }

    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirst {
            viewGraph.layoutIfNeeded()
            for btn in btnDuration {
                btn.layoutIfNeeded()
            }
            self.changedDuration(0)
            isFirst = false
        }
    }
    
    @IBAction func onDuration(_ sender: UIButton) {
        changedDuration(sender.tag)
    }
    
    func showShadow() {
        Global_ShowFrostGlass(self.view)
    }
    
    func fetchWeight() {
        val.removeAll()
        tblHistory.reloadData()
        var afValue : [CGFloat] = []
        let unit = " lbs"
        
        var xValues: [String] = []
        
        if currentTab == 0 {
            
            SVProgressHUD.show()
            let arrayList = manager.fetchWeightGetAllWeekData()
            SVProgressHUD.dismiss()
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            let ymNow : String = String(result.prefix(7))
            let dNow : Int! = Int(String(result.suffix(2)))
            
            var month = String()
            let tempFormatter = DateFormatter()
            tempFormatter.dateFormat = "yyyy-MM"
            if let date = tempFormatter.date(from: ymNow) {
                tempFormatter.dateFormat = "MMMM"
                month = tempFormatter.string(from: date)
            }
            
            let shortMonth = month.substring(to: 3)
            var start : Int = Int(dNow / 7) * 7
            var end : Int = start + 6
            start = start == 0 ? 1 : start
            end = end > 30 ? 30 : end
            for i in start..<end + 1 {
                var dic : [String:String] = [:]
                dic["date"] = month + " " + String(i)
                dic["value"] = "-"
                val.append(dic)
                afValue.append(0)
                xValues.append(shortMonth + " " + String(i))
            }
            
            for i in 0..<arrayList.count {
                let dicList = arrayList[i] as! NSDictionary
                let strWeek = dicList["strWeek"] as! String
                let ymData: String = String(strWeek.prefix(7))
                let day : String = String(strWeek.suffix(4))
                let sday : Int! = Int(String(day.prefix(2)))
                let eday : Int! = Int(String(day.suffix(2)))
                
                if ymData == ymNow && sday <= dNow && dNow <= eday {
                    for data in dicList["data"] as! NSArray {
                        
                        let dicData = data as! NSDictionary
                        let strDate = dicData["strDate"] as! String
                        let numValue = dicData["value"] as! Double
                        let idx : Int! = Int(String(strDate.suffix(2)))
                        
                        val[idx - sday]["value"] = String(format: "%li%@", numValue, unit)
                        afValue[idx - sday] = CGFloat(numValue)
                    }
                }
            }
            
        } else {
            
            SVProgressHUD.show()
            let arrayList = self.manager.fetchWeightGetAllMonthData()
            SVProgressHUD.dismiss()
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            let ymNow : String = String(result.prefix(7))
            
            var month = String()
            let tempFormatter = DateFormatter()
            tempFormatter.dateFormat = "yyyy-MM"
            if let date = tempFormatter.date(from: ymNow) {
                tempFormatter.dateFormat = "MMMM"
                month = tempFormatter.string(from: date)
            }
            let shortMonth = month.substring(to: 3)
            
            var dic : [String:String] = [:]
            dic["date"] = String(format: "%@ 1-6", month)
            dic["value"] = "-"
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 1-6", shortMonth))
            dic["date"] = String(format: "%@ 7-13", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 7-13", shortMonth))
            dic["date"] = String(format: "%@ 14-20", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 14-20", shortMonth))
            dic["date"] = String(format: "%@ 21-27", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 21-27", shortMonth))
            dic["date"] = String(format: "%@ 28-30", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 28-30", shortMonth))
            
            for i in 0..<arrayList.count {
                let dicList = arrayList[i] as! NSDictionary
                let mon = dicList["month"] as! String
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM"
                let result = formatter.string(from: date)
                
                if result == mon {
                    let value0106 = dicList["avg0106"] as! NSInteger
                    let value0713 = dicList["avg0713"] as! NSInteger
                    let value1420 = dicList["avg1420"] as! NSInteger
                    let value2127 = dicList["avg2127"] as! NSInteger
                    let value2830 = dicList["avg2830"] as! NSInteger
                    
                    if value0106 > 0 {
                        let strBefore0106 = String(format: "%li%@", value0106, unit)
                        val[0]["value"] = strBefore0106
                        afValue[0] = CGFloat(value0106)
                    }
                    if value0713 > 0 {
                        let strBefore0713 = String(format: "%li%@", value0713, unit)
                        val[1]["value"] = strBefore0713
                        afValue[1] = CGFloat(value0713)
                    }
                    if value1420 > 0 {
                        let strBefore1420 = String(format: "%li%@", value1420, unit)
                        val[2]["value"] = strBefore1420
                        afValue[2] = CGFloat(value1420)
                    }
                    if value2127 > 0 {
                        let strBefore2127 = String(format: "%li%@", value2127, unit)
                        val[3]["value"] = strBefore2127
                        afValue[3] = CGFloat(value2127)
                    }
                    if value2830 > 0 {
                        let strBefore2830 = String(format: "%li%@", value2830, unit)
                        val[4]["value"] = strBefore2830
                        afValue[4] = CGFloat(value2830)
                    }
                }
            }
        }
        
        self.updateChart(dataAfter: afValue, dataBefore: afValue, xValues: xValues)
        tblHistory.reloadData()
    }
    
    func changedDuration(_ idx: Int) {
        currentTab = idx
        
        for btn in btnDuration {
            btn.setTitleColor(UIColor(hex: "969696"), for: .normal)
        }
        
        btnDuration[idx].setTitleColor(UIColor(hex: "847FE5"), for: .normal)
        
        for constraint in constraintsBarHorz {
            constraint.isActive = false
        }
        UIView.animate(withDuration: 0.3) {
            self.constraintsBarHorz[idx].isActive = true
            self.view.layoutIfNeeded()
        }
        
        fetchWeight()
    }
    
    func updateUI() {
        constraintHeightTblvHistory.constant = CGFloat(60 * val.count)
    }

    func updateChart(dataAfter: [CGFloat], dataBefore: [CGFloat], yValues: [CGFloat] = [60, 100, 140, 180, 220], xValues: [String]) {
        viewGraph.setData(dataAfter: dataAfter, dataBefore: dataBefore, yValues: yValues, xValues: xValues)
        updateUI()
    }

    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onClickAdd() {
        let addWeightViewController = (self.storyboard?.instantiateViewController(withIdentifier: "AddWeightViewController") as? AddWeightViewController)!
        addWeightViewController.modalPresentationStyle = .overFullScreen
        addWeightViewController.modalTransitionStyle = .crossDissolve
        self.present(addWeightViewController, animated: true, completion: nil)
    }
}

extension WeightViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return val.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellWeightHistory", for: indexPath) as! CellWeightHistory
        cell.selectionStyle = .none
        let dicData = val[indexPath.row]
        cell.setInfo(info:dicData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class CellWeightHistory: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblWeight: UILabel!
    
    func setInfo(info:[String:String]) {
        lblTime.text = info["date"]
        lblWeight.text = info["value"]
    }
}
