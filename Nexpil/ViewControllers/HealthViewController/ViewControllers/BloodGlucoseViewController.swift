//
//  BloodGlucoseViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CareKit
import Charts
import SVProgressHUD

struct Glucose {
    var time: String
    var before: String
    var after: String
    var strDate: String
    var beValue: CGFloat = 0.0
    var afValue: CGFloat = 0.0
}

public struct Week {
    var start: Date
    var end: Date
}

class BloodGlucoseViewController: UIViewController, ScanBloodGlucoseViewControllerDelegate {
    @IBOutlet weak var lblMeal: UILabel!
    @IBOutlet var btnDuration: [UIButton]!
    @IBOutlet weak var addButton: FAButton!
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet weak var vwDurationBar: UIView!
    @IBOutlet weak var tblHistory: UITableView!
    @IBOutlet var constraintsBarHorz: [NSLayoutConstraint]!
    @IBOutlet weak var viewGraph: HealthBloodGlucoseGraphView!
    @IBOutlet weak var constraintHeightTblvHistory: NSLayoutConstraint!
    
    var val : [Glucose] = []
    var currentTab : Int = 0
    var manager = DataManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
        addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickAdd)))
        addButton.isUserInteractionEnabled = true
        
        viewGraph.setData(dataAfter: [], dataBefore: [], yValues: [], xValues: [])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let notificationNameBloodG = Notification.Name.Action.UpdateBloodGlucose
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameBloodG, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateHealthData(notification:Foundation.Notification) {
        fetchBloodGlucose()
    }
    
    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirst {
            viewGraph.layoutIfNeeded()
            for btn in btnDuration {
                btn.layoutIfNeeded()
            }
            self.changedDuration(1)
            isFirst = false
        }
    }
    
    @IBAction func onDuration(_ sender: UIButton) {
        changedDuration(sender.tag)
    }

    private func insertBloodGlucose(date: Date, whenIndex: String, value: NSInteger) {
        SVProgressHUD.show()
        DataManager.shared.writeSampleByBloodGlucose(value: value, date: date, whenIndex: Int(whenIndex) ?? 0, note: "") { (success, error) in
            SVProgressHUD.dismiss()
            NotificationCenter.default.post(name: Notification.Name.Action.UpdateBloodGlucose, object: nil)
        }
    }

    func didTapButtonAddGlucoseScanBloodGlucoseViewController(date: Date, whenIndex: String, value: NSInteger) {
        self.insertBloodGlucose(date: date, whenIndex: whenIndex, value: value)
    }
    
    func showShadow() {
        Global_ShowFrostGlass(self.view)
    }
    
    func getAllDaysOfTheCurrentWeek() -> [Date] {
        var dates: [Date] = []
        let today = Date.today()
        var weekStart = today.startOfWeek!
        let weekEnd = today.endOfWeek!
        while weekStart < weekEnd {
            dates.append(weekStart)
            weekStart = weekStart.adding(days: 1)
        }
//        var calendar = Calendar.current
//        calendar.firstWeekday = 2
//        guard let dateInterval = calendar.dateInterval(of: .weekOfYear, for: Date.today()) else {
//            return dates
//        }
//
//        calendar.enumerateDates(startingAfter: dateInterval.start, matching: DateComponents(hour:0), matchingPolicy: .nextTime) { date, _, stop in
//            guard let date = date else {
//                return
//            }
//            if date <= dateInterval.end {
//                dates.append(date)
//            } else {
//                stop = true
//            }
//        }
//
        return dates
    }
    
    private func fetchBloodGlucose() {
        val.removeAll()
        DispatchQueue.main.async {
            self.tblHistory.reloadData()
        }
        
        if currentTab == 0 {
            SVProgressHUD.show()
            let glucose1 = Glucose(time: "Breakfast", before: "-", after: "-", strDate: "")
            val.append(glucose1)
            
            let glucose2 = Glucose(time: "Lunch", before: "-", after: "-", strDate: "")
            val.append(glucose2)
            
            let glucose3 = Glucose(time: "Dinner", before: "-", after: "-", strDate: "")
            val.append(glucose3)
            
            BloodGlucoseController.fetchTodayHistory(handler: { result in
                for item in result {
                    let timing = Int(item["timing"] as! String)!
                    let value = item["measurement"] as! String
                    
                    if timing < 3 {
                        self.val[timing].before = value + " mg/dl"
                        self.val[timing].beValue = CGFloat((value as NSString).floatValue)
                    } else {
                        self.val[timing - 3].after = value + " mg/dl"
                        self.val[timing - 3].afValue = CGFloat((value as NSString).floatValue)
                    }
                }
                 
                self.updateChart(dataAfter: self.val.compactMap { $0.afValue }, dataBefore: self.val.compactMap { $0.beValue }, xValues: ["Breakfast", "Lunch", "Dinner"])
                self.tblHistory.reloadData()
            })
            SVProgressHUD.dismiss()
        } else if currentTab == 1 {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"

            var datesForGraph: [String] = []
            
            // ARIF SOLUTION//
            let weekFormatter = DateFormatter()
            weekFormatter.dateFormat = "LLLL dd"
            let weekDates = getAllDaysOfTheCurrentWeek()
            for date in weekDates {
                let weekDay = weekFormatter.string(from: date)
                let glucose = Glucose(time: weekDay, before: "-", after: "-", strDate: formatter.string(from: date))
                val.append(glucose)
                datesForGraph.append(weekDay)
            }
            print(datesForGraph)
            
            BloodGlucoseController.fetchWeekHistory(handler: { result in
                var sumBefore :[String: [Int]] = [:]
                var sumAfter :[String: [Int]] = [:]
                
                for history in result {
                    if let before = history["before"] as? [[String: Any]] {
                        for bf in before {
                            if let date = bf["strDate"] as? String {
                                if sumBefore[date] == nil {
                                    sumBefore[date] = [bf["value"] as! Int]
                                } else {
                                    sumBefore[date]?.append(bf["value"] as! Int)
                                }
                            }
                        }
                    }
                    if let after = history["after"] as? [[String: Any]] {
                        for af in after {
                            if let date = af["strDate"] as? String {
                                if sumAfter[date] == nil {
                                    sumAfter[date] = [af["value"] as! Int]
                                } else {
                                    sumAfter[date]?.append(af["value"] as! Int)
                                }
                            }
                        }
                    }
                }
                
                self.val = self.val.map({ (v: Glucose) -> Glucose in
                    var item = v
                    if let befores = sumBefore[item.strDate] {
                        let avgBefore = befores.reduce(0, +) / befores.count
                        item.before = "\(avgBefore) mg/dl"
                        item.beValue = CGFloat(avgBefore)
                    }
                    if let afters = sumAfter[item.strDate] {
                        let avgAfter = afters.reduce(0, +) / afters.count
                        item.after = "\(avgAfter) mg/dl"
                        item.afValue = CGFloat(avgAfter)
                    }
                    return item
                })

                self.updateChart(dataAfter: self.val.compactMap { $0.afValue }, dataBefore: self.val.compactMap { $0.beValue }, xValues: datesForGraph)
                
                self.tblHistory.reloadData()
            })
        } else {
            var datesForGraph: [String] = []
            let weeks = self.getWeeksOfCurrentMonth()
            let currentMonth = Date.today().adding(months: 0).monthName(true)
            for week in weeks {
                let startDate = week.start.date()
                let endDate = week.end.date()
                var time = ""
                if startDate == endDate {
                    time = String(format: "%@ %@", currentMonth, startDate)
                } else {
                    time = String(format: "%@ %@ - %@", currentMonth, startDate, endDate)
                }
                datesForGraph.append(time)
            }
            
            BloodGlucoseController.fetchCurrentMonthHistory(forWeeks: weeks) { result in
                self.val = result
                self.updateChart(dataAfter: self.val.compactMap { $0.afValue }, dataBefore: self.val.compactMap { $0.beValue }, xValues: datesForGraph)
                self.tblHistory.reloadData()
            }
        }
    }
    
    func getWeeksOfCurrentMonth() -> [Week] {
        var calendar = Calendar.current
        calendar.firstWeekday = 2
        let date = Date.today().adding(months: 0)
        let weekRange = calendar.range(of: .weekOfYear, in: .month, for: date)
        let weekRanges = Array(weekRange!.min()!...weekRange!.max()!)
        var weeks = [Week]()
        for (index, value) in weekRanges.enumerated() {
            if index == 0 {
                let week = Week(start: date.startOfMonth(), end: date.startOfMonth().endOfWeek!)
                weeks.append(week)
            } else if index == weekRanges.count - 1 {
                let monthEnd = date.endOfMonth()
//                let weekStart = monthEnd.startOfWeek!
                let date = Date.getWeekOfYear(from: value, year: date.year!)
                let week = Week(start: date!.startOfWeek!.adding(days: 1), end: monthEnd)
                weeks.append(week)
            } else {
                let date = Date.getWeekOfYear(from: value, year: date.year!)
                let week = Week(start: date!.startOfWeek!.adding(days: 1), end: date!.endOfWeek!)
                weeks.append(week)
            }
        }
        return weeks
    }
    
    func changedDuration(_ idx: Int) {
        currentTab = idx
        
        for btn in btnDuration {
            btn.setTitleColor(UIColor(hex: "969696"), for: .normal)
        }
        btnDuration[idx].setTitleColor(UIColor(hex: "397EE3"), for: .normal)
        
        for constraint in constraintsBarHorz {
            constraint.isActive = false
        }
        UIView.animate(withDuration: 0.3) {
            self.constraintsBarHorz[idx].isActive = true
            self.view.layoutIfNeeded()
        }
        
        lblMeal.isHidden = (idx != 0)
        
        fetchBloodGlucose()
    }
    
    func updateUI() {
        constraintHeightTblvHistory.constant = CGFloat(60 * val.count)
    }

    func updateChart(dataAfter: [CGFloat], dataBefore: [CGFloat], yValues: [CGFloat] = [60, 100, 140, 180, 220], xValues: [String]) {
        viewGraph.setData(dataAfter: dataAfter, dataBefore: dataBefore, yValues: yValues, xValues: xValues)
        updateUI()
    }
    
    func setChartView(xVal: NSArray, yVal1: NSArray, yVal2: NSArray) {
        var allLineChartDataSets: [LineChartDataSet] = [LineChartDataSet]()
        
        var dataEntries: [ChartDataEntry] = []
        let dataPoints = xVal
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: xVal[i] as! Double, y: yVal1[i] as! Double)
            dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet1: LineChartDataSet = LineChartDataSet(values: dataEntries, label: "Before")
        
        // set dataset1
        let gradientColors = [ChartColorTemplates.colorFromString("#aecbf1").cgColor,
                              ChartColorTemplates.colorFromString("#8bb3ec").cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        lineChartDataSet1.setColor(.cyan)
        lineChartDataSet1.setCircleColor(.cyan)
        lineChartDataSet1.circleRadius = 5
        lineChartDataSet1.drawCircleHoleEnabled = false
        lineChartDataSet1.axisDependency = .left
        
        lineChartDataSet1.fillAlpha = 1
        lineChartDataSet1.fill = Fill(linearGradient: gradient, angle: 60)
        lineChartDataSet1.drawFilledEnabled = false
        lineChartDataSet1.mode = .cubicBezier
        
        allLineChartDataSets.append(lineChartDataSet1)

        var dataEntries2: [ChartDataEntry] = []
        let dataPoints2 = xVal
        
        for i in 0..<dataPoints2.count {
            let dataEntry2 = ChartDataEntry(x: xVal[i] as! Double, y: yVal2[i] as! Double)
            dataEntries2.append(dataEntry2)
        }
        
        let lineChartDataSet2 = LineChartDataSet(values: dataEntries2, label: "After")
        // set dataset1
        let gradientColors2 = [ChartColorTemplates.colorFromString("#8db5eb").cgColor,
                               ChartColorTemplates.colorFromString("#5c95e4").cgColor]
        let gradient2 = CGGradient(colorsSpace: nil, colors: gradientColors2 as CFArray, locations: nil)!
        
        lineChartDataSet2.setColor(.blue)
        lineChartDataSet2.setCircleColor(.blue)
        lineChartDataSet2.circleRadius = 5
        lineChartDataSet2.drawCircleHoleEnabled = false
        lineChartDataSet2.axisDependency = .right
        
        lineChartDataSet2.fillAlpha = 1
        lineChartDataSet2.fill = Fill(linearGradient: gradient2, angle: 60)
        lineChartDataSet2.drawFilledEnabled = false
        lineChartDataSet2.mode = .cubicBezier
                
        let noZeroFormatter = NumberFormatter()
        noZeroFormatter.zeroSymbol = ""
        lineChartDataSet1.valueFormatter = DefaultValueFormatter(formatter: noZeroFormatter)
        lineChartDataSet2.valueFormatter = DefaultValueFormatter(formatter: noZeroFormatter)
        lineChartDataSet2.drawCirclesEnabled = false
        allLineChartDataSets.append(lineChartDataSet2)
        
        let lineChartData = LineChartData(dataSets: allLineChartDataSets)
        
//        chartView.data = lineChartData
//        chartView.setYAxisMaxWidth(YAxis.AxisDependency.left, width: 0)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onClickAdd() {
        let scanBloodGlucoseViewController = (self.storyboard?.instantiateViewController(withIdentifier: "ScanBloodGlucoseViewController") as? ScanBloodGlucoseViewController)!
        scanBloodGlucoseViewController.delegate = self
        scanBloodGlucoseViewController.modalPresentationStyle = .overFullScreen
        scanBloodGlucoseViewController.modalTransitionStyle = .crossDissolve
        self.present(scanBloodGlucoseViewController, animated: true, completion: nil)
    }
}

extension BloodGlucoseViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return val.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBGHistory", for: indexPath) as! CellBGHistory
        cell.selectionStyle = .none
        let dicData = val[indexPath.row]
        cell.setInfo(info: dicData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class CellBGHistory: UITableViewCell {
    @IBOutlet weak var lblTime  : UILabel!
    @IBOutlet weak var lblBefore: UILabel!
    @IBOutlet weak var lblAfter : UILabel!
    
    func setInfo(info: Glucose) {
        lblTime.text = info.time
        lblBefore.text = info.before
        lblAfter.text = info.after
    }
}
