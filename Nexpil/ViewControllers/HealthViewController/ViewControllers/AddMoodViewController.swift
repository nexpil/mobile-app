//
//  AddMoodViewController.swift
//  Nexpil
//
//  Created by Nexpil Admin on 11/5/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AddMoodViewController: UIViewController {

    @IBOutlet weak var sliderMood: GradientSlider!
    @IBOutlet weak var moodIcon: UIImageView!
    @IBOutlet weak var moodLabel: UILabel!
    
    let moodList = ["Very Sad", "Sad", "Neutral", "Happy", "Very Happy"]
    var feeling = 2
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sliderMood.thumbSize = 20
        sliderMood.thumbColor = UIColor(hex: "7CE2EC")
        updateMood()
    }
    
    @IBAction func addNextClick(_ sender: Any) {
        weak var pvc = self.presentingViewController
        self.dismiss(animated: true, completion: {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMood2ViewController") as! AddMood2ViewController
            vc.feeling = Int(self.sliderMood.value)
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            pvc?.present(vc, animated: true, completion: nil)
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global_ShowFrostGlass(self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Global_HideFrostGlass()
    }
    @IBAction func updateMoodStatus(_ sender: Any) {
        self.feeling = Int(sliderMood.value)
        updateMood()
    }
    
    func updateMood() {
        let mood = moodList[self.feeling]
        moodLabel.text = mood
        moodIcon.image = UIImage(named: mood)
        sliderMood.setValue(CGFloat(self.feeling))
    }
    
    @IBAction func closeModal(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
