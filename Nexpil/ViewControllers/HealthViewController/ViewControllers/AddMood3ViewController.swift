//
//  AddMood3ViewController.swift
//  Nexpil
//
//  Created by Nexpil Admin on 11/5/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire

class AddMood3ViewController: UIViewController {

    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var dateLabel: UILabel!
    
    private let apiURL = DataUtils.APIURL + DataUtils.HEALTH_MOOD
    
    var feeling = 0
    var note =  ""
    var date = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        CalendarView.Style.cellShape                = .round
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.headerTextColor          = NPColorScheme(rawValue: 1)!.color
        CalendarView.Style.cellTextColorDefault     = UIColor.darkText
        CalendarView.Style.cellTextColorToday       = UIColor.white
        CalendarView.Style.firstWeekday             = .sunday
        CalendarView.Style.cellTextColorWeekend     = NPColorScheme(rawValue: 1)!.color
        CalendarView.Style.locale                   = Locale(identifier: "en_US")
        CalendarView.Style.timeZone                 = TimeZone(abbreviation: "UTC")!
        CalendarView.Style.cellColorToday = NPColorScheme(rawValue: 1)!.color
        CalendarView.Style.headerFontName = "Montserrat"
        
        CalendarView.Style.hideCellsOutsideDateRange = false
        CalendarView.Style.changeCellColorOutsideRange = false
        CalendarView.Style.cellSelectedBorderColor = NPColorScheme(rawValue: 1)!.color
        CalendarView.Style.colorTheme = 1
        
        calendarView.backgroundColor = UIColor.clear
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.reloadData()
        
//        updateDateLabel()
    }

    @IBAction func doneButtonClick(_ sender: Any) {
        //-----PING-----
        /*let _ = DataManager.shared.insertMood(date: date, feeling: feeling, notes: note)
        NotificationCenter.default.post(name: Notification.Name.Action.UpdateMoodList, object: nil)
        //print("Add mood result: \(result)")
        self.dismiss(animated: true, completion: nil)*/
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        
        let params = [
            "choice": 0,
            "feeling_type" : feeling + 1,//For now
            "mood_note" : note,
            "note_id" : 0,
            "date_posted": formatter.string(from: date),
            "user_id": PreferenceHelper().getId()
        ] as [String : Any]
        
        SVProgressHUD.show()
        Alamofire.request(apiURL, method: .post, parameters: params)
            .responseJSON(completionHandler: { response in
                SVProgressHUD.dismiss()
                switch response.result {
                case .success:
                    let result = DataManager.shared.insertMood(date: self.date, feeling: self.feeling, notes: self.note)
                    if result {
                        NotificationCenter.default.post(name: Notification.Name.Action.UpdateMoodList, object: nil)
                        self.dismiss(animated: true, completion: nil)
                    }
                case .failure(let error):
                    print(error.localizedDescription)
                }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global_ShowFrostGlass(self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Global_HideFrostGlass()
    }
    
    func updateDateLabel() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        dateLabel.text = formatter.string(from: MoodDataManager.Date)
    }
    
    @IBAction func backToPrevModal(_ sender: Any) {
        weak var pvc = self.presentingViewController
        self.dismiss(animated: true, completion: {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMood2ViewController") as! AddMood2ViewController
            vc.feeling = self.feeling
            vc.note = self.note
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            pvc?.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func closeModal(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension AddMood3ViewController: CalendarViewDataSource {
    func startDate() -> Date {
        return Date()
    }
    
    func endDate() -> Date {
        
        var dateComponents = DateComponents()
        
        dateComponents.year = 2
        let today = Date()
        
        let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoYearsFromNow
    }
}

extension AddMood3ViewController: CalendarViewDelegate {
    func calendar(_ calendar: CalendarView, didSelectDate date: Date, withEvents events: [CalendarEvent]) {
        self.date = date
//        MoodDataManager.Date = date
//        self.updateDateLabel()
    }
    func calendar(_ calendar : CalendarView, didScrollToMonth date : Date) {
        
    }
}
