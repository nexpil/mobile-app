//
//  LabResultsLipidPanelViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LabResultsLipidPanelViewController: UIViewController {
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet weak var tblHistory   : UITableView!
    @IBOutlet weak var addButton    : FAButton!

    var lipidPanels         : [ShortLipidPanel]  = []
    var lipidPanelsByDate   : [String: [ShortLipidPanel]] = [:]
    var dates = Array<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2

        for item in lipidPanels {
            var ary = lipidPanelsByDate[item.strDate] ?? []
            ary.append(item)
            lipidPanelsByDate[item.strDate] = ary
        }
        dates = Array(lipidPanelsByDate.keys)
        dates.sort { (date1, date2) -> Bool in
            return date1 > date2
        }
        
        self.tblHistory.reloadData()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension LabResultsLipidPanelViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return dates.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return dates[section].dateString()
    }

    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            headerView.backgroundView?.backgroundColor = .white
            headerView.textLabel?.textColor = UIColor(hex: "333333")
            headerView.textLabel?.font = UIFont.systemFont(ofSize: 30)
        }
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabLipidPanel", for: indexPath) as! CellLabLipidPanel
        cell.setValue(lipidPanelsByDate[dates[indexPath.section]])
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(lipidPanelsByDate[dates[indexPath.row]]!.count) * 60.0 + 10.0
    }
}
