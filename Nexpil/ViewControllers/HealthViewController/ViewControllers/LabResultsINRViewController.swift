//
//  LabResultsINRViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LabResultsINRViewController: UIViewController {
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet weak var tblHistory   : UITableView!
    @IBOutlet weak var addButton    : FAButton!

    var inrs  : [ShortINR]  = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
        
        self.inrs.sort { (item1, item2) -> Bool in
            return item1.strDate > item2.strDate
        }
        self.tblHistory.reloadData()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension LabResultsINRViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return inrs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabINR", for: indexPath) as! CellLabINR
        let lab = inrs[indexPath.row]
        cell.setValue(lab.strDate, value: lab.value)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
