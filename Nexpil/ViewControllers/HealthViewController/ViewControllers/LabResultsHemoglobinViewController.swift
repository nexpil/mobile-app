//
//  LabResultsHemoglobinViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LabResultsHemoglobinViewController: UIViewController {
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet weak var tblHistory   : UITableView!
    @IBOutlet weak var addButton    : FAButton!

    var hemoglobinAlcs  : [ShortHemoglobinAlc]  = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
        
        self.hemoglobinAlcs.sort { (item1, item2) -> Bool in
            return item1.strDate > item2.strDate
        }
        self.tblHistory.reloadData()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}

extension LabResultsHemoglobinViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return hemoglobinAlcs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabHemoglobinAlc", for: indexPath) as! CellLabHemoglobinAlc
        let lab = hemoglobinAlcs[indexPath.row]
        cell.setValue(lab.strDate, value: lab.value)
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
