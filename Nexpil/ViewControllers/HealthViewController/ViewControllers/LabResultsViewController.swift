//
//  LabResultsViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CareKit
import HealthKit
import Charts
import SVProgressHUD

class LabResultsViewController: UIViewController {
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet weak var tblHistory   : UITableView!
    @IBOutlet var buttonsType       : [UIButton]!
    @IBOutlet weak var addButton    : FAButton!
    @IBOutlet var constraintsLeadingButtonType  : [NSLayoutConstraint]!
    
    var manager = DataManager.shared
    
    var currentType = 0
    
    fileprivate var labsByDate      : [String: [String: [Any]]] = [:]
    fileprivate var hemoglobinAlcs  : [ShortHemoglobinAlc]  = []
    fileprivate var lipidPanels     : [ShortLipidPanel]     = []
    fileprivate var inrs            : [ShortINR]            = []
    fileprivate var dates           : [String]              = []
    
    fileprivate var labCategories = ["Hemoglobin A1c",
                                     "Lipid Panel",
                                     "PT - INR"]

    fileprivate let kHemoglobin     = "Hemoglobin A1c"
    fileprivate let kINR            = "PT - INR"
    fileprivate let kCholesterol    = "Total cholesterol"
    fileprivate let kHDL            = "Cholesterol HDL"
    fileprivate let kLDL            = "LDL-cholesterol"
    fileprivate let kTriglycerides  = "Triglycerides"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
    }
    
    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirst {
            for btn in buttonsType {
                btn.layoutIfNeeded()
            }
            changeType(index: 0)
            getSampleData()
            getLabs()
            isFirst = false
        }
    }
    
    private func getLabs() {
        /*
        getHemoglobinAlc()
        getLipidPanel()
        getINR()*/
        
        guard let labResultType = HKObjectType.clinicalType(forIdentifier: .labResultRecord) else {
            return
        }

        let labResultQuery = HKSampleQuery(sampleType: labResultType, predicate: nil, limit: HKObjectQueryNoLimit, sortDescriptors: nil) { (query, samples, error) in

            guard let actualSamples = samples else {
                print("*** An error occurred: \(error?.localizedDescription ?? "nil") ***")
                return
            }
            
            let labResultSamples = actualSamples as! [HKClinicalRecord]
            for clinicalRecord in labResultSamples {
                if let fhirRecord = clinicalRecord.fhirResource {
                    do {
                        let jsonDictionary = try JSONSerialization.jsonObject(with: fhirRecord.data, options: [])
                        self.parseRAWLabResult(json: jsonDictionary)
                    }
                    catch let error {
                        print("*** An error occurred while parsing the FHIR data: \(error.localizedDescription) ***")
                    }
                }
            }
            
            self.dates = Array(self.labsByDate.keys).sorted(by: { (date1, date2) -> Bool in
                return date1 > date2
            })
            DispatchQueue.main.async {
                self.tblHistory.reloadData()
            }
        }
        HKHealthStore().execute(labResultQuery)
    }
    
    private func parseRAWLabResult(json: Any) {
        guard let labResult = json as? [String: Any] else {
            return
        }
        guard let code = labResult["code"] as? [String: Any],
            let issued = labResult["issued"] as? String,
            let valueQuantity = labResult["valueQuantity"] as? [String: Any] else {
            return
        }
        let strDate = issued.substring(to: 10)
        let value = valueQuantity["value"] as? Double
        if let text = code["text"] as? String {
            switch text {
            case kHemoglobin:
                let hemoglobinAlc = ShortHemoglobinAlc(value: value ?? 0, strDate: strDate)
                hemoglobinAlcs.append(hemoglobinAlc)
                addLab(key: "HemoglobinAlc", date: strDate, data: hemoglobinAlc)
                break
            case kCholesterol, kHDL, kLDL, kTriglycerides:
                let lipidPanel = ShortLipidPanel(name: text, value: value ?? 0, strDate: strDate)
                lipidPanels.append(lipidPanel)
                addLab(key: "LipidPanel", date: strDate, data: lipidPanel)
                break
            case kINR:
                let inr = ShortINR(value: value ?? 0, strDate: strDate)
                inrs.append(inr)
                addLab(key: "INR", date: strDate, data: inr)
                break
            default:
                break
            }
        }
    }
    
    private func getSampleData() {
        for _ in 0 ..< 10 {
            let strDate = String(format: "%04d-%02d-%02d", 2019, arc4random() % 12 + 1, arc4random() % 28 + 1)
            let hemoglobinAlc = ShortHemoglobinAlc(value: Double(arc4random() % 10 + 1), strDate: strDate)
            hemoglobinAlcs.append(hemoglobinAlc)
            addLab(key: "HemoglobinAlc", date: strDate, data: hemoglobinAlc)

            let strDate1 = String(format: "%04d-%02d-%02d", 2019, arc4random() % 12 + 1, arc4random() % 28 + 1)
            let inr = ShortINR(value: Double(arc4random() % 10 + 1), strDate: strDate1)
            inrs.append(inr)
            addLab(key: "INR", date: strDate1, data: inr)
        }
        for _ in 0 ..< 30 {
            let strDate = String(format: "%04d-%02d-%02d", 2019, arc4random() % 12 + 1, arc4random() % 28 + 1)
            
            let lipidPanel = ShortLipidPanel(name: kCholesterol, value: Double(arc4random() % 100), strDate: strDate)
            lipidPanels.append(lipidPanel)
            addLab(key: "LipidPanel", date: strDate, data: lipidPanel)
            
            let lipidPanel1 = ShortLipidPanel(name: kHDL, value: Double(arc4random() % 100), strDate: strDate)
            lipidPanels.append(lipidPanel1)
            addLab(key: "LipidPanel", date: strDate, data: lipidPanel1)

            let lipidPanel2 = ShortLipidPanel(name: kLDL, value: Double(arc4random() % 100), strDate: strDate)
            lipidPanels.append(lipidPanel2)
            addLab(key: "LipidPanel", date: strDate, data: lipidPanel2)
            
            let lipidPanel3 = ShortLipidPanel(name: kTriglycerides, value: Double(arc4random() % 100), strDate: strDate)
            lipidPanels.append(lipidPanel3)
            addLab(key: "LipidPanel", date: strDate, data: lipidPanel3)
        }
    }
    /*
    private func getHemoglobinAlc() {
        let array = manager.fetchHemoglobinAlc() as! [HemoglobinAlc]
        for data in array {
            let hemoglobinAlc = ShortHemoglobinAlc(value: data.value, strDate: data.strDate)
            hemoglobinAlcs.append(hemoglobinAlc)
            addLab(key: "HemoglobinAlc", date: data.strDate, data: hemoglobinAlc)
        }
    }

    private func getLipidPanel() {
        let array = manager.fetchLipidPanel() as! [LipidPanel]
        for data in array {
            let lipidPanel = ShortLipidPanel(name: data.index ?? "", value: data.value, strDate: data.strDate!)
            lipidPanels.append(lipidPanel)
            addLab(key: "LipidPanel", date: data.strDate!, data: lipidPanel)
        }
    }
    
    private func getINR() {
        let array = manager.fetchINR() as! [INR]
        for data in array {
            let inr = ShortINR(value: data.value, strDate: data.strDate)
            inrs.append(inr)
            addLab(key: "INR", date: data.strDate, data: inr)
        }
    }*/

    private func addLab(key: String, date: String, data: Any) {
        var labs = labsByDate[date] ?? [:]
        var results = labs[key] ?? []
        results.append(data)
        labs[key] = results
        labsByDate[date] = labs
    }

    func showShadow() {
        Global_ShowFrostGlass(self.view)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onBtnType(_ sender: UIButton) {
        changeType(index: sender.tag)
    }
    
    private func changeType(index: Int) {
        for constraint in constraintsLeadingButtonType {
            constraint.isActive = false
        }
        for buttonType in buttonsType {
            buttonType.titleLabel?.textColor = UIColor(hex: "333333")
        }
        buttonsType[index].setTitleColor(.white, for: .normal)
        constraintsLeadingButtonType[index].isActive = true
        
        currentType = index
        tblHistory.reloadData()
    }
}

class ShortHemoglobinAlc {
    var value   : Double = 0
    var strDate : String = ""
    init(value: Double, strDate: String) {
        self.value = value
        self.strDate = strDate
    }
}

class ShortLipidPanel {
    var name    : String = ""
    var value   : Double = 0
    var strDate : String = ""
    init(name: String, value: Double, strDate: String) {
        self.name = name
        self.value = value
        self.strDate = strDate
    }
}

class ShortINR {
    var value   : Double = 0
    var strDate : String = ""
    init(value: Double, strDate: String) {
        self.value = value
        self.strDate = strDate
    }
}

extension LabResultsViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return currentType == 0 ? dates.count : 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return currentType == 0 ? dates[section].dateString() : nil
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if currentType == 0 {
            if let headerView = view as? UITableViewHeaderFooterView {
                headerView.contentView.backgroundColor = .white
                headerView.backgroundView?.backgroundColor = .white
                headerView.textLabel?.textColor = UIColor(hex: "333333")
                headerView.textLabel?.font = UIFont.systemFont(ofSize: 30)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return currentType == 0 ? 60 : 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return currentType == 0 ? (labsByDate[dates[section]]!.count) : 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var tblvCell: UITableViewCell!
        
        if currentType == 0 {
            let labs = labsByDate[dates[indexPath.section]]!
            let key = Array(labs.keys)[indexPath.row]
            if key == "HemoglobinAlc" {
                let lab = labs[key]?.first as! ShortHemoglobinAlc
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabHemoglobinAlc", for: indexPath) as! CellLabHemoglobinAlc
                cell.setValue(value: lab.value)
                tblvCell = cell
            } else if key == "LipidPanel" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabLipidPanel", for: indexPath) as! CellLabLipidPanel
                cell.setValue(labs[key] as? [ShortLipidPanel])
                tblvCell = cell
            } else if key == "INR" {
                let lab = labs[key]?.first as! ShortINR
                let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabINR", for: indexPath) as! CellLabINR
                cell.setValue(value: lab.value)
                tblvCell = cell
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabCategory", for: indexPath) as! CellLabCategory
            cell.setTitle(labCategories[indexPath.row])
            tblvCell = cell
        }
        
        let bgSelected = UIView()
        bgSelected.backgroundColor = UIColor(white: 1, alpha: 0)
        tblvCell.selectedBackgroundView =  bgSelected
        
        return tblvCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if currentType == 0 {
            let labs = labsByDate[dates[indexPath.section]]!
            let key = Array(labs.keys)[indexPath.row]
            if key == "LipidPanel" {
                return CGFloat(labs[key]?.count ?? 1) * 60.0 + 10.0
            }
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentType == 1 {
            switch indexPath.row {
            case 0:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabResultsHemoglobinViewController") as! LabResultsHemoglobinViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.hemoglobinAlcs = self.hemoglobinAlcs
                self.present(vc, animated: true, completion: nil)
                break
            case 1:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabResultsLipidPanelViewController") as! LabResultsLipidPanelViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.lipidPanels = self.lipidPanels
                self.present(vc, animated: true, completion: nil)
                break
            case 2:
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "LabResultsINRViewController") as! LabResultsINRViewController
                vc.modalPresentationStyle = .overFullScreen
                vc.modalTransitionStyle = .crossDissolve
                vc.inrs = self.inrs
                self.present(vc, animated: true, completion: nil)
                break
            default:
                break
            }
        }
    }
}

class CellLabHemoglobinAlc: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    func setValue(_ title: String? = nil, value: Double) {
        if let title = title {
            lblTitle.text = title
        }
        lblValue.text = String(format: "%.1f%%", value)
    }
}

class CellLabINR: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    func setValue(_ title: String? = nil, value: Double) {
        if let title = title {
            lblTitle.text = title
        }
        lblValue.text = String(format: "%.1f", value)
    }
}

class CellLabCategory: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    func setTitle(_ title: String) {
        lblTitle.text = title
    }
}

class CellLabLipidPanel: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tblvLabs: UITableView!

    private var labs: [ShortLipidPanel] = []
    func setValue(_ labs: [ShortLipidPanel]?) {
        if let labs = labs {
            self.labs = labs
        }
        tblvLabs.reloadData()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        tblvLabs.dataSource = self
        tblvLabs.delegate = self
        tblvLabs.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tblvLabs.frame.size.width, height: 1))
        tblvLabs.separatorInset = .init(top: 0, left: 10, bottom: 0, right: 10)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return labs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellLabLipidPanelItem", for: indexPath) as! CellLabLipidPanelItem
        cell.setLab(labs[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
}

class CellLabLipidPanelItem: UITableViewCell {
    @IBOutlet weak var lblLab: UILabel!
    @IBOutlet weak var lblValue: UILabel!
    func setLab(_ lab: ShortLipidPanel) {
        lblLab.text = lab.name
        lblValue.text = String(format: "%.1f mg/dl", lab.value)
    }
}
