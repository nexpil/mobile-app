//
//  OxygenLevelsViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CareKit
import Charts
import SVProgressHUD

class OxygenLevelsViewController: UIViewController, ScanOxygenLevelsViewControllerDelegate {
    
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet var btnDuration       : [UIButton]!
    @IBOutlet weak var vwDurationBar: UIView!
    @IBOutlet weak var tblHistory   : UITableView!
    @IBOutlet weak var addButton    : FAButton!
    
    @IBOutlet weak var constraintHeightTblvHistory  : NSLayoutConstraint!
    @IBOutlet weak var viewGraph    : HealthOxygenLevelsGraphView!
    @IBOutlet var constraintsBarHorz : [NSLayoutConstraint]!
    
    var manager = DataManager.shared
    var val : [[String:String]] = []
    var currentTab : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
        addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickAdd)))
        addButton.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let notificationNameOxygen = Notification.Name.Action.UpdateOxygenLevels
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameOxygen, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateHealthData(notification:Foundation.Notification) {
        fetchOxygenLevel()
    }
    
    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirst {
            viewGraph.layoutIfNeeded()
            for btn in btnDuration {
                btn.layoutIfNeeded()
            }
            self.changedDuration(1)
            isFirst = false
        }
    }

    @IBAction func onDuration(_ sender: UIButton){
        changedDuration(sender.tag)
    }
    
    func insertOxygenLevel(date: Date, time: String, timeIndex: String, value: Int) {
        SVProgressHUD.show()
        DataManager.shared.writeSampleByOxygenLevel(value: value, date: date) { (success, error) in
            SVProgressHUD.dismiss()
            NotificationCenter.default.post(name: Notification.Name.Action.UpdateOxygenLevels, object: nil)
        }
    }

    func didTapButtonAddOxygenLevelViewController(date: Date, time: String, timeIndex: String, value: NSInteger) {
        self.insertOxygenLevel(date: date, time: time, timeIndex: timeIndex, value: value)
    }
    
    func showShadow() {
        Global_ShowFrostGlass(self.view)
    }
    
    func fetchOxygenLevel() {
        val.removeAll()
        tblHistory.reloadData()
        var afValue : [CGFloat] = []
        var xValues: [String] = []
        let unit = "%"
        
        if currentTab == 0 {
            SVProgressHUD.show()
            let arrayList = manager.fetchOxygenLevelGetAllDaysData()
            SVProgressHUD.dismiss()
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            
            for i in 0..<arrayList.count {
                let dicList = arrayList[i] as! NSDictionary
                let day = dicList["day"] as! String
                
                if day == result {
                    let arrayData = dicList["data"] as! NSArray
                    
                    for dicDate in arrayData {
                        let model = dicDate as! OxygenLevel
                        let value = Int(model.value)
                        let day = model.day!
                        let time = model.time!
                        formatter.dateFormat = "MMM"
                        let month = formatter.string(from: date)
                        
                        var data: [String: String] = [:]
                        data["date"] = "\(month) \(day) - \(time)"
                        data["value"] = "\(value) \(unit)"
                        val.append(data)
                        afValue.append(CGFloat(value))
                        xValues.append(time)
                    }
                }
            }
        } else if currentTab == 1 {
            SVProgressHUD.show()
            let arrayList = manager.fetchOxygenLevelGetAllWeekData()
            SVProgressHUD.dismiss()
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            let ymNow : String = String(result.prefix(7))
            let dNow : Int! = Int(String(result.suffix(2)))
            
            var month = String()
            let tempFormatter = DateFormatter()
            tempFormatter.dateFormat = "yyyy-MM"
            if let date = tempFormatter.date(from: ymNow) {
                tempFormatter.dateFormat = "MMMM"
                month = tempFormatter.string(from: date)
            }
            let shortMonth = month.substring(to: 3)
            
            var start : Int = Int(dNow / 7) * 7
            var end : Int = start + 6
            start = start == 0 ? 1 : start
            end = end > 30 ? 30 : end
            for i in start..<end + 1 {
                var dic : [String:String] = [:]
                dic["date"] = month + " " + String(i)
                dic["value"] = "-"
                val.append(dic)
                afValue.append(0)
                xValues.append(shortMonth + " " + String(i))
            }
            
            for i in 0..<arrayList.count {
                let dicList = arrayList[i] as! NSDictionary
                let strWeek = dicList["strWeek"] as! String
                let ymData: String = String(strWeek.prefix(7))
                let day : String = String(strWeek.suffix(4))
                let sday : Int! = Int(String(day.prefix(2)))
                let eday : Int! = Int(String(day.suffix(2)))
                
                if ymData == ymNow && sday <= dNow && dNow <= eday {
                    for data in dicList["data"] as! NSArray {
                        let dicValue = data as! NSDictionary
                        let ddate = dicValue["strDate"] as! String
                        let idx : Int! = Int(String(ddate.suffix(2)))
                        let value = dicValue["value"] as! Int
                        val[idx - sday]["value"] = "\(value) \(unit)"
                        afValue[idx - sday] = CGFloat(value)
                    }
                }
            }
        } else {
            SVProgressHUD.show()
            let arrayList = manager.fetchOxygenLevelGetAllMonthData()
            SVProgressHUD.dismiss()
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            let result = formatter.string(from: date)
            let ymNow : String = String(result.prefix(7))
            
            var month = String()
            let tempFormatter = DateFormatter()
            tempFormatter.dateFormat = "yyyy-MM"
            if let date = tempFormatter.date(from: ymNow) {
                tempFormatter.dateFormat = "MMMM"
                month = tempFormatter.string(from: date)
            }
            let shortMonth = month.substring(to: 3)
            
            var dic : [String:String] = [:]
            dic["date"] = String(format: "%@ 1-6", month)
            dic["value"] = "-"
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 1-6", shortMonth))
            dic["date"] = String(format: "%@ 7-13", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 7-13", shortMonth))
            dic["date"] = String(format: "%@ 14-20", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 14-20", shortMonth))
            dic["date"] = String(format: "%@ 21-27", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 21-27", shortMonth))
            dic["date"] = String(format: "%@ 28-30", month)
            val.append(dic)
            afValue.append(0)
            xValues.append(String(format: "%@ 28-30", shortMonth))
            
            for i in 0..<arrayList.count {
                let dicList = arrayList[i] as! NSDictionary
                let mon = dicList["month"] as! String
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM"
                let result = formatter.string(from: date)
                
                if result == mon {
                    let value0106 = dicList["avg0106"] as! NSInteger
                    let value0713 = dicList["avg0713"] as! NSInteger
                    let value1420 = dicList["avg1420"] as! NSInteger
                    let value2127 = dicList["avg2127"] as! NSInteger
                    let value2830 = dicList["avg2830"] as! NSInteger
                    
                    if value0106 > 0 {
                        let strBefore0106 = "\(value0106) \(unit)"
                        val[0]["value"] = strBefore0106
                        afValue[0] = CGFloat(value0106)
                    }
                    if value0713 > 0 {
                        let strBefore0713 = "\(value0713) \(unit)"
                        val[1]["value"] = strBefore0713
                        afValue[1] = CGFloat(value0713)
                    }
                    if value1420 > 0 {
                        let strBefore1420 = "\(value1420) \(unit)"
                        val[2]["value"] = strBefore1420
                        afValue[2] = CGFloat(value1420)
                    }
                    if value2127 > 0 {
                        let strBefore2127 = "\(value2127) \(unit)"
                        val[3]["value"] = strBefore2127
                        afValue[3] = CGFloat(value2127)
                    }
                    if value2830 > 0 {
                        let strBefore2830 = "\(value2830) \(unit)"
                        val[4]["value"] = strBefore2830
                        afValue[4] = CGFloat(value2830)
                    }
                }
            }
        }
        //-----PING-----
        DispatchQueue.main.async {
            self.updateChart(dataAfter: afValue, dataBefore: afValue, yValues: [90, 95, 100], xValues: xValues)
            self.tblHistory.reloadData()
        }
    }
    
    func changedDuration(_ idx: Int) {
        currentTab = idx
        
        for btn in btnDuration {
            btn.setTitleColor(UIColor(hex: "969696"), for: .normal)
        }
        btnDuration[idx].setTitleColor(UIColor(hex: "397EE3"), for: .normal)
        
        for constraint in constraintsBarHorz {
            constraint.isActive = false
        }
        UIView.animate(withDuration: 0.3) {
            self.constraintsBarHorz[idx].isActive = true
            self.view.layoutIfNeeded()
        }
        
        fetchOxygenLevel()
    }
    
    func updateUI() {
        constraintHeightTblvHistory.constant = CGFloat(60 * val.count)
    }
    
    func updateChart(dataAfter: [CGFloat], dataBefore: [CGFloat], yValues: [CGFloat] = [60, 100, 140, 180, 220], xValues: [String]) {
        viewGraph.setData(dataAfter: dataAfter, dataBefore: dataBefore, yValues: yValues, xValues: xValues)
        updateUI()
    }
    
    func setChartView(xVal: NSArray, yVal1: NSArray, yVal2: NSArray) {
        var allLineChartDataSets: [LineChartDataSet] = [LineChartDataSet]()
        
        var dataEntries: [ChartDataEntry] = []
        let dataPoints = xVal
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: xVal[i] as! Double, y: yVal1[i] as! Double)
            dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet1: LineChartDataSet = LineChartDataSet(values: dataEntries, label: "Before")
        
        // set dataset1
        let gradientColors = [ChartColorTemplates.colorFromString("#aecbf1").cgColor,
                              ChartColorTemplates.colorFromString("#8bb3ec").cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        lineChartDataSet1.setColor(.cyan)
        lineChartDataSet1.setCircleColor(.cyan)
        lineChartDataSet1.circleRadius = 5
        lineChartDataSet1.drawCircleHoleEnabled = false
        lineChartDataSet1.axisDependency = .left
        
        lineChartDataSet1.fillAlpha = 1
        lineChartDataSet1.fill = Fill(linearGradient: gradient, angle: 60)
        lineChartDataSet1.drawFilledEnabled = false
        lineChartDataSet1.mode = .cubicBezier
        
        allLineChartDataSets.append(lineChartDataSet1)

        var dataEntries2: [ChartDataEntry] = []
        let dataPoints2 = xVal
        
        for i in 0..<dataPoints2.count {
            let dataEntry2 = ChartDataEntry(x: xVal[i] as! Double, y: yVal2[i] as! Double)
            dataEntries2.append(dataEntry2)
        }
        
        let lineChartDataSet2 = LineChartDataSet(values: dataEntries2, label: "After")
        // set dataset1
        let gradientColors2 = [ChartColorTemplates.colorFromString("#8db5eb").cgColor,
                               ChartColorTemplates.colorFromString("#5c95e4").cgColor]
        let gradient2 = CGGradient(colorsSpace: nil, colors: gradientColors2 as CFArray, locations: nil)!
        
        lineChartDataSet2.setColor(.blue)
        lineChartDataSet2.setCircleColor(.blue)
        lineChartDataSet2.circleRadius = 5
        lineChartDataSet2.drawCircleHoleEnabled = false
        lineChartDataSet2.axisDependency = .right
        
        lineChartDataSet2.fillAlpha = 1
        lineChartDataSet2.fill = Fill(linearGradient: gradient2, angle: 60)
        lineChartDataSet2.drawFilledEnabled = false
        lineChartDataSet2.mode = .cubicBezier
                
        let noZeroFormatter = NumberFormatter()
        noZeroFormatter.zeroSymbol = ""
        lineChartDataSet1.valueFormatter = DefaultValueFormatter(formatter: noZeroFormatter)
        lineChartDataSet2.valueFormatter = DefaultValueFormatter(formatter: noZeroFormatter)
        lineChartDataSet2.drawCirclesEnabled = false
        allLineChartDataSets.append(lineChartDataSet2)
        
        let lineChartData = LineChartData(dataSets: allLineChartDataSets)
        
//        chartView.data = lineChartData
//        chartView.setYAxisMaxWidth(YAxis.AxisDependency.left, width: 0)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onClickAdd() {
        let scanOxygenLevelsViewController = (self.storyboard?.instantiateViewController(withIdentifier: "ScanOxygenLevelsViewController") as? ScanOxygenLevelsViewController)!
        scanOxygenLevelsViewController.delegate = self
        scanOxygenLevelsViewController.modalPresentationStyle = .overFullScreen
        scanOxygenLevelsViewController.modalTransitionStyle = .crossDissolve
        self.present(scanOxygenLevelsViewController, animated: true, completion: nil)
    }
}

extension OxygenLevelsViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return val.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellOxygenHistory", for: indexPath) as! CellOxygenHistory
        cell.selectionStyle = .none
        let dicData = val[indexPath.row]
        cell.setInfo(info:dicData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class CellOxygenHistory: UITableViewCell {
    @IBOutlet weak var lblDate  : UILabel!
    @IBOutlet weak var lblValue : UILabel!
    
    func setInfo(info:[String:String]) {
        lblDate.text = info["date"]
        lblValue.text = info["value"]
    }
}
