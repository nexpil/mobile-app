//
//  AddMood2VewController.swift
//  Nexpil
//
//  Created by TeamPlayer on 1/12/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AddMood2ViewController: KeyboardViewController {
    
    var feeling: Int = 0
    var note: String = ""
    
    @IBOutlet weak var viewDialog       : GradientView!
    @IBOutlet weak var noteTextField    : UITextField!
    @IBOutlet weak var constraintDialogY: NSLayoutConstraint!

    private var dialogYOffset: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        noteTextField.text = note
        
        self.setKeyboardCallbacks()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global_ShowFrostGlass(self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Global_HideFrostGlass()
    }

    private func setKeyboardCallbacks() {
        self.callbackKeyboardWillShow = { (keyboardHeight) -> () in
            let bottomOfDialog = self.viewDialog.frame.origin.y + self.viewDialog.frame.size.height
            let topOfKeyboard = UIScreen.main.bounds.height - keyboardHeight
            if bottomOfDialog > topOfKeyboard {
                self.dialogYOffset = bottomOfDialog - topOfKeyboard
                UIView.animate(withDuration: 0.5) {
                    self.constraintDialogY.constant = -self.dialogYOffset
                    self.view.layoutIfNeeded()
                }
            }
        }
        
        self.callbackKeyboardWillHide = { () -> () in
            if self.dialogYOffset > 0 {
                UIView.animate(withDuration: 0.5) {
                    self.constraintDialogY.constant = 0
                    self.view.layoutIfNeeded()
                }
            }
        }
    }

    @IBAction func addNextClick(_ sender: Any) {
        weak var pvc = self.presentingViewController
        self.dismiss(animated: true, completion: {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMood3ViewController") as! AddMood3ViewController
            vc.feeling = self.feeling
            vc.note = self.noteTextField.text!
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            pvc?.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func backToPrevModal(_ sender: UIButton) {
        weak var pvc = self.presentingViewController
        self.dismiss(animated: true, completion: {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMoodViewController") as! AddMoodViewController
            vc.feeling = self.feeling
            vc.modalPresentationStyle = .overFullScreen
            vc.modalTransitionStyle = .crossDissolve
            pvc?.present(vc, animated: true, completion: nil)
        })
    }
    
    @IBAction func closeModal(_ sender: Any) {
        if noteTextField.isFirstResponder {
            noteTextField.resignFirstResponder()
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
}
