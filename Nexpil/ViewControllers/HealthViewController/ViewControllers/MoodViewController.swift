//
//  MoodViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CareKit
import Charts
import SVProgressHUD

class MoodViewController: UIViewController {
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet weak var tblHistory   : UITableView!
    @IBOutlet weak var addButton    : FAButton!
    @IBOutlet weak var recentMoodLabel: UILabel!
    @IBOutlet weak var recentMoodIcon: UIImageView!
    @IBOutlet weak var recentMoodNote: UILabel!
    @IBOutlet weak var recentMoodView: GradientView!
    
    var moodList: [[String: Any]] = []
    let moodStatus = ["Very Sad", "Sad", "Neutral", "Happy", "Very Happy"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
        addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickAdd)))
        addButton.isUserInteractionEnabled = true
        getMoods()
        
        NotificationCenter.default.addObserver(self, selector: #selector(updateMoodList), name: Notification.Name.Action.UpdateMoodList, object: nil)
    }
    
    @objc func updateMoodList(noti: Notification) {
        self.getMoods()
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @objc func onClickAdd() {
        let addMoodViewController = (self.storyboard?.instantiateViewController(withIdentifier: "AddMoodViewController") as? AddMoodViewController)!
        addMoodViewController.modalPresentationStyle = .overFullScreen
        addMoodViewController.modalTransitionStyle = .crossDissolve
        self.present(addMoodViewController, animated: true, completion: nil)
    }
    
    func getMoods() {
        var arrayList = DataManager.shared.fetchMood()
        
        // Sort array by date
        arrayList = arrayList.sorted(by: {($0 as! Mood).date!.compare(($1 as! Mood).date! as Date) == .orderedDescending}) as NSArray

        self.recentMoodView.isHidden = arrayList.count < 1
        if arrayList.count < 1 {
            return
        }

        // Get recent mood
        let recentMood = arrayList.firstObject as! Mood
        recentMoodLabel.text = moodStatus[Int(recentMood.feeling)]
        recentMoodIcon.image = UIImage(named: moodStatus[Int(recentMood.feeling)])
        recentMoodNote.text = recentMood.notes
        
        self.moodList = []
        for (idx, item) in arrayList.enumerated() {
            if idx > 0 {
                let mood = item as! Mood
                var data: [String: Any] = [:]
                data["date"] = mood.date! as Date
                data["notes"] = mood.notes!
                data["feeling"] = Int(mood.feeling)
                self.moodList.append(data)
            }
        }
        
        tblHistory.reloadData()
    }
}

extension MoodViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return moodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellMoodHistory", for: indexPath) as! CellMoodHistory
        cell.selectionStyle = .none
        let mood = self.moodList[indexPath.row]

        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy"
        cell.labelDate.text = formatter.string(from: mood["date"] as! Date)
        cell.notes.text = mood["notes"] as? String
        
        cell.moodIcon.image = UIImage(named: self.moodStatus[mood["feeling"] as! Int])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

class CellMoodHistory: UITableViewCell {
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var notes: UILabel!
    @IBOutlet weak var moodIcon: UIImageView!
}
