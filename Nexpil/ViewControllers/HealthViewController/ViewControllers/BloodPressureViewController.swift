//
//  BloodPressureViewController.swift
//  Nexpil
//
//  Created by mac on 12/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Charts
import CareKit
import SVProgressHUD

class BloodPressureViewController: UIViewController, ScanBloodPressureViewControllerDelegate {
    @IBOutlet weak var imgBlueCircle: UIView!
    @IBOutlet var btnDuration       : [UIButton]!
    @IBOutlet weak var vwDurationBar: UIView!
    @IBOutlet weak var tblHistory   : UITableView!
    @IBOutlet weak var addButton    : FAButton!
    @IBOutlet weak var lblMostRecent: UILabel!
    
    @IBOutlet weak var constraintHeightTblvHistory  : NSLayoutConstraint!
    @IBOutlet weak var viewGraph        : HealthBloodPressureGraphView!
    @IBOutlet var constraintsBarHorz    : [NSLayoutConstraint]!

    var currentTab : Int = 0
    var manager = DataManager.shared
    var val : [[String:String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onClickAdd)))
        addButton.isUserInteractionEnabled = true
        imgBlueCircle.layer.cornerRadius = imgBlueCircle.frame.size.width / 2
        
        let strMostRecent = getBloodPressure()
        lblMostRecent.text = strMostRecent.length > 0 ? strMostRecent : "-/-"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let notificationNameBloodP = Notification.Name.Action.UpdateBloodPressure
        NotificationCenter.default.addObserver(self, selector: #selector(updateHealthData(notification:)), name: notificationNameBloodP, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func updateHealthData(notification:Foundation.Notification) {
        fetchBloodPressure()
    }
    
    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirst {
            viewGraph.layoutIfNeeded()
            for btn in btnDuration {
                btn.layoutIfNeeded()
            }
            self.changedDuration(1)
            isFirst = false
        }
    }

    @IBAction func onDuration(_ sender: UIButton){
        changedDuration(sender.tag)
    }
    
    func insertBloodPressurel(date: Date, time: String, timeIndex: String, value1: NSInteger, value2: NSInteger) {
        SVProgressHUD.show()
        DataManager.shared.writeSampleByBloodPressure(systolic: value1, diastolic: value2, date: date) { (success, error) in
            SVProgressHUD.dismiss()
            NotificationCenter.default.post(name: Notification.Name.Action.UpdateBloodPressure, object: nil)
        }
    }
    
    func didTapButtonAddScanBloodPressureViewController(date: Date, time: String, timeIndex: String, value1: NSInteger, value2: NSInteger) {
        self.insertBloodPressurel(date: date, time: time, timeIndex: timeIndex, value1: value1, value2: value2)
    }
    
    func showShadow() {
        Global_ShowFrostGlass(self.view)
    }
    
    func fetchBloodPressure() {
        DispatchQueue.main.async {
            self.val.removeAll()
            var beValue : [CGFloat] = []
            var afValue : [CGFloat] = []
            let unit = "mmHg"
            
            var xValues: [String] = []
            
            if self.currentTab == 0 {
                SVProgressHUD.show()
                let arrayList = self.manager.fetchBloodPressureGetAllDaysData()
                SVProgressHUD.dismiss()
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "MMM dd"
                var result = formatter.string(from: date)
                
                var dic : [String:String] = [:]
                dic["time"] = result
                dic["pressure"] = "-"
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append("")
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append("")
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append("")

                for i in 0..<arrayList.count {
                    let dicList = arrayList[i] as! NSDictionary
                    let day = dicList["day"] as! String
                    formatter.dateFormat = "yyyy-MM-dd"
                    result = formatter.string(from: date)
                    
                    if day == result {
                        let arrayData = dicList["data"] as! NSArray
                        
                        for dicDate in arrayData {
                            let model = dicDate as! BloodPressure
                            let timeIndex = Int(model.timeIndex!)
                            let time = model.time
                            let value1 = Int(model.value1)
                            let value2 = Int(model.value2)
                            
                            self.val[timeIndex!]["time"]! += " - " + String(time!)
                            self.val[timeIndex!]["pressure"] = String(format: "%li/%li%@", value1, value2, unit)
                            beValue[timeIndex!] = CGFloat(value1)
                            afValue[timeIndex!] = CGFloat(value2)
                            xValues[timeIndex!] = time!
                        }
                    }
                }
            } else if self.currentTab == 1 {
                SVProgressHUD.show()
                let arrayList = self.manager.fetchBloodPressureGetAllWeekData()
                SVProgressHUD.dismiss()
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let result = formatter.string(from: date)
                let ymNow : String = String(result.prefix(7))
                let dNow : Int! = Int(String(result.suffix(2)))
                
                var month = String()
                let tempFormatter = DateFormatter()
                tempFormatter.dateFormat = "yyyy-MM"
                if let date = tempFormatter.date(from: ymNow) {
                    tempFormatter.dateFormat = "MMMM"
                    month = tempFormatter.string(from: date)
                }
                
                let shortMonth = month.substring(to: 3)
                var start : Int = Int(dNow / 7) * 7
                var end : Int = start + 6
                start = start == 0 ? 1 : start
                end = end > 30 ? 30 : end
                for i in start..<end + 1 {
                    var dic : [String:String] = [:]
                    dic["time"] = month + " " + String(i)
                    dic["pressure"] = "-"
                    self.val.append(dic)
                    beValue.append(0)
                    afValue.append(0)
                    xValues.append(shortMonth + " " + String(i))
                }
                
                for i in 0..<arrayList.count {
                    let dicList = arrayList[i] as! NSDictionary
                    let strWeek = dicList["strWeek"] as! String
                    let ymData: String = String(strWeek.prefix(7))
                    let day : String = String(strWeek.suffix(4))
                    let sday : Int! = Int(String(day.prefix(2)))
                    let eday : Int! = Int(String(day.suffix(2)))
                    
                    if ymData == ymNow && sday <= dNow && dNow <= eday {
                        for data in dicList["data"] as! NSArray {
                            
                            let dicData = data as! NSDictionary
                            let strDate = dicData["strDate"] as! String
                            let numValue1 = dicData["value1"] as! Int
                            let numValue2 = dicData["value2"] as! Int
                            let idx : Int! = Int(String(strDate.suffix(2)))
                            
                            self.val[idx - sday]["pressure"] = String(format: "%li/%li%@", numValue1, numValue2, unit)
                            beValue[idx - sday] = CGFloat(numValue1)
                            afValue[idx - sday] = CGFloat(numValue2)
                        }
                    }
                }
            } else {
                SVProgressHUD.show()
                let arrayList = self.manager.fetchBloodPressureGetAllMonthData()
                SVProgressHUD.dismiss()
                
                let date = Date()
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                let result = formatter.string(from: date)
                let ymNow : String = String(result.prefix(7))
                
                var month = String()
                let tempFormatter = DateFormatter()
                tempFormatter.dateFormat = "yyyy-MM"
                if let date = tempFormatter.date(from: ymNow) {
                    tempFormatter.dateFormat = "MMMM"
                    month = tempFormatter.string(from: date)
                }
                
                let shortMonth = month.substring(to: 3)
                var dic : [String:String] = [:]
                dic["time"] = String(format: "%@ 1-6", month)
                dic["pressure"] = "-"
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append(String(format: "%@ 1-6", shortMonth))
                dic["time"] = String(format: "%@ 7-13", month)
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append(String(format: "%@ 7-13", shortMonth))
                dic["time"] = String(format: "%@ 14-20", month)
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append(String(format: "%@ 14-20", shortMonth))
                dic["time"] = String(format: "%@ 21-27", month)
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append(String(format: "%@ 21-27", shortMonth))
                dic["time"] = String(format: "%@ 28-30", month)
                self.val.append(dic)
                beValue.append(0)
                afValue.append(0)
                xValues.append(String(format: "%@ 28-30", shortMonth))
                
                for i in 0..<arrayList.count {
                    let dicList = arrayList[i] as! NSDictionary
                    let mon = dicList["month"] as! String
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM"
                    let result = formatter.string(from: date)
                    
                    if result == mon {
                        let val1_0106 = dicList["avg1_0106"] as! NSInteger
                        let val1_0713 = dicList["avg1_0713"] as! NSInteger
                        let val1_1420 = dicList["avg1_1420"] as! NSInteger
                        let val1_2127 = dicList["avg1_2127"] as! NSInteger
                        let val1_2830 = dicList["avg1_2830"] as! NSInteger
                        let val2_0106 = dicList["avg2_0106"] as! NSInteger
                        let val2_0713 = dicList["avg2_0713"] as! NSInteger
                        let val2_1420 = dicList["avg2_1420"] as! NSInteger
                        let val2_2127 = dicList["avg2_2127"] as! NSInteger
                        let val2_2830 = dicList["avg2_2830"] as! NSInteger
                        
                        if (val1_0106 != 0 && val2_0106 != 0) {
                            let lblValue0106 = String(format: "%li/%li%@", val1_0106, val2_0106, unit)
                            self.val[0]["pressure"] = lblValue0106
                            beValue[0] = CGFloat(val1_0106)
                            afValue[0] = CGFloat(val2_0106)
                        }
                        
                        if (val1_0713 != 0 && val2_0713 != 0) {
                            let lblValue0713 = String(format: "%li/%li%@", val1_0713, val2_0713, unit)
                            self.val[1]["pressure"] = lblValue0713
                            beValue[1] = CGFloat(val1_0713)
                            afValue[1] = CGFloat(val2_0713)
                        }
                        
                        if (val1_1420 != 0 && val2_1420 != 0) {
                            let lblValue1420 = String(format: "%li/%li%@", val1_1420, val2_1420, unit)
                            self.val[2]["pressure"] = lblValue1420
                            beValue[2] = CGFloat(val1_1420)
                            afValue[2] = CGFloat(val2_1420)
                        }
                        
                        if (val1_2127 != 0 && val2_2127 != 0) {
                            let lblValue2127 = String(format: "%li/%li%@", val1_2127, val2_2127, unit)
                            self.val[3]["pressure"] = lblValue2127
                            beValue[3] = CGFloat(val1_2127)
                            afValue[3] = CGFloat(val2_2127)
                        }
                        
                        if (val1_2830 != 0 && val2_2830 != 0) {
                            let lblValue2830 = String(format: "%li/%li%@", val1_2830, val2_2830, unit)
                            self.val[4]["pressure"] = lblValue2830
                            beValue[4] = CGFloat(val1_2830)
                            afValue[4] = CGFloat(val2_2830)
                        }
                    }
                }
            }
            
            self.updateChart(dataAfter: beValue, dataBefore: afValue, xValues: xValues)
            self.tblHistory.reloadData()
        }
    }
    
    func changedDuration(_ idx: Int) {
        currentTab = idx
        
        for btn in btnDuration {
            btn.setTitleColor(UIColor(hex: "969696"), for: .normal)
        }
        btnDuration[idx].setTitleColor(UIColor(hex: "847FE5"), for: .normal)
        
        for constraint in constraintsBarHorz {
            constraint.isActive = false
        }
        UIView.animate(withDuration: 0.3) {
            self.constraintsBarHorz[idx].isActive = true
            self.view.layoutIfNeeded()
        }
        
        fetchBloodPressure()
    }
    
    func updateUI() {
        constraintHeightTblvHistory.constant = CGFloat(60 * val.count)
    }

    func updateChart(dataAfter: [CGFloat], dataBefore: [CGFloat], yValues: [CGFloat] = [60, 80, 100, 120, 140, 160], xValues: [String]) {
        viewGraph.setData(dataAfter: dataAfter, dataBefore: dataBefore, yValues: yValues, xValues: xValues)
        updateUI()
    }

    func setChartView(xVal: NSArray, yVal1: NSArray, yVal2: NSArray) {
        var allLineChartDataSets: [LineChartDataSet] = [LineChartDataSet]()
        
        var dataEntries: [ChartDataEntry] = []
        let dataPoints = xVal
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(x: xVal[i] as! Double, y: yVal1[i] as! Double)
            dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet1: LineChartDataSet = LineChartDataSet(values: dataEntries, label: "Before")
        
        // set dataset1
        let gradientColors = [ChartColorTemplates.colorFromString("#aecbf1").cgColor,
                              ChartColorTemplates.colorFromString("#8bb3ec").cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        lineChartDataSet1.setColor(.cyan)
        lineChartDataSet1.setCircleColor(.cyan)
        lineChartDataSet1.circleRadius = 5
        lineChartDataSet1.drawCircleHoleEnabled = false
        lineChartDataSet1.axisDependency = .left
        
        lineChartDataSet1.fillAlpha = 1
        lineChartDataSet1.fill = Fill(linearGradient: gradient, angle: 60)
        lineChartDataSet1.drawFilledEnabled = false
        lineChartDataSet1.mode = .cubicBezier
        
        allLineChartDataSets.append(lineChartDataSet1)

        var dataEntries2: [ChartDataEntry] = []
        let dataPoints2 = xVal
        
        for i in 0..<dataPoints2.count {
            let dataEntry2 = ChartDataEntry(x: xVal[i] as! Double, y: yVal2[i] as! Double)
            dataEntries2.append(dataEntry2)
        }
        
        let lineChartDataSet2 = LineChartDataSet(values: dataEntries2, label: "After")
        // set dataset1
        let gradientColors2 = [ChartColorTemplates.colorFromString("#8db5eb").cgColor,
                               ChartColorTemplates.colorFromString("#5c95e4").cgColor]
        let gradient2 = CGGradient(colorsSpace: nil, colors: gradientColors2 as CFArray, locations: nil)!
        
        lineChartDataSet2.setColor(.blue)
        lineChartDataSet2.setCircleColor(.blue)
        lineChartDataSet2.circleRadius = 5
        lineChartDataSet2.drawCircleHoleEnabled = false
        lineChartDataSet2.axisDependency = .right
        
        lineChartDataSet2.fillAlpha = 1
        lineChartDataSet2.fill = Fill(linearGradient: gradient2, angle: 60)
        lineChartDataSet2.drawFilledEnabled = false
        lineChartDataSet2.mode = .cubicBezier
                
        let noZeroFormatter = NumberFormatter()
        noZeroFormatter.zeroSymbol = ""
        lineChartDataSet1.valueFormatter = DefaultValueFormatter(formatter: noZeroFormatter)
        lineChartDataSet2.valueFormatter = DefaultValueFormatter(formatter: noZeroFormatter)
        lineChartDataSet2.drawCirclesEnabled = false
        allLineChartDataSets.append(lineChartDataSet2)
        
        _ = LineChartData(dataSets: allLineChartDataSets)
        
//        chartView.data = lineChartData
//        chartView.setYAxisMaxWidth(YAxis.AxisDependency.left, width: 0)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func onClickAdd() {
        let scanBloodPressureViewController = (self.storyboard?.instantiateViewController(withIdentifier: "ScanBloodPressureViewController") as? ScanBloodPressureViewController)!
        scanBloodPressureViewController.delegate = self
        scanBloodPressureViewController.modalPresentationStyle = .overFullScreen
        scanBloodPressureViewController.modalTransitionStyle = .crossDissolve
        self.present(scanBloodPressureViewController, animated: true, completion: nil)
    }
}

extension BloodPressureViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return val.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellBGHistory", for: indexPath) as! CellBPHistory
        cell.selectionStyle = .none
        let dicData = val[indexPath.row]
        cell.setInfo(info:dicData)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

class CellBPHistory: UITableViewCell {
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    
    func setInfo(info:[String:String]) {
        lblTime.text = info["time"]
        lblPressure.text = info["pressure"]
    }
}
