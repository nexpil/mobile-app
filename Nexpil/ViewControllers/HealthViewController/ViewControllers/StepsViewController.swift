//
//  StepsViewController.swift
//  Nexpil
//
//  Created by mac on 12/24/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Charts
import CareKit
import SVProgressHUD
import HealthKit
import UICircularProgressRing

class StepsViewController: UIViewController {
    @IBOutlet var btnDuration       : [UIButton]!
    @IBOutlet weak var vwDurationBar: UIView!
    @IBOutlet weak var addButton    : FAButton!
    @IBOutlet weak var labelSteps   : UILabel!
    @IBOutlet weak var labelGoal    : UILabel!
    @IBOutlet weak var labelDate    : UILabel!
    @IBOutlet weak var progressRing : UICircularProgressRing!
    @IBOutlet weak var labelDistance: UILabel!
    @IBOutlet weak var labelCalorie : UILabel!
    
    var currentTab : Int = 0
    var manager = DataManager.shared
    var val : [[String:String]] = []
    
    var dateString: String = ""
    var stepCount: Int = 0
    var goal: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    private func initUI() {
        initProgressRing()
    }
    
    private func initProgressRing() {
        progressRing.style = .ontop
        progressRing.innerRingWidth = 12
        progressRing.outerRingWidth = 1.5
        progressRing.shouldShowValueText = false
        progressRing.innerRingColor = UIColor(hex: "#5A62FB")
        progressRing.outerRingColor = .lightGray
        progressRing.startAngle = -90
        progressRing.maxValue = 1.0
        progressRing.resetProgress()
    }
    
    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirst {
            for btn in btnDuration {
                btn.layoutIfNeeded()
            }
            self.changedDuration(1)
            isFirst = false
        }
    }

    @IBAction func onDuration(_ sender: UIButton){
        changedDuration(sender.tag)
    }
    
    func showShadow() {
        Global_ShowFrostGlass(self.view)
    }
    
    func changedDuration(_ idx: Int) {
        currentTab = idx
        
        for btn in btnDuration {
            btn.setTitleColor(UIColor(hex: "969696"), for: .normal)
        }
        
        btnDuration[idx].setTitleColor(UIColor(hex: "847FE5"), for: .normal)
        UIView.animate(withDuration: TimeInterval(0.2)) {
            var pos = self.vwDurationBar.frame
            pos.origin.x = self.btnDuration[idx].frame.origin.x
            pos.size.width = self.btnDuration[idx].frame.size.width
            self.vwDurationBar.frame = pos
        }
        
        self.stepCount = 0
        if idx == 0 {
            let arrayList = manager.fetchStepsGetAllDaysData()
            for item in arrayList {
                let dicData = item as! NSDictionary
                let arrayData = dicData["data"] as! NSArray
                for dicData in arrayData {
                    let model = dicData as! Steps
                    self.stepCount += Int(model.value)
                }
            }
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM dd, yyyy"
            dateString = formatter.string(from: date)
            self.goal = 10000
        } else if idx == 1 {
            let arrayList = manager.fetchStepsGetAllWeekData()
            for item in arrayList {
                let dicData = item as! NSDictionary
                let arrayData = dicData["data"] as! NSArray
                for dicData in arrayData {
                    let model = dicData as! NSDictionary
                    self.stepCount += model["value"] as! Int
                }
            }
            self.goal = 70000
            
            // Get the weekdays of the current week
            let calendar = Calendar.current
            let today = calendar.startOfDay(for: Date())
            let dayOfWeek = calendar.component(.weekday, from: today)
            let weekdays = calendar.range(of: .weekday, in: .weekOfYear, for: today)!
            let days = (weekdays.lowerBound ..< weekdays.upperBound)
                .compactMap { calendar.date(byAdding: .day, value: $0 - dayOfWeek, to: today) }  // use `flatMap` in Xcode versions before 9.3
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd"
            dateString = formatter.string(from: days[0]) + " - " + formatter.string(from: days[6])
        } else {
            let arrayList = manager.fetchStepsGetAllMonthData()
            for item in arrayList {
                let dicData = item as! NSDictionary
                var sum = 0
                sum += dicData["avg0106"] as! Int
                sum += dicData["avg0713"] as! Int
                sum += dicData["avg1420"] as! Int
                sum += dicData["avg2127"] as! Int
                sum += dicData["avg2830"] as! Int
                
                self.stepCount = sum
            }
            let formatter = DateFormatter()
            formatter.dateFormat = "MMMM yyyy"
            dateString = formatter.string(from: Date())
            self.goal = 310000
        }
        
        updateChart()
        getWalkingRunningDiatance(idx)
        getCalorieBurnt(idx)
    }
    
    @IBAction func onClickBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateChart() {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        labelDate.text = dateString
        labelSteps.text = numberFormatter.string(from: NSNumber(value: stepCount))
        labelGoal.text = numberFormatter.string(from: NSNumber(value: goal))
        let progress = Float(stepCount) / Float(goal)
        progressRing.startProgress(to: CGFloat(progress), duration: 0.5)
    }
    
    func getWalkingRunningDiatance(_ mode: Int) {
        guard let type = HKSampleType.quantityType(forIdentifier: .distanceWalkingRunning) else {
            fatalError("Something went wrong retriebing quantity type distanceWalkingRunning")
        }

        var predicate: NSPredicate?
        
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let today = calendar.startOfDay(for: Date())
        if mode == 0 {
            predicate = HKQuery.predicateForSamples(withStart: today, end: Date(), options: .strictStartDate)
        } else if mode == 1 {
            let dayOfWeek = calendar.component(.weekday, from: today)
            let startOfWeek = calendar.date(byAdding: .day, value: 1 - dayOfWeek, to: today)
            let endOfWeek = calendar.date(byAdding: .day, value: 7 - dayOfWeek, to: today)
            predicate = HKQuery.predicateForSamples(withStart: startOfWeek, end: endOfWeek, options: .strictStartDate)
        } else if mode == 2 {
            let components = calendar.dateComponents([.year, .month], from: today)
            let startOfMonth = calendar.date(from: components)
            let components2 = NSDateComponents()
            components2.month = 1
            components2.day = -1
            let endOfMonth = calendar.date(byAdding: components2 as DateComponents, to: startOfMonth!)
            predicate = HKQuery.predicateForSamples(withStart: startOfMonth, end: endOfMonth, options: .strictStartDate)
        }

        let query = HKStatisticsQuery(quantityType: type, quantitySamplePredicate: predicate, options: [.cumulativeSum]) { (query, statistics, error) in
            var value: Double = 0

            if error != nil {
                print("something went wrong")
            } else if let quantity = statistics?.sumQuantity() {
                value = quantity.doubleValue(for: HKUnit.mile())
            }
            DispatchQueue.main.async {
                self.labelDistance.text = String(format: "%.1f", value)
            }
        }
        let healthStore = HKHealthStore()
        healthStore.execute(query)
    }
    
    func getCalorieBurnt(_ mode: Int) {
        guard let quantityType = HKQuantityType.quantityType(forIdentifier: .activeEnergyBurned) else {
          fatalError("Something went wrong retriebing quantity type distanceWalkingRunning")
        }
        
        var predicate: NSPredicate?
        
        let calendar = Calendar.current
        let today = calendar.startOfDay(for: Date())
        if mode == 0 {
            predicate = HKQuery.predicateForSamples(withStart: today, end: Date(), options: .strictStartDate)
        } else if mode == 1 {
            let dayOfWeek = calendar.component(.weekday, from: today)
            let startOfWeek = calendar.date(byAdding: .day, value: 1 - dayOfWeek, to: today)
            let endOfWeek = calendar.date(byAdding: .day, value: 7 - dayOfWeek, to: today)
            predicate = HKQuery.predicateForSamples(withStart: startOfWeek, end: endOfWeek, options: .strictStartDate)
        } else if mode == 2 {
            let components = calendar.dateComponents([.year, .month], from: today)
            let startOfMonth = calendar.date(from: components)
            let components2 = NSDateComponents()
            components2.month = 1
            components2.day = -1
            let endOfMonth = calendar.date(byAdding: components2 as DateComponents, to: startOfMonth!)
            predicate = HKQuery.predicateForSamples(withStart: startOfMonth, end: endOfMonth, options: .strictStartDate)
        }
        
        let query = HKStatisticsQuery(quantityType: quantityType, quantitySamplePredicate: predicate, options: [.cumulativeSum]) { (query, statistics, error) in
            var value: Double = 0

            if error != nil {
                print("something went wrong")
            } else if let quantity = statistics?.sumQuantity() {
                value = quantity.doubleValue(for: HKUnit.kilocalorie())
            }
            DispatchQueue.main.async {
                self.labelCalorie.text = String(format: "%.1f", value)
            }
        }
        let healthStore = HKHealthStore()
        healthStore.execute(query)
    }
}
