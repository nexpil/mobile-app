//
//  HealthkitModule.swift
//  Nexpil
//
//  Created by mac on 8/27/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

struct HealthkitModule: Codable {
    let id: Int
    let name: String
    let status: Int
    let last_updated: String
}
