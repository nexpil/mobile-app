//
//  OxygenLevelData.swift
//  Nexpil
//
//  Created by mac on 8/27/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

struct OxygenLevelData: Codable {
    let id: Int
    let user_id: Int
    let oxygen_value: Int
    let date_posted: String
}
