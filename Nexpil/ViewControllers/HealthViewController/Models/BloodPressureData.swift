//
//  BloodPressureData.swift
//  Nexpil
//
//  Created by mac on 8/27/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

struct BloodPressureData: Codable {
    let id: Int
    let user_id: Int
    let systolic: Int
    let diastolic: Int
    let date_posted: String
}
