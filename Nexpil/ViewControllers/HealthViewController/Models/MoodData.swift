//
//  MoodData.swift
//  Nexpil
//
//  Created by mac on 8/27/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

struct MoodData: Codable {
    let id: Int
    let user_id: Int
    let feeling_type: Int
    let mood_note: String
    let note_id: Int
    let date_posted: String
}
