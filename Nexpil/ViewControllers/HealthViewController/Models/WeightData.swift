//
//  OxygenLevelData.swift
//  Nexpil
//
//  Created by mac on 8/27/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

struct WeightData: Codable {
    let id: Int
    let user_id: Int
    let weight: Double
    let date_posted: String
}
