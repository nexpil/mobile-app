//
//  AddHealthDataViewController.swift
//  Nexpil
//
//  Created by Ajai Nair on 4/9/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import HealthKitUI
import HealthKit
import SVProgressHUD

protocol AddHealthDataViewControllerDelegate: class {
    func didSelectHealthDataType(index: Int)
}

class AddHealthDataViewController: UIViewController {

    @IBOutlet var tbl_healthData: UITableView!
    
    var delegate: AddHealthDataViewControllerDelegate?
    var _healthData: NSMutableArray!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let item1 = ["name":"Blood Glucose", "img":"Group 3625"]
        let item2 = ["name":"Blood Pressure", "img":"Heart"]
        let item3 = ["name": "Mood", "img": "Happy"]
        let item4 = ["name":"Weight", "img":"Union 93"]
        let item5 = ["name":"Oxygen Level", "img":"Group 3177"]
        let item6 = ["name":"Steps", "img":"Group 3636"]
        let item7 = ["name":"Labs", "img":"Group 3177"]
        
        let moduleIds = UserDefaults.standard.array(forKey: "selected_health") as? [Int] ?? []
        
        _healthData = NSMutableArray.init()
        _healthData.add(item1)
        _healthData.add(item2)
        _healthData.add(item3)
        _healthData.add(item4)
        _healthData.add(item5)
        if !moduleIds.contains(5) {
            _healthData.add(item6)
        }
        if !moduleIds.contains(6) {
            _healthData.add(item7)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //-----PING-----
        Global_ShowFrostGlass(self.view)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //-----PING-----
        Global_HideFrostGlass()
    }
    
    @IBAction func OnClose(){
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddHealthDataViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _healthData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddHealthDataCell", for: indexPath) as? AddHealthDataCell
        //-----PING-----
        let item = _healthData.object(at: indexPath.row) as? [String: String]
        cell?.name.text = item?["name"]
        cell?.icon.image = UIImage(named: item?["img"] ?? "")
        cell?.selectionStyle = UITableViewCellSelectionStyle.none
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 74
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var ary:[Int] = []
        let tmp = UserDefaults.standard.array(forKey: "selected_health")
        //-----PING-----
        let item = _healthData.object(at: indexPath.row) as? [String: String]
        let name = item?["name"]
        
        if let arry = tmp as? [Int] {
            ary = arry
            if name == "Labs" {
                ary.append(6)
            } else if !ary.contains(indexPath.row){
                ary.append(indexPath.row)
            }
        } else{
            if name == "Labs" {
                ary.append(6)
            } else {
                ary.append(indexPath.row)
            }
        }
        
        UserDefaults.standard.set(ary, forKey: "selected_health")
        
        if indexPath.row == 2 {//mood

            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    self.delegate?.didSelectHealthDataType(index: indexPath.row)
                }
            }
        } else {
    
            var types = Set<HKSampleType>()
            
            switch indexPath.row {
                case 0:
                    types = Set([(HKObjectType.quantityType(forIdentifier: .bloodGlucose)!)])
                case 1:
                    types = Set([
                        (HKObjectType.quantityType(forIdentifier: .bloodPressureDiastolic)!),
                        (HKObjectType.quantityType(forIdentifier: .bloodPressureSystolic)!)
                    ])
                case 3:
                    types = Set([(HKObjectType.quantityType(forIdentifier: .bodyMass)!)])
                case 4:
                    types = Set([(HKObjectType.quantityType(forIdentifier: .oxygenSaturation)!)])
                default:
                    break
            }
            
            if name == "Steps" {
                types = Set([
                    (HKObjectType.quantityType(forIdentifier: .stepCount)!),
                    (HKObjectType.quantityType(forIdentifier: .distanceWalkingRunning)!),
                    (HKObjectType.quantityType(forIdentifier: .activeEnergyBurned)!)
                ])
            } else if name == "Labs" {
                types = Set([
                    HKObjectType.clinicalType(forIdentifier: .labResultRecord)!
                ])
            }
            
            if HKHealthStore.isHealthDataAvailable() {
                var typesToWrite: Set<HKSampleType> = []
                if name != "Labs" {
                    typesToWrite = types
                }
                
                HKHealthStore().requestAuthorization(toShare: typesToWrite, read: types) { (success, error) in
                    if success {
                        DispatchQueue.main.async {
                            self.dismiss(animated: true) {
                                self.delegate?.didSelectHealthDataType(index: indexPath.row)
                            }
                        }
                    }
                }
            }
        }
    }
}
