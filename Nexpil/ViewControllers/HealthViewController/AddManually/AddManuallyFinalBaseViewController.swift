//
//  AddManuallyFinalBaseViewController.swift
//  Nexpil
//
//  Created by Shine on 2/15/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AddManuallyFinalBaseViewController: UIViewController {

    var callbackSuccess: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapView = UITapGestureRecognizer(target: self, action: #selector(tapOnView))
        self.view.addGestureRecognizer(tapView)
    }

    @objc func tapOnView(_ sender: UITapGestureRecognizer) {
        self.dismiss(animated: false) {
            self.callbackSuccess?()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        Global_ShowFrostGlass(self.view)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        Global_HideFrostGlass()
    }
}
