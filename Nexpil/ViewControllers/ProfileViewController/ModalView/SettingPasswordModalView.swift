//
//  SettingPasswordModalView.swift
//  Nexpil
//
//  Created by JinYingZhe on 1/30/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol SettingPasswordModalViewDelegate {
    func popSettingPasswordModalViewDismissal()
    func popSettingPasswordSaveView(CurrentPassword: String, newPassword: String, ConfirmPassword: String)
}

class SettingPasswordModalView: UIView {
    @IBOutlet weak var backUV: UIView!
    @IBOutlet weak var backUB: UIButton!
    @IBOutlet weak var saveUB: UIButton!
    
    @IBOutlet weak var ConfirmPassword: UITextField!
    @IBOutlet weak var newPasswordTF: UITextField!
    @IBOutlet weak var currentPasswordTF: UITextField!
    
    var delegate: SettingPasswordModalViewDelegate?
    @IBOutlet weak var constraintCenter: NSLayoutConstraint!
    
    private var isCurrentPasswordVisible = false {
        didSet {
            currentPasswordTF.isSecureTextEntry = !isCurrentPasswordVisible
        }
    }
    private var isNewPasswordVisible = false {
        didSet {
            newPasswordTF.isSecureTextEntry = !isNewPasswordVisible
        }
    }
    private var isConfirmPasswordVisible = false {
        didSet {
            ConfirmPassword.isSecureTextEntry = !isConfirmPasswordVisible
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //  Initialation code
        backUV.setPopItemViewStyle(radius: 30.0, title: .large)
        backUB.setPopItemViewStyle(radius: 22.5)
    }
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBAction func onClickBackUB(_ sender: Any) {
        self.delegate?.popSettingPasswordModalViewDismissal()
    }
    
    @IBAction func onClickSaveUB(_ sender: Any) {
        if let CurrentPassword = currentPasswordTF.text, let newPassword = newPasswordTF.text,let confirmPassword = ConfirmPassword.text{
            self.delegate?.popSettingPasswordSaveView(CurrentPassword: CurrentPassword, newPassword: newPassword, ConfirmPassword: confirmPassword)
        }
    }

    @IBAction func onBtnPasswordShow(_ sender: UIButton) {
        if sender.tag == 100 {
            isCurrentPasswordVisible = !isCurrentPasswordVisible
            sender.setTitle(isCurrentPasswordVisible ? "HIDE" : "SHOW", for: .normal)
        } else if sender.tag == 200 {
            isNewPasswordVisible = !isNewPasswordVisible
            sender.setTitle(isNewPasswordVisible ? "HIDE" : "SHOW", for: .normal)
        } else {
            isConfirmPasswordVisible = !isConfirmPasswordVisible
            sender.setTitle(isConfirmPasswordVisible ? "HIDE" : "SHOW", for: .normal)
        }
    }
}

extension SettingPasswordModalView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == ConfirmPassword {
            self.constraintCenter.constant = -70
            UIView.animate(withDuration: 0.3) {
                self.layoutIfNeeded()
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == ConfirmPassword {
            self.constraintCenter.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.layoutIfNeeded()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return string != " "
    }
}
