//
//  ProfileMedicationsViewController.swift
//  Nexpil
//
//  Created by Nexpil Admin on 11/6/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Alamofire

class ProfileMedicationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MedicationInfoDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var medicationList: [[String: Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableViewAutomaticDimension
        
        getMedicationList()
    }

    @IBAction func closeDialog(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickAddMedications(_ sender: Any) { // Goto Add medications dialog
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddDrugNavigationVC") as! UINavigationController
        viewController.modalPresentationStyle = .overFullScreen
        viewController.modalTransitionStyle = .crossDissolve
        present(viewController, animated: true, completion: nil)
    }
    
    private func getMedicationList() {
        let userId = PreferenceHelper().getId()
         let params = [
             "userid": userId,
             "choice": "15"
             ] as [String : Any]
        
         Alamofire.request(DataUtils.APIURL + DataUtils.MYDRUG_URL, method: .post, parameters: params)
             .responseJSON(completionHandler: {response in
                 
             if let data = response.result.value {
                //-----PING-----
                if let json = data as? [String: Any] {
                    self.medicationList = json["data"] as! [[String: Any]]
                } else {
                    DataUtils.messageShow(view: self, message: "Server Error", title: "")
                }
                self.tableView.reloadData()
            }
        })
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medicationList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMedicationsTableViewCell", for: indexPath) as? ProfileMedicationsTableViewCell
        cell?.medicationName.text = self.medicationList[indexPath.row]["medicationname"] as? String

        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell?.selectedBackgroundView = backgroundView
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 64
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let drugInfo = self.medicationList[indexPath.row]
        let drugId = drugInfo["id"] as! String
        let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "MedicationInfoMainViewController") as! MedicationInfoMainViewController
        viewController.id = Int(drugId)
        viewController.delegate = self
        viewController.modalPresentationStyle = .overFullScreen
        viewController.modalTransitionStyle = .crossDissolve
        
        self.present(viewController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        Global_ShowFrostGlass(self.view)
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame

        self.view.insertSubview(blurEffectView, at: 0)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        Global_HideFrostGlass()
    }
    
    func closeMedicationInfoDialog() {
        self.dismiss(animated: true, completion: nil)
    }
}

class ProfileMedicationsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var medicationName: UILabel!
    
}
