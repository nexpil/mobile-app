//
//  KeyboardViewController.swift
//  Nexpil
//
//  Created by Shine on 2/6/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class KeyboardViewController: UIViewController {

    var callbackKeyboardWillShow: ((CGFloat) -> ())?
    var callbackKeyboardWillHide: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: Notification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            self.callbackKeyboardWillShow?(keyboardHeight)
        }
    }

    @objc func keyboardWillHide(_ notification: Notification) {
        self.callbackKeyboardWillHide?()
    }
    
    public func addDoneButtonOnKeyboard(forTextView textView: UITextView) {
           let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
           doneToolbar.barStyle       = UIBarStyle.default
           let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
           let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: textView, action: #selector(UIResponder.resignFirstResponder))
           
           var items = [UIBarButtonItem]()
           items.append(flexSpace)
           items.append(done)
           doneToolbar.items = items
           doneToolbar.sizeToFit()
           textView.inputAccessoryView = doneToolbar
       }

       public func addDoneButtonOnKeyboard(forTextField textField: UITextField) {
           let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
           doneToolbar.barStyle       = UIBarStyle.default
           let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
           let done: UIBarButtonItem  = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: textField, action: #selector(UIResponder.resignFirstResponder))
           
           var items = [UIBarButtonItem]()
           items.append(flexSpace)
           items.append(done)
           doneToolbar.items = items
           doneToolbar.sizeToFit()
           textField.inputAccessoryView = doneToolbar
    }
}

