//
//  AddMedicationSuccessViewController.swift
//  Nexpil
//
//  Created by mac on 12/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SVProgressHUD

class AddMedicationSuccessViewController: UIViewController {

    var callback: (() -> ())?
    @IBOutlet weak var modalView: GradientView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        modalView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoNextScreen)))
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { (_) in
            self.gotoNextScreen()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        Global_ShowFrostGlass(self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        Global_HideFrostGlass()
    }
    
    @objc func gotoNextScreen() {
        self.dismiss(animated: true) {
            self.callback?()
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
