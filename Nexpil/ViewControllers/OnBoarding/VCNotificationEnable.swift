//
//  VCNotificationEnable.swift
//  Nexpil
//
//  Created by mac on 7/2/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import UserNotifications
import Alamofire

enum NotificationPresentedFrom {
    case CommunityOnboardingSignupViewController
    case SignUpViewController
    case SignUpViewControllerWithCode
    case HomeTabViewController
}

class VCNotificationEnable: UIViewController, UNUserNotificationCenterDelegate {

    public var presentedFrom: NotificationPresentedFrom!
    public var m_communityUser: CommunityUser!
    @IBOutlet weak var btnAllow: NPButton!
    @IBOutlet weak var btnBack: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if presentedFrom != NotificationPresentedFrom.CommunityOnboardingSignupViewController {
            btnAllow.setValue(0, forKey: "colorScheme")
        }
        
        if presentedFrom == NotificationPresentedFrom.SignUpViewController || presentedFrom == NotificationPresentedFrom.SignUpViewControllerWithCode {
            btnBack.isHidden = true
        }
    }
    
    private func gotoSignupScreen() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let signupVC = storyBoard.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.navigationController?.pushViewController(signupVC, animated: true)
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func onEnable() {
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound, .badge]) { (auth, error) in
            if auth {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                    
                    let snoozeAction = UNNotificationAction(
                      identifier: "snooze", title: "Snooze",
                      options: [.foreground])
                    
                    // 2
                    let snoozeCategory = UNNotificationCategory(
                      identifier: "snoozeCategory", actions: [snoozeAction],
                      intentIdentifiers: [], options: [])

                    // 3
                    UNUserNotificationCenter.current().setNotificationCategories([snoozeCategory])
                }
            }
            
            if UserDefaults.standard.value(forKey: "id") != nil {
                if self.presentedFrom == NotificationPresentedFrom.CommunityOnboardingSignupViewController {
                    
                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainviewcontroller") as! UITabBarController
                    let vc_community = viewController.viewControllers![2] as! CommunityMainViewController
                    vc_community.showCongPopup = true
                    vc_community.m_communityUser = self.m_communityUser
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = viewController
                    
                } else if self.presentedFrom == NotificationPresentedFrom.HomeTabViewController  {
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainviewcontroller") as! UITabBarController
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = viewController
                        viewController.modalPresentationStyle = .overFullScreen
                        self.present(viewController, animated: false, completion: nil)
                    }
                }
            } else {
                if self.presentedFrom == NotificationPresentedFrom.SignUpViewControllerWithCode {
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func SetupPushNOtification(application: UIApplication) -> () {
        UNUserNotificationCenter.current().delegate = self
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge], completionHandler: {(granted, error) in
            print("granted: \(granted)")
            if granted {
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            } else {
                print("User Notification permission denied: \(error?.localizedDescription ?? "error")")
            }
        })
    }
    
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        completionHandler([.alert, .sound])
//    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        completionHandler()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        print("VCNotificationEnable.swift deviceToken: \(tokenString(deviceToken))")
        UserDefaults.standard.set(tokenString(deviceToken), forKey: "DeviceToken")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
    }
    
    func tokenString(_ deviceToken: Data) -> String {
        let bytes = [UInt8](deviceToken)
        var token = ""
        for byte in bytes {
            token += String(format: "%02x", byte)
        }
        print("tokenString  \(token)")
        return token
    }
}
