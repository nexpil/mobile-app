//
//  OnboardingTwoViewController.swift
//  Nexpil
//
//  Created by Cagri Sahan on 9/25/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Lottie

class OnboardingTwoViewController: UIViewController {
            
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: NPPageControl!
    @IBOutlet var animationView: AnimationView?
    
    public var m_fromWelcome = true
    override func viewDidLoad() {
        super.viewDidLoad()
        let attributedString = NSMutableAttributedString(string: labelText.text!)

        let paragraphStyle = NSMutableParagraphStyle()
        
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = .center
        attributedString.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        labelText.attributedText = attributedString
        self.initializeAnimation()
    }
    
    func initializeAnimation() {
        let animation = Animation.named("scanAnimation")
        animationView!.animation = animation
        animationView!.contentMode = .scaleAspectFit
        animationView!.loopMode = .loop
//        self.startAnimation()
//        animationView?.pause()
    }
    
    func startAnimation() {
        if !self.animationView!.isAnimationPlaying {
            self.animationView?.play()
        }
    }
    
    func stopAnimation() {
        self.animationView?.stop()
    }
    
    @objc func closeWindow() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let datas = DBManager.getObject().getMedications()
        if datas.count > 0 {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let medicationController = storyBoard.instantiateViewController(withIdentifier: "AddMedicationListViewController") as! AddMedicationListViewController
            self.navigationController?.pushViewController(medicationController, animated: true)
        } else {
            if m_fromWelcome == false {
                self.navigationController?.popViewController(animated: false)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
       // self.navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillDisappear(animated)
        m_fromWelcome = false
    }
    @IBAction func gotoFirstMedicationAddViewController(_ sender: Any) {
        if pageControl.currentPage == 1 {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FirstMedicationAddViewController") as! FirstMedicationAddViewController
            let navController = UINavigationController(rootViewController: viewController)
            navController.navigationBar.isHidden = true
            navController.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(navController, animated: false, completion: nil)
        } else {
            self.scrollView.setContentOffset(CGPoint(x: UIScreen.main.bounds.size.width, y: 0), animated: true)
//            let pageIndex = Int(round(scrollView.contentOffset.x/scrollView.frame.width))
//            pageControl.currentPage = pageIndex
        }
    }
    @IBAction func backButton(_ sender: UIButton) {        
        self.navigationController?.popViewController(animated: false)
    }
    
}

extension OnboardingTwoViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = Int(round(scrollView.contentOffset.x/scrollView.frame.width))
        pageControl.currentPage = pageIndex
        if pageIndex == 1 {
            self.startAnimation()
            self.btnNext.setTitle("Start Scan", for: UIControlState.normal)
        } else {
            self.btnNext.setTitle("Next Step", for: UIControlState.normal)
        }
    }
}
