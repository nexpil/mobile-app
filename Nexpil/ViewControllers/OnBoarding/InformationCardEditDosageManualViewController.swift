//
//  InformationCardEditDosageManualViewController.swift
//  Nexpil
//
//  Created by Teamplayer on 2/25/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class InformationCardEditDosageManualViewController: InformationCardEditViewController {

    @IBOutlet weak var txtfStrength : InformationCardEditable!
    @IBOutlet weak var doselabel    : UILabel!
    @IBOutlet weak var viewNav :UIView!
    
    @IBOutlet weak var doneButtonBottomConstraint: NSLayoutConstraint!
    
    private var dosage: String = ""
    
    var drugUnit: String = ""
    var isFromScanning = false
    
    override func viewDidLoad() {
        self.doneButtonConstraint = doneButtonBottomConstraint
        super.viewDidLoad()
        
        txtfStrength.textView.keyboardType = .numbersAndPunctuation
        txtfStrength.textView.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        if isFromScanning {
            self.viewNav.isHidden = false
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let name = DataUtils.getMedicationName()! + "?"
        doselabel.text = "What's the strength of your " + name
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if let text = textField.text, !text.contains(self.drugUnit) {
            textField.text = "\(text.trimmingCharacters(in: CharacterSet(charactersIn: "0123456789.").inverted))\(self.drugUnit)"
        }
    }
    
    @IBAction func doneButtonTapped(_ sender: Any) {
        dosage = txtfStrength.textView.text ?? ""
        DataUtils.setMedicationStrength(name: dosage)
        
        if dosage.isEmpty {
            DataUtils.messageShow(view: self, message: "Please enter strength", title: "")
            return
        }
        DispatchQueue.main.async { [unowned self] in
            if self.isFromScanning {
                DataUtils.setMedicationStrength(name: self.dosage)
                self.navigationController?.popViewController(animated: true)
            } else {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let medicationController = storyBoard.instantiateViewController(withIdentifier: "SiriMedicationViewController") as!     SiriMedicationViewController
                medicationController.delegate = self.delegate
                self.navigationController?.pushViewController(medicationController, animated: true)
            }
        }
    }

    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func crossButton(_ sender: UIButton) {
        self.view.addSubview(visualEffectView!)
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CloseAddMedicationViewController") as! CloseAddMedicationViewController
        viewController.delegate = self
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
    }
}
