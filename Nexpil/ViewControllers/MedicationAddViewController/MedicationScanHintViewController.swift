//
//  MedicationScanHintViewController.swift
//  Nexpil
//
//  Created by Arif on 5/27/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class MedicationScanHintViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()        
        runAfter(0.5) {
            UIView.animate(withDuration: 0.5) {
                self.view.alpha = 1.0
            }
        }
    }
    
    @IBAction func actionClose(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0
        }) { (finished) in
            self.dismiss(animated: true)
        }
    }
}
