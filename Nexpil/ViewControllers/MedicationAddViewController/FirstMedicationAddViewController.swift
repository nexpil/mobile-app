//
//  CameraViewController.swift
//  Vision
//
//  Created by Cagri Sahan on 6/14/18.
//  Copyright © 2018 Cagri Sahan. All rights reserved.
//

import UIKit
import AVKit
import MLKit
import Alamofire

class FirstMedicationAddViewController: UIViewController, ShadowDelegate {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var addManuallyButton: UIButton!
    @IBOutlet weak var btnScan: UIButton!
    @IBOutlet weak var btnManually: NPButton!
    
    @IBOutlet weak var progressView: VideoProgressView!
    @IBOutlet weak var viewRatioPreview: UIView!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var stackButtons: UIStackView!
    
    var lastSampleTaken = Date()
    var timer: Timer!
    let session = AVCaptureSession()
    
    let dataOutput = AVCaptureVideoDataOutput()
    
    var visualEffectView:VisualEffectView?
        
    static var directionMode = 0
    
    var isRefillPills = false
    var refillPillsDelegate: RefillPillsDelegate?
    var medicineData: RefillMedicationShortData!
    var results = ScanMedicationResponse()
    var isScanStarted = false
    var texts = [Text]()
    var resultText = ""
    
    func removeShadow() {
        visualEffectView?.removeFromSuperview()
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let medicationController = storyBoard.instantiateViewController(withIdentifier: "InformationCardMedicationSelectViewController") as! InformationCardMedicationSelectViewController
        self.navigationController?.pushViewController(medicationController, animated: true)
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        if timer != nil && self.timer.isValid {
            self.timer.invalidate()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addManually(_ sender: Any) {
        if self.isRefillPills {
            self.dismiss(animated: true) {
                self.refillPillsDelegate?.onManually(medicineData: self.medicineData)
            }
        } else {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if FirstMedicationAddViewController.directionMode == 1 {
                let medicationController = storyBoard.instantiateViewController(withIdentifier: "SiriMedicationViewController") as! SiriMedicationViewController
                medicationController.delegate = self
                self.navigationController?.pushViewController(medicationController, animated: true)
            } else {
                DataUtils.setMedicationName(name: "")
                DataUtils.setMedicationStrength(name: "")
                DataUtils.setMedicationFrequency(name: "")

                let medicationController = storyBoard.instantiateViewController(withIdentifier: "InformationCardMedicationSelectViewController") as! InformationCardMedicationSelectViewController
                medicationController.delegate = self
                self.navigationController?.pushViewController(medicationController, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        closeButton.setGradient(colors: [#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor, #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1).cgColor], angle: 134.0)
        closeButton.roundCorners(.allCorners, radius: closeButton.frame.width / 2)
        closeButton.addShadow(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), alpha: 0.16, x: 0, y: 5.0, blur: 15.0)
        
        guard let inputDevice = AVCaptureDevice.default(for: .video) else {
//            self.showLoader()
//            self.sendScanmedicationRequest(forVideo: "https://api-media-files.s3.amazonaws.com/input%2FFE3A99B5-D20B-4AEF-BB29-EEC1BD7EEB2C.mp4")
            return
        }
        guard let input = try? AVCaptureDeviceInput(device: inputDevice) else { fatalError("Can't get camera input") }

        session.addInput(input)

        let preview = AVCaptureVideoPreviewLayer(session: session)
        preview.frame = UIScreen.main.bounds
                
        preview.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        view.layer.insertSublayer(preview, at: 0)

        session.addOutput(dataOutput)
        session.sessionPreset = AVCaptureSession.Preset.photo

        let sampleBufferQueue = DispatchQueue(label: "sampleBufferQueue")

        dataOutput.alwaysDiscardsLateVideoFrames = true
        dataOutput.setSampleBufferDelegate(self, queue: sampleBufferQueue)
        dataOutput.videoSettings = [
          (kCVPixelBufferPixelFormatTypeKey as String): kCVPixelFormatType_32BGRA,
        ]
        self.session.startRunning()
        
        self.viewRatioPreview.isHidden = false
        self.btnScan.isHidden = false
        self.view.bringSubview(toFront: self.viewRatioPreview)
        self.view.bringSubview(toFront: self.stackButtons)
        self.view.bringSubview(toFront: self.closeButton)
        
        visualEffectView = self.view.backgroundBlur(view: self.view)
    }

    func currentVideoOrientation() -> AVCaptureVideoOrientation {
        var orientation: AVCaptureVideoOrientation
        switch UIDevice.current.orientation {
        case .portrait:
            orientation = AVCaptureVideoOrientation.portrait
        case .landscapeRight:
            orientation = AVCaptureVideoOrientation.landscapeLeft
        case .portraitUpsideDown:
            orientation = AVCaptureVideoOrientation.portraitUpsideDown
        default:
            orientation = AVCaptureVideoOrientation.landscapeRight
        }
        return orientation
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if DataUtils.getCameraStatus() == true
        {
            self.dismiss(animated: true, completion: nil)
            DataUtils.setCameraStatus(name: false)
            return
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "summarySegue" {
            timer.invalidate()
            URLSession.shared.finishTasksAndInvalidate()
            let vc = segue.destination as! SummaryScreenViewController
        }
    }
    
    func tryAgain() {
        let hintVC = MedicationScanHintViewController(nibName: "MedicationScanHintViewController", bundle: nil)
        hintVC.modalPresentationStyle = .overCurrentContext
        self.present(hintVC, animated: false)
    }
    
    @IBAction func actionStartScan(_ sender: UIButton) {
        if !isScanStarted {
            self.texts = []
            sender.setTitle("Scanning", for: .normal)
            self.isScanStarted = true
            
            self.progressView.progress = 0.0
            timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { (timerr) in
                UIView.animate(withDuration: 0.1) {
                    self.progressView.progress = self.progressView.progress + 0.023                    
                }
            })
            
            runAfter(4) {
                if self.timer != nil && self.timer.isValid {
                    self.progressView.progress = 1
                    self.timer.invalidate()
                }
                self.isScanStarted = false
                sender.setTitle("Start Scan", for: .normal)
                if self.texts.count > 0 && self.texts.count % 2 != 0 {
                    self.texts.remove(at: 0)
                }
                self.lblInfo.text = "Processing..."
                DataUtils.customActivityIndicatory(self.view, startAnimate: true)
                runAfter(0.5) {
                    self.getScanResult()
                }
            }
        }
    }
    
    func getScanResult() {
        var doctorName = ""
        var lines = [[String]]()
        for index in 0..<self.texts.count {
            if index % 2 == 0 {
                let firstText = self.texts[index]
                let nextText = self.texts[index + 1]
                var lines1 = [String]()
                var lines2 = [String]()
                for block in firstText.blocks {
                    for line in block.lines {
                        if doctorName == "" && line.text.lowercased().hasSuffix("md") {
                            doctorName.append(line.text)
                        }
                        if !(line.text.contains("Substituted") || line.text.contains("Substtuted")){
                            lines1.append(line.text)
                        }
                    }
                }
                for block in nextText.blocks {
                    for line in block.lines {
                        if doctorName == "" && line.text.lowercased().hasSuffix("md"){
                            doctorName.append(line.text)
                        }
                        if !(line.text.contains("Substituted") || line.text.contains("Substtuted")){
                            lines2.append(line.text)
                        }
                    }
                }
                lines1 = lines1.removingDuplicates()
                lines2 = lines2.removingDuplicates()
                var combined = [String]()
                for line1 in lines1 {
                    for line2 in lines2 {
                        let commonString = self.longestCommonSubstringDP(of: line1, and: line2)
                        if commonString != "" && commonString.count > 3 {
                            guard let index1 = line1.index(of: commonString, options: String.CompareOptions.caseInsensitive) else { break }
                            guard let index2 = line2.index(of: commonString, options: String.CompareOptions.caseInsensitive) else { break }
                            let string1 = line1[..<index1]
                            let string2 = line2[index2...]
                            let combinedString = String(string1) + String(string2)
                            combined.append(combinedString)
                            break
                        }
                    }
                }
                combined = combined.removingDuplicates()
                lines.append(combined)
            }
            
        }
        let newLines1 = lines[0]
        let newLines2 = lines[1]
        var finalLines = [String]()
        for line1 in newLines1 {
            for line2 in newLines2 {
                let commonString = self.longestCommonSubstringDP(of: line1, and: line2)
                if commonString != "" && commonString.count > 3 {
                    guard let index1 = line1.index(of: commonString, options: String.CompareOptions.caseInsensitive) else { break }
                    guard let index2 = line2.index(of: commonString, options: String.CompareOptions.caseInsensitive) else { break }
                    let string1 = line1[..<index1]
                    let string2 = line2[index2...]
                    let combinedString = String(string1) + String(string2)
                    finalLines.append(combinedString)
                    break
                }
            }
        }
        
        if lines.count == 3 {
            let finalLines1 = finalLines
            let finalLines2 = lines[2]
            var lines = [String]()
            for line1 in finalLines1 {
                for line2 in finalLines2 {
                    let commonString = self.longestCommonSubstringDP(of: line1, and: line2)
                    if commonString != "" && commonString.count > 3 {
                        guard let index1 = line1.index(of: commonString, options: String.CompareOptions.caseInsensitive) else { break }
                        guard let index2 = line2.index(of: commonString, options: String.CompareOptions.caseInsensitive) else { break }
                        let string1 = line1[..<index1]
                        let string2 = line2[index2...]
                        lines.append(String(string1) + String(string2))
                        break
                    }
                 }
                if !lines.contains(line1) {
                    lines.append(line1)
                }
            }
            finalLines = lines
        }
        
        
        var finalResults = finalLines.removingDuplicates()
        self.lblInfo.text = "Rotate the bottle slowly"
        if doctorName != "" {
            finalResults.append(doctorName)
        }
        
        if finalResults.count > 1 && finalResults.first!.count > 4 {
            let finalString = finalResults.joined(separator: "\n")
            self.scarpeScanResultsFromAWS(finalString)
        } else {
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            self.tryAgain()
        }
    }
    
    public func longestCommonSubstringDP(of left: String, and right: String) -> String {
        let leftLength = left.count
        let rightLength = right.count
        
        var m: [[Int]] = Array(repeating: Array(repeating: 0, count: rightLength + 1), count: leftLength + 1)
        var lcsLeftEnd = 0
        var lcsLength = 0
        
        for i in 1..<(leftLength + 1) {
            for j in 1..<(rightLength + 1) {
                let leftChar = left[left.index(left.startIndex, offsetBy: i-1)]
                let rightChar = right[right.index(right.startIndex, offsetBy: j-1)]
                
                if leftChar.lowercased() == rightChar.lowercased() {
                    m[i][j] = 1 + m[i-1][j-1]
                    
                    if m[i][j] > lcsLength {
                        lcsLength = m[i][j]
                        lcsLeftEnd = i
                    }
                } else {
                    m[i][j] = 0
                }
            }
        }
        
        let lcsLeftStartIndex = left.index(left.startIndex, offsetBy: lcsLeftEnd - lcsLength)
        let lcsLeftEndIndex = left.index(left.startIndex, offsetBy: lcsLeftEnd)
        return String(left[lcsLeftStartIndex..<lcsLeftEndIndex])        
    }
    
    func scarpeScanResultsFromAWS(_ stringToScrape: String) {
        let headers = [ "Content-Type":"application/json; charset=utf-8" ]
        let body: [String : Any] = [ "label_text": stringToScrape]
        Alamofire.request("https://ttjhz8fwrl.execute-api.us-east-2.amazonaws.com/v1/nextpill_comp_api", method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers).responseScanMedicationResponse { (response) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if (response.result.error == nil) {
                if let scanMedicationResponse = response.result.value {
//                    print(scanMedicationResponseElement)
                    let filteredResults = scanMedicationResponse.filter({ medicationResponse in
                        return (medicationResponse.type?.lowercased() == "name" || medicationResponse.type?.lowercased() == "brand_name" || medicationResponse.type?.lowercased() == "generic_name") && (medicationResponse.text != nil && medicationResponse.text != "")
                    })
                    if filteredResults.count == 0 {
                        self.tryAgain()
                    } else {
                        self.resultText = stringToScrape
                        self.results = filteredResults
                        AudioServicesPlaySystemSound(1007);
                        AudioServicesPlaySystemSound(4095);
                        runAfter(0) {
                            let doneAnimationVC = DoneScanningViewController(nibName: "DoneScanningViewController", bundle: nil)
                            doneAnimationVC.delegate = self
                            doneAnimationVC.modalPresentationStyle = .overCurrentContext
                            self.present(doneAnimationVC, animated: false)
                        }
                    }
                } else {
                    DataUtils.messageShow(view: self, message: response.result.description, title: "")
                }
            } else {
                DataUtils.messageShow(view: self, message: response.result.error!.localizedDescription, title: "")
            }
        }
    }
}


extension FirstMedicationAddViewController : AVCaptureVideoDataOutputSampleBufferDelegate {
    
    func imageOrientation( deviceOrientation: UIDeviceOrientation, cameraPosition: AVCaptureDevice.Position) -> UIImage.Orientation {
      switch deviceOrientation {
        case .portrait:
            return cameraPosition == .front ? .leftMirrored : .right
        case .landscapeLeft:
            return cameraPosition == .front ? .downMirrored : .up
        case .portraitUpsideDown:
            return cameraPosition == .front ? .rightMirrored : .left
        case .landscapeRight:
            return cameraPosition == .front ? .upMirrored : .down
        case .faceDown, .faceUp, .unknown:
            return .up
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if lastSampleTaken.timeIntervalSinceNow < -0.75 && isScanStarted {
            guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
                 print("Failed to get image buffer from sample buffer.")
                 return
            }
            
            let visionImage = VisionImage(buffer: sampleBuffer)
            visionImage.orientation = imageOrientation(deviceOrientation: UIDevice.current.orientation, cameraPosition: AVCaptureDevice.Position.back)
            let imageWidth = CGFloat(CVPixelBufferGetWidth(imageBuffer))
            let imageHeight = CGFloat(CVPixelBufferGetHeight(imageBuffer))
            lastSampleTaken = Date()
            recognizeTextOnDevice(in: visionImage, width: imageWidth, height: imageHeight)
        }
    }
    
    private func recognizeTextOnDevice(in image: VisionImage, width: CGFloat, height: CGFloat) {
        var recognizedText: Text
        do {
            recognizedText = try TextRecognizer.textRecognizer().results(in: image)
            DispatchQueue.main.sync {
                self.texts.append(recognizedText)
//                print(recognizedText.text)
//                print("-----------------")
            }
        } catch let error {
            print("Failed to recognize text with error: \(error.localizedDescription).")
        }
    }
}

extension FirstMedicationAddViewController: DoneAnimationCompleteDelegate {
    func animationCompleted() {
        let scanMedicationVC = ScanMedicationDetailViewController(nibName: "ScanMedicationDetailViewController", bundle: nil)
        scanMedicationVC.results = self.results
        scanMedicationVC.resultText = self.resultText
        self.navigationController?.pushViewController(scanMedicationVC, animated: true)
    }
}
