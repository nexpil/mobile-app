//
//  DoneScanningViewController.swift
//  Nexpil
//
//  Created by Arif on 6/18/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
protocol DoneAnimationCompleteDelegate {
    func animationCompleted()
}
class DoneScanningViewController: UIViewController {

    @IBOutlet weak var lblDone: UILabel!
    @IBOutlet weak var viewAnimation: GradientView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var width: NSLayoutConstraint!
    
    var delegate: DoneAnimationCompleteDelegate!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        runAfter(0.1) {
            UIView.animate(withDuration: 0.4) {
                self.view.alpha = 1
            }
            
            runAfter(1) {
                self.animateContent()
            }
        }
        
    }
    
    func animateContent() {
        self.viewAnimation.isHidden = false
        self.width.constant = UIScreen.main.bounds.size.width + 100
        self.height.constant = self.width.constant
        UIView.animate(withDuration: 0.5) {
            self.lblDone.font = UIFont(name: "Montserrat-Medium", size: 30)//.systemFont(ofSize: 25)
            self.viewAnimation.cornerRadius = (UIScreen.main.bounds.size.width / 2) + 50
            self.view.layoutIfNeeded()
        }
            
        runAfter(0.35) {
            self.height.constant = UIScreen.main.bounds.size.height + 250
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
        
        runAfter(1.5) {
            if let delegate = self.delegate {
                delegate.animationCompleted()
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.alpha = 0.0
                }) { (isFinished) in
                    self.dismiss(animated: true)
                }
            }
        }
    }
}
