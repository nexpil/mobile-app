//
//  ScanMedicationDetailViewController.swift
//  Nexpil
//
//  Created by Arif on 5/24/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import DWAnimatedLabel

class ScanMedicationDetailViewController: UIViewController {

    @IBOutlet weak var viewInfoLoaderContainer: UIView!
    @IBOutlet var lblInformation: [DWAnimatedLabel]!
    @IBOutlet var infoAnimatableWidth: [NSLayoutConstraint]!
    
    
    @IBOutlet weak var fullNameCard: AnimatableInfoCardView!
    @IBOutlet weak var pharmacyCard: AnimatableInfoCardView!
    @IBOutlet weak var medicationCard: AnimatableInfoCardView!
    @IBOutlet weak var strengthCard: AnimatableInfoCardView!
    @IBOutlet weak var doctorCard: AnimatableInfoCardView!
    @IBOutlet weak var quantityCard: AnimatableInfoCardView!
    @IBOutlet weak var directionsCard: AnimatableInfoCardView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblError: DWAnimatedLabel!
    var results = ScanMedicationResponse()
    var valueLoaded = false
    var pillType = ""
    var resultText = ""
    @IBOutlet weak var constraintsShowError: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fullNameCard.addShadow(color: UIColor.darkGray.cgColor, alpha: 0.5, x: 0, y: 0, blur: 5)
        self.pharmacyCard.addShadow(color: UIColor.darkGray.cgColor, alpha: 0.5, x: 0, y: 0, blur: 5)
        self.medicationCard.addShadow(color: UIColor.darkGray.cgColor, alpha: 0.5, x: 0, y: 0, blur: 5)
        self.strengthCard.addShadow(color: UIColor.darkGray.cgColor, alpha: 0.5, x: 0, y: 0, blur: 5)
        self.doctorCard.addShadow(color: UIColor.darkGray.cgColor, alpha: 0.5, x: 0, y: 0, blur: 5)
        self.quantityCard.addShadow(color: UIColor.darkGray.cgColor, alpha: 0.5, x: 0, y: 0, blur: 5)
        self.directionsCard.addShadow(color: UIColor.darkGray.cgColor, alpha: 0.5, x: 0, y: 0, blur: 5)
        
        print("============")
        print(resultText)
        print("============")
        self.setdata()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if valueLoaded {
            self.fullNameCard.setValueText(DataUtils.getPatientFullName())
            self.pharmacyCard.setValueText(DataUtils.getPharmacyName())
            self.medicationCard.setValueText(DataUtils.getMedicationName() ?? "")
            self.strengthCard.setValueText(DataUtils.getMedicationStrength() ?? "")
            self.doctorCard.setValueText(DataUtils.getDoctorName())
            self.quantityCard.setValueText(DataUtils.getQuantity())
            self.directionsCard.setValueText(DataUtils.getMedicationFrequency() ?? "")
            self.checkAndHideError()
        }
    }
    
    func checkAndHideError() {
        if self.fullNameCard.valueText != "" && self.pharmacyCard.valueText != "" && self.medicationCard.valueText != "" && self.strengthCard.valueText != "" && self.doctorCard.valueText != "" && self.quantityCard.valueText != "" && self.directionsCard.valueText != "" && !self.lblError.isHidden{
            self.btnDone.isEnabled = true
            self.lblError.isHidden = true
        }
    }
    
    func setdata() {
        let name = patientName()
        let mecdicName = medicationName()
        let quantity = findDrugQuantity(in: self.resultText)
        let doctorName = doctorsName()
        let direction = getDirection()
        
        DataUtils.setPatientFullName(patientfullname: name ==  "                    " ? "" : name)
        DataUtils.setMedicationName(name: mecdicName ==  "                    " ? "" : mecdicName)
        DataUtils.setPharmacyName(pharmacy: "")
        DataUtils.setDoctorName(name: doctorName ==  "                    " ? "" : doctorName)
        DataUtils.setQuantity(quantity: quantity ==  "                    " ? "" : quantity)
        DataUtils.setMedicationFrequency(name: direction ==  "                    " ? "" : direction)
                
        self.fullNameCard.animateAndSetValueText(name, unhideAfter: 1.2, runAfterTime: 0.3)
        self.pharmacyCard.animateAndSetValueText("                    ", unhideAfter: 1.2, runAfterTime: 0.3)
        self.medicationCard.animateAndSetValueText(mecdicName, unhideAfter: 1.2, runAfterTime: 0.3)
        self.setStrength()
        self.doctorCard.animateAndSetValueText(doctorName, unhideAfter: 1.2, runAfterTime: 0.3)
        self.quantityCard.animateAndSetValueText(quantity, unhideAfter: 1.2, runAfterTime: 0.3)
        self.directionsCard.animateAndSetValueText(direction, unhideAfter: 1.2, runAfterTime: 0.3)
        runAfter(0.3) {
            self.animateInfoLabel()
        }
        runAfter(3) {
            self.lblError.isHidden = false
            self.constraintsShowError.constant = 60
            UIView.animate(withDuration: 0.45) {
                self.view.layoutIfNeeded()
            }
        }
        runAfter(4) {
            self.btnDone.alpha = 1
            self.valueLoaded = true
        }
    }
    
    func patientName() -> String {
        let names = self.results.filter( { return $0.type == "NAME" } ).sorted { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
        if names.count > 0 {
            return names[0].text!
        }
        return "                    "
    }
    
    func doctorsName() -> String {
        let names = self.resultText.components(separatedBy: "\n")
        if names.last!.lowercased().hasSuffix("md") {
            return names.last!.replacingOccurrences(of: "MD", with: "").replacingOccurrences(of: "md", with: "")
        }
        return "                    "
    }
    
    func medicationName() -> String {
        let generic_names = self.results.filter( { return $0.type == "GENERIC_NAME" } ).sorted { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
        if generic_names.count > 0 {
            return generic_names[0].text!
        } else {
            let brand_names = self.results.filter( { return $0.type == "BRAND_NAME" } ).sorted { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
            if brand_names.count > 0 {
                return brand_names[0].text!
            }
        }
        return "                    "
    }
    
    func setStrength() {
        let generic_names = self.results.filter( { return $0.type == "GENERIC_NAME" } ).sorted { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
        let brand_names = self.results.filter( { return $0.type == "BRAND_NAME" } ).sorted { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
        let names = generic_names + brand_names
        if names.count > 0 {
            for name in names {
                for attr in name.attributes ?? [] {
                    if attr.type == "STRENGTH" || attr.text!.lowercased().contains("mg") || attr.text!.lowercased().contains("ml") || attr.text!.lowercased().contains("mcg") {
                        DataUtils.setMedicationStrength(name: attr.text!)
                        self.strengthCard.animateAndSetValueText(attr.text!, unhideAfter: 1.2, runAfterTime: 0.3)
                        return
                    }
                }
            }
        }
        self.strengthCard.animateAndSetValueText("                    ", unhideAfter: 1.2, runAfterTime: 0.3)
    }
    
    func getDirection() -> String {
        let generic_names = self.results.filter( { return $0.type == "GENERIC_NAME" && ($0.attributes?.count ?? 0) > 0 } ).sorted { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
        let brand_names = self.results.filter( { return $0.type == "BRAND_NAME" && ($0.attributes?.count ?? 0) > 0 } ).sorted { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
        let names = generic_names + brand_names
        var attributes = [ScanMedicationResponseElement]()
        for name in names {
            for attribute in (name.attributes ?? []) {
                if (attribute.type == "DOSAGE" && !attribute.text!.lowercased().contains("mg") && !attribute.text!.lowercased().contains("ml") && !attribute.text!.lowercased().contains("mcg") && !attribute.text!.lowercased().contains("mca")) || attribute.type == "ROUTE_OR_MODE" || attribute.type == "FREQUENCY" {
                    attributes.append(attribute)
                }
            }
        }
        attributes.sort { ($0.score ?? 0.0) > ($1.score ?? 0.0)}
        var dosage = ""
        var route = ""
        var frequency = ""
        for attribute in attributes {
            if dosage == "" && attribute.type == "DOSAGE" && (attribute.score ?? 0.0) > 0.5  {
                dosage = attribute.text!
            } else if attribute.type == "ROUTE_OR_MODE" && route == "" {
                route = attribute.text!
            } else if attribute.type == "FREQUENCY" && frequency == "" {
                frequency = attribute.text!
            }
        }
        var directions = [String]()
        if dosage != "" {
            directions.append("Take " + dosage)
        }
        if route != "" {
            directions.append(route)
        }
        if frequency != "" {
            directions.append(frequency)
        }
        
        return directions.count > 0 ? directions.joined(separator: ", ") : "                    "
    }
    
    func animateInfoLabel() {
        for i in 0...2 {
            self.lblInformation[i].sizeToFit()            
            self.lblInformation[i].animationType = .fade
            self.infoAnimatableWidth[i].priority = UILayoutPriority(rawValue: 995)
        }

        UIView.animate(withDuration: 1, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.view.layoutIfNeeded()
        }) { finished in
            runAfter(0.2) {
                self.viewInfoLoaderContainer.isHidden = true
                for infoLabel in self.lblInformation! {
                    infoLabel.startAnimation(duration: 1) {}
                }
            }
        }
    }
    
    @IBAction func actionDone(_ sender: UIButton) {
       var datas: [MyMedication] = []
       let currentDateTime = Date()
       let formatter = DateFormatter()
       var currentDate1 = ""
       formatter.timeZone = TimeZone.current
       let locale = NSLocale.current
       formatter.dateFormat = "yyyy-MM-dd"
       let currentDate = formatter.string(from: currentDateTime)
       let formatter1 : String = DateFormatter.dateFormat(fromTemplate: "j", options:0, locale:locale)!
       if formatter1.contains("a") {           
           //phone is set to 12 hours
           formatter.dateFormat = "h:mm a"
           let time1 = formatter.string(from: currentDateTime)
           formatter.locale = Locale(identifier: "en_US_POSIX")
           let date = formatter.date(from: time1)
           formatter.dateFormat = "HH:mm"
           currentDate1 = formatter.string(from: date!)
       } else {
           //phone is set to 24 hours
           formatter.dateFormat = "HH:mm"
           currentDate1 = formatter.string(from: currentDateTime)
       }

       let hour = currentDate1.components(separatedBy: ":")[0]
       let min = currentDate1.components(separatedBy: ":")[1]
       var min1 = Int(min)!/5
       min1 = min1 * 5
        var asNeeded = 1
        if self.directionsCard.valueText.contains("as needed") {
            asNeeded = 1
        } else {
            asNeeded = 0
        }
//        let date = self.results.filter { $0}
        let frequency = self.getFrequency()
        let data = MyMedication.init(prescribe: self.doctorCard.valueText, directions: directionsCard.valueText, dose: "", image: "", quantity: Int(self.quantityCard.valueText) ?? 0, type: "", taketime: "", medicationname: medicationCard.valueText, filedDate: "", warning: "", frequency: frequency, strength: self.strengthCard.valueText, pharmacy: self.pharmacyCard.valueText, patientname: self.fullNameCard.valueText, lefttablet: "", prescription: 0, createat: currentDate + " \(hour):\(min1)", endat:"", id:0, amount: "", asneeded: asNeeded)
       datas.append(data)
       validateText(directionsCard.valueText,datas:datas)
    }
    
    func validateText(_ text : String, datas:[MyMedication]) {
        //addScheduling(ForText:directionsCard.valueText)
        addScheduling(datas: datas)
        DBManager.getObject().insetMedicationHistoryData1(datas: datas)
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let medicationController = storyBoard.instantiateViewController(withIdentifier: "AddMedicationListViewController") as! AddMedicationListViewController
        self.navigationController?.pushViewController(medicationController, animated: true)
    }
    
    private func addScheduling(datas: [MyMedication]) {
        for medication in datas {
            for strTiming in medication.taketime.split(separator: ",") {
                let strTiming1 = strTiming.trimmingCharacters(in: .whitespaces)
                if let hour = Int(strTiming1.substring(to: 2)), let min = Int(strTiming1.substring(from: 2)) {
                    let date  = createDate(hour: hour, minute: min)
                    let id = "\(medication.medicationname)\(strTiming1)"
                    scheduleNotification(at: date, body: "Time To Take Your Medicine \(medication.medicationname)", titles: "Reminder",id: id)
                    scheduleNotification(at: date.addingTimeInterval(-5*60), body: "5 Mins To Take Your Medicine \(medication.medicationname)", titles: "Reminder", id: id)
                }
            }
        }
    }
    
    func findDrugQuantity(in text: String) -> String {
        let string = text
        let regex = try! NSRegularExpression(pattern: "QTY:? ?([0-9]+)", options: [.caseInsensitive])
        let matches = regex.matches(in: string, options: [], range: NSRange(location: 0, length: string.count))
        if let match = matches.first {
            let group = match.range(at: 1)
            let range = Range(group, in: string)!
            let drugQuantity = Int(string[range].replacingOccurrences(of: ":", with: "").replacingOccurrences(of: "QTY", with: ""))
            return drugQuantity != nil ? String(drugQuantity!) : "                    "
        }
        return "                    "
    }
    
    func createDate(hour: Int, minute: Int)->Date{
        var components = DateComponents()
        components.hour = hour
        components.minute = minute
        components.timeZone = .current
        
        let calendar = Calendar(identifier: .gregorian)
        return calendar.date(from: components)!
    }
    
    //Schedule Notification with weekly bases.
    func scheduleNotification(at date: Date, body: String, titles:String,id:String) {
        let triggerWeekly = Calendar.current.dateComponents([.hour,.minute,.second,], from: date)
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggerWeekly, repeats: true)
        let content = UNMutableNotificationContent()
        content.title = titles
        content.body = body
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = "alarm"
        content.userInfo = ["id":id]
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        
        //UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
    }
    
    func getFrequency() -> String {
        let regex = try! NSRegularExpression(pattern: "((once|twice)|((one|two|three|four|five|[1-9]) times?)) (daily|(a day))", options: [.caseInsensitive])
        let stringToMatch = directionsCard.valueText
        let matches = regex.matches(in: stringToMatch, options: [], range: NSRange(location: 0, length: stringToMatch.count)).map {
            String(stringToMatch[Range($0.range, in: stringToMatch)!])
        }
        return (matches.first ?? "").capitalized
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionEnterInfo(_ sender: UITapGestureRecognizer) {
        if self.valueLoaded {
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if sender.view?.tag == 1 {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "InformationCardEditNameViewController") as! InformationCardEditNameViewController
                viewController.isFromScanning = true
                self.navigationController?.pushViewController(viewController, animated: true)
            } else if sender.view?.tag == 2 {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "InformationCardEditPharmacyViewController") as! InformationCardEditPharmacyViewController
                viewController.isFromScanning = true
                self.navigationController?.pushViewController(viewController, animated: true)
            } else if sender.view?.tag == 3 {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "InformationCardMedicationSelectViewController") as! InformationCardMedicationSelectViewController
                viewController.isFromScanning = true
                self.navigationController?.pushViewController(viewController, animated: true)
            } else if sender.view?.tag == 4 {
                if DataUtils.getMedicationName() == nil || DataUtils.getMedicationName() == "" {
                } else {
                    let viewController = storyBoard.instantiateViewController(withIdentifier: "InformationCardEditDosageManualViewController") as! InformationCardEditDosageManualViewController
                    viewController.isFromScanning = true
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
            } else if sender.view?.tag == 5 {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "InformationCardEditDoctorViewController") as! InformationCardEditDoctorViewController
                viewController.isFromScanning = true
                self.navigationController?.pushViewController(viewController, animated: true)
            } else if sender.view?.tag == 6 {
                let viewController = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "RefillPillsManuallyDoneViewController") as! RefillPillsManuallyDoneViewController
                viewController.modalPresentationStyle = .overFullScreen
                viewController.modalTransitionStyle = .crossDissolve
                viewController.isFromScanning = true
                viewController.manualDelegate = self
                self.present(viewController, animated: true, completion: nil)
            } else if sender.view?.tag == 7 {
                let viewController = storyBoard.instantiateViewController(withIdentifier: "SiriMedicationViewController") as! SiriMedicationViewController
                viewController.isFromScanning = true
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
        }
    }
}

extension ScanMedicationDetailViewController: ManualPillsQuantityDelegate {
    func onDone(_ quantity: Int) {
        DataUtils.setQuantity(quantity: String(quantity))
        self.quantityCard.setValueText(String(quantity))
        self.checkAndHideError()
    }
}
