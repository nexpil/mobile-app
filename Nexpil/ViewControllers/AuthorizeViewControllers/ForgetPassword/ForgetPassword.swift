//
//  ForgetPassword.swift
//  Nexpil
//
//  Created by ankit vaish on 10/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ForgetPassword: UIViewController {
    
    @IBOutlet weak var submitButton: NPButton!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    public var delegate: LoginScreenViewController!
    
    private var isPasswordVisible = false
    private var isConfirmPasswordVisible = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onClose(){
        delegate.m_forgetPasswordProc = 0
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func submitButtonClicked(_ sender: Any) {
        delegate.m_forgetPasswordProc = 3
        self.dismiss(animated: false, completion: {
            self.delegate.ProcessForgetPassword()
        })
    }
                    
    @IBAction func onBtnPasswordShow(_ sender: UIButton) {
        if sender.tag == 100 {
            isPasswordVisible = !isPasswordVisible
            sender.setTitle(isPasswordVisible ? "HIDE" : "SHOW", for: .normal)
        } else {
            isConfirmPasswordVisible = !isConfirmPasswordVisible
            sender.setTitle(isConfirmPasswordVisible ? "HIDE" : "SHOW", for: .normal)
        }
        setPasswordText()
    }
    
    private func setPasswordText() {
        newPassword.isSecureTextEntry = !isPasswordVisible
        confirmPassword.isSecureTextEntry = !isConfirmPasswordVisible
    }
}
