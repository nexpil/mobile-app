//
//  PhoneNumberViewController.swift
//  Nexpil
//
//  Created by mac on 6/29/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class PhoneNumberViewController: UIViewController {

    @IBOutlet weak var txtPhoneNumber: InformationCardEditable!
    @IBOutlet weak var bottomButtonConstant: NSLayoutConstraint!
    let center = NotificationCenter.default
    var originalHeight: CGFloat?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let backImage = UIImage(named: "Back")
        let logoImage = UIImage(named: "nexpil logo - alternate")
        self.navigationItem.titleView = UIImageView(image: logoImage)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: backImage, style: .plain, target: self, action: #selector(self.closeWindow))
        UINavigationBar.appearance().barTintColor = UIColor.init(hex: "#FCFCFC")
        UINavigationBar.appearance().tintColor = UIColor.init(hex: "#FCFCFC")
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.init(hex: "#FCFCFC")]
        UINavigationBar.appearance().isTranslucent = true
        UINavigationBar.appearance().layer.borderColor = UIColor.init(hex: "#FCFCFC").cgColor
        UINavigationBar.appearance().layer.borderWidth = 7.0
        self.navigationController?.navigationBar.setValue(true, forKey: "hidesShadow")
        originalHeight = bottomButtonConstant.constant
        txtPhoneNumber.textView.becomeFirstResponder()
        txtPhoneNumber.textView.delegate = self
    }
    
    @objc func closeWindow() {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if DataUtils.getPhoneNumber() !=  nil && DataUtils.getPhoneNumber() != "" {
            txtPhoneNumber.textView.text = DataUtils.getPhoneNumber()
        }
        txtPhoneNumber.textView.keyboardType = .numberPad
        center.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        txtPhoneNumber.textView.resignFirstResponder()
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        center.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        center.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(_ notification: Notification)
    {
        let keyboardHeight = (notification.userInfo?["UIKeyboardBoundsUserInfoKey"] as! CGRect).height
        bottomButtonConstant.constant = self.originalHeight! + keyboardHeight
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(_ notification:Notification) {
        bottomButtonConstant?.constant = self.originalHeight!
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }

    private func isValidPhoneNo(phoneNo: String?) -> Bool {
        return !(self.txtPhoneNumber.textView.text!.count < 10)
//        guard let phoneNo = phoneNo else {
//            return false
//        }
//        let charset  = CharacterSet(charactersIn: "+()0123456789").inverted
//        let filtered = (phoneNo.components(separatedBy: charset)).joined(separator: "")
//        return phoneNo == filtered
    }
    
    @IBAction func gotoNext(_ sender: Any) {
        if !isValidPhoneNo(phoneNo: txtPhoneNumber.textView.text) {
            DataUtils.messageShow(view: self, message: "Phone number should be of 10 digits", title: "")
            return
        }
                
        DataUtils.setPhoneNumber(phone: txtPhoneNumber.textView.text!)
        if let signUpVC = self.navigationController?.viewControllers.first(where: { (viewController) -> Bool in
            return viewController is SignUpViewController
        }) {
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
            transition.type = kCATransitionPush
            transition.subtype = kCATransitionFromRight
            self.navigationController?.view.layer.add(transition, forKey: nil)
            self.navigationController?.popToViewController(signUpVC, animated: false)
        } else {
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension PhoneNumberViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= 10
    }
}
