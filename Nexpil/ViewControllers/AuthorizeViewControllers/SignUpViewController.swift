//
//  SignUpViewController.swift
//  Nexpil
//
//  Created by Admin on 4/8/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SkyFloatingLabelTextField

class SignUpViewController: InformationCardEditViewController {

    var USER_CODE = ""
    var passwordShow = true
    var currentTextField = 0
    var confirmpasswordShow = true
    var patient: Patient?
    
    @IBOutlet weak var backBtn: GradientView!
    @IBOutlet weak var signupBtn: GradientView!
    @IBOutlet weak var fullName: InformationCard!
    @IBOutlet weak var password: InformationCard!
    @IBOutlet weak var phoneNum: InformationCard!
    @IBOutlet weak var emailAddress: InformationCard!
    @IBOutlet weak var btnShowPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()        
        let logoImage = UIImage(named: "nexpil logo - alternate")
        self.navigationItem.titleView = UIImageView(image: logoImage)
        self.navigationItem.rightBarButtonItem = nil
        
        if USER_CODE != "" {
            DataUtils.customActivityIndicatory(self.view, startAnimate: true)
            self.getUserDetailsFromCode()
            self.enableNotification()
        }
    }
    
    func getUserDetailsFromCode() {
        let router = Router.getPatientDetails(USER_CODE)
        ApiManager.request(router: router) { (response: AuthResponse) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if let patient = response.userinfo {
                self.patient = response.userinfo!
                print(patient)
                let names = [patient.firstName ?? "", patient.lastName ?? ""]
                self.fullName.setText(names.joined(separator: " "), withTextColor: UIColor.init(hex: "333333"))
                self.emailAddress.setText(patient.email ?? "", withTextColor: UIColor.init(hex: "333333"))
                
                if let phoneNumber = patient.phoneNumber, phoneNumber != "" {
                    self.phoneNum.setText(phoneNumber.applyPatternOnNumbers(pattern: "(###)###-####", replacmentCharacter: "#"), withTextColor: UIColor.init(hex: "333333"))
                }
                
                DataUtils.setPatientFullName(patientfullname: names.joined(separator: " "))
                DataUtils.setEmail(email: patient.email ?? "")
                DataUtils.setPhoneNumber(phone: patient.phoneNumber ?? "")
            }
        } failure: { (error) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
        }
    }
    
    func enableNotification() {
        SVProgressHUD.show()
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: { (settings) in
            SVProgressHUD.dismiss()

            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if settings.authorizationStatus == .notDetermined {
                // goto notification permission screen
                DispatchQueue.main.async {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "VCNotificationEnable") as! VCNotificationEnable
                    vc.presentedFrom = NotificationPresentedFrom.SignUpViewControllerWithCode
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                if let token = UserDefaults.standard.string(forKey: "DeviceToken") {
                    print("TOKEN + " + token)
                }
            }
        })
    }
        
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        fullName.value!.text = DataUtils.getPatientFullName()
        emailAddress.value!.text = DataUtils.getEmail()
        password.value!.text = DataUtils.getPassword()
        if let phoneNumber = UserDefaults.standard.string(forKey: "phone_number"), phoneNumber != "" {
            phoneNum.value!.text = phoneNumber.applyPatternOnNumbers(pattern: "(###)###-####", replacmentCharacter: "#")
        }
        
        let commonWidth: CGFloat = UIScreen.main.bounds.size.width - 20
        fullName.width = commonWidth
        emailAddress.width = commonWidth
        password.width = commonWidth
        phoneNum.width = commonWidth
                
        if fullName.value.text == "" {
            fullName.value.text = "Full Name"
            fullName.value.textColor = UIColor.init(hex: "333333", alpha: 0.5)
            fullName.identifier.text = ""
            fullName.identifier.isHidden = true
        } else {
            fullName.value.textColor = UIColor.init(hex: "333333")
            fullName.identifier.isHidden = false
        }
        
        if emailAddress.value.text == "" {
            emailAddress.value.text = "Email Address"
            emailAddress.value.textColor = UIColor.init(hex: "333333", alpha: 0.5)
            emailAddress.identifier.text = ""
            emailAddress.identifier.isHidden = true
        } else {
            emailAddress.value.textColor = UIColor.init(hex: "333333")
            emailAddress.identifier.isHidden = false
        }
        
        if password.value.text == "" {
            password.value.text = "Password"
            password.value.textColor = UIColor.init(hex: "333333", alpha: 0.5)
            password.identifier.text = ""
            password.identifier.isHidden = true
            btnShowPassword.isHidden = true
        } else {
            password.value.textColor = UIColor.init(hex: "333333")
            password.identifier.isHidden = false
            btnShowPassword.isHidden = false
            
            passwordText = password.value.text
            setPasswordText()
        }
        
        if phoneNum.value.text == "" {
            phoneNum.value.text = "Phone Number"
            phoneNum.value.textColor = UIColor.init(hex: "333333", alpha: 0.5)
            phoneNum.identifier.text = ""
            phoneNum.identifier.isHidden = true
        } else {
            phoneNum.value.textColor = UIColor.init(hex: "333333")
            phoneNum.identifier.isHidden = false
        }
    }
    
    private var isPasswordVisible = false
    private var passwordText: String!
    private func setPasswordText() {
        if isPasswordVisible {
            password.value.text = passwordText
        } else {
            password.value.text = String(repeating: "*", count: passwordText.length)
        }
    }
    
    @IBAction func onBtnPasswordShow(_ sender: UIButton) {
        isPasswordVisible = !isPasswordVisible
        sender.setTitle(isPasswordVisible ? "HIDE" : "SHOW", for: .normal)
        setPasswordText()
    }
    
    @IBAction func userSignup(_ sender: Any) {
        if DataUtils.getPatientFullName() == "" {
            DataUtils.messageShow(view: self, message: "Please input Full Name", title: "")
            return
        }
        if DataUtils.getEmail() == "" {
            DataUtils.messageShow(view: self, message: "Please input email address", title: "")
            return
        }
        if DataUtils.isValidEmailAddress(emailAddressString: DataUtils.getEmail()!) == false {
            DataUtils.messageShow(view: self, message: "Please input valid email", title: "")
            return
        }
        if DataUtils.getPassword() == "" {
            DataUtils.messageShow(view: self, message: "Please input password", title: "")
            return
        }
        if UserDefaults.standard.string(forKey: "phone_number") == "" {
            DataUtils.messageShow(view: self, message: "Please input phone number", title: "")
            return
        }
        
        if USER_CODE != "" {
            self.updatePasswordAndLogin()
        } else {
            gotoSignupAndMedication()
        }
    }
    
    func updatePasswordAndLogin() {
        let localTimeZoneName = TimeZone.current.identifier
        let deviceToken = UserDefaults.standard.value(forKey: "DeviceToken") as? String ?? ""
        let patientName = DataUtils.getPatientFullName().components(separatedBy: " ")
        let firstName = patientName.first!
        var lastName = ""
        
        if patientName.count > 1 {
            lastName = patientName[1]
        }
        
        let authRequest = AuthRequest(email: emailAddress.value.text!, password: passwordText!, firstName: firstName, lastName: lastName, timezone: localTimeZoneName, deviceToken: deviceToken, patientId: self.patient!.id!)
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.patientLoginFromCode(authRequest)
        ApiManager.request(router: router) { (response: AuthResponse) in
            print(response)
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if let patient = response.userinfo {
                let defaults = UserDefaults.standard
                defaults.set(self.passwordText!, forKey: "password")
                let patientInfo = try? PatientInfo.init(json: patient.asDictionary())
                patientInfo!.saveUserInfo()
                self.gotoMainScreen()
            } else if let message = response.message {
                DataUtils.messageShow(view: self, message: message, title: "")
            } else if let message = response.error {
                DataUtils.messageShow(view: self, message: message, title: "")
            } else {
                DataUtils.messageShow(view: self, message: "Error occurred while processing your request", title: "")
            }
        } failure: { (error) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
        }
        
    }
    
    func convertToDictionary(datas:[MyMedication]) -> [[String : Any]] {
        var dics = [[String : Any]]()
        for data in datas {
            if data.asneeded == 1 {
                let dic: [String: Any] = ["direction":data.directions, "dose":data.dose, "quantity":data.quantity, "prescribe":data.prescribe, "taketime":data.taketime, "patientname":data.patientname, "pharmacy":data.pharmacy,"medicationname":data.medicationname,"strength":data.strength,"filed_date":data.filedDate,"warnings":data.warnings,"frequency":data.frequency,"lefttablet":data.lefttablet,"prescription":data.prescription,"createat":data.createat, "endat":data.endat, "amount": data.amount, "asneeded": 1]
                dics.append(dic)
            } else {
                let timings = data.taketime.components(separatedBy: ",") as [String]
                for time in timings {
                    let dic: [String: Any] = ["direction":data.directions, "dose":data.dose, "quantity":data.quantity, "prescribe":data.prescribe, "taketime":time.trimmingCharacters(in: .whitespaces), "patientname":data.patientname, "pharmacy":data.pharmacy,"medicationname":data.medicationname,"strength":data.strength,"filed_date":data.filedDate,"warnings":data.warnings,"frequency":data.frequency,"lefttablet":data.lefttablet,"prescription":data.prescription,"createat":data.createat, "endat":data.endat, "amount": data.amount]
                    dics.append(dic)
                }
            }
        }
        return dics
    }
    
    func gotoSignupAndMedication() {
        do {
            var datas:[MyMedication] = DBManager.getObject().getMedications()
            for i in 0..<datas.count {
                let endDate = Date().addingTimeInterval(TimeInterval(3600 * 24 * 7))
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd hh:mm"
                datas[i].endat = formatter.string(from: endDate)
            }
            let dicArray = convertToDictionary(datas:datas)
            let jsonData = try JSONSerialization.data(withJSONObject: dicArray, options: [])
            if let JSONString = String(data: jsonData, encoding: String.Encoding.utf8) {
                let params = [
                    "datas" : JSONString,                    
                    "email" : emailAddress.value.text!,
                    "password" : passwordText,
                    "phone_number" : phoneNum.value.text!.removingPatternFromNumbers(),
                    "first_name" : fullName.value.text!,
                    "last_name" : "",
                    "usertype" : "patient",
                    "choice" : "4"
                    ] as [String : Any]
                DataUtils.customActivityIndicatory(self.view,startAnimate: true)
                Alamofire.request(DataUtils.APIURL + DataUtils.MYDRUG_URL, method: .post, parameters: params)
                    .responseJSON(completionHandler: { response in
                        DataUtils.customActivityIndicatory(self.view, startAnimate: false)
                        print(response)
                        if let data = response.result.value {
                            //-----PING-----
                            if let json : [String:Any] = data as? [String : Any] {
                                let result = json["status"] as? String
                                if result == "true" {
                                    let alert = UIAlertController(title: "", message: "You have signed up successfully", preferredStyle: UIAlertControllerStyle.alert)
                                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                        DataUtils.setSkipButton(time: true)
                                        let defaults = UserDefaults.standard
                                        defaults.set(self.passwordText, forKey: "password")
                                        let patientInfo = PatientInfo.init(json: json["userinfo"] as! [String:Any])
                                        patientInfo.saveUserInfo()
                                        _ = DBManager.getObject().deleteTmpDrug()
                                        
                                        self.gotoMainScreen()
                                    }
                                    alert.addAction(OKAction)
                                    self.present(alert, animated: true, completion: nil)
                                } else {
                                    let message = json["message"] as! String
                                    if message == "Your email is in use" {
                                        let alert = UIAlertController(title: "", message: "Account with specified email exist.", preferredStyle: UIAlertControllerStyle.alert)
                                        let OKAction = UIAlertAction(title: "Login", style: .default) { (action:UIAlertAction!) in
                                            if let navVC = self.navigationController?.presentingViewController as? UINavigationController {
                                                    let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LoginScreenViewController") as! LoginScreenViewController
                                                    navVC.setViewControllers([viewController], animated: false)
                                                    self.dismiss(animated: false) {
                                                }
                                            }
                                        }
                                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                                        alert.addAction(OKAction)
                                        alert.addAction(cancelAction)
                                        self.present(alert, animated: true, completion: nil)
                                    } else {
                                        DataUtils.messageShow(view: self, message: message, title: "")
                                    }
                                }
                            } else {
                                DataUtils.messageShow(view: self, message: "Server Error", title: "")
                            }
                        } else {
                            DataUtils.messageShow(view: self, message: response.error!.localizedDescription, title: "")
                        }
                    })
            }
        } catch {
            
        }
    }
    
    func gotoSignup() {
        let localTimeZoneName = TimeZone.current.identifier
        let deviceToken = UserDefaults.standard.value(forKey: "DeviceToken") as? String ?? ""
        
        let params = [
            "email" : emailAddress.value.text!,
            "password" : password.valueText,
            "phone_number" : phoneNum.value.text!,
            "first_name" : fullName.valueText,
            "last_name" : "",
            "usertype" : DataUtils.getPatient()!,
            "choice" : "0",
            "timezone": localTimeZoneName,
            "deviceToken": deviceToken
        ] as [String : Any]
        
        DataUtils.customActivityIndicatory(self.view,startAnimate: true)
        Alamofire.request(DataUtils.APIURL + DataUtils.AUTH_URL, method: .post, parameters: params)
            .responseJSON(completionHandler: { response in
                DataUtils.customActivityIndicatory(self.view,startAnimate: false)
//                debugPrint(response)
                if let data = response.result.value {
                    //-----PING-----
                    if let json : [String:Any] = data as? [String : Any] {
                        let result = json["status"] as? String
                        if result == "true" {
                            let defaults = UserDefaults.standard
                            defaults.set(self.password.valueText, forKey: "password")
                            let patientInfo = PatientInfo.init(json: json["userinfo"] as! [String:Any])
                            patientInfo.saveUserInfo()
                            self.gotoMainScreen()
                        } else {
                            let message = json["message"] as! String
                            DataUtils.messageShow(view: self, message: message, title: "")
                        }
                    } else {
                        DataUtils.messageShow(view: self, message: "Server Error", title: "")
                    }
                }
            })
    }
    
    func gotoMainScreen() {
        setupTimeRange()        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCNotificationEnable") as! VCNotificationEnable
        viewController.modalPresentationStyle = .overFullScreen
        viewController.presentedFrom = .SignUpViewController
        self.present(viewController, animated: false, completion: nil)
    }
    
    func setupTimeRange() {
        DataUtils.setTimeRange(index: 0, time: DataUtils.morningRange)
        DataUtils.setTimeRange(index: 1, time: DataUtils.midTimeRange)
        DataUtils.setTimeRange(index: 2, time: DataUtils.afterRange)
        DataUtils.setTimeRange(index: 3, time: DataUtils.nightRange)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
//        let viewControllers = self.navigationController!.viewControllers as [UIViewController]
//        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
        self.navigationController?.popViewController(animated: true)
    }
}
