//
//  MedicationEmptyStateViewController.swift
//  Nexpil
//
//  Created by Arif on 11/14/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol EmptyStateDelegate {
    func addTapped()
}

class MedicationEmptyStateViewController: UIViewController {

    var delegate: EmptyStateDelegate?
    var selectedDate = GlobalManager.GetToday()
    @IBOutlet weak var lblDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        self.lblDate.text = formatter.string(from: self.selectedDate)
}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.view.backgroundColor = .clear
//        if DataUtils.hasMedicationAdded() {
//            self.dismiss(animated: true)
//        }
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.dismiss(animated: true)
    }
    
    @IBAction func actionAddMedication(_ sender: UIButton) {
        self.delegate?.addTapped()
    }
}
