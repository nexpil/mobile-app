//
//  TaskCell.swift
//  Nexpil
//
//  Created by Shadow on 2021/2/9.
//  Copyright © 2021 Admin. All rights reserved.
//

import Foundation
import UIKit

class NewTaskCell:UITableViewCell {
    
    @IBOutlet weak var vwBackground: GradientView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
}
