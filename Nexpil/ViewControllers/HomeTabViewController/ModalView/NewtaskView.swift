//
//  NewtaskView.swift
//  Nexpil
//
//  Created by Shadow on 2021/2/8.
//  Copyright © 2021 Admin. All rights reserved.
//

import Foundation
import Alamofire

protocol NewtaskDelegate {
    func popNewtaskViewDismissal()
}

class NewtaskView: UIView,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var btnGotit: NPButton!
    @IBOutlet weak var tblTasks: UITableView!    
    @IBOutlet weak var vwMain: UIView!
    var delegate: NewtaskDelegate?
    var groupId:Int = 0
    private var tableDatas: [[String: Any]] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        vwMain.setPopItemViewStyle(radius: 30.0, title: .large)
        
        self.tblTasks.separatorStyle = UITableViewCellSeparatorStyle.none
        self.tblTasks.allowsSelection = false
        self.tblTasks.alwaysBounceVertical = false
        self.tblTasks.delegate = self;
        self.tblTasks.dataSource = self;
        self.tblTasks.register(UINib(nibName: "NewTaskCell", bundle: nil), forCellReuseIdentifier: "NewTaskCell")
        
        btnGotit.addTarget(self, action: #selector(onClickCloseUB(_:)), for: .touchUpInside)
        
    }
    
    
    @IBAction func onClickCloseUB(_ sender: Any) {
        self.delegate?.popNewtaskViewDismissal()
    }
    
    func loadTaskGroup()
    {
        let params = [
            "group" : self.groupId,
            ] as [String : Any]
        self.tableDatas = [];
        Alamofire.request("https://twilio.nexp.xyz/nexpil/task_by_group.php",
                          method: .post, parameters: params)
            .responseJSON(completionHandler: { response in
//                var tasks = [MyTask]()
                switch response.result {
                    case .success(let value):
                        if let json : [String:Any] = value as? [String : Any] {
                            let taskList = json["data"] as? [NSDictionary]
                            for item in taskList ?? []{
                                self.tableDatas.append(item as! [String : Any])
                            }
                        }
                        self.tblTasks.reloadData()
                        print(self.tableDatas)
                        
                    case .failure(let error):
                        print(error)
                }
            })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let rowIdx = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewTaskCell") as? NewTaskCell
        cell?.selectionStyle = .none
        let data = tableDatas[rowIdx];
        print (data);
        cell?.lblTitle.text = data["name"] as! String;
        if (data["type_id"] as! String == "1")
        {
            cell?.lblDescription.text = ""
        }
        else if (data["type_id"] as! String == "2")
        {
            cell?.lblDescription.text = data["description"] as! String;
        }
        else if (data["type_id"] as! String == "3")
        {
            cell?.lblDescription.text = ""
        }
        else if (data["type_id"] as! String == "4")
        {
            cell?.lblDescription.text = ""
        }
        else if (data["type_id"] as! String == "5")
        {
            cell?.lblDescription.text = ""
        }
        else if (data["type_id"] as! String == "6")
        {
            cell?.lblDescription.text = ""
        }

        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return tableDatas.count
        return tableDatas.count;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60;
    }
    
}
