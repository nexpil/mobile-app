//
//  RefillPillsManuallyDoneViewController.swift
//  Nexpil
//
//  Created by Golder on 3/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol ManualPillsQuantityDelegate {
    func onDone(_ quantity: Int)
}

class RefillPillsManuallyDoneViewController: RefillPillsBaseViewController {

    @IBOutlet weak var lblNumberOfDragForm: UILabel!
    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var infoNumberOfPills: InformationCardEditable!
    @IBOutlet weak var constraintDialogBottom: NSLayoutConstraint!
    var isFromScanning = false
    var manualDelegate: ManualPillsQuantityDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoNumberOfPills.textView.textAlignment = .center
        infoNumberOfPills.textView.keyboardType = .numberPad
        
        self.setKeyboardCallbacks()
        
        self.initUI()
    }
    
    private func initUI() {
        if isFromScanning {
            lblQuestion.text = "How many pills do you have?"
        } else {
            lblNumberOfDragForm.text = "Number of \(self.medicineData.type)s"
            lblQuestion.text = "How many \(self.medicineData.type)s do you have left?"
        }
    }
    
    private func setKeyboardCallbacks() {
        self.callbackKeyboardWillShow = { (keyboardHeight) -> () in
            UIView.animate(withDuration: 0.5) {
                self.constraintDialogBottom.constant = keyboardHeight
                self.view.layoutIfNeeded()
            }
        }
        
        self.callbackKeyboardWillHide = { () -> () in
            UIView.animate(withDuration: 0.5) {
                self.constraintDialogBottom.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }

    @IBAction func onBtnDone(_ sender: Any) {
        var amount = 0
        if let txt = self.infoNumberOfPills.textView.text, let value = Int(txt) {
            amount = value
        }
        
        if amount == 0 {
            showAlert(self, title: "Amount", message: "Please enter the valid number.")
        } else {
            
            self.dismiss(animated: true) {
                if !self.isFromScanning {
                    self.delegate.onDone(medicineData: self.medicineData, pillsAmount: amount)
                } else {
                    if let del = self.manualDelegate {
                        del.onDone(amount)
                    }
                }
            }
        }
    }
    
    @IBAction func onBtnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
