//
//  CalenderVC.swift
//  Nexpil
//
//  Created by mac on 8/23/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CareKit
import FSCalendar

public protocol CalenderVCDelegate {
    func closeCalendarDialog(_ date: Date)
}

class CalenderVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var vwCalendar: CalendarView!
    @IBOutlet weak var btnDone: NPButton!
    public var m_themeIdx = 0
    public var medicationCreateList: [Date] = []
    public var delegate: CalenderVCDelegate?
    @IBOutlet weak var dialogHeight: NSLayoutConstraint!
    @IBOutlet weak var eventView: UIView!
    @IBOutlet weak var eventTableView: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    private var selectDate: Date = Date.startOfToday() //Date().startOfDay
    var today = Date.startOfToday()
    private var tasks = [OCKTask]()
    let store = OCKStore(name: "NextPil", type: .onDisk)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CalendarView.Style.cellShape                = .round
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.headerTextColor          = NPColorScheme(rawValue: m_themeIdx)!.color
        CalendarView.Style.cellTextColorDefault     = UIColor.darkText
        CalendarView.Style.cellTextColorToday       = UIColor.white// NPColorScheme(rawValue: 0)!.color
        CalendarView.Style.firstWeekday             = .sunday
        CalendarView.Style.cellTextColorWeekend     = NPColorScheme(rawValue: m_themeIdx)!.color
        CalendarView.Style.locale                   = Locale(identifier: "en_US")
        CalendarView.Style.timeZone                 = TimeZone(abbreviation: "UTC")!
        CalendarView.Style.cellColorToday = NPColorScheme(rawValue: m_themeIdx)!.color
        CalendarView.Style.headerFontName = "Montserrat"
        CalendarView.Style.colorTheme = m_themeIdx
        CalendarView.Style.hideCellsOutsideDateRange = false
        CalendarView.Style.changeCellColorOutsideRange = false
        CalendarView.Style.cellSelectedBorderColor = NPColorScheme(rawValue: m_themeIdx)!.color
        CalendarView.Style.cellEventColor = NPColorScheme(rawValue: m_themeIdx)!.color

        vwCalendar.backgroundColor = UIColor.clear
        vwCalendar.dataSource = self
        vwCalendar.delegate = self
        self.getAllEventsAndShowDots()
        
        btnDone.colorScheme = m_themeIdx
        self.getTasks()
        eventTableView.delegate = self
        eventTableView.dataSource = self
        
        let btnList = ["icon_add_more_morning", "icon_add_more_midday", "icon_add_more_evening", "icon_add_more_night"]
        if let image = UIImage(named: btnList[m_themeIdx]) {
            addButton.setImage(image, for: .normal)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showTaskSuccess()
    }
    
    func getAllEventsAndShowDots() {
        let endDate = self.selectDate.adding(years: 1)
        let dateInterval = DateInterval(start: self.today, end: endDate)
        let allQuery = OCKTaskQuery(dateInterval: dateInterval)
        var dates = [Date]()
        store.fetchTasks(query: allQuery, callbackQueue: .main) { result in
            dates = try! result.get().map { $0.effectiveDate }
            var events = [CalendarEvent]()
            for date in dates {
                let event = CalendarEvent(title: date.taskString(), startDate: date, endDate: date)
                events.append(event)
            }
            self.vwCalendar.events = events
            self.vwCalendar.reloadData()
        }
    }
    
    func getTasks() {
        tasks.removeAll()
        let allQuery = OCKTaskQuery(for: self.selectDate)
        store.fetchTasks(query: allQuery, callbackQueue: .main) { result in
            self.tasks = try! result.get()
            if self.tasks.count > 0 {
                if self.tasks.count == 1 {
                    self.dialogHeight.constant = 520
                } else {
                    self.dialogHeight.constant = 565
                }
            } else {
                self.dialogHeight.constant = 460
            }
            self.eventTableView.reloadData()
        }
    }

    @IBAction func onClose() {
        self.delegate?.closeCalendarDialog(self.selectDate)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onAdd() {
        let addTaskViewController = (self.storyboard?.instantiateViewController(withIdentifier: "AddTaskViewController") as? AddTaskViewController)!
        addTaskViewController.selectedDate = self.selectDate
//        addTaskViewController.callbackAddManually = {
//            let addManuallyViewController = AddTaskViewUsingController(nibName: "AddTaskViewUsingController", bundle: nil)
//            addManuallyViewController.modalPresentationStyle = .overFullScreen
//            addManuallyViewController.modalTransitionStyle = .crossDissolve
//            addManuallyViewController.callbackEnterCode = {
//                let enterCodeVC = AddTaskUsingCodeViewController(nibName: "AddTaskUsingCodeViewController", bundle: nil)
//                enterCodeVC.modalPresentationStyle = .overFullScreen
//                enterCodeVC.modalTransitionStyle = .crossDissolve
//                enterCodeVC.selectedDate = self.selectDate
//                enterCodeVC.callbackTaskAddded = {
//                    self.showTaskSuccess()
//                }
//                self.present(enterCodeVC, animated: true, completion: nil)
//            }
//
//            addManuallyViewController.callbackEnterInformation = {
//                let enterInfoVC = AddTaskUsingInformationViewController(nibName: "AddTaskUsingInformationViewController", bundle: nil)
//                enterInfoVC.modalPresentationStyle = .overFullScreen
//                enterInfoVC.modalTransitionStyle = .crossDissolve
//                enterInfoVC.selectedDate = self.selectDate
//                enterInfoVC.callbackTaskAddded = {
//                    self.showTaskSuccess()
//                }
//                self.present(enterInfoVC, animated: true, completion: nil)
//            }
//            self.present(addManuallyViewController, animated: true, completion: nil)
//        }
        addTaskViewController.modalPresentationStyle = .overFullScreen
        addTaskViewController.modalTransitionStyle = .crossDissolve
        self.present(addTaskViewController, animated: true, completion: nil)
    }
    
    func showTaskSuccess() {
        self.getAllEventsAndShowDots()
        self.getTasks()
//        let successVC = AddTaskFinalViewController(nibName: "AddTaskFinalViewController", bundle: nil)
//        successVC.modalPresentationStyle = .fullScreen
//        self.present(successVC, animated: false)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = eventTableView.dequeueReusableCell(withIdentifier: "CalendarEventTableViewCell", for: indexPath) as! CalendarEventTableViewCell
        cell.eventText.text = self.tasks[indexPath.row].title ?? ""
        cell.eventBg.topColor = NPColorScheme(rawValue: m_themeIdx)!.color.withAlphaComponent(0.8)
        cell.eventBg.bottomColor = NPColorScheme(rawValue: m_themeIdx)!.color
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
//            let taskDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
//            taskDetailsVC.task = self.tasks[indexPath.row]
//            taskDetailsVC.modalPresentationStyle = .overFullScreen
//            self.present(taskDetailsVC, animated: true)
        }
    }
}

extension CalenderVC: CalendarViewDataSource {
    func startDate() -> Date {
        return Date().startOfMonth()
    }
    
    func endDate() -> Date {
        
        var dateComponents = DateComponents()
        
        dateComponents.year = 2
        let today = Date()
        
        let twoYearsFromNow = self.vwCalendar.calendar.date(byAdding: dateComponents, to: today)!
        
        return twoYearsFromNow
        
    }
}


extension CalenderVC: CalendarViewDelegate {
    func calendar(_ calendar: CalendarView, didSelectDate date: Date, withEvents events: [CalendarEvent]) {
        if date >= today {
            self.eventView.isHidden = false
            self.selectDate = date
            self.getTasks()
        } else {
            self.eventView.isHidden = true
            self.dialogHeight.constant = 400
        }
    }
    
    func calendar(_ calendar: CalendarView, didScrollToMonth date: Date) {
        
    }
}

class CalendarEventTableViewCell: UITableViewCell {
    @IBOutlet weak var eventText: UILabel!
    @IBOutlet weak var eventBg: GradientView!
}

class CalendarEventCollectionCell: UICollectionViewCell {
    @IBOutlet weak var eventText: UILabel!
    @IBOutlet weak var eventBg: GradientView!
}
