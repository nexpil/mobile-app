//
//  AddTaskViewController.swift
//  Nexpil
//
//  Created by Arif on 3/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import AVKit

class AddTaskViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var imgScan: UIImageView!
    @IBOutlet weak var viewScanCodeText: GradientView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var manualButton: UIButton!
    
    var callbackAddManually: (() -> ())?

    var lastSampleTaken = Date()
    var timer: Timer!
    let session = AVCaptureSession()
    
    let dataOutput = AVCaptureVideoDataOutput()
    
    var visualEffectView:VisualEffectView?
    
    var prescription: NPPrescription?
    
    let finder = NPPrescriptionFinder.shared
    var selectedDate = Date()
    static var directionMode = 0
    
    // Can add logo detection if need arises.
    let features: [Feature] = {
        let feature1: Feature = {
            let type = Type.DOCUMENT_TEXT_DETECTION
            return Feature(type: type)
        }()
        return [feature1]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        closeButton.addShadow(color: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), alpha: 0.16, x: 0, y: 5.0, blur: 15.0)
        
        guard let inputDevice = AVCaptureDevice.default(for: .video) else {
            return
        }
        guard let input = try? AVCaptureDeviceInput(device: inputDevice) else {
            fatalError("Can't get camera input.")
        }

        session.addInput(input)
        session.startRunning()

        let preview = AVCaptureVideoPreviewLayer(session: session)
        preview.frame = self.view.bounds
        preview.videoGravity = AVLayerVideoGravity.resizeAspectFill

        view.layer.addSublayer(preview)//.//insertSublayer(preview, at: 0)

        session.addOutput(dataOutput)
        session.sessionPreset = AVCaptureSession.Preset.photo

        let sampleBufferQueue = DispatchQueue(label: "sampleBufferQueue")

        dataOutput.alwaysDiscardsLateVideoFrames = true
        dataOutput.setSampleBufferDelegate(self, queue: sampleBufferQueue)
        
        visualEffectView = self.view.backgroundBlur(view: self.view)
        self.view.bringSubview(toFront: self.closeButton)
        self.view.bringSubview(toFront: self.manualButton)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if DataUtils.getCameraStatus() == true {
            self.dismiss(animated: false, completion: nil)
            DataUtils.setCameraStatus(name: false)
            return
        }
        timer = Timer.scheduledTimer(withTimeInterval: 15, repeats: false, block: { (timer) in
            self.session.stopRunning()
            if self.finder.drug != nil {
                self.prescription = self.finder.getPrescription()
                self.performSegue(withIdentifier: "summarySegue", sender: self)
            }
            else {
                print("notComplete")
            }
        })
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.timer != nil {
            self.timer.invalidate()
            self.timer = nil
        }
    }
    
    @IBAction func onCLose(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addManually(_ sender: UIButton) {
        self.view.backgroundColor = UIColor(hex: "4939E3")
        self.imgScan.isHidden = true
        self.viewScanCodeText.isHidden = true
        let addManuallyViewController = AddTaskViewUsingController(nibName: "AddTaskViewUsingController", bundle: nil)
        addManuallyViewController.selectedDate = self.selectedDate
        addChildViewController(addManuallyViewController)
        
        addManuallyViewController.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
        addManuallyViewController.view.backgroundColor = .clear
        view.addSubview(addManuallyViewController.view)
        addManuallyViewController.didMove(toParentViewController: self)
        
        
//        self.dismiss(animated: false) {
//            self.callbackAddManually?()
//        }
    }
}

extension AddTaskViewController : AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if lastSampleTaken.timeIntervalSinceNow < -0.75 {

            let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
            let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
            let context:CIContext = CIContext.init(options: nil)
            let cgImage:CGImage = context.createCGImage(ciimage, from: ciimage.extent)!
            let uiimage = UIImage(cgImage: cgImage)
            let data = UIImageJPEGRepresentation(uiimage, 0.10)!


            lastSampleTaken = Date()
            let image = Image(fromContent: data.base64EncodedString())

            let requests = AnnotateImageRequest(image: image, features: features, context: nil)
            let request = Request(requests: [requests])
            request.perform { response in
                guard let texts = response.responses.first!.fullTextAnnotation else { return }
                let page = texts.pages[0]
                for i in 0..<page.blocks.count {
                    print("Block: \(i)")
                    for j in 0..<page.blocks[i].paragraphs.count {
                        print("Paragraph: \(j)")
                        print(page.blocks[i].paragraphs[j].text)
                    }
                }
//                self.finder.delegate = self
//                self.finder.userName = "Patient Test"
//                self.finder.addResponse(response)
            }
        }
    }
}

extension AddTaskViewController: NPPrescriptionEventHandler {
    func handleEvent(_ event: NPPrescriptionEvent) {
        print(event)
        DispatchQueue.main.async { [unowned self] in
            switch event {
            case .prescriptionComplete:
                self.prescription = self.finder.getPrescription()
                self.performSegue(withIdentifier: "summarySegue", sender: self)
            default:
                break
            }
        }
    }
}
