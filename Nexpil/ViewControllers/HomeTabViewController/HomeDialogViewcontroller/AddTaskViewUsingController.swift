//
//  AddTaskViewUsingController.swift
//  Nexpil
//
//  Created by Arif on 3/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AddTaskViewUsingController: UIViewController {

    var callbackEnterCode: (() -> ())?
    var callbackEnterInformation: (() -> ())?
    var selectedDate = Date()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        print(self.parent?.childViewControllers)
    }
    
    @IBAction func actionEnterTaskInformation(_ sender: UIButton) {
        if let parentVC = self.parent {
            willMove(toParentViewController: nil)
            removeFromParentViewController()
            view.removeFromSuperview()
            
            let enterInfoVC = AddTaskUsingInformationViewController(nibName: "AddTaskUsingInformationViewController", bundle: nil)
            enterInfoVC.selectedDate = self.selectedDate
            parentVC.addChildViewController(enterInfoVC)
            
            enterInfoVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            enterInfoVC.view.backgroundColor = .clear
            parentVC.view.addSubview(enterInfoVC.view)
            enterInfoVC.didMove(toParentViewController: parentVC)
        }
        
        
                
//        self.dismiss(animated: true) {
//            self.callbackEnterInformation?()
//        }
        
        
//        enterInfoVC.selectedDate = self.selectDate
//        self.present(enterInfoVC, animated: true, completion: nil)
    }
    
    @IBAction func actionEnterTaskCode(_ sender: UIButton) {
        if let parentVC = self.parent {
            willMove(toParentViewController: nil)
            removeFromParentViewController()
            view.removeFromSuperview()
            
            let enterCodeVC = AddTaskUsingCodeViewController(nibName: "AddTaskUsingCodeViewController", bundle: nil)
            enterCodeVC.selectedDate = self.selectedDate
            parentVC.addChildViewController(enterCodeVC)
            
            enterCodeVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
            enterCodeVC.view.backgroundColor = .clear
            parentVC.view.addSubview(enterCodeVC.view)
            enterCodeVC.didMove(toParentViewController: parentVC)
        }
        
//        self.dismiss(animated: true) {
//            self.callbackEnterCode?()
//        }
    }
    
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}

//extension UIViewController {
//    func remove() {
//        guard parent != nil else { return }
//
//        willMove(toParentViewController: nil)
//        removeFromParentViewController()
//        view.removeFromSuperview()
//    }
//}
