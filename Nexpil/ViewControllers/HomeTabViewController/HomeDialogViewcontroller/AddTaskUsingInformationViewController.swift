//
//  AddTaskUsingInformationViewController.swift
//  Nexpil
//
//  Created by Arif on 3/20/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import CareKit
import Alamofire

class AddTaskUsingInformationViewController: KeyboardViewController {

    @IBOutlet weak var fieldTitle: InformationCardEditable!
    @IBOutlet weak var txtInformation: UITextView!
    @IBOutlet weak var lblDate: UILabel!
    
    var selectedDate = Date()
    var callbackTaskAddded: (() -> ())?
    let store = OCKStore(name: "NextPil", type: .onDisk)
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var timeView: UIStackView!
    
    @IBOutlet weak var amButton: UIButton!
    @IBOutlet weak var pmButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    @IBOutlet var hourViews: [GradientView]!
    @IBOutlet var minViews: [GradientView]!
    @IBOutlet var hourLabels: [UILabel]!
    @IBOutlet var minLabels: [UILabel]!
    
    
    var hour = 8
    var min = 0
    var identifyAmPm = 0 // 0: AM 1: PM
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblDate.text = selectedDate.taskString()
        self.addDoneButtonOnKeyboard(forTextView: self.txtInformation)
        
        self.fieldTitle.textView.delegate = self
        self.customizeCalendar()
        self.customizeTimeView()
    }
    
    func customizeTimeView() {
        for i in 0..<12 {
            hourViews[i].topColor = UIColor.white
            hourViews[i].bottomColor = UIColor.white
            hourViews[i].cornerRadius = 20
            hourViews[i].tag = i
            hourViews[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleHourViewTap(sender:))))
            
            minViews[i].topColor = UIColor.white
            minViews[i].bottomColor = UIColor.white
            minViews[i].cornerRadius = 20
            minViews[i].tag = i
            minViews[i].addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleMinVIewTap(sender:))))
            
            hourLabels[i].textColor = UIColor.init(hex: "333333")
            minLabels[i].textColor = UIColor.init(hex: "333333")
        }
        
        // Init
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(abbreviation: "GMT")!
        var hour = calendar.component(.hour, from: GlobalManager.GetToday())
        var min = calendar.component(.minute, from: GlobalManager.GetToday())
        
        self.hour = hour
        self.min = min
        
        min = (min / 5) * 5
        
        if hour > 12 {
            hour = hour - 12
            setPmButtonActive()
        } else {
            if hour == 0 {
                hour = 12
            }
            setAmButtonActive()
        }
        
        let timeText = min < 10 ? "\(hour):0\(min)" : "\(hour):\(min)"
        timeLabel.text = timeText
        let amPm = (identifyAmPm == 0) ? " a.m." : " p.m."
        self.lblTime.text = timeText + amPm
        
//        timeLabel.text = "\(hour):\(min)"
//        var hourIndex = hour > 0 ? hour - 1 : 0
        setViewActive(view: hourViews[hour - 1], label: hourLabels[hour - 1])
        setViewActive(view: minViews[min/5], label: minLabels[min/5])                                
    }
    
    func customizeCalendar() {
        CalendarView.Style.cellShape                = .round
        CalendarView.Style.cellColorDefault         = UIColor.clear
        CalendarView.Style.headerTextColor          = NPColorScheme(rawValue: 0)!.color
        CalendarView.Style.cellTextColorDefault     = UIColor.darkText
        CalendarView.Style.cellTextColorToday       = UIColor.white
        CalendarView.Style.firstWeekday             = .sunday
        CalendarView.Style.cellTextColorWeekend     = NPColorScheme(rawValue: 0)!.color
        CalendarView.Style.locale                   = Locale(identifier: "en_US")
        CalendarView.Style.timeZone                 = TimeZone(abbreviation: "UTC")!
        CalendarView.Style.cellColorToday = NPColorScheme(rawValue: 0)!.color
        CalendarView.Style.headerFontName = "Montserrat"
        
        CalendarView.Style.hideCellsOutsideDateRange = false
        CalendarView.Style.changeCellColorOutsideRange = false
        CalendarView.Style.cellSelectedBorderColor = NPColorScheme(rawValue: 0)!.color
        CalendarView.Style.colorTheme = 0
        
        calendarView.backgroundColor = UIColor.clear
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.selectDate(self.selectedDate)
        calendarView.reloadData()
    }
    
    @IBAction func amButtonClick(_ sender: Any) {
        setAmButtonActive()
        
    }
    
    @IBAction func pmButtonClick(_ sender: Any) {
        setPmButtonActive()
    }
    
    private func setAmButtonActive() {
        identifyAmPm = 0
        amButton.setImage(UIImage(named: "AM"), for: .normal)
        pmButton.setImage(UIImage(named: "night_off"), for: .normal)
    }
    
    private func setPmButtonActive() {
        identifyAmPm = 1
        amButton.setImage(UIImage(named: "midday_off"), for: .normal)
        pmButton.setImage(UIImage(named: "PM"), for: .normal)
    }
    
    private func setViewActive(view: GradientView, label: UILabel) {
        view.topColor = UIColor(cgColor: NPColorScheme(rawValue: 0)!.gradient[0])
        view.bottomColor = UIColor(cgColor: NPColorScheme(rawValue: 0)!.gradient[1])
        label.textColor = UIColor.white
    }
    
    @objc func handleHourViewTap(sender: UITapGestureRecognizer) {
        let index = sender.view!.tag
        hour = index + 1 + identifyAmPm * 12
        updateTimeLabel()
        
        for i in 0..<12 {
            hourViews[i].topColor = UIColor.white
            hourViews[i].bottomColor = UIColor.white
            hourLabels[i].textColor = UIColor.init(hex: "333333")
        }
        setViewActive(view: hourViews[index], label: hourLabels[index])
    }
    
    private func updateTimeLabel() {
        let h = hour > 12 ? hour - 12 : hour
        let m = (min/5) * 5
        let timeText = min < 10 ? "\(h):0\(m)" : "\(h):\(m)"
        timeLabel.text = timeText
        let amPm = (identifyAmPm == 0) ? " a.m." : " p.m."
        self.lblTime.text = timeText + amPm
    }
    
    @objc func handleMinVIewTap(sender: UITapGestureRecognizer) {
        let index = sender.view!.tag
        min = index * 5
        updateTimeLabel()
        for i in 0..<12 {
            minViews[i].topColor = UIColor.white
            minViews[i].bottomColor = UIColor.white
            minLabels[i].textColor = UIColor.init(hex: "333333")
        }
        setViewActive(view: minViews[index], label: minLabels[index])
    }
    
    @IBAction func actionSelectDate(_ sender: UIButton) {
        self.view.endEditing(false)
        self.calendarView.isHidden = !self.calendarView.isHidden
        self.timeView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func actionSelectTime(_ sender: UIButton) {
        self.view.endEditing(false)
        self.timeView.isHidden = !self.timeView.isHidden
        self.calendarView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }        
    }
    
    @IBAction func actionClose(_ sender: UIButton) {
        self.dismiss(animated: true)
    }

    @IBAction func actionAddTask(_ sender: UIButton) {
        self.view.endEditing(true)
        
        let charSet = CharacterSet.whitespacesAndNewlines
        let description = self.txtInformation.text.trimmingCharacters(in: charSet)
        guard let name = self.fieldTitle.textView.text?.trimmingCharacters(in: charSet), !name.isEmpty, description != "Description" else {
            showAlert(self, title: nil, message: "Please enter the valid information.")
            return
        }
            
        let starttime = self.lblTime.text!
        let time = Date().timeString()
        let element = OCKScheduleElement(start: self.selectedDate, end: self.selectedDate.adding(hours: 1),interval: DateComponents(year: 1))
        let breakfastSchedule = OCKSchedule(composing: [element])
        let taskID = name.lowercased().replacingOccurrences(of: " ", with: "").appending(time)
        var task = OCKTask(id: taskID, title: name, carePlanID: nil, schedule: breakfastSchedule)
        
        let params = [
            "choice": "0",
            "type_id": "\(TaskType.simple.rawValue)",
            "user_id": "\(PreferenceHelper().getId())",
            "name": name,
            "description": description,
            "start_date": self.selectedDate.dateString(),
            "start_time": starttime
            ] as [String : String]

        task.userInfo = params
        store.addTask(task) { result in
            switch result {
            case .failure(let error):
                print("Error: \(error)")
                showAlert(self, title: nil, message: error.localizedDescription)
            case .success:
                print("Successfully saved a new task!")
                
                Alamofire.request(DataUtils.APIURL + DataUtils.TASK_BYUSER_URL,method: .post, parameters: params)
                    .responseJSON(completionHandler: { response in
                })

                if let parentVC = self.parent {
                    self.willMove(toParentViewController: nil)
                    self.removeFromParentViewController()
                    self.view.removeFromSuperview()
                    
                    let successVC = AddTaskFinalViewController(nibName: "AddTaskFinalViewController", bundle: nil)
                   parentVC.addChildViewController(successVC)
                   
                   successVC.view.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
                   successVC.view.backgroundColor = .clear
                   parentVC.view.addSubview(successVC.view)
                   successVC.didMove(toParentViewController: parentVC)
                }
            }
        }
    }
}

extension AddTaskUsingInformationViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.timeView.isHidden = true
        self.calendarView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if textView.text == "Description" {
            textView.text = ""
            textView.textColor = .label
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
            textView.text = "Description"
            textView.textColor = .lightGray
        }
    }
}


extension AddTaskUsingInformationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.timeView.isHidden = true
        self.calendarView.isHidden = true
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.text = textField.text?.capitalizingFirstLetter()
    }
}

extension AddTaskUsingInformationViewController: CalendarViewDataSource, CalendarViewDelegate {
    func startDate() -> Date {
        return Date().startOfMonth()
    }
    
    func endDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.year = 2
        let today = Date()
        let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        return twoYearsFromNow
    }
    
    func calendar(_ calendar: CalendarView, didSelectDate date: Date, withEvents events: [CalendarEvent]) {
        if date >= Date.startOfToday() {
            self.selectedDate = date
            self.lblDate.text = selectedDate.taskString()
        }
    }
    
    func calendar(_ calendar : CalendarView, didScrollToMonth date : Date) {
    }
}
