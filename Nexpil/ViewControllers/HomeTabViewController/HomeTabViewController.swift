//
//  HomeTabViewController.swift
//  Nexpil
//
//  Created by Admin on 4/6/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit
import CareKit
import Alamofire
import SwiftyJSON
import CVCalendar
import SVProgressHUD
import XLPagerTabStrip
import UserNotifications

protocol HomeSubMenuDelegate {
    func selectDay(value:Int) -> String
    func navColorChange(value:Int)
    func addMedicationTappedFromEmptyState()
}


extension HomeSubMenuDelegate {
    func addMedicationTappedFromEmptyState() {
        print("addMedicationTappedFromEmptyState")
    }
}

class HomeTabViewController: ButtonBarPagerTabStripViewController,HomeSubMenuDelegate,NewtaskDelegate,UITabBarControllerDelegate  {
    
    @IBOutlet weak var viewTasks: UIView!
    @IBOutlet weak var m_vwTopNavBar: UIView!
    @IBOutlet weak var btnShowTask: UIButton!
    @IBOutlet weak var m_vwRadialLeft: UIView!
    @IBOutlet weak var m_vwRadialRight: UIView!
    @IBOutlet weak var viewTaskdivider: UIView!
    @IBOutlet weak var m_vwTopContainer: UIView!
    @IBOutlet weak var collectionTask: TaskCollectionView!
    
    @IBOutlet var vwDays: [UIView]!
    @IBOutlet var vwTodayFooters: [GradientView]!
    @IBOutlet weak var constraintTaskTop: NSLayoutConstraint!
    @IBOutlet weak var constraintScrollTop: NSLayoutConstraint!
    
    @IBOutlet weak var vwAgoDays: UIView!
    @IBOutlet weak var m_fabAdd: FAButton!
    @IBOutlet weak var m_vwSeperate: UIView!
    @IBOutlet weak var m_stvwCalender: UIStackView!
            
    @IBOutlet var lblDays: [UILabel]!
    @IBOutlet var lblDates: [UILabel]!
    @IBOutlet var lblOverlayDays: [UILabel]!
    
    @IBOutlet weak var lcAgoDay: NSLayoutConstraint!
    @IBOutlet var imgTaskIndicators: [DateImageView]!
    
    @IBOutlet weak var m_vwMorning: UIView!
    @IBOutlet weak var m_ivMorning: UIImageView!
    @IBOutlet weak var m_cnstvwMorning: NSLayoutConstraint!
    
    @IBOutlet weak var m_vwMidDay: UIView!
    @IBOutlet weak var m_ivMidDay: UIImageView!
    @IBOutlet weak var m_cnstvwMidDay: NSLayoutConstraint!
    
    @IBOutlet weak var m_vwEvening: UIView!
    @IBOutlet weak var m_ivEvening: UIImageView!
    @IBOutlet weak var m_cnstvwEvening: NSLayoutConstraint!
    
    @IBOutlet weak var m_vwNight: UIView!
    @IBOutlet weak var m_ivNight: UIImageView!
    @IBOutlet weak var m_cnstvwNight: NSLayoutConstraint!
    
    @IBOutlet weak var m_cnstTopContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var vwGrey: GradientView!
    
    
    var oldCell: ButtonBarViewCell?
    var newCell: ButtonBarViewCell?
    @IBOutlet weak var currentDate: UILabel!
    
    var dateShow:Bool = false
    var taskShow:Bool = false {
        didSet {
            self.viewTaskdivider.isHidden = !taskShow
            self.collectionTask.isShown = taskShow
            self.constraintScrollTop.constant = -10
            if taskShow  {
                self.btnShowTask.isHidden = true
                self.constraintTaskTop.constant = 10
            } else {
                self.constraintTaskTop.constant = -90
                self.btnShowTask.isHidden = false
                self.collectionTask.layer.masksToBounds = false
                self.collectionTask.addShadow(color: #colorLiteral(red: 0.6666666667, green: 0.6666666667, blue: 0.6666666667, alpha: 1), alpha: 1, x: 0, y: 2, blur: 5.0)
            }
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    var child_1:MorningViewController1?
    
    var selectedDay = 0
    var dateColor:UIColor?
    
    var colorMorningActive = UIColor.init(hexString: "#39D3E3")
    var colorMidDayActive = UIColor.init(hexString: "#397EE3")
    var colorEveningActive = UIColor.init(hexString: "#415CE3")
    var colorNightActive = UIColor.init(hexString: "#4939E3")
    var colorDeactive = UIColor.init(hexString: "#707070")
    
    var m_fltNavLbHeight = 10
    var firstDrugUsageDate: Date!
    var navVWArray: [UIView] = []
    var navIVArray: [UIImageView] = []
    var topNavActiveArray: [String] = []
    var topNavDeactiveArray: [String] = []
    var navColorActiveArray: [UIColor] = []
    var addBtnImagePathArray: [String] = []
    
    @IBOutlet weak var dayofWeek: UIButton!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var currentDateLabel: UILabel!
    
    var m_curtabPos = 0
    var m_showDate: Date!
    var m_taskDate: Date!
    var m_selectedDate: Date!
    var taskList = [String]()
    var allTasks = [OCKTask]()
    var allTaskDates = [Date]()
    var tasksOfDate = [OCKTask]()
    var medicationCreateList: [Date] = []
    @IBOutlet weak var wendesdayBubble: UIView!
    @IBOutlet weak var rightBubble: NSLayoutConstraint!
    @IBOutlet weak var todayBackgroundView: GradientView!
    
    var todayTasks = [String]()
    var todayTaskIds = [Int]()
    var tasks = [JSON]()
    var dateTasks = [JSON]()
    
    let store = OCKStore(name: "NextPil", type: .onDisk)
    
    var popNewtaskView: NewtaskView = NewtaskView()
    
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.initializeContent), name: Notification.Name.NSCalendarDayChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.initializeContent), name: Notification.Name.UpdateHomeDates, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTasks), name: Notification.Name.reloadTasks, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openNewTaskModal), name: Notification.Name.createdTask, object: nil)
        
        self.initializeContent()
        
        navColorActiveArray = [colorMorningActive, colorMidDayActive, colorEveningActive, colorNightActive] as! [UIColor]
        super.viewDidLoad()
        
        dateColor = UIColor.init(hex: "39d3e3")
        initNavBar()
        self.tabBarController?.delegate = self
        
//        self.view.backgroundColor = UIColor.white//UIColor.init(hexString: "#F7F7F7")
        containerView.isScrollEnabled = false
        barSettings()
        
        showCalendar(bShow: false)
        
        self.buttonBarView.backgroundColor = nil
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if type(of: appDelegate.window?.rootViewController) == UITabBarController.self {
            let vc = appDelegate.window?.rootViewController as! UITabBarController
            let vc_community = vc.viewControllers![2] as! CommunityMainViewController
            if vc_community.showCongPopup == true {
                mTabView.setSelectedIndex(2)
            }
        }
            
        // Event for double tap Home Tab Item
        NotificationCenter.default.addObserver(self, selector: #selector(onDoubleTapHomeTab(noti:)), name: Notification.Name.Action.DoubleTapHomeTab, object: nil)
        
        // Event for Top current date
        let currentDateTapGesture = UITapGestureRecognizer(target: self, action: #selector(dateShow(_:)))
        currentDateLabel.addGestureRecognizer(currentDateTapGesture)
        currentDateLabel.isUserInteractionEnabled = true
        
        let downSwipe = UIPanGestureRecognizer(target: self, action: #selector(handleDownSwipe(_:)))
        self.btnShowTask.addGestureRecognizer(downSwipe)
        
        runAfter(1) {
            self.reloadTasks()
            
            //self.openNewTaskModal()
        }
        
        runAfter(0.2) {
            self.chatLogin()
        }
        
//        goTo()
    }
    
    @objc func openNewTaskModal(notification: NSNotification){
        popNewtaskView = Bundle.main.loadNibNamed("NewtaskView", owner: self, options: nil)?.first as! NewtaskView
        popNewtaskView.delegate = self
        //popScheduleDetailView.type = value
//        popScheduleDetailView.data = arraySchedule[value]
        popNewtaskView.frame = self.view.frame
        popNewtaskView.groupId = (notification.userInfo?["taskId"] as? Int)!
        popNewtaskView.loadTaskGroup()
        self.view.addSubview(popNewtaskView)
    }
    
    func popNewtaskViewDismissal() {
        self.popNewtaskView.removeFromSuperview()
    }
    
    
    func chatLogin() {
        if App.channelName != "" {
            SVProgressHUD.show(withStatus: "Initializing Chat")
        }
        NexpilManager.shared.chatManager.login() { (success) in
            DispatchQueue.main.async {
                if success {
                    print("CHAT LOGIN SUCCESS")
                    if App.channelName != "" {
                        SVProgressHUD.show(withStatus: "Initializing Chat")
                        NexpilManager.shared.chatManager.client?.channelsList()?.channel(withSidOrUniqueName: App.channelName, completion: { (result, channel) in
                            SVProgressHUD.dismiss()
                            App.channelName = ""
                            if result.isSuccessful(), let channel = channel {
                                NexpilManager.shared.activeChannel = channel
                                let chatVC = CommunityChatViewController(nibName: "CommunityChatViewController", bundle: nil)
                                let chatNavVC = UINavigationController(rootViewController: chatVC)
                                chatNavVC.modalPresentationStyle = .fullScreen
                                self.present(chatNavVC, animated: true)
                            } else {
                                print(result.error)
                                DataUtils.messageShow(view: self, message: "Error occured while initializing chat", title: "")
                            }
                        })
                    }
                } else {
                    print("CHAT LOGIN FAILURE")                    
                    runAfter(7.5) {
                        self.chatLogin()
                    }
                }
            }
        }
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        if !DataUtils.hasMedicationAdded() {
//            runAfter(0) {
//                self.showEmptyState()
//            }
//        }
//    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.dismiss(animated: true)
//    }
    
    @objc func reloadTasks() {
//        do {
//            try self.store.delete()
//        } catch {
//            print(error)
//        }
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        self.getMyTasks()
    }
    
//    func showEmptyState() {
//        let vc = MedicationEmptyStateViewController(nibName: "MedicationEmptyStateViewController", bundle: nil)
//        vc.delegate = self
//        vc.modalPresentationStyle = .overCurrentContext
//        self.present(vc, animated: false)
//    }
    
    func getMyTasks() {
        //        let router = Router.getMyTasks(String(PreferenceHelper().getId()))
        //        ApiManager.request(router: router) { (response: APIResponse<[MyTask]>) in
        //            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
        //            print(response.data)
        //            for task in response.data! {
        //                print(task)
        //            }
        //            if let tasks = response.data {
        //                self.saveLoadedTasks(tasks)
        //            }
        //        } failure: { (error) in
        //            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
        //            print(error)
        //        }
        
        let preference = PreferenceHelper()
        let params = [
            "patient_id" : preference.getId(),
        ] as [String : Any]
        DataUtils.customActivityIndicatory(self.view,startAnimate: true)
        
        Alamofire.request("https://twilio.nexp.xyz/webapi/v1/tasks", method: .get, parameters: params)
            .responseJSON(completionHandler: { response in
                DataUtils.customActivityIndicatory(self.view,startAnimate: false)
                
                switch response.result {
                    case .success(let value):
                        let values = JSON(value)
                        for item in values.arrayValue {
                            let formatter = DateFormatter()
                            formatter.dateFormat = "YYYY-MM-dd"
                            formatter.timeZone = TimeZone(abbreviation: "GMT")
                            let type = item["type_id"].stringValue
                            var addedDate = item["added_date"].stringValue.components(separatedBy: " ")[0]
                            switch type{
                            case "1":
                                addedDate = item["task_start_date"].stringValue.components(separatedBy: " ")[0]
                                break;
                            case "4":
                                addedDate = item["task_start_date"].stringValue.components(separatedBy: " ")[0]
                                break;
                            case "6":
                                addedDate = item["task_start_date"].stringValue.components(separatedBy: " ")[0]
                                break;
                            default:
                                break;
                            }
                            print("Added Date:" + addedDate)
                            if addedDate == formatter.string(from: GlobalManager.GetToday()) {
                                self.todayTasks.append(item["name"].stringValue)
                                self.todayTaskIds.append(item["id"].intValue)
                            }
                            self.tasks.append(item)
                            print("tasks")
                            print(self.tasks)
                            
                            
                            //                            let jsonTaskDetails = JSON(item["details"])
                            //                            let jsonTaskSequencs = JSON(jsonTaskDetails["sequences"])
                            //                            let jsonTaskItems = JSON(jsonTaskSequencs["sequences"]["items"])
                            //                            for tItem in jsonTaskItems{
                            //                                  print(tItem)
                            //                                let taskItem = TaskItem(sequenceTime: tItem["sequence_time"].stringValue, instruction: tItem["instruction"].stringValue)
                            //                                taskItems.append(taskItem)
                            //                            }
                            //                            let sequences = Sequences(medicationsToStop: item["details"]["sequences"]["medications_to_stop"].stringValue, items: <#T##[TaskItem]?#>)
                            //
                            //                            let taskDetails = TaskDetails(items: item["details"]["items"].arrayValue, sequences: , frequency: item["details"]["frequencey"].stringValue, link: item["link"].stringValue, strength: item["strength"].stringValue, quantity: item["strength"].stringValue, dosage: item["strength"].stringValue, refillNumber: item["refill_number"].stringValue, phoneNumber: item["phone_number"].stringValue, address: item["address"].stringValue, comments: item["comments"].stringValue, questions: )
                            //
                            //                            let task = MyTask(id: item["id"].intValue, typeId: item["type_id"].stringValue, taskGroupId: item["task_group_id"].stringValue, name: item["name"].stringValue, datumDescription: item["description"].stringValue, details: TaskDetails?, startDate: item["start_date"].stringValue, endDate: item["end_date"].stringValue, addedDate: item["added_date"].stringValue, patientId: item["patient_id"].stringValue, physicianId: item["physician_id"].stringValue, isTemplate: item["is_template"].stringValue, templateName: item["template_name"].stringValue, createdAt: item["created_at"].stringValue, updatedAt: item["updated_at"].stringValue)
                        }
                        self.getTasksForDate(self.m_selectedDate)
                        self.hideUnhideTaskDates()
                    case .failure(let error):
                        print(error)
                }
            })
    }
    
    func saveLoadedTasks(_ tasks: [MyTask]) {
//        let starttime = self.lblTime.text!
        
        for task in tasks {
            let onDate = (task.endDate == nil || task.endDate == "") ? task.startDate?.toTaskDate() : task.endDate?.toTaskDate()
//            let time = Date().timeString()
            let element = OCKScheduleElement(start: onDate!, end: onDate!.adding(hours: 1),interval: DateComponents(year: 1))
            let breakfastSchedule = OCKSchedule(composing: [element])
            let taskID = String(task.id!)
            var ocTask = OCKTask(id: taskID, title: task.name!, carePlanID: nil, schedule: breakfastSchedule)
//            var ocTask = NXCareTask()//OCKTask(id: taskID, title: task.name!, carePlanID: nil, schedule: breakfastSchedule)
            
            var details = ""
            if let taskDetails = task.details {
                do {
                    let data = try JSONEncoder().encode(taskDetails)
                    details = String(data: data, encoding: .utf8)!
                } catch {
                    print(error)
                }
            }
            
            let params = [
                "type_id": "\(task.typeId!)",
                "user_id": "\(PreferenceHelper().getId())",
                "name": task.name!,
                "description": task.datumDescription,//"",
                "start_date": onDate!.dateString(),
                "start_time": "",
                "id": taskID,
                "details": details
                ] as? [String: String]
//            print(details)
            ocTask.userInfo = params
            store.addTask(ocTask) { result in
                print(result)
//                switch result {
//                case .failure(let error):
//                    print("Error: \(error)")
//                    showAlert(self, title: nil, message: error.localizedDescription)
//                case .success:
//                    print("Successfully saved a new task!")
//                }
            }
        }
        
        runAfter(1) {
            self.getAlltasks(self.m_selectedDate)
        }
    }
    
    @objc func initializeContent() {
        self.m_selectedDate = GlobalManager.GetToday()
        self.m_showDate = GlobalManager.GetToday()
        self.collectionTask.delegate = self
        self.getAlltasks(self.m_selectedDate)
        
        self.initCalenderLabels(self.m_selectedDate)
        DispatchQueue.main.async {
            for index in 0 ..< 7 {
                self.vwDays![index].tag = index
                self.vwDays![index].layer.cornerRadius = 10
                self.vwDays![index].layer.masksToBounds = true
                self.vwTodayFooters[index].isHidden = true
            }
            self.vwTodayFooters[3].isHidden = false
        }
    }
    
    @IBAction func handleDownSwipe(_ panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: viewTasks)
        if panGesture.state == UIGestureRecognizer.State.changed {
            if translation.y > 0 {
                let currentValue = -90 + translation.y
                if currentValue < 10 {
                    self.constraintTaskTop.constant = currentValue
                }
            }
        } else if panGesture.state == .ended {
            self.taskShow = !(translation.y > 0 && translation.y < 42.5)
        }
    }
    
    private func transform(view: UIView, for translation: CGPoint) -> CGAffineTransform {
        let moveBy = CGAffineTransform(translationX: translation.x, y: translation.y)
        
        let rotation = -sin(translation.x / (view.frame.width * 4.0))
        return moveBy.rotated(by: rotation)
    }
    
    func getAlltasks(_ date: Date) {
        let startDate = date.adding(days: -3)
        let endDate = date.adding(days: 3)
        var allQuery = OCKTaskQuery(dateInterval: DateInterval(start: startDate, end: endDate))
        if allTasks.count > 0 {
            allQuery = OCKTaskQuery()
        }
        allTasks = []
        store.fetchTasks(query: allQuery, callbackQueue: .main) { [weak self] result in
            self?.allTasks = try! result.get()
            self?.getTasksForDate(date)
            self?.getUniqueTaskDates()
        }
    }
    
    @IBAction func showTask(_ sender: UIButton) {
        if !taskShow {
            self.taskShow = true
//            self.btnShowTask.isHidden = true
        }
    }
    
    func getUniqueTaskDates() {
        self.allTaskDates = []
        for task in allTasks {
            if !self.allTaskDates.contains(where: { (date) -> Bool in
                return date.isSameDay(date: task.effectiveDate)
            }) {
                self.allTaskDates.append(task.effectiveDate)
            }
            print(self.allTaskDates)
        }
        self.hideUnhideTaskDates()
    }
    
    func hideUnhideTaskDates() {
        for imageView in self.imgTaskIndicators {
            print ("Image Date: " + imageView.imageDate.startOfDate.dateString())
            imageView.isHidden = true
            for task in self.tasks {
                var addedDate = task["task_start_date"].stringValue
                if task["type_id"] == "2" || task["type_id"] == "3" || task["type_id"] == "5" {
                    addedDate = task["start_date"].stringValue
                }
                print ("Start Date: " + addedDate)
                if addedDate == imageView.imageDate.startOfDate.dateString() {
                    imageView.isHidden = false
                    break;
                }
                else
                {
                    imageView.isHidden = true
                }
            }
//            if self.allTaskDates.contains(where: { (date) -> Bool in
//                return date.isSameDay(date: imageView.imageDate.startOfDate)
//            }) {
//                imageView.isHidden = false
//            } else {
//                imageView.isHidden = true
//            }
        }
//        self.imgTaskIndicators[3].isHidden = true
    }
    
    func getTasksForDate(_ date: Date) {
//        self.taskList = []
//        self.tasksOfDate = self.allTasks.filter({ (task) -> Bool in
//            return date.startOfDate.isSameDay(date: task.effectiveDate)
//        })
//        self.taskList = tasksOfDate.map() { $0.title! }
//        self.collectionTask.taskList = self.taskList
//        if self.dateShow {
//            if self.taskList.count > 0 {
//                self.viewTasks.isHidden = false
//                if taskShow {
//                    self.constraintTaskTop.constant = 10
//                } else {
//                    self.constraintTaskTop.constant = -90
//                }
//                self.view.layoutIfNeeded()
//
//                return
//            }
//        }
//        self.viewTasks.isHidden = true
        
        self.taskList = []
//
//        let addedDate = item["added_date"].stringValue.components(separatedBy: " ")[0]
//        print("Added Date:" + addedDate)
//        if addedDate == formatter.string(from: GlobalManager.GetToday()) {
//            self.todayTasks.append(item["name"].stringValue)
//            self.todayTaskIds.append(item["id"].intValue)
//            self.tasks.append(item)
//        }
//
//
        self.dateTasks = [];
        for task in self.tasks {
            var addedDate = task["task_start_date"].stringValue
            if task["type_id"] == "2" || task["type_id"] == "3" || task["type_id"] == "5" {
                addedDate = task["start_date"].stringValue
            }
            if addedDate == date.dateString() {
                self.dateTasks.append(task)
                self.taskList.append(task["name"].stringValue)
            }
        }
        self.collectionTask.taskList = self.taskList
        print(self.taskList)
        if self.dateShow {
            if self.taskList.count > 0 {
                self.viewTasks.isHidden = false
                if taskShow {
                    self.constraintTaskTop.constant = 10
                } else {
                    self.constraintTaskTop.constant = -90
                }
                self.view.layoutIfNeeded()
                
                return
            }
        }
        self.viewTasks.isHidden = true
    }
    
    func initCalenderLabels(_ date: Date) {
        let fmtDate = DateFormatter()
        fmtDate.dateFormat = "dd"
        fmtDate.timeZone = TimeZone(abbreviation: "GMT")
        
        let fmtDay = DateFormatter()
        fmtDay.dateFormat = "E"
        fmtDay.timeZone = TimeZone(abbreviation: "GMT")
        
        let fmtFull = DateFormatter()
        fmtFull.dateFormat = "yyyy-MM-dd"
        fmtFull.timeZone = TimeZone(abbreviation: "GMT")
        var fromGrey: Int?
        var toGrey: Int?
        
        // Get first medication date
        let params = [
            "userid": PreferenceHelper().getId(),
            "choice": "12"
            ] as [String : Any]
        Alamofire.request(DataUtils.APIURL + DataUtils.MYDRUG_URL, method: .post, parameters: params).responseJSON { response in
            if let data = response.result.value {
                //-----PING-----
                if let json : [String:Any] = data as? [String : Any] {
                    let result = json["status"] as? String
                    if result == "true" {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        
                        let firstDate_str = json["first_date"] as? String
                        let medication_createList = json["medication_createList"] as? [NSDictionary]
                        self.medicationCreateList = []
                        for item in medication_createList ?? []{
                            self.medicationCreateList.append(dateFormatter.date(from: item["createat"] as! String)!)
                        }
                        
                        var firstDate = GlobalManager.GetToday()
                        if firstDate_str != "" {
                            firstDate = dateFormatter.date(from: firstDate_str!)!
                        }
    //                    print(firstDate)
                        
                        for i in 0..<7 {
                            if i < 3 {
                                let pickedDate = date.addingTimeInterval(TimeInterval(3600 * 24 * (i - 3)))
                                
                                self.lblOverlayDays[i].text = fmtDate.string(from: pickedDate)
                                if self.firstDrugUsageDate != nil {
                                    if pickedDate >= self.firstDrugUsageDate && pickedDate <= GlobalManager.GetToday() {
                                        if fromGrey == nil {
                                            fromGrey = i
                                        }
                                        if toGrey == nil && fromGrey != nil {
                                            toGrey = i
                                        }
                                        if toGrey != nil {
                                            if i > toGrey! {
                                                toGrey = i
                                            }
                                        }
                                    }
                                }
                            }
                            let weekLabel = String((fmtDay.string(from: date.addingTimeInterval(TimeInterval(3600 * 24 * (i - 3)))) ).prefix(1))
                            self.lblDays[i].text = weekLabel
                            self.lblDates[i].text = fmtDate.string(from: date.addingTimeInterval(TimeInterval(3600 * 24 * (i - 3))))
                            
    //print(fmtDate.string(from: date.addingTimeInterval(TimeInterval(3600 * 24 * (i - 3)))))
                            
                            let calendarDate = date.addingTimeInterval(TimeInterval(3600 * 24 * (i-3)))
                            self.imgTaskIndicators[i].imageDate = calendarDate
                            self.hideUnhideTaskDates()
    //                        print(calendarDate.startOfDate)
                            if (calendarDate < firstDate) {
                                for recognizer in self.vwDays[i].gestureRecognizers ?? [] {
                                    self.vwDays[i].removeGestureRecognizer(recognizer)
                                }
                            } else {
                                let gesture2 = UITapGestureRecognizer(target: self, action:  #selector(self.timeSelect(sender:)))
                                self.vwDays[i].tag = i
                                self.vwDays![i].addGestureRecognizer(gesture2)
                            }
    //                        self.imgTaskIndicators[i].isHidden = true
                        }
                        fmtDate.dateFormat = "MMMM dd"
                        fmtDate.timeZone = TimeZone(abbreviation: "GMT")
                        self.currentDate.text = fmtDate.string(from: self.m_selectedDate).uppercased()
                        
                        self.showSelectedDate()
                    }
                } else {
                    DataUtils.messageShow(view: self, message: "Server Error", title: "")
                }
            }
        }
    }
    
    func initNavBar() {
        navIVArray = [m_ivMorning, m_ivMidDay, m_ivEvening, m_ivNight]
        navVWArray = [m_vwMorning, m_vwMidDay, m_vwEvening, m_vwNight]
        
        addBtnImagePathArray = ["icon_add_more_morning", "icon_add_more_midday", "icon_add_more_evening", "icon_add_more_night"]
        topNavActiveArray = ["morn_on", "midday_on", "even_on", "night_on"]
        topNavDeactiveArray = ["morn_off", "midday_off", "even_off", "night_off"]
        
        selectNavItem(position: 0)
        navColorChange(value: 0)
    }

    func selectNavItem(position: Int) {
        for i in 0...3 {
            if (i != position) {
                navIVArray[i].tintColor = colorDeactive
                navIVArray[i].image = UIImage.init(named: topNavDeactiveArray[i])
            }
        }
        selectedDay = position
//        self.collectionTask.selectedColor = selectedDay
        navIVArray[position].image = UIImage.init(named: topNavActiveArray[position])
        m_curtabPos = position
        child_1!.m_currentColor = navColorActiveArray[position]
        child_1!.getPatientMedications(position)
        navColorChange(value: position)

        pointLabel.textColor = navColorActiveArray[position]
        
        todayBackgroundView.topColor = UIColor(cgColor: NPColorScheme(rawValue: position)!.gradient[0])
        todayBackgroundView.bottomColor = UIColor(cgColor: NPColorScheme(rawValue: position)!.gradient[1])
        
        for i in 0...6 {
            vwTodayFooters[i].topColor = UIColor(cgColor: NPColorScheme(rawValue: position)!.gradient[0])
            vwTodayFooters[i].bottomColor = UIColor(cgColor: NPColorScheme(rawValue: position)!.gradient[1])
            if i != 3 {
                lblDays[i].textColor = NPColorScheme(rawValue: position)!.color
            } else {
                vwTodayFooters[i].topColor = .white
                vwTodayFooters[i].bottomColor = .white
                vwTodayFooters[i].backgroundColor = .white
            }
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

    }
    
    @objc func timeSelect(sender : UITapGestureRecognizer) {
        let tag = sender.view!.tag
        let fmtDate = DateFormatter()
        fmtDate.dateFormat = "MMMM dd"
        fmtDate.timeZone = TimeZone(abbreviation: "GMT")
        
        let today = GlobalManager.GetToday()
        let date = today.addingTimeInterval(TimeInterval(3600 * 24 * (tag - 3)))
        if GlobalManager.compareDate(date, m_selectedDate) {
            popupCalendar()
        }
        for i in 0...6 {
            if (i == tag) {
                self.vwTodayFooters[i].isHidden = false
            } else {
                self.vwTodayFooters[i].isHidden = true
            }
        }
        
        m_selectedDate = today.addingTimeInterval(TimeInterval(3600 * 24 * (tag - 3)))
        self.currentDate.text = fmtDate.string(from: self.m_selectedDate).uppercased()
        self.taskShow = false
//        self.btnShowTask.isHidden = false
        self.getTasksForDate(m_selectedDate)
        self.showSelectedDate()
//        checkWendsdayBubble()
    }
    
    
    
    func checkWendsdayBubble(_ show: Bool) {
        if show {
            wendesdayBubble.isHidden = false
            m_vwRadialRight.isHidden = true
        } else {
            wendesdayBubble.isHidden = true
            m_vwRadialRight.isHidden = false
        }
    }
    
    func showSelectedDate() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        
        if formatter.string(from: m_selectedDate) == formatter.string(from: GlobalManager.GetToday()) {
            currentDateLabel.text = "Today"
        } else {
            let fmtDay = DateFormatter()
            fmtDay.dateFormat = "EEEE"
            fmtDay.timeZone = TimeZone(abbreviation: "GMT")
            currentDateLabel.text = fmtDay.string(from: m_selectedDate)
        }
        
        let fmtFull = DateFormatter()
        fmtFull.dateFormat = "yyyy-MM-dd"
        fmtFull.timeZone = TimeZone(abbreviation: "GMT")
        child_1!.currentDate = formatter.string(from: m_selectedDate)
        child_1!.selectedDate = m_selectedDate
        child_1!.getPatientMedications(m_curtabPos)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private var isFirst = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        m_vwRadialLeft.layer.cornerRadius = m_vwRadialLeft.frame.width / 2
        m_vwRadialRight.layer.cornerRadius = m_vwRadialRight.frame.width / 2
        wendesdayBubble.layer.cornerRadius = wendesdayBubble.frame.width / 2
        lcAgoDay.constant = 146 * buttonBarView.frame.size.width / 355
        
        if isFirst {
            isFirst = false
            self.checkPushNotificationPermission()
        }
    }

    private func checkPushNotificationPermission() {
        SVProgressHUD.show()
        
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings(completionHandler: { (settings) in
            SVProgressHUD.dismiss()

            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            if settings.authorizationStatus == .notDetermined {
                // goto notification permission screen
                DispatchQueue.main.async {
                    let vc = storyBoard.instantiateViewController(withIdentifier: "VCNotificationEnable") as! VCNotificationEnable
                    vc.presentedFrom = NotificationPresentedFrom.HomeTabViewController
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            } else {
                if let token = UserDefaults.standard.string(forKey: "DeviceToken") {
                    print("TOKEN + " + token)
                }
            }
        })
    }

    override func viewDidAppear(_ animated: Bool) {
        Alamofire.request(DataUtils.APIURL + DataUtils.MYDRUG_URL, method: .post, parameters: ["userid":PreferenceHelper().getId(), "choice":"8"])
            .responseJSON(completionHandler: { response in
                DataUtils.customActivityIndicatory(self.view, startAnimate: false)
                if let data = response.result.value {
                    //-----PING-----
                    if let json : [String:Any] = data as? [String : Any] {
                        let result = json["status"] as? String
                        if result == "true" {
                            var firstDrugDate = json["createat"] as? String
                            firstDrugDate = firstDrugDate!.components(separatedBy: " ")[0]
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd"
                            formatter.timeZone = TimeZone(abbreviation: "GMT")
                            self.firstDrugUsageDate = formatter.date(from: firstDrugDate!)
                            
                            let today = GlobalManager.GetToday()
                            self.initCalenderLabels(today)
                        }
                    } else {
                        DataUtils.messageShow(view: self, message: "Server Error", title: "")
                    }
                }
            })
    }

    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        child_1 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "morningviewcontroller1") as? MorningViewController1
        child_1!.m_currentColor = navColorActiveArray[0]
        child_1!.delegate = self
        return [child_1!]
    }
    
    func barSettings() {
        buttonBarView.selectedBar.backgroundColor = UIColor.init(hex: "39d3e3")
        settings.style.buttonBarBackgroundColor = UIColor.init(hex: "ffffff")
        settings.style.buttonBarItemBackgroundColor = UIColor.clear//init(hex: "f7f7fa")
        settings.style.selectedBarBackgroundColor = UIColor.init(hex: "ffffff")
        settings.style.buttonBarItemFont = UIFont(name: "Montserrat-Medium", size: 18)!
        settings.style.selectedBarHeight = 0.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = UIColor.init(hex: "333333")
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarLeftContentInset = 0
        settings.style.buttonBarRightContentInset = 0
        
        m_vwTopContainer.layer.cornerRadius = 15.0
        m_vwTopContainer.addShadow(color: #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8392156863, alpha: 1), alpha: 1, x: 0, y: 5, blur: 15.0)
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else {
                return }
            
            oldCell?.isSelected = false
            oldCell?.isHighlighted = false
            oldCell?.label.textColor = UIColor.init(hex: "333333").withAlphaComponent(0.5)
            
            self?.oldCell = oldCell

            newCell?.isHighlighted = true
            newCell?.label.textColor = UIColor.init(hex: "39d3e3")
            self?.newCell = newCell
        }
    }
    
    func selectDay(value: Int) -> String{
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        
        switch value{
        case 0:
            dateColor = UIColor.init(hex: "39d3e3")
        case 1:
            dateColor = UIColor.init(hex: "397ee3")
        case 2:
            dateColor = UIColor.init(hex: "415ce3")
        case 3:
            dateColor = UIColor.init(hex: "4939e3")
        default:
            break
        }
        pointLabel.textColor = dateColor
        
        if newCell != nil && oldCell != nil {
            selectedDay = value
//            self.collectionTask.selectedColor = selectedDay
            switch value{
            case 0:
                self.newCell?.label.textColor = UIColor.init(hex: "39d3e3")
                self.newCell?.label.font = UIFont(name: "Montserrat-Medium", size: 20)
                self.oldCell?.label.font = UIFont(name: "Montserrat-Medium", size: 18)
                buttonBarView.selectedBar.backgroundColor = UIColor.init(hex: "39d3e3")
            case 1:
                self.newCell?.label.textColor = UIColor.init(hex: "397ee3")
                self.newCell?.label.font = UIFont(name: "Montserrat-Medium", size: 20)
                self.oldCell?.label.font = UIFont(name: "Montserrat-Medium", size: 18)
                buttonBarView.selectedBar.backgroundColor = UIColor.init(hex: "397ee3")
            case 2:
                self.newCell?.label.textColor = UIColor.init(hex: "415ce3")
                self.newCell?.label.font = UIFont(name: "Montserrat-Medium", size: 20)
                self.oldCell?.label.font = UIFont(name: "Montserrat-Medium", size: 18)
                buttonBarView.selectedBar.backgroundColor = UIColor.init(hex: "415ce3")
            case 3:
                self.newCell?.label.textColor = UIColor.init(hex: "4939e3")
                self.newCell?.label.font = UIFont(name: "Montserrat-Medium", size: 20)
                self.oldCell?.label.font = UIFont(name: "Montserrat-Medium", size: 18)
                buttonBarView.selectedBar.backgroundColor = UIColor.init(hex: "4939e3")
            default:
                break
            }
            showSelectedDate()
            return formatter.string(from: m_selectedDate)
        } else {
            return formatter.string(from: m_selectedDate)
        }
        
    }
    
    func navColorChange(value: Int) {
        m_vwRadialLeft.backgroundColor = navColorActiveArray[value]
        m_vwRadialRight.backgroundColor = navColorActiveArray[value]
        wendesdayBubble.backgroundColor = navColorActiveArray[value]
        
        m_fabAdd.buttonImage = UIImage.init(named: addBtnImagePathArray[value])
    }
    
    @objc func dateShow(_ sender: Any) {
        dateShow = !dateShow
        showCalendar(bShow: dateShow)
        checkWendsdayBubble(dateShow)
    }
    
    func popupCalendar(){
        let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CalenderVC") as! CalenderVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.medicationCreateList = self.medicationCreateList
        vc.m_themeIdx = selectedDay
        vc.delegate = self
        self.present(vc, animated: true)
    }
    
    func showCalendar(bShow: Bool) {
        if bShow == true {
            m_cnstTopContainerHeight.constant = 183
            m_vwSeperate.isHidden = false
            m_stvwCalender.isHidden = false
            vwAgoDays.isHidden = false
            viewTasks.isHidden = false
            self.getTasksForDate(m_selectedDate)
        } else {
            m_cnstTopContainerHeight.constant = 83
            m_vwSeperate.isHidden = true
            m_stvwCalender.isHidden = true
            vwAgoDays.isHidden = true
            viewTasks.isHidden = true
            self.taskShow = false
            
        }
        self.constraintScrollTop.constant = -10
    }
    
    @IBAction func tapNavMorning(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Action.UpdateTabBarItem, object: ["index": "0"])
        selectNavItem(position: 0)
    }
    
    @IBAction func tapNavMidDay(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Action.UpdateTabBarItem, object: ["index": "1"])
        selectNavItem(position: 1)
    }
    
    @IBAction func tapNavEvening(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Action.UpdateTabBarItem, object: ["index": "2"])
        selectNavItem(position: 2)
    }
    
    @IBAction func tapNavNight(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Action.UpdateTabBarItem, object: ["index": "3"])
        selectNavItem(position: 3)
    }
    
    @IBAction func addMedication(_ sender: Any?) {
        UserDefaults.standard.set("y", forKey: "logged")
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddDrugNavigationVC") as! UINavigationController
        UserDefaults.standard.set("true", forKey: "reloadProfileItems")
        viewController.modalPresentationStyle = .overFullScreen
        present(viewController, animated: false, completion: nil)
    }
    
    @objc func onDoubleTapHomeTab(noti: Notification) {
        selectNavItem(position: 0)
        navColorChange(value: 0)
        NotificationCenter.default.post(name: Notification.Name.Action.UpdateTabBarItem, object: ["index": "0"])
        child_1?.setTableViewScrollTop()
    }
    
    @IBAction func closeTasks(_ sender: Any) {
        self.taskShow = false
    }
}


extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem as Any, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

public class CellTask: UITableViewCell{
    @IBOutlet weak var lblName: UILabel!
    public func SetData(_ name: String)
    {
        lblName.text = name
    }
}

extension HomeTabViewController: CalenderVCDelegate {
    func closeCalendarDialog(_ date: Date) {
        self.getAlltasks(Date.startOfToday())
        let fmtDate = DateFormatter()
        fmtDate.dateFormat = "MMMM dd"
        fmtDate.timeZone = TimeZone(abbreviation: "GMT")
        
        self.m_selectedDate = date
        self.currentDate.text = fmtDate.string(from: self.m_selectedDate).uppercased()
        self.showSelectedDate()
        
        let today = GlobalManager.GetToday()
        for i in 0...6 {
            let pickDate = today.addingTimeInterval(TimeInterval(3600 * 24 * (i - 3)))
            if fmtDate.string(from: date) == fmtDate.string(from: pickDate) {
                vwTodayFooters[i].isHidden = false
            } else {
                vwTodayFooters[i].isHidden = true
            }
        }
    }
}

extension HomeTabViewController: CollectionViewDelegate {
    func didSelectItemAt(_ indexPath: IndexPath) {
        print("aaaaaa")
        print(indexPath)
        print(self.dateTasks[indexPath.row])
        var task = self.dateTasks[indexPath.row];
        print (task)
        switch task["type_id"].stringValue {
            case "1":
                if (task["details"]["items"].arrayValue.count > 0){
                    let checkListVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckListViewController") as! CheckListViewController
                    checkListVC.task = task
                    checkListVC.modalPresentationStyle = .overFullScreen
                    self.present(checkListVC, animated: true)
                } else if (task["details"]["sequences"]["items"].arrayValue.count > 0){
                    let sequenceListVC = self.storyboard?.instantiateViewController(withIdentifier: "SequenceListViewController") as! SequenceListViewController
                    sequenceListVC.task = task
                    sequenceListVC.modalPresentationStyle = .overFullScreen
                    self.present(sequenceListVC, animated: true)
                }
            case "2":
                
                UserDefaults.standard.set("y", forKey: "logged")
                let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AddDrugNavigationVC") as! UINavigationController
                UserDefaults.standard.set("true", forKey: "reloadProfileItems")
                viewController.modalPresentationStyle = .overFullScreen
                present(viewController, animated: false, completion: nil)
            case "3":
                let treatmentVC = self.storyboard?.instantiateViewController(withIdentifier: "TreatmentViewController") as! TreatmentViewController
                treatmentVC.task = task
                treatmentVC.modalPresentationStyle = .overFullScreen
                self.present(treatmentVC, animated: true)
            case "4":
                let appointmentVC = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentViewController") as! AppointmentViewController
                appointmentVC.task = task
                appointmentVC.modalPresentationStyle = .overFullScreen
                self.present(appointmentVC, animated: true)
            case "5":
                let taskDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "TaskDetailsViewController") as! TaskDetailsViewController
                taskDetailsVC.task = task
                taskDetailsVC.modalPresentationStyle = .overFullScreen
                self.present(taskDetailsVC, animated: true)
            case "6":
                let questionVC = self.storyboard?.instantiateViewController(withIdentifier: "QuestionaireViewController") as! QuestionaireViewController
                questionVC.task = task
                questionVC.modalPresentationStyle = .overFullScreen
                self.present(questionVC, animated: true)
            default:
                break
        }
    }
}

extension HomeTabViewController {//: EmptyStateDelegate {
    func addMedicationTappedFromEmptyState() {
        self.addMedication(nil)
    }
//    func addTapped() {
//        print("Add Tapped")
//        self.addMedication(nil)
//    }
}

//class NXCareTask: NSObject, OCKTask {
//    var details: TaskDetails!
//}
