//
//  RefillPillsContinueViewController.swift
//  Nexpil
//
//  Created by Golder on 3/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class RefillPillsContinueViewController: RefillPillsBaseViewController {

    @IBOutlet weak var lblMedicineInfo: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var constraintMedicineInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintMedicineInfoBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
    }
    
    private func initUI() {
        if self.mode == .quantity {
            lblMedicineInfo.text = "\(self.medicineData.medicationname) - \(self.medicineData.strength)"
        } else {
            constraintMedicineInfoHeight.constant = 0
            constraintMedicineInfoBottom.constant = 0
        }
        lblDescription.text = "Place your \(self.medicineData.type) onto a clean surface and take a picture."
    }
    
    @IBAction func onBtnContinue(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate.onContinue(medicineData: self.medicineData)
        }
    }
    
    @IBAction func onBtnBack(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate.onStartUp(medicineData: self.medicineData)
        }
    }
}
