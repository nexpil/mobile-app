//
//  RefillPillsBaseViewController.swift
//  Nexpil
//
//  Created by Golder on 3/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol RefillPillsDelegate {
    func onStartUp(medicineData: RefillMedicationShortData)
    func onRefill(medicineData: RefillMedicationShortData)
    func onContinue(medicineData: RefillMedicationShortData)
    func onManually(medicineData: RefillMedicationShortData)
    func onDone(medicineData: RefillMedicationShortData, pillsAmount: Int)
}

enum RefillMode {
    case quantity
    case refill
}

class RefillMedicationShortData {
    var id              : String = "-1"
    var medicationname  : String = ""
    var strength        : String = ""
    var type            : String = ""
    var quantity        : Int = 0
    
    init(data: [String: Any]) {
        self.id = data["id"] as? String ?? "-1"
        self.medicationname = data["medicationname"] as? String ?? ""
        self.strength = data["strength"] as? String ?? ""
        self.type = data["type"] as? String ?? ""
        self.quantity = Int(data["quantity"] as? String ?? "0")!
    }
}

class RefillPillsBaseViewController: KeyboardViewController {
    
    var mode = RefillMode.refill

    var medicineData: RefillMedicationShortData!
    var delegate    : RefillPillsDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        Global_ShowFrostGlass(self.view)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Global_HideFrostGlass()
    }
}
