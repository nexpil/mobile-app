//
//  AppointmentViewController.swift
//  Nexpil
//
//  Created by CROCODILE on 16.01.2021.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class AppointmentViewController: UIViewController {
    @IBOutlet weak var lblDoctorInfo: UILabel!
    @IBOutlet weak var lblDoctorPhone: UILabel!
    @IBOutlet weak var lblScheduleInfo: UILabel!
    @IBOutlet weak var lblDoctorName: UILabel!
    @IBOutlet weak var lblScheduleDate: UILabel!
    @IBOutlet weak var lblScheduleTime: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var btnAm1: UIButton!
    @IBOutlet weak var btnAm2: UIButton!
    @IBOutlet weak var btnPm1: UIButton!
    @IBOutlet weak var btnPm2: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var task = JSON()
    var scheduleTime = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initUI()
        self.initData()
        // Do any additional setup after loading the view.
    }
    
    
    func initData(){
        if(self.task["details"]["phone_number"].stringValue != "" && self.task["details"]["address"].stringValue != ""){
            self.lblDoctorPhone.text = self.task["details"]["phone_number"].stringValue
            self.lblAddress.text = self.task["details"]["address"].stringValue
        }
        if(self.task["dr_name"].stringValue != ""){
            self.lblScheduleInfo.text = "Pick from " + self.task["dr_name"].stringValue + "’s available times below:"
            self.lblDoctorName.text = self.task["dr_name"].stringValue
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/YYYY"
        formatter.timeZone = TimeZone(abbreviation: "GMT")
        let today = formatter.string(from: GlobalManager.GetToday())
        self.lblScheduleDate.text = today
    }

    func initUI(){
        self.containerView.isHidden = true
        
        self.btnSchedule.layer.cornerRadius = 24
        self.btnSchedule.layer.masksToBounds = true
        self.btnSchedule.isEnabled = false
        
        self.resetBtnStyle()
    }
    
    
    func resetBtnStyle(){
        self.btnAm1.layer.borderWidth = 2
        self.btnAm1.layer.cornerRadius = 5
        self.btnAm1.layer.backgroundColor = UIColor.white.cgColor
        self.btnAm1.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnAm1.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
        
        self.btnAm2.layer.borderWidth = 2
        self.btnAm2.layer.cornerRadius = 5
        self.btnAm2.layer.backgroundColor = UIColor.white.cgColor
        self.btnAm2.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnAm2.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
        
        self.btnPm1.layer.borderWidth = 2
        self.btnPm1.layer.cornerRadius = 5
        self.btnPm1.layer.backgroundColor = UIColor.white.cgColor
        self.btnPm1.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnPm1.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
        
        self.btnPm2.layer.borderWidth = 2
        self.btnPm2.layer.cornerRadius = 5
        self.btnPm2.layer.backgroundColor = UIColor.white.cgColor
        self.btnPm2.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnPm2.setTitleColor(UIColor.init(hex: "333333"), for: .normal)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func completeSchedule(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnAm1Clicked(_ sender: UIButton) {
        self.scheduleTime = "10:00 am"
        
        self.resetBtnStyle()
        self.btnSchedule.isEnabled = true
        
        self.btnAm1.layer.borderWidth = 2
        self.btnAm1.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnAm1.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnAm1.setTitleColor(UIColor.white, for: .normal)
        self.lblScheduleTime.text = self.scheduleTime
        self.containerView.isHidden = false
    }
    
    @IBAction func btnAm2Clicked(_ sender: UIButton) {
        self.scheduleTime = "11:30 am"
        
        self.resetBtnStyle()
        self.btnSchedule.isEnabled = true
        
        self.btnAm2.layer.borderWidth = 2
        self.btnAm2.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnAm2.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnAm2.setTitleColor(UIColor.white, for: .normal)
        self.lblScheduleTime.text = self.scheduleTime
        self.containerView.isHidden = false
        
        
    }
    
    @IBAction func btnPm1Clicked(_ sender: UIButton) {
        self.scheduleTime = "12:30 pm"
        
        self.resetBtnStyle()
        self.btnSchedule.isEnabled = true
        
        self.btnPm1.layer.borderWidth = 2
        self.btnPm1.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnPm1.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnPm1.setTitleColor(UIColor.white, for: .normal)
        self.lblScheduleTime.text = self.scheduleTime
        self.containerView.isHidden = false
    }
    
    @IBAction func btnPm2Clicked(_ sender: UIButton) {
        self.scheduleTime = "2:45 pm"
        
        self.resetBtnStyle()
        self.btnSchedule.isEnabled = true
        
        self.btnPm2.layer.borderWidth = 2
        self.btnPm2.layer.backgroundColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnPm2.layer.borderColor = UIColor.init(hex: "5d60e9").cgColor
        self.btnPm2.setTitleColor(UIColor.white, for: .normal)
        self.lblScheduleTime.text = self.scheduleTime
        self.containerView.isHidden = false
    }

}
