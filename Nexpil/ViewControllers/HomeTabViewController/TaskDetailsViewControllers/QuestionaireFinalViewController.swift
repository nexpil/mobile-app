//
//  QuestionaireFinalViewController.swift
//  Nexpil
//
//  Created by CROCODILE on 16.01.2021.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class QuestionaireFinalViewController: UIViewController {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnComplete: UIButton!
    
    var lastIndex = 0
    var task = JSON()
    var stepCompleted = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
        // Do any additional setup after loading the view.
    }
    
    func initUI(){
        self.btnComplete.layer.cornerRadius = 24
        self.btnComplete.layer.masksToBounds = true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func goHomeScreen(_ sender: UIButton) {
        let homeTabVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeTabViewController") as! HomeTabViewController
        homeTabVC.modalPresentationStyle = .overFullScreen
        self.present(homeTabVC, animated: true)
    }
    
    @IBAction func backScreen(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

}
