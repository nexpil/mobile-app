//
//  SequenceListViewController.swift
//  Nexpil
//
//  Created by CROCODILE on 21.01.2021.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class SequenceListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var completeBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    var task = JSON()
    var timeList = [JSON]()
    var contentList = [JSON]()
    var checkedArray = [Bool]()
    var checkedCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getList()
        tableView.reloadData()
        self.completeBtn.layer.cornerRadius = 24
        self.completeBtn.layer.masksToBounds = true
        // Do any additional setup after loading the view.
    }
    
    func getList(){
        let values = JSON(self.task["details"]["sequences"]["items"])
        for item in values.arrayValue {
            self.timeList.append(item["sequence_time"])
            self.contentList.append(item["instruction"])
            self.checkedArray.append(false)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension SequenceListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.timeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sequenceCell", for: indexPath) as! sequenceCell
        let time = self.timeList[indexPath.row].stringValue
        let content = self.contentList[indexPath.row].stringValue
        cell.timeLbl.text = time
        cell.contentLbl.text = content
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didselected \(indexPath.row)")
        let cell = tableView.cellForRow(at: indexPath) as! sequenceCell
        if (self.checkedArray[indexPath.row]){
            cell.gradientView.topColor = UIColor.white
            cell.gradientView.bottomColor = UIColor.white
            cell.gradientView.shadowColor = UIColor.systemGray
            cell.contentLbl.textColor = UIColor.systemGray
            let image = UIImage(named: "Drkpurple_Take Medication")
            cell.btnCheck.setImage(image, for: .normal)
            self.checkedArray[indexPath.row] = false
            self.checkedCount -= 1
        } else {
            cell.gradientView.topColor = UIColor.init(hex: "877CEC")
            cell.gradientView.bottomColor = UIColor.init(hex: "877CEC")
            cell.gradientView.shadowColor = UIColor.systemGray
            cell.contentLbl.textColor = UIColor.white
            let image = UIImage(named: "check_asneed")
            cell.btnCheck.setImage(image, for: .normal)
            self.checkedArray[indexPath.row] = true
            self.checkedCount += 1
        }
        if (self.checkedCount > 0){
            self.completeBtn.isEnabled = true
        }else if (self.checkedCount == 0){
            self.completeBtn.isEnabled = false
        }
    }
    
    @IBAction func complete(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

class sequenceCell: UITableViewCell {
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var gradientView: GradientView!
}
