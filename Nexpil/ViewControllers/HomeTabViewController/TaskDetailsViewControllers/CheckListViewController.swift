//
//  CheckListViewController.swift
//  Nexpil
//
//  Created by CROCODILE on 19.01.2021.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class CheckListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var completeBtn: UIButton!
    
    var task = JSON()
    var list = [JSON]()
    var checkedArray = [Bool]()
    var checkedCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.getList()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.reloadData()
        
        self.completeBtn.layer.cornerRadius = 24
        self.completeBtn.layer.masksToBounds = true
        
        self.completeBtn.isEnabled = false
        
        let initialTaskStatus = self.task["status"].intValue
        if initialTaskStatus == 0 {
            self.completeBtn.isEnabled = true
            self.completeBtn.setTitle("Complete", for: .normal)
        } else {
            self.completeBtn.isEnabled = false
            self.completeBtn.setTitle("Completed", for: .normal)
        }
        
    }
    
    func getList(){
        let values = JSON(self.task["details"]["items"])
        for item in values.arrayValue {
            self.list.append(item)
            self.checkedArray.append(false)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func back(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func complete(_ sender: UIButton) {
        let preference = PreferenceHelper()
        let params = [
            "patient_id" : preference.getId(),
            "task_id" : self.task["id"].intValue
            ] as [String : Any]
        DataUtils.customActivityIndicatory(self.view,startAnimate: true);
        Alamofire.request("https://twilio.nexp.xyz/webapi/v1/mark-task-as-complete", method: .post, parameters: params)
            .responseJSON(completionHandler: { response in
                DataUtils.customActivityIndicatory(self.view,startAnimate: false)
                
                switch response.result {
                    case .success(let value):
                        let values = JSON(value)
                        print(values)
                        if let nav = self.navigationController {
                            nav.popViewController(animated: true)
                        } else {
                            self.dismiss(animated: true, completion: nil)
                        }
                    case .failure(let error):
                        print(error)
                }
            })
        
    }
}


extension CheckListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! tableCell
        let title = self.list[indexPath.row].stringValue
        cell.txtLbl.text = title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didselected \(indexPath.row)")
        let cell = tableView.cellForRow(at: indexPath) as! tableCell
        
        if (self.checkedArray[indexPath.row]){
            cell.gradientView.topColor = UIColor.white
            cell.gradientView.bottomColor = UIColor.white
            cell.gradientView.shadowColor = UIColor.systemGray
            cell.txtLbl.textColor = UIColor.systemGray
            let image = UIImage(named: "Drkpurple_Take Medication")
            cell.btnCheck.setImage(image, for: .normal)
            self.checkedArray[indexPath.row] = false
            self.checkedCount -= 1
        } else {
            cell.gradientView.topColor = UIColor.init(hex: "877CEC")
            cell.gradientView.bottomColor = UIColor.init(hex: "877CEC")
            cell.gradientView.shadowColor = UIColor.systemGray
            cell.txtLbl.textColor = UIColor.white
            let image = UIImage(named: "check_asneed")
            cell.btnCheck.setImage(image, for: .normal)
            self.checkedArray[indexPath.row] = true
            self.checkedCount += 1
        }
        if (self.checkedCount > 0) {
            self.completeBtn.isEnabled = true
            self.completeBtn.setTitle("Complete", for: .normal)
        } else if (self.checkedCount == 0) {
            self.completeBtn.isEnabled = false
        }
    }
}

class tableCell: UITableViewCell {
    @IBOutlet weak var txtLbl: UILabel!
    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var gradientView: GradientView!
}
