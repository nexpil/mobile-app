//
//  TreatmentViewController.swift
//  Nexpil
//
//  Created by CROCODILE on 19.01.2021.
//  Copyright © 2021 Admin. All rights reserved.
//

import UIKit
import SwiftyJSON

class TreatmentViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var completeBtn: UIButton!
    var checkedArray = [Bool]()
    var checkedCount = 0
    
    var task = JSON()
    var list = [JSON]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.completeBtn.layer.cornerRadius = 24
        self.completeBtn.layer.masksToBounds = true
        
        for i in 0 ..< 7 {
            self.checkedArray.append(false)
        }
    }
    
    @IBAction func back(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func complete(_ sender: UIButton) {
        if let nav = self.navigationController {
            nav.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }

}

extension TreatmentViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! tableCell
        let title = self.task["name"].stringValue
        cell.txtLbl.text = title
        
        if (self.checkedArray[indexPath.section]){
            cell.gradientView.topColor = UIColor.init(hex: "877CEC")
            cell.gradientView.bottomColor = UIColor.init(hex: "877CEC")
            cell.gradientView.shadowColor = UIColor.systemGray
            cell.txtLbl.textColor = UIColor.white
            let image = UIImage(named: "check_asneed")
            cell.btnCheck.setImage(image, for: .normal)
        } else {
            cell.gradientView.topColor = UIColor.white
            cell.gradientView.bottomColor = UIColor.white
            cell.gradientView.shadowColor = UIColor.systemGray
            cell.txtLbl.textColor = UIColor.systemGray
            let image = UIImage(named: "Drkpurple_Take Medication")
            cell.btnCheck.setImage(image, for: .normal)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 16
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let returnedView = UIView()
        returnedView.backgroundColor = UIColor.white
        let label = UILabel(frame: CGRect(x: 8, y: 0, width: 100, height: 16))
        label.font = UIFont(name: "Montserrat Medium", size: 20)
        label.textColor = UIColor.darkGray
        let formatter = DateFormatter()
        let startDate = self.task["start_date"].stringValue;
        formatter.dateFormat = "YYYY-MM-dd"
        let sDate = formatter.date(from: startDate)
        let addedDate = Calendar.current.date(byAdding: .day, value: section, to: sDate!)
        formatter.dateFormat = "M/dd"
        label.text = formatter.string(from: addedDate!)
        returnedView.addSubview(label)
        return returnedView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("didselected \(indexPath.section)")
//        let cell = tableView.cellForRow(at: indexPath) as! tableCell
        if (self.checkedArray[indexPath.section]){
            self.checkedArray[indexPath.section] = false
            self.checkedCount -= 1
        } else {
            self.checkedArray[indexPath.section] = true
            self.checkedCount += 1
        }
        if (self.checkedCount > 0){
            self.completeBtn.isEnabled = true
        }else if (self.checkedCount == 0){
            self.completeBtn.isEnabled = false
        }
        self.tableView.reloadData()
    }
}
