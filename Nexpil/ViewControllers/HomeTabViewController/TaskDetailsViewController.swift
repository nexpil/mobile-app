//
//  TaskDetailsViewController.swift
//  Nexpil
//
//  Created by Arif on 3/22/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import MapKit
import CareKit
import SwiftyJSON

class TaskDetailsViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var detailsTableView: UITableView!
    
    @IBOutlet weak var constraintLocationHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintPhoneHeight: NSLayoutConstraint!
    
    private var taskLocation: CLLocationCoordinate2D!
    private var userLocation: CLLocationCoordinate2D!
    var task: JSON!
    
    var detailsItems = [String()]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.initUI()
    }
    
    private func initUI() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)!
        
        if let task = self.task {
//            let dateText = formatter.string(from: task["start_date"].stringValue)
//            var timeText = ""
//            if let time = task.userInfo?["start_time"] {
//                timeText = ", " + time
//            }
            
            let detailValues = JSON(self.task["details"])
                        
            
            lblDate.text = task["added_date"].stringValue
            lblName.text = task["name"].stringValue
            
            lblDescription.text = detailValues["comments"].stringValue
            /*if let description = task.userInfo?["description"], description.count > 0 {
                lblDescription.text = description
            } else {
                lblDescription.text = "No description"
            }*/
            
            /*if let detailsJSON = task.userInfo?["details"] {
                if let data = detailsJSON.data(using: .utf8) {
                    do {
                        if let detailsDic = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any], let detailsArray = detailsDic["items"] as? [String] {
                            self.detailsItems = detailsArray
                            self.detailsTableView.reloadData()
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                }
            }
            var hasLocation = false
            if let strLocation = task.userInfo?["location"] {
                let strTrim = strLocation.trimmingCharacters(in: .whitespacesAndNewlines)
                let long_lati = strTrim.split(separator: ",")
                if long_lati.count == 2, let longitude = Double(long_lati[0]), let latitude = Double(long_lati[1]) {
                    hasLocation = true

                    self.taskLocation = CLLocationCoordinate2DMake(CLLocationDegrees(exactly: longitude)!, CLLocationDegrees(exactly: latitude)!)

                    let regionRadius: CLLocationDistance = 1000
                    let coordinateRegion = MKCoordinateRegionMakeWithDistance(taskLocation, regionRadius, regionRadius)
                    mapView.setRegion(coordinateRegion, animated: true)
                    
                    
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = taskLocation
                    mapView.addAnnotation(annotation)
                }
            }*/
            
            mapView.showsUserLocation = true
            
//            if !hasLocation && detailValues["phone_number"] == nil {
//                constraintLocationHeight.constant = 0
//                constraintPhoneHeight.constant = 0
//            }
        }
    }
    
    @IBAction func onBtnLocation(_ sender: Any) {
        guard let userLocation = self.userLocation else {
            showAlert(self, title: nil, message: "Getting your location...")
            return
        }
        
        guard let taskLocation = self.taskLocation else {
            showAlert(self, title: nil, message: "There is no location for this task.")
            return
        }
        
        let request = MKDirections.Request()
        request.source = MKMapItem(placemark: MKPlacemark(coordinate: userLocation, addressDictionary: nil))
        request.destination = MKMapItem(placemark: MKPlacemark(coordinate: taskLocation, addressDictionary: nil))
        request.requestsAlternateRoutes = true
        request.transportType = .automobile

        let directions = MKDirections(request: request)

        directions.calculate { [unowned self] response, error in
            guard let unwrappedResponse = response else { return }

            for route in unwrappedResponse.routes {
                self.mapView.add(route.polyline)
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
            }
        }
    }
    
    @IBAction func onBtnPhone(_ sender: Any) {
        let detailValues = JSON(self.task["details"])
        let phoneNo = detailValues["phone_no"].stringValue
        if phoneNo == nil {
            showAlert(self, title: nil, message: "There is no phone number for this task.")
            return
        }
        if let url = URL(string: "tel://\(phoneNo)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            showAlert(self, title: nil, message: "Not available")
        }
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension TaskDetailsViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3
        return renderer
    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
        self.userLocation = userLocation.location?.coordinate
    }
}

extension TaskDetailsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.detailsItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailsItemTableViewCell", for: indexPath) as! DetailsItemTableViewCell
        cell.itemText.text = self.detailsItems[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
}

class DetailsItemTableViewCell: UITableViewCell {
    @IBOutlet weak var itemText: UILabel!
    @IBOutlet weak var itemBg: GradientView!
}
