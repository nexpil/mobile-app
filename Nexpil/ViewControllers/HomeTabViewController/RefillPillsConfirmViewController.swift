//
//  RefillPillsConfirmViewController.swift
//  Nexpil
//
//  Created by Golder on 3/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class RefillPillsConfirmViewController: RefillPillsBaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMedicineInfo: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblTitle.text = self.medicineData.quantity >= 2 ? "\(self.medicineData.quantity) Pills Left!" : "\(self.medicineData.quantity) Pill Left!"
        lblMedicineInfo.text = "\(self.medicineData.medicationname) - \(self.medicineData.strength)"
    }
    
    @IBAction func onBtnRefill(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate.onRefill(medicineData: self.medicineData)
        }
    }
    
    @IBAction func onBtnHaveMoreLeft(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate.onManually(medicineData: self.medicineData)
        }
    }
    
    @IBAction func onBtnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
