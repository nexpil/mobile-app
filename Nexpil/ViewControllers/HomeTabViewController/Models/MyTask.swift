//
//  MyTask.swift
//  Nexpil
//
//  Created by Arif on 11/12/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct MyTask: Codable {
    var id: Int?
    var typeId, taskGroupId, name, datumDescription: String?
    var details: TaskDetails?
    var startDate, endDate, addedDate, patientId: String?
    var physicianId, isTemplate, templateName, createdAt: String?
    var updatedAt: String?
    var taskType: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case typeId = "type_id"
        case taskGroupId = "task_group_id"
        case name
        case datumDescription = "description"
        case details
        case startDate = "start_date"
        case endDate = "end_date"
        case addedDate = "added_date"
        case patientId = "patient_id"
        case physicianId = "physician_id"
        case isTemplate = "is_template"
        case templateName = "template_name"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
}

struct TaskDetails: Codable {
    var items: [String]?
    var sequences: Sequences?
    var questions: Questions?
    var frequency: String?
    var link: String?
    var strength, quantity, dosage, refillNumber: String?
    var phoneNumber, address: String?
    var comments: String?

    enum CodingKeys: String, CodingKey {
        case items, sequences, frequency, link, strength, quantity, dosage
        case refillNumber = "refill_number"
        case phoneNumber = "phone_number"
        case address
        case comments
    }
}

// MARK: - Sequences
struct Sequences: Codable {
    var medicationsToStop: String?
    var items: [TaskItem]?

    enum CodingKeys: String, CodingKey {
        case medicationsToStop = "medications_to_stop"
        case items
    }
}

// MARK: - Questions
struct Questions: Codable {
    var type: String?
    var question: String?
    var answers: [AnswerItem]?

    enum CodingKeys: String, CodingKey {
        case type
        case question
        case answers
    }
}


// MARK: - Item
struct TaskItem: Codable {
    var sequenceTime, instruction: String?

    enum CodingKeys: String, CodingKey {
        case sequenceTime = "sequence_time"
        case instruction
    }
}

// MARK: - Answer
struct AnswerItem: Codable {
    var answer: String?

    enum CodingKeys: String, CodingKey {
        case answer
    }
}
