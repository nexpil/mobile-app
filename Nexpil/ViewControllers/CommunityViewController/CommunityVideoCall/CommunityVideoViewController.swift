//
//  CommunityVideoViewController.swift
//  Nexpil
//
//  Created by Arif on 7/20/20.
//  Copyright © 2020 Admin. All rights reserved.

//https://video-app-1745-dev.twil.io?passcode=3192571745

import UIKit

import TwilioVideo
import TwilioChatClient

class CommunityVideoViewController: UIViewController {
    
    // Configure remote URL to fetch token from
    var tokenUrl = "http://twilio.nexp.xyz/nexpil/video_access_token.php"
    
    // Video SDK components
    var room: Room?
    var camera: CameraSource?
    var localVideoTrack: LocalVideoTrack?
    var localAudioTrack: LocalAudioTrack?
    var remoteParticipant: RemoteParticipant?
    var participants: [Participant] = []
    var isSelf = true
    var index = 0
    var chatChannel: TCHChannel?
    
    // `VideoView` created from a storyboard
    @IBOutlet weak var previewView: VideoView!
    
    @IBOutlet weak var disconnectButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var flipButton: UIButton!
    @IBOutlet weak var collectionUsers: UICollectionView!
    @IBOutlet weak var viewNoVideo: UIView!
    @IBOutlet weak var imgNoVideoProfile: UIImageView!
    @IBOutlet weak var imgNoVideoBg: UIImageView!
    @IBOutlet weak var blurNoVideo: UIVisualEffectView!
    
    var roomName = ""
    
    deinit {        
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
    }
    
    let frontCamera = CameraSource.captureDevice(position: .front)
    let backCamera = CameraSource.captureDevice(position: .back)
    
//    var chatManager = ChatManager(withChannelName: "#" + App.roomName, friendlyChannelName: "#" + App.roomName, identity: PreferenceHelper().getIdentity())
    
    // MARK:- UIViewController
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.chatManager.delegate = self
        self.roomName = App.roomName
        self.setupCollectionView()
        self.startPreview()
        self.connect()
        runAfter(0) {
            self.chatLogin()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func chatLogin() {
        if NexpilManager.shared.chatManager.client != nil {
            if NexpilManager.shared.channels.count > 0 {
                if let channel = NexpilManager.shared.channels.first(where: { (ch) -> Bool in
                    return ch.uniqueName == self.roomName
                }) {
                    self.chatChannel = channel
                }
            } else {
                NexpilManager.shared.chatManager.client?.channelsList()?.channel(withSidOrUniqueName: self.roomName, completion: { (result, channel) in
                    if result.isSuccessful(), let channel = channel {
                        self.chatChannel = channel
                    }
                })
            }
        } else {
            //CHAT LOGIN IN PROCESS
            runAfter(2) {
                self.chatLogin()
            }
        }
    }
    
    func setupCollectionView() {
        self.collectionUsers.registerCellNib(VideoCallUserCell.self)
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 20
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: 80, height: 80)
        self.collectionUsers.collectionViewLayout = layout
    }
    
    func connect() {
        let preference = PreferenceHelper()
        let name = preference.getId()
        
        //?room_name=TESTROOM&identity=Arif%20Vadkey
        let tokenURL = self.tokenUrl + "?identity=\(name)&roomName=\(roomName)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        TokenUtils.fetchToken(url: tokenURL) { (token: TwilioToken!, error: Error!) in
            if error == nil {
                self.prepareLocalMedia()
                
                let connectOptions = ConnectOptions(token: token.accessToken) { (builder) in
                    builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [LocalAudioTrack]()
                    builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [LocalVideoTrack]()
                    
                    if let preferredAudioCodec = TwilioSettings.shared.audioCodec {
                        builder.preferredAudioCodecs = [preferredAudioCodec]
                    }
                    
                    if let preferredVideoCodec = TwilioSettings.shared.videoCodec {
                        builder.preferredVideoCodecs = [preferredVideoCodec]
                    }
                    
                    if let encodingParameters = TwilioSettings.shared.getEncodingParameters() {
                        builder.encodingParameters = encodingParameters
                    }
                    
                    if let signalingRegion = TwilioSettings.shared.signalingRegion {
                        builder.region = signalingRegion
                    }
                    
                    builder.roomName = self.roomName
                }
                
                self.room = TwilioVideoSDK.connect(options: connectOptions, delegate: self)
                
                self.logMessage(messageText: "Attempting to connect to room \(self.roomName)")
                
                self.showRoomUI(inRoom: true)
            } else {
                self.logMessage(messageText: error.localizedDescription)
            }
        }
    }
    
    // MARK:- IBActions
    @IBAction func disconnect(_ sender: UIButton) {
        if self.room != nil {
            self.room!.disconnect()
        }
        self.dismiss(animated: true)
//        logMessage(messageText: "Attempting to disconnect from room \(room!.name)")
    }
    
//    @IBAction func toggleMic(sender: AnyObject) {
//        if (self.localAudioTrack != nil) {
//            self.localAudioTrack?.isEnabled = !(self.localAudioTrack?.isEnabled)!
//
//            // Update the button title
//            if (self.localAudioTrack?.isEnabled == true) {
//                self.micButton.setTitle("Mute", for: .normal)
//            } else {
//                self.micButton.setTitle("Unmute", for: .normal)
//            }
//        }
//    }
    
//    @IBAction func toggleVideo(sender: AnyObject) {
//        if (self.localVideoTrack != nil) {
//            self.localVideoTrack?.isEnabled = !(self.localVideoTrack?.isEnabled)!
//
//            if (self.localVideoTrack?.isEnabled == true) {
//                self.videoButton.setTitle("Video", for: .normal)
//            } else {
//                self.videoButton.setTitle("No Video", for: .normal)
//            }
//        }
//    }
    
    func setNoVideoProfileImage() {
        let me = PreferenceHelper()
        if let image = me.getUserImage(), image != "" {
            let url = URL(string: DataUtils.PROFILEURL + image)
            imgNoVideoProfile.kf.setImage(with: url)
            imgNoVideoBg.kf.setImage(with: url)
        }
    }
    
    // MARK:- Private
    func startPreview() {
        self.setNoVideoProfileImage()
        if PlatformUtils.isSimulator {
            self.viewNoVideo.isHidden = false
            return
        }
        
        if (frontCamera != nil || backCamera != nil) {
            let options = CameraSourceOptions { (builder) in
                if #available(iOS 13.0, *) {
                    builder.orientationTracker = UserInterfaceTracker(scene: UIApplication.shared.keyWindow!.windowScene!)
                }
            }
            camera = CameraSource(options: options, delegate: self)            
            localVideoTrack = LocalVideoTrack(source: camera!, enabled: true, name: "Camera")
                        
            localVideoTrack!.addRenderer(self.previewView)
            logMessage(messageText: "Video track created")
            
            if (frontCamera != nil && backCamera != nil) {
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.flipCamera))
                tap.numberOfTapsRequired = 2
                self.previewView.addGestureRecognizer(tap)
            }
            
            self.startCameraCapture()
        } else {
            self.logMessage(messageText:"No front or back capture device found!")
        }
    }
    
    func startCameraCapture() {
        camera!.startCapture(device: frontCamera != nil ? frontCamera! : backCamera!) { (captureDevice, videoFormat, error) in
            if let error = error {
                self.logMessage(messageText: "Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
            } else {
                self.previewView.shouldMirror = (captureDevice.position == .front)
            }
        }
    }
    
    @IBAction func actionOpenChat(_ sender: UIButton) {
        if self.chatChannel != nil {
            NexpilManager.shared.activeChannel = self.chatChannel!
            let chatVC = CommunityChatViewController(nibName: "CommunityChatViewController", bundle: nil)
            let chatNavVC = UINavigationController(rootViewController: chatVC)
            chatNavVC.modalPresentationStyle = .fullScreen
            UIApplication.shared.topMostViewController()!.present(chatNavVC, animated: true)
        } else {
            if let topVC = UIApplication.shared.topMostViewController() {
                DataUtils.messageShow(view: topVC, message: "Please wait chat system is getting initialized", title: "")
            }
        }
    }
    
    @IBAction func flipCamera(_ sender: Any) {
        let senderType = type(of: sender)
        if senderType == UITapGestureRecognizer.self && !isSelf {
            return
        }
        
        var newDevice: AVCaptureDevice?
        
        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = CameraSource.captureDevice(position: .back)
            } else {
                newDevice = CameraSource.captureDevice(position: .front)
            }
            
            if let newDevice = newDevice {
                camera.selectCaptureDevice(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        self.logMessage(messageText: "Error selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self.previewView.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
        }
    }
    
    func prepareLocalMedia() {
        if (localAudioTrack == nil) {
            localAudioTrack = LocalAudioTrack(options: nil, enabled: true, name: "Microphone")
            
            if (localAudioTrack == nil) {
                logMessage(messageText: "Failed to create audio track")
            }
        }
                
        if (localVideoTrack == nil) {
            self.startPreview()
        }
    }
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
        self.chatButton.isUserInteractionEnabled = inRoom
        UIApplication.shared.isIdleTimerDisabled = inRoom
        self.setNeedsUpdateOfHomeIndicatorAutoHidden()
    }
    
    func logMessage(messageText: String) {
        NSLog(messageText)
//        DataUtils.messageShow(view: self, message: messageText, title: "")
    }
    
    func renderRemoteParticipant(participant : RemoteParticipant) -> Bool {
        let videoPublications = participant.remoteVideoTracks
        for publication in videoPublications {
            if let subscribedVideoTrack = publication.remoteTrack,
                publication.isTrackSubscribed {
                subscribedVideoTrack.addRenderer(self.previewView!)
                self.remoteParticipant = participant
                return true
            }
        }
        return false
    }
    
    func renderRemoteParticipants(participants : Array<RemoteParticipant>) {
        for participant in participants {
            if participant.remoteVideoTracks.count > 0,
                renderRemoteParticipant(participant: participant) {
                break
            }
        }
    }
    
    func cleanupRemoteParticipant() {
        if self.remoteParticipant != nil {
            self.remoteParticipant?.remoteVideoTracks.forEach({ remoteTracks in
                remoteTracks.remoteTrack?.removeRenderer(self.previewView)
            })
            self.remoteParticipant = nil
        }
    }
}


// MARK:- RoomDelegate
extension CommunityVideoViewController : RoomDelegate {
    func roomDidConnect(room: Room) {
        logMessage(messageText: "Connected to room \(room.name) as \(room.localParticipant?.identity ?? "")")
        self.room = room
        reloadParticipants()
    }
    
    func roomDidDisconnect(room: Room, error: Error?) {
        logMessage(messageText: "Disconnected from room \(room.name), error = \(String(describing: error))")
        self.cleanupRemoteParticipant()
        self.room = nil
        self.participants = []
        self.showRoomUI(inRoom: false)
    }
    
    func roomDidFailToConnect(room: Room, error: Error) {
        logMessage(messageText: "Failed to connect to room with error = \(String(describing: error))")
        self.cleanupRemoteParticipant()
        self.room = nil
        self.participants = []
        self.showRoomUI(inRoom: false)
    }
    
    func roomIsReconnecting(room: Room, error: Error) {
        logMessage(messageText: "Reconnecting to room \(room.name), error = \(String(describing: error))")
    }
    
    func roomDidReconnect(room: Room) {
        logMessage(messageText: "Reconnected to room \(room.name)")
        self.room = room
        reloadParticipants()
    }
    
    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        self.room = room
        
        logMessage(messageText: "Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
        self.collectionUsers.reloadData()
    }
    
    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        logMessage(messageText: "Room \(room.name), Participant \(participant.identity) disconnected")
        self.reloadParticipants()
    }
    
    func reloadParticipants() {
        self.participants = []
        self.participants.append(self.room!.localParticipant!)
        for remoteParticipant in (self.room?.remoteParticipants ?? []){
            remoteParticipant.delegate = nil
            remoteParticipant.delegate = self
            self.participants.append(remoteParticipant)
        }
        self.collectionUsers.reloadData()
    }
}

// MARK:- RemoteParticipantDelegate
extension CommunityVideoViewController : RemoteParticipantDelegate {
    
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) published \(publication.trackName) video track")
    }
    
    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) unpublished \(publication.trackName) video track")
    }
    
    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) published \(publication.trackName) audio track")
    }
    
    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) unpublished \(publication.trackName) audio track")
    }
    
    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        logMessage(messageText: "Subscribed to \(publication.trackName) video track for Participant \(participant.identity)")
        if self.remoteParticipant != nil && self.remoteParticipant?.sid == participant.sid || (self.remoteParticipant == nil && !isSelf) {
            self.remoteParticipant = participant
            _ = renderRemoteParticipant(participant: participant)
        }
        self.collectionUsers.reloadData()
    }
    
    func didUnsubscribeFromVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        logMessage(messageText: "Unsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")
        if self.remoteParticipant != nil && self.remoteParticipant?.sid == participant.sid {
            self.cleanupRemoteParticipant()
            showMyUser()
        }
        self.collectionUsers.reloadData()
    }
    
    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        logMessage(messageText: "Subscribed to \(publication.trackName) audio track for Participant \(participant.identity)")
    }
    
    func didUnsubscribeFromAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        logMessage(messageText: "Unsubscribed from \(publication.trackName) audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipantDidEnableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) enabled \(publication.trackName) video track")
    }
    
    func remoteParticipantDidDisableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) disabled \(publication.trackName) video track")
    }
    
    func remoteParticipantDidEnableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) enabled \(publication.trackName) audio track")
    }
    
    func remoteParticipantDidDisableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "Participant \(participant.identity) disabled \(publication.trackName) audio track")
    }
    
    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "FailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
    }
    
    func didFailToSubscribeToVideoTrack(publication: RemoteVideoTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "FailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
    }
}

// MARK:- VideoViewDelegate
extension CommunityVideoViewController : VideoViewDelegate {
    func videoViewDimensionsDidChange(view: VideoView, dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}

// MARK:- CameraSourceDelegate
extension CommunityVideoViewController : CameraSourceDelegate {
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        logMessage(messageText: "Camera source failed with error: \(error.localizedDescription)")
    }
}

// MARK:- Collection Delegate and Datasource
extension CommunityVideoViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if let room = self.room {
            count = room.remoteParticipants.count
        }
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCallUserCell", for: indexPath) as? VideoCallUserCell

        if indexPath.item < index {
            cell?.setParticipant(self.participants[indexPath.item])
        } else {
            cell?.setParticipant(self.participants[indexPath.item + 1])
        }
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item < index {
            self.index = indexPath.row
        } else {
            self.index = indexPath.row + 1
        }
        
        
        let participant = self.participants[index]
        if participant is RemoteParticipant {
            if isSelf {
                self.localVideoTrack?.removeRenderer(self.previewView)
            }
            self.isSelf = false
            if let remoteParticipant = participant as? RemoteParticipant  {
                self.previewView.contentMode = .scaleAspectFit
                if remoteParticipant.remoteVideoTracks.count == 0 {
                    self.cleanupRemoteParticipant()
                    self.setNoVideoProfileImage()
                    self.viewNoVideo.isHidden = false
                } else {
                    self.viewNoVideo.isHidden = true
                    if self.remoteParticipant == nil {
                        _ = renderRemoteParticipant(participant: remoteParticipant)
                    }
                }
            }
        } else {
            print("LocalParticipant")
            self.isSelf = true
            self.cleanupRemoteParticipant()
            self.showMyUser()
        }
        collectionView.reloadData()
        print(participant)
//        if indexPath.item < index {
//            cell?.setParticipant(self.participants[indexPath.item])
//        } else {
//            cell?.setParticipant(self.participants[indexPath.item + 1])
//        }
//        if indexPath.item == 0 {//&& !isSelf {
//            self.isSelf = true
//            self.cleanupRemoteParticipant()
//            self.showMyUser()
//        } else {
//            if isSelf {
//                self.localVideoTrack?.removeRenderer(self.previewView)
//            }
//            self.isSelf = false
//            if let room = self.room {
//                self.previewView.contentMode = .scaleAspectFit
//                let participant = room.remoteParticipants[indexPath.item - 1]
//                if participant.remoteVideoTracks.count == 0 {
//                    self.cleanupRemoteParticipant()
//                    self.setNoVideoProfileImage()
//                    self.viewNoVideo.isHidden = false
//                } else {
//                    self.viewNoVideo.isHidden = true
//                    if self.remoteParticipant == nil {
//                        _ = renderRemoteParticipant(participant: participant)
//                    }
//                }
//            }
//        }
        
//        if indexPath.item == 0 {
//            self.cleanupRemoteParticipant()
//            self.previewView.contentMode = .scaleAspectFill
//            if PlatformUtils.isSimulator {
//                self.setNoVideoProfileImage()
//                self.viewNoVideo.isHidden = false
//            } else {
//                self.viewNoVideo.isHidden = true
//                self.localVideoTrack?.addRenderer(self.previewView)
//            }
//        } else {
//            self.localVideoTrack?.removeRenderer(self.previewView)
//            if let room = self.room {
//                self.previewView.contentMode = .scaleAspectFit
//                let participant = room.remoteParticipants[indexPath.item - 1]
//                if participant.remoteVideoTracks.count == 0 {
//                    self.cleanupRemoteParticipant()
//                    self.setNoVideoProfileImage()
//                    self.viewNoVideo.isHidden = false
//                } else {
//                    self.viewNoVideo.isHidden = true
//                    if self.remoteParticipant == nil {
//                        _ = renderRemoteParticipant(participant: participant)
//                    }
//                }
//            }
//        }
    }
    
    func showMyUser() {
        self.previewView.contentMode = .scaleAspectFill
        if PlatformUtils.isSimulator {
            self.setNoVideoProfileImage()
            self.viewNoVideo.isHidden = false
        } else {
            self.viewNoVideo.isHidden = true
            self.localVideoTrack?.addRenderer(self.previewView)
        }
    }
}


extension CommunityVideoViewController: ChatManagerDelegate {
    func receivedNewMessage(_ message: TCHMessage) {
        print("receivedNewMessage")
    }
}
