//
//  VideoCallUserCell.swift
//  Nexpil
//
//  Created by Arif on 7/31/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Kingfisher
import TwilioVideo

class VideoCallUserCell: UICollectionViewCell {

    @IBOutlet weak var imgUser: RoundImageView!
    @IBOutlet weak var previewView: VideoView!
    var rendered = false
    var participant: Participant?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.previewView.layer.cornerRadius = 40
        self.previewView.layer.masksToBounds = true
    }
    
    func setMe() {
        let user = PreferenceHelper()
        if let image = user.getUserImage(), image != "" {
            let url = URL(string: DataUtils.PROFILEURL + image)
            imgUser.kf.setImage(with: url)
        }
    }
    
    func setParticipant(_ participant: Participant) {
        print(participant.videoTracks.count)
        if participant.videoTracks.count == 0 {
            self.previewView.isHidden = true
        } else {
            if rendered && self.participant != nil && self.participant?.sid != participant.sid {
                self.previewView.invalidateRenderer()
                self.participant!.videoTracks.forEach({ remoteTracks in
                    remoteTracks.videoTrack?.removeRenderer(self.previewView)
                })
                self.participant = nil
            }
            self.participant = participant
            self.rendered = renderRemoteParticipant(participant: participant)
            self.previewView.isHidden = false
        }
    }
    
    func renderRemoteParticipant(participant : Participant) -> Bool {
        let videoPublications = participant.videoTracks
        for publication in videoPublications {
            if let subscribedVideoTrack = publication.videoTrack {
                subscribedVideoTrack.addRenderer(self.previewView!)
                return true
            }
        }
        return false
    }
}
