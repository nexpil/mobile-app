//
//  FadeTransitionNavigationController.swift
//  iOS-UI-TEST
//
//  Created by Arif on 8/11/20.
//  Copyright © 2020 Arif. All rights reserved.
//

import UIKit

class FadeTransitionNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
    }        
}

// Navigation Controller Delegate
extension FadeTransitionNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .push {
            return FadeAnimator(presenting: true)
        } else {
            return FadeAnimator(presenting: false)
        }
    }
}
