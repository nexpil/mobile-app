//
//  ChatListViewController.swift
//  Nexpil
//
//  Created by Arif on 8/29/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import TwilioChatClient

class ChatListViewController: UIViewController {
    @IBOutlet weak var tblChats: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationController()
        
        self.tblChats.tableFooterView = UIView(frame: CGRect.zero)
        self.tblChats.separatorStyle = .none
        self.tblChats.register(UINib(nibName: "ChatListCell", bundle: nil), forCellReuseIdentifier: "ChatListCell")
        if NexpilManager.shared.channels.count == 0 {
            self.fetchChannels()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tblChats.reloadData()
    }
    
    func fetchChannels() {
        NexpilManager.shared.chatManager.getMyChannels { (result, descriptors) in
            if result {                
                let group = DispatchGroup()
                var channels = [TCHChannel]()
                for chDescriptor in descriptors {
                    group.enter()
                    chDescriptor.channel { (result, channel) in
                        if result.isSuccessful() {
                            channels.append(channel!)
                        } else {
                            print(result.error)
                        }
                        group.leave()
                    }
                }
                group.notify(queue: DispatchQueue.main) {
                    NexpilManager.shared.channels = channels
                    self.tblChats.reloadData()
                }
            }
        }
    }
    
    func customizeNavigationController() {
        let backItem = UIBarButtonItem(image: UIImage(named: "Back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.actionBack(_:)))
        self.navigationItem.setLeftBarButton(backItem, animated: true)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    @IBAction func actionBack(_ sender: UIBarButtonItem) {        
        self.dismiss(animated: true)
    }
}

extension ChatListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NexpilManager.shared.channels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell") as? ChatListCell
        cell?.selectionStyle = .none
        let channel = NexpilManager.shared.channels[indexPath.row]
        cell?.lblTitle.text = channel.friendlyName ?? ""
        cell?.lblMessage.text = "-"
        cell?.lblDate.text = channel.lastMessageDate?.chatTimeString()
        if let messages = channel.messages, let lastIndex = channel.lastMessageIndex {
            messages.message(withIndex: lastIndex) { (result, message) in
                if result.isSuccessful() {
                    runOnMainThread {
                        cell?.lblMessage.text = message?.body!
                    }
                }
            }
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        NexpilManager.shared.activeChannel = NexpilManager.shared.channels[indexPath.row]
        let chatVC = CommunityChatViewController(nibName: "CommunityChatViewController", bundle: nil)
        let chatNavVC = UINavigationController(rootViewController: chatVC)
        chatNavVC.modalPresentationStyle = .fullScreen
        self.present(chatNavVC, animated: true)
    }
}

extension ChatListViewController: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return UIImage(named: "ic_no_chats")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text = "no chats available";
        let allAttributes = [NSAttributedString.Key.font: UIFont(name: "Montserrat-Medium", size: 15)!,
                             NSAttributedString.Key.foregroundColor: UIColor.darkGray]
        let attribString = NSMutableAttributedString(string: text, attributes: allAttributes)
        return attribString
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}
