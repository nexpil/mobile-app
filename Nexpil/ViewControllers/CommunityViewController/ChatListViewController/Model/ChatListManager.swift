//
//  ChatListManager.swift
//  Nexpil
//
//  Created by Arif on 11/14/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import TwilioChatClient

class ChatListManager: NSObject, TwilioChatClientDelegate {
    
    let TOKEN_URL = "https://twilio.nexp.xyz/nexpil/chat_access_token.php"    
    
    // MARK: Chat variables
    var client: TwilioChatClient?
    private var identity: String
    
    var userId: String {
        return "@" + self.identity.replacingOccurrences(of: " ", with: "")
    }
    
    var userName: String {
        return self.identity
    }
    
    init(withIdentity identity: String) {
        self.identity = identity
        print(identity)
    }
    
    func chatClientTokenWillExpire(_ client: TwilioChatClient) {
        print("Chat Client Token will expire.")
        // the chat token is about to expire, so refresh it
        refreshAccessToken()
    }
    
    private func refreshAccessToken() {
        let urlString = "\(TOKEN_URL)?identity=\(identity)"
        
        TokenUtils.retrieveToken(url: urlString) { (token, error) in
            guard let token = token else {
                print("Error retrieving token: \(error.debugDescription)")
                return
            }
            self.client?.updateToken(token.accessToken, completion: { (result) in
                if (result.isSuccessful()) {
                    print("Access token refreshed")
                } else {
                    print("Unable to refresh access token")
                }
            })
        }
    }
    
    func login(_ completion: @escaping (Bool) -> Void) {
        let urlString = "\(TOKEN_URL)?identity=\(self.identity)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        print(self.identity)
        TokenUtils.retrieveToken(url: urlString!) { (token, error) in
//            print(token)
//            print(error)
            guard let token = token else {
                print("Error retrieving token: \(error.debugDescription)")
                completion(false)
                return
            }
            // Set up Twilio Chat client
            TwilioChatClient.chatClient(withToken: token.accessToken, properties: nil,
                                        delegate: self) { (result, chatClient) in
                self.client = chatClient
                completion(result.isSuccessful())
            }
        }
    }
    
    func shutdown() {
        if let client = client {
            client.delegate = nil
            client.shutdown()
            self.client = nil
        }
    }
    
    func getAllChannels(_ completion: @escaping (Bool, [TCHChannelDescriptor]) -> Void) {
        self.client?.channelsList()?.publicChannelDescriptors(completion: { (result, paginator) in
            if result.isSuccessful() {
//                for channel in paginator!.items() {
//                    if let channelName = channel.friendlyName {
//                        self.channels.append(channel)
//                    }
//                    print("Channel: \(channel.friendlyName)")
//                    print("Channel: \(channel.membersCount())")
//                }
                completion(true, paginator!.items())
                return
            }
            
            completion(false, [])
        })
    }
}


//TWILIO_AUTH_SID=AC132918a1de74ca248bc728882394b198
//TWILIO_AUTH_TOKEN=aff4fb0a56ef6381befbbb307a45f364
//TWILIO_SERVICE_SID=IS94e2867bfe234ff9a35c5f757f803ca1
//TWILIO_API_SECRET=yLHftOd5tpj9anAkv2AplRLrQXnAJXo1
//TWILIO_API_SID=SKb4a9d707c203887cae002067a0386fe4
//TWILIO_FROM=+17819900748
