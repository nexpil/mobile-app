//
//  CommunityChatViewController.swift
//  Nexpil
//
//  Created by Arif on 7/31/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import MessageKit
import SVProgressHUD
import TwilioChatClient
import InputBarAccessoryView

public struct Sender: SenderType {
    public let senderId: String
    public let displayName: String
}

struct Message: MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind        
}

//extension CommunityChatViewController: ChatManagerDelegate {
//    func receivedNewMessage(_ message: TCHMessage) {
//        let message = createNexpilMessageFromTwilioMessage(message)
//        self.messages.append(message)
//        self.messagesCollectionView.reloadDataAndKeepOffset()
//    }
//}

class CommunityChatViewController: MessagesViewController {
        
    var sender = Sender(senderId: String(PreferenceHelper().getId()), displayName: PreferenceHelper().getIdentity())
    var messages: [MessageType] = []
    var twilioMsgs: [TCHMessage] = []
    private let refreshControl = UIRefreshControl()
    private var lastCount = 0
    
    func createNexpilMessageFromTwilioMessage(_ message: TCHMessage) -> Message {
        let senderID = message.author!
        let sender = Sender(senderId: senderID, displayName: message.author!)
        let color = (senderID == self.sender.senderId) ? UIColor(hex: "333333") : .white
        let attributedMessage = NSAttributedString(string: message.body!, attributes: [NSAttributedStringKey.font : UIFont(name: "Montserrat", size: 16)!, NSAttributedStringKey.foregroundColor: color])
        let message = Message(sender: sender, messageId: message.sid!, sentDate: message.dateCreatedAsDate!, kind: MessageKind.attributedText(attributedMessage))
        return message
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customizeNavigationController()
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messageInputBar.delegate = self
        
        NexpilManager.shared.activeChannel?.delegate = self
        
//        self.addPullToRefresh()
    }
    
    func addPullToRefresh() {
        refreshControl.addTarget(self, action: #selector(getPreviousMessages), for: .valueChanged)
        self.messagesCollectionView.alwaysBounceVertical = true
        self.messagesCollectionView.refreshControl = refreshControl // iOS 10+
    }
    
    @objc
    func getPreviousMessages() {
//        NexpilManager.shared.activeChannel?.getMessagesCount(completion: { (result, msgCount) in
//            if result.isSuccessful() {
//                print("MESSAGES -  \(msgCount)")
//                lastCount = Int(msgCount)
//            }
//        })
        NexpilManager.shared.activeChannel?.messages?.getBefore(UInt(self.messages.count), withCount: 10, completion: { (result, messages) in
            self.refreshControl.endRefreshing()
            if result.isSuccessful(), let msgs = messages  {
                runOnMainThread {
                    for msg in msgs {
                        if !self.twilioMsgs.contains(msg) {
                            self.twilioMsgs.insert(msg, at: 0)
//                            self.twilioMsgs.append(msg)
                            let nxMessage = self.createNexpilMessageFromTwilioMessage(msg)
                            self.messages.insert(nxMessage, at: 0)
                            self.messagesCollectionView.reloadData()//reloadDataAndKeepOffset()
                        }
                    }
                }
            } else {
                print(result.error)
            }
        })
    }
    
    func customizeNavigationController() {
//        self.navigationItem.title = App.physicianName
        self.navigationItem.title = NexpilManager.shared.activeChannel?.friendlyName ?? "-"
        let backItem = UIBarButtonItem(image: UIImage(named: "Back"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.actionBack(_:)))
        self.navigationItem.setLeftBarButton(backItem, animated: true)
        
        let whiteIMG = UIImage.from(color: .white)
        self.navigationController?.navigationBar.setBackgroundImage(whiteIMG, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = whiteIMG
        self.navigationController?.navigationBar.isTranslucent = false
        
        let textAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.foregroundColor: UIColor(hex: "4939E3"), NSAttributedStringKey.font: UIFont(name: "Montserrat-Bold", size: 20)!]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.messageInputBar.inputTextView.placeholder = "Type a message…"
        self.messageInputBar.inputTextView.font = UIFont(name: "Montserrat", size: 16)
        self.messageInputBar.sendButton.setTitleColor(UIColor(hex: "4939E3"), for: .normal)
    }
    
    @IBAction func actionBack(_ sender: UIBarButtonItem) {
        NexpilManager.shared.activeChannel?.delegate = nil
        NexpilManager.shared.activeChannel = nil
        self.dismiss(animated: true)
    }
}

extension CommunityChatViewController: MessagesDataSource {
    func currentSender() -> SenderType {
        return self.sender
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
         return messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
}

extension CommunityChatViewController: MessagesLayoutDelegate {
}

extension CommunityChatViewController: MessagesDisplayDelegate {
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        if message.sender.senderId == self.sender.senderId {
            return UIColor(hex: "F7F7FA")
        }
        return UIColor(hex: "4939E3")
    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        if message.sender.senderId == self.sender.senderId {
            return UIColor(hex: "333333")
        }
        return UIColor.white
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        avatarView.image = UIImage(named: "ic_user")
    }
}

extension CommunityChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        if let messages = NexpilManager.shared.activeChannel!.messages {
            let options = TCHMessageOptions().withBody(text)
            messages.sendMessage(with: options) { result, message in
                if result.isSuccessful() {
//                    print("Message sent.")
                    self.messageInputBar.inputTextView.text = ""
                } else {
                    DataUtils.messageShow(view: self, message: result.error?.localizedDescription ?? "", title: "")
                }
            }
        }
    }
}

extension CommunityChatViewController: TCHChannelDelegate {
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        if !twilioMsgs.contains(message) {
            twilioMsgs.append(message)
            let nxMessage = createNexpilMessageFromTwilioMessage(message)
            self.messages.append(nxMessage)
            self.messagesCollectionView.reloadDataAndKeepOffset()
            self.messagesCollectionView.scrollToBottom() 
        }
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, memberJoined member: TCHMember) {
//        addMessages(newMessages: [StatusMessage(statusMember:member, status:.Joined)])
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, memberLeft member: TCHMember) {
//        addMessages(newMessages: [StatusMessage(statusMember:member, status:.Left)])
    }
    
    func chatClient(_ client: TwilioChatClient, channelDeleted channel: TCHChannel) {
        DispatchQueue.main.async {
            if NexpilManager.shared.activeChannel == channel {
                NexpilManager.shared.activeChannel?.delegate = nil
                NexpilManager.shared.activeChannel = nil
                self.dismiss(animated: true)
            }
        }
    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, synchronizationStatusUpdated status: TCHChannelSynchronizationStatus) {
        if status == .all {
            SVProgressHUD.show(withStatus: "Loading...")
            NexpilManager.shared.activeChannel?.messages?.getLastWithCount(20, completion: { (result, messages) in
                SVProgressHUD.dismiss()
                if result.isSuccessful(), let msgs = messages {
                    runOnMainThread {
                        for msg in msgs {
                            if !self.twilioMsgs.contains(msg) {
                                self.twilioMsgs.append(msg)
                                let nxMessage = self.createNexpilMessageFromTwilioMessage(msg)
                                self.messages.append(nxMessage)
                                self.messagesCollectionView.reloadData()//reloadDataAndKeepOffset()
                                self.messagesCollectionView.scrollToBottom()
                            }
                        }
                    }
                }
            })            
        }
    }
}
