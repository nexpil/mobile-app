//
//  CommunityStartCallHeaderView.swift
//  Nexpil
//
//  Created by Arif on 7/26/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class CommunityStartCallHeaderView: UIView {
    @IBOutlet weak var viewContainer: RoundView!
    @IBOutlet weak var lblNameDescription: UILabel!
    @IBOutlet weak var btnTelehealth: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContainer.addShadow(color: UIColor.lightGray.cgColor, alpha: 0.5, x: 0, y: 1, blur: 3)
    }
}
