//
//  CommunityTelehealthHeader.swift
//  Nexpil
//
//  Created by Arif on 7/26/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

enum CommunityTelehealthType: Int {
    case call = 1
    case direction
    case message
    case telehealth
}

protocol CommunityTelehealthOptionDelegate {
    func telehealthOptionTap(_ option: CommunityTelehealthType)
}

class CommunityTelehealthHeader: UIView {

    @IBOutlet var viewContainer: UIView!
    @IBOutlet var imgOptions: [UIImageView]!
    @IBOutlet var lblOptions: [UILabel]!
    var delegate: CommunityTelehealthOptionDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewContainer.addShadow(color: UIColor.lightGray.cgColor, alpha: 0.5, x: 0, y: 1, blur: 3)
    }
    
    @IBAction func actionTap(_ sender: UITapGestureRecognizer) {
        if let delegate = self.delegate {
            delegate.telehealthOptionTap(CommunityTelehealthType(rawValue: sender.view!.tag)!)
        }
    }
}
