//
//  PhysicianSelectCell.swift
//  Nexpil
//
//  Created by Arif on 9/21/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import Kingfisher

class PhysicianSelectCell: UICollectionViewCell {
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgProfile.layer.cornerRadius = 30
        imgProfile.layer.masksToBounds = true
    }
    
    func setPhysician(_ physician: CommunityUser, isSelected: Bool) {
        lblName.text = physician.firstName + " " + physician.lastName.capitalized
        if physician.userimage == "" {
            imgProfile.image = UIImage(named: "ic_user")
        } else {
            let url = URL(string: DataUtils.PROFILEURL + physician.userimage)
            imgProfile.kf.setImage(with: url)
        }
        
        if isSelected {
            imgProfile.layer.borderColor = UIColor(hex: "877CEC").cgColor
            imgProfile.layer.borderWidth = 3.0
        } else {
            imgProfile.layer.borderWidth = 0.0
        }
    }
}
