//
//  UpcommingVisitCell.swift
//  Nexpil
//
//  Created by Arif on 10/23/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class UpcommingVisitCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblDayTime: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var lblPhysicianName: UILabel!
    @IBOutlet weak var imgPhysician: RoundImageView!
    @IBOutlet weak var lblPhysicianSpeciality: UILabel!
    @IBOutlet weak var lblCheckInMsg: UILabel!
    @IBOutlet weak var btnReschedule: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnCheckIn: UIButton!
    @IBOutlet weak var viewCheckIn: GradientView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    // 258 with Check in Left
    // 341.5
    
    func setAppointment(_ appointment: Appointment) {
        if appointment.hasPatientCheckedIn.value() == "1" {
            self.lblCheckInMsg.text = "You have successfully checked in! We will notify you when Dr. \(appointment.doctorName!) is ready."
            self.btnCheckIn.isHidden = true
            self.viewCheckIn.isHidden = false   
        } else {
            self.btnCheckIn.isHidden = false
            self.viewCheckIn.isHidden = true
        }
        
        let dateText = appointment.appointmentDate.upcomingVisitFormatDay()
        let timeText = appointment.timeslot_start.toAMPM()
        let completeText = "Starts " + dateText + " at " + timeText
        let mainAttribs: [NSAttributedString.Key: Any] = [ .font: UIFont(name: "Montserrat-Medium", size: 15)!, .foregroundColor: UIColor(hex: "877CEC") ]
        let range1 = NSString(string: completeText).range(of: "Starts ")
        let range2 = NSString(string: completeText).range(of: " at ")
//        Starts Today at 12:30pm
        let attributedText = NSMutableAttributedString(string: completeText, attributes: mainAttribs)
        attributedText.addAttributes([.foregroundColor : UIColor(hex: "333333")], range: range1)
        attributedText.addAttributes([.foregroundColor : UIColor(hex: "333333")], range: range2)
        self.lblDayTime.attributedText = attributedText
        
        self.lblDate.text = appointment.appointmentDate.upcomingVisitFormatDate()
        
        var reasonTime = (appointment.reason ?? "Follow up visit")
        if reasonTime == "" {
            reasonTime = "Follow up visit"
        }
        reasonTime += " (30 mins)"
        
        let attributedReason = NSMutableAttributedString(string: reasonTime, attributes: mainAttribs)
        let timeRange = NSString(string: reasonTime).range(of: "(30 mins)")
        attributedReason.addAttributes([.foregroundColor : UIColor(hex: "333333"), .font: UIFont(name: "Montserrat", size: 12)!], range: timeRange)
        self.lblReason.attributedText = attributedReason
        
        self.lblPhysicianName.text = appointment.doctorName
        if let speciality = appointment.speciality {
            self.lblPhysicianSpeciality.text = speciality
        } else {
            self.lblPhysicianSpeciality.text = "Primary Care"
        }
    }
}
