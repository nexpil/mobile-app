//
//  AvailabilityTimeCell.swift
//  Nexpil
//
//  Created by Arif on 9/21/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class AvailabilityTimeCell: UICollectionViewCell {

    @IBOutlet weak var viewBg: RoundView!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewBg.layer.borderWidth = 2.0
        viewBg.layer.borderColor = UIColor(hex: "4939E3").cgColor
    }
    
    func setAvailability(_ availabilityString: String, isSelected: Bool) {
        lblTime.text = availabilityString
        if isSelected {
            self.viewBg.backgroundColor = UIColor(hex: "4939E3")
            self.lblTime.textColor = .white
        } else {
            self.lblTime.textColor = UIColor(hex: "4939E3")
            self.viewBg.backgroundColor = .white
        }
    }
}
