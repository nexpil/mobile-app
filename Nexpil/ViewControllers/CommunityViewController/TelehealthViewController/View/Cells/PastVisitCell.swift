//
//  PastVisitCell.swift
//  Nexpil
//
//  Created by Arif on 9/28/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class PastVisitCell: UITableViewCell {

    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var viewSeparator: UIView!
    @IBOutlet weak var lblPhysicianName: UILabel!
    @IBOutlet weak var imgPhysician: RoundImageView!
    @IBOutlet weak var lblReasonDescription: UILabel!
    @IBOutlet weak var lblPhysicianSpeciality: UILabel!    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAppointment(_ appointment: Appointment) {
        let dateText = appointment.appointmentDate.pastVisitFormatMonthDay()
        let timeText = appointment.timeslot_start.toAMPM()
        let completeText = dateText + " at " + timeText
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "Montserrat-Medium", size: 15)!,
            .foregroundColor: UIColor(hex: "877CEC"),
        ]
        let atRange = NSString(string: completeText).range(of: "at")
        let attributedText = NSMutableAttributedString(string: completeText, attributes: attributes)
        attributedText.addAttributes([.foregroundColor : UIColor(hex: "333333")], range: atRange)
        self.lblDateTime.attributedText = attributedText
        
        var reasonDescription = appointment.reason ?? "Follow up visit"
        var rangeText = ""
        if appointment.description != nil {
            reasonDescription = reasonDescription + " - " + appointment.description!
            rangeText = appointment.description!
        }
        let attributedReason = NSMutableAttributedString(string: reasonDescription, attributes: attributes)
        if rangeText != "" {
            let descRange = NSString(string: reasonDescription).range(of: rangeText)
            attributedReason.addAttributes([.foregroundColor : UIColor(hex: "333333")], range: descRange)
        }
        self.lblReasonDescription.attributedText = attributedReason
        
        self.lblPhysicianName.text = appointment.doctorName
        if let speciality = appointment.speciality {
            self.lblPhysicianSpeciality.text = speciality
        } else {
            self.lblPhysicianSpeciality.text = ""
        }
    }
    
}
