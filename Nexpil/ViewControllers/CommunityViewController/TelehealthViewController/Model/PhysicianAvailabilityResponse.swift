//
//  PhysicianAvailabilityResponse.swift
//  Nexpil
//
//  Created by Arif on 10/22/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

// MARK: - PhysicianAvailabilityResponse
struct PhysicianAvailabilityResponse: Codable {
    var status: Bool
    var data: [TimeSlot]?
    var error: String?
}

// MARK: - Result
struct TimeSlot: Codable {
    var timeSlotId: Int
    var timeSlot: String

    enum CodingKeys: String, CodingKey {
        case timeSlotId = "time_slot_id"
        case timeSlot = "time_slot"
    }
}
