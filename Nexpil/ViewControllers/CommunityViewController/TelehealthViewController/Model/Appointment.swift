//
//  Appointment.swift
//  Nexpil
//
//  Created by Arif on 9/28/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct Appointment: Codable {
    var id: ID
    var doctorName: String?
    var doctorId: Int?
    var patientName: String?
    var description, speciality, reason: String?
    var appointmentDate, appointmentTime, status: String
    var timeslot_start,timeslot_end:String
    var hasPatientCheckedIn: ID
    
    enum CodingKeys: String, CodingKey {
        case id
        case doctorName = "doctor_name"
        case doctorId = "doctor_id"
        case speciality
        case patientName = "patient_name"
        case reason
        case description
        case appointmentDate = "appointment_date"
        case appointmentTime = "appointment_time"
        case timeslot_start = "t_start"
        case timeslot_end = "t_end"
        case status, hasPatientCheckedIn
    }
}

enum ID: Codable, Equatable {
    case integer(Int)
    case string(String)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Int.self) {
            self = .integer(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(ID.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for ID"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .integer(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
    
    func value() -> String {
        switch self {
        case .integer(let x):
            return String(x)
        case .string(let x):
            return x
        }
    }
}

struct PostAppointment {
    var patientId: Int
    var doctorId: String
    var reason: String
    var timeslotId:Int
    var description, appointmentDate, appointmentTime: String
}
