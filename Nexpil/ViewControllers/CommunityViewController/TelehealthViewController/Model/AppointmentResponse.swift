//
//  AppointmentResponse.swift
//  Nexpil
//
//  Created by Arif on 10/23/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

// MARK: - AppointmentResponse
struct AppointmentResponse: Codable {
    var status: Bool
    var data: AllAppointments?
    var error: String?
}

// MARK: - DataClass
struct AllAppointments: Codable {
    var upcoming, past: [Appointment]?
}
