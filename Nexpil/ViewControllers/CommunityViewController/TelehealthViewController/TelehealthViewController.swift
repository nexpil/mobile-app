//
//  TelehealthViewController.swift
//  Nexpil
//
//  Created by Arif on 8/28/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class TelehealthViewController: KeyboardViewController {
                    
    @IBOutlet weak var btnSchedule: NPButton!
    @IBOutlet weak var txtReason: UITextField!
    @IBOutlet weak var lblSuccessMsg: UILabel!
    @IBOutlet weak var viewReason: GradientView!
    @IBOutlet weak var viewSuccess: GradientView!
    @IBOutlet weak var lblAvailableDate: UILabel!
    @IBOutlet weak var tblPastVisits: UITableView!
    @IBOutlet weak var txtDescription: UITextView!
    @IBOutlet weak var calendarView: CalendarView!
    @IBOutlet weak var scrollContent: UIScrollView!
    @IBOutlet weak var viewPastVisits: GradientView!
    @IBOutlet weak var viewDescripton: GradientView!
    @IBOutlet weak var viewScheduleVisit: GradientView!
    @IBOutlet weak var tblUpcommingVisits: UITableView!
    @IBOutlet weak var stackAvailabilities: UIStackView!
    @IBOutlet weak var viewUpcommingVisits: GradientView!
    @IBOutlet weak var collectionUsers: UICollectionView!
    @IBOutlet weak var actLoadSlots: UIActivityIndicatorView!
    @IBOutlet weak var collectionAvailability: UICollectionView!
    @IBOutlet weak var constraintAvailabilityHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintPassVisitTableHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintUpcommingVisitTableHeight: NSLayoutConstraint!
    
    var selectedIndex = -1 {
        didSet {
            self.userDidSelect()
        }
    }
    
    var selectedDate = Date()
    var timeSlots = [TimeSlot]()
    var pastVisits = [Appointment]()
    var physicians = [CommunityUser]()
    var upCommingVisits = [Appointment]()
    
    var appointmentReasons = ["Follow-Up Visit", "Pre-Surgical Visit", "Back Problems", "Skin Disorders", "Back Problems", "Cholesterol Problems", "High Blood Pressure", "Diabetes"]
    
    var selectedTimeIndex = -1 {
        didSet {
            if selectedTimeIndex != -1 {
                self.slotDidSelect()
            } else {
                self.viewReason.isHidden = true
                self.viewDescripton.isHidden = true
                self.btnSchedule.isHidden = true
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollContent.contentInsetAdjustmentBehavior = .never
//        self.physicians = self.physicians + self.physicians + self.physicians + self.physicians + self.physicians
        
        self.addDoneButtonOnKeyboard(forTextView: self.txtDescription)
        
        self.customizeCalendar()
        self.manageKeyboard()
        
//        let visit = Appointment(id: "", doctorName: "", patientName: "", appointmentDate: "", appointmentTime: "", status: "", hasPatientCheckedIn: "")
//        let visit1 = Appointment(id: "", doctorName: "", patientName: "", appointmentDate: "", appointmentTime: "", status: "", hasPatientCheckedIn: "")
//        self.pastVisits = [visit, visit1]
        self.initializeTableAndCollections()
        
        runAfter(0) {
            self.getAllAppointments()
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         NotificationCenter.default.addObserver(self, selector: #selector(onDoubleTapCommunityTab(noti:)), name: Notification.Name.Action.DoubleTapCommunityTab, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    private func manageKeyboard() {
        self.callbackKeyboardWillShow = { height in
//            print("Presented: \(height)")
            self.scrollContent.contentInset = UIEdgeInsetsMake(0.0, 0.0, height, 0.0)
            self.scrollContent.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, 0.0, height, 0.0)
            var aRect = self.view.frame;
            aRect.size.height -= height
            if aRect.contains(self.viewDescripton.frame.origin) {
//                self.scrollContent.setContentOffset(CGPoint(x: 0.0, y: self.scrollContent.contentOffset.y + height), animated: true)
                var scrollFrame = self.scrollContent.convert(self.viewDescripton.frame, from: self.viewScheduleVisit)
                scrollFrame.origin.y += height
                self.scrollContent.scrollRectToVisible(scrollFrame, animated: true)
            }

        }
        
        self.callbackKeyboardWillHide = {
            print("Dismiss")
            self.scrollContent.contentInset = UIEdgeInsets.zero
            self.scrollContent.scrollIndicatorInsets = UIEdgeInsets.zero
        }
    }
    
    func initializeTableAndCollections() {
        self.collectionUsers.registerCellNib(PhysicianSelectCell.self)
        self.collectionAvailability.registerCellNib(AvailabilityTimeCell.self)
        
        self.tblPastVisits.separatorStyle = .none
        self.tblPastVisits.tableFooterView = UIView(frame: .zero)
        self.tblPastVisits.registerCellNib(PastVisitCell.self)
        self.tblUpcommingVisits.separatorStyle = .none
        self.tblUpcommingVisits.tableFooterView = UIView(frame: .zero)
        self.tblUpcommingVisits.registerCellNib(UpcommingVisitCell.self)
        self.reloadVisits()
    }
    
    func reloadVisits() {
        if self.pastVisits.count > 0 {
            self.viewPastVisits.isHidden = false
        } else {
            self.viewPastVisits.isHidden = true
        }
        
        if self.upCommingVisits.count > 0 {
            self.viewUpcommingVisits.isHidden = false
        } else {
            self.viewUpcommingVisits.isHidden = true
        }
        self.tblPastVisits.reloadData()
        self.tblUpcommingVisits.reloadData()
        
        let pastHt = 165 * self.pastVisits.count
        self.constraintPassVisitTableHeight.constant = CGFloat(pastHt)
        
        var upcommingHt = 0//165 * self.upCommingVisits.count
        for visit in upCommingVisits {
            if visit.hasPatientCheckedIn.value() == "1" {
                upcommingHt += 295
            } else {
                upcommingHt += 258
            }
        }
        self.constraintUpcommingVisitTableHeight.constant = CGFloat(upcommingHt)
        self.view.layoutIfNeeded()
    }
    
    @IBAction func onDoubleTapCommunityTab(noti: Notification) {        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionChat(_ sender: UIButton) {
        let chatListVC = ChatListViewController(nibName: "ChatListViewController", bundle: nil)
        let chatListNav = UINavigationController(rootViewController: chatListVC)
        chatListNav.modalPresentationStyle = .fullScreen
        chatListNav.modalTransitionStyle = .crossDissolve        
        self.present(chatListNav, animated: true)
    }
    
    private func userDidSelect() {
        self.calendarView.isHidden = false
    }
    
    private func slotDidSelect() {
        self.btnSchedule.isHidden = false
        self.viewReason.isHidden = false
        self.viewDescripton.isHidden = false
    }
    
    private func customizeCalendar() {
        CalendarView.Style.cellShape = .round
        CalendarView.Style.cellColorDefault = UIColor.clear
        CalendarView.Style.headerTextColor = NPColorScheme(rawValue: 2)!.color
        CalendarView.Style.cellTextColorDefault = UIColor.darkText
        CalendarView.Style.cellTextColorToday = UIColor.darkGray
        CalendarView.Style.firstWeekday = .sunday
        CalendarView.Style.cellTextColorWeekend = NPColorScheme(rawValue: 2)!.color
        CalendarView.Style.locale = Locale(identifier: "en_US")
        CalendarView.Style.timeZone = TimeZone(abbreviation: "UTC")!
        CalendarView.Style.cellColorToday = .clear//NPColorScheme(rawValue: 2)!.color
        CalendarView.Style.headerFontName = "Montserrat"
        
        CalendarView.Style.hideCellsOutsideDateRange = false
        CalendarView.Style.changeCellColorOutsideRange = false
        CalendarView.Style.cellSelectedBorderColor = NPColorScheme(rawValue: 2)!.color
        CalendarView.Style.cellSelectedColor = NPColorScheme(rawValue: 2)!.color
        CalendarView.Style.cellSelectedTextColor = .white
        CalendarView.Style.colorTheme = 2
        
        calendarView.enableDeslection = false
        calendarView.backgroundColor = UIColor.clear
        calendarView.dataSource = self
        calendarView.delegate = self
        calendarView.reloadData()
    }
    
    
    @IBAction func actionSelectReason(_ sender: UIButton) {
        let alertViewController = UIAlertController(title: "", message: "Please select reason", preferredStyle: .actionSheet)
        for (_, reason) in appointmentReasons.enumerated() {
//            print(index)
            let alertAction = UIAlertAction(title: reason, style: .default) { _ in
                self.txtReason.text = reason
            }
            alertViewController.addAction(alertAction)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
        alertViewController.addAction(cancelAction)
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    fileprivate func getAllAppointments() {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.getAllAppointments(String(PreferenceHelper().getId()))
        ApiManager.request(router: router, success: { (response: AppointmentResponse) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status && response.data != nil {
                if let past = response.data!.past {
                    self.pastVisits = past
                }
                
                if let future = response.data!.upcoming {
                    self.upCommingVisits = future
                }
                
                runOnMainThread {
                    self.reloadVisits()
                }
            }
        }) { error in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            print(error)
            runAfter(10) {
                self.getAllAppointments()
            }
        }
    }
    
    fileprivate func getAvailableTimeSlot() {
        self.calendarView.isUserInteractionEnabled = false
        actLoadSlots.startAnimating()
        self.timeSlots = []
        let router = Router.getAvailablePhysicianSlots(self.physicians[self.selectedIndex].userid, selectedDate.dateString())
        ApiManager.request(router: router, success: { (response: PhysicianAvailabilityResponse) in
            self.stopLoading()
            if response.status {
                if let data = response.data, data.count > 0 {
                    self.timeSlots = data
                    self.collectionAvailability.reloadData()
                    let linesCount = (Double(self.timeSlots.count) / 3.0).rounded(FloatingPointRoundingRule.up)
                    self.constraintAvailabilityHeight.constant = CGFloat(linesCount * 50.0)
                } else {
                    self.constraintAvailabilityHeight.constant =  50.0
                }
            } else {
                DataUtils.messageShow(view: self, message: response.error!, title: "")
            }
        }) { error in
            self.stopLoading()
            print(error)
            DataUtils.messageShow(view: self, message: error.localizedDescription, title: "")
        }
    }
    
    func stopLoading() {
        self.actLoadSlots.stopAnimating()
        self.calendarView.isUserInteractionEnabled = true
    }
    
    @IBAction func actionSchedule() {
        self.view.endEditing(false)
        if txtReason.text == "" {
            DataUtils.messageShow(view: self, message: "Please select appointment reason", title: "")
        } else {
            if self.txtDescription.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" || self.txtDescription.text == "Description"{
                DataUtils.messageShow(view: self, message: "Please enter description for your appointment", title: "")
            } else {
                self.saveAppointment()
            }
        }
    }
    
    fileprivate func saveAppointment() {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let postAppointment = PostAppointment(patientId: PreferenceHelper().getId(), doctorId: self.physicians[self.selectedIndex].userid, reason: self.txtReason.text!, timeslotId:self.timeSlots[self.selectedTimeIndex].timeSlotId, description: self.txtDescription.text!, appointmentDate: self.selectedDate.dateString(), appointmentTime: String(self.timeSlots[self.selectedTimeIndex].timeSlot.dropLast(2)))
        
        let router = Router.postAppointment(postAppointment)
        ApiManager.request(router: router, success: { (response: APIResponse<Appointment>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success {
                var appointMent = response.data!
                appointMent.doctorId = Int(postAppointment.doctorId)
                appointMent.reason = postAppointment.reason
                appointMent.description = postAppointment.description
                self.showSuccessAndHide(appointment: appointMent)
            } else {
                DataUtils.messageShow(view: self, message: "Error occurred while saving response", title: "")
            }
        }) { error in
            print(error)
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            DataUtils.messageShow(view: self, message: error.localizedDescription, title: "")
        }
    }
    
    func showSuccessAndHide(appointment: Appointment) {
        let completeText = """
Your appointment with Dr. \(appointment.doctorName!)
has been scheduled. See you on
\(appointment.appointmentDate.scheculeSuccessDateTime()) at \(appointment.appointmentTime.toAMPM())!
"""
        let attributes: [NSAttributedString.Key: Any] = [ .font: UIFont(name: "Montserrat-Medium", size: 15)!, .foregroundColor: UIColor(hex: "333333")]
        let attributedText = NSMutableAttributedString(string: completeText, attributes: attributes)
        let range1 = NSString(string: completeText).range(of: "Dr. \(appointment.doctorName!)")
        let range2 = NSString(string: completeText).range(of: "\(appointment.appointmentDate.scheculeSuccessDateTime()) at \(appointment.appointmentTime.toAMPM())!")
        attributedText.addAttributes([.foregroundColor : UIColor(hex: "877CEC")], range: range1)
        attributedText.addAttributes([.foregroundColor : UIColor(hex: "877CEC")], range: range2)
        self.lblSuccessMsg.attributedText = attributedText
        self.viewSuccess.alpha = 0.0
        self.viewSuccess.isHidden = false
        self.scrollContent.scrollRectToVisible(self.viewSuccess.frame, animated: true)
        
        UIView.animate(withDuration: 0.3) {
            self.viewScheduleVisit.alpha = 0.0
            self.viewSuccess.alpha = 1.0
        } completion: { (isFinished) in
            self.stackAvailabilities.isHidden = true
            self.selectedTimeIndex = -1
            self.txtReason.text = ""
            self.txtDescription.text = "Description"
            self.txtDescription.textColor = UIColor(hex: "BEBEBE")
            
            let physician = self.physicians[self.selectedIndex]
            let cell = self.collectionUsers.cellForItem(at: IndexPath(item: self.selectedIndex, section: 0)) as! PhysicianSelectCell
            cell.setPhysician(physician, isSelected: false)
            self.selectedIndex = -1
            self.calendarView.isHidden = true
            
            self.viewScheduleVisit.isHidden = true
            runAfter(2.5) {
                self.viewScheduleVisit.isHidden = false
                self.upCommingVisits.append(appointment)
                self.tblUpcommingVisits.reloadData()
                self.reloadVisits()
//                self.tblUpcommingVisits.insertRows(at: [IndexPath(row: self.upCommingVisits.count - 1, section: 0)], with: .automatic)
                UIView.animate(withDuration: 0.3) {
                    self.viewSuccess.alpha = 0.0
                    self.viewScheduleVisit.alpha = 1.0
                } completion: { (isFinished) in
                    self.viewSuccess.isHidden = true
                }

                self.viewSuccess.isHidden = true
            }
        }
    }
    
    @IBAction func actionReschedule(_ sender: UIButton) {
        DataUtils.messageShow(view: self, message: "Functionality Implementation Pending", title: "")
    }
    
    @IBAction func actionCancel(_ sender: UIButton) {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        let router = Router.CancelAppointment(self.upCommingVisits[sender.tag].id.value())
        ApiManager.request(router: router, success: { (response: APIResponse<Appointment>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success {
                self.upCommingVisits.remove(at: sender.tag)
                self.reloadVisits()
//                self.tblUpcommingVisits.reloadData()
            } else {
                DataUtils.messageShow(view: self, message: "Error occurred while cancelling appointmen", title: "")
            }
        }) { error in
            print(error)
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            DataUtils.messageShow(view: self, message: error.localizedDescription, title: "")
        }
    }
    
    @IBAction func actionCheckIn(_ sender: UIButton) {
        DataUtils.customActivityIndicatory(self.view, startAnimate: true)
        var appointment = self.upCommingVisits[sender.tag]
        let router = Router.checkIntoAppointment(appointment.id.value())
        ApiManager.request(router: router, success: { (response: APIResponse<Appointment>) in
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            if response.status == ResponseStatus.success {
                appointment.hasPatientCheckedIn = .integer(1)
                self.upCommingVisits[sender.tag] = appointment
                self.tblUpcommingVisits.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: UITableViewRowAnimation.automatic)
            } else {
                DataUtils.messageShow(view: self, message: "Error occurred while checking in", title: "")
            }
        }) { error in
            print(error)
            DataUtils.customActivityIndicatory(self.view, startAnimate: false)
            DataUtils.messageShow(view: self, message: error.localizedDescription, title: "")
        }
    }
}


extension TelehealthViewController: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionAvailability {
            return self.timeSlots.count
        }
        return self.physicians.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == collectionAvailability {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AvailabilityTimeCell", for: indexPath) as! AvailabilityTimeCell
            cell.setAvailability(timeSlots[indexPath.item].timeSlot, isSelected: self.selectedTimeIndex == indexPath.item)
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhysicianSelectCell", for: indexPath) as! PhysicianSelectCell
        let physician = self.physicians[indexPath.row]
        cell.setPhysician(physician, isSelected: self.selectedIndex == indexPath.item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == collectionAvailability {
            return CGSize(width: 90, height: 35)
        }
        return CGSize(width: 60, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == collectionUsers {
            if self.selectedIndex != -1 {
                let physician = self.physicians[self.selectedIndex]
                let cell = collectionView.cellForItem(at: IndexPath(item: self.selectedIndex, section: 0)) as! PhysicianSelectCell
                cell.setPhysician(physician, isSelected: false)
            }
            self.selectedIndex = indexPath.item
            collectionView.reloadItems(at: [indexPath])
        } else {
            if self.selectedTimeIndex != -1 {
                let cell = collectionView.cellForItem(at: IndexPath(item: self.selectedTimeIndex, section: 0)) as! AvailabilityTimeCell
                cell.setAvailability(timeSlots[self.selectedTimeIndex].timeSlot, isSelected: false)
            }
            self.selectedTimeIndex = indexPath.item
            collectionView.reloadItems(at: [indexPath])
        }
    }
}

extension TelehealthViewController: CalendarViewDataSource, CalendarViewDelegate {
    func startDate() -> Date {
        return Date().startOfMonth()
    }
    
    func endDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.year = 2
        let today = Date()
        let twoYearsFromNow = self.calendarView.calendar.date(byAdding: dateComponents, to: today)!
        return twoYearsFromNow
    }
    
    func calendar(_ calendar: CalendarView, didSelectDate date: Date, withEvents events: [CalendarEvent]) {
        if date >= Date.startOfToday() {
            if date.startOfDay.isSameDay(date: Date().startOfDay) {
                CalendarView.Style.cellColorToday = NPColorScheme(rawValue: 2)!.color
            } else {
                CalendarView.Style.cellColorToday = .clear
            }
            self.resetSlots()
            self.selectedDate = date
            self.stackAvailabilities.isHidden = false
            self.selectedTimeIndex = -1
            self.getAvailableTimeSlot()
            
            self.lblAvailableDate.text = date.monthDay()
        } else {
            CalendarView.Style.cellColorToday = .clear
            self.stackAvailabilities.isHidden = true
            self.resetSlots()
        }
    }
    
    func calendar(_ calendar : CalendarView, didScrollToMonth date : Date) {
    }
    
    func resetSlots() {
        self.selectedTimeIndex = -1
        self.collectionAvailability.reloadData()
    }
}

extension TelehealthViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblUpcommingVisits {
            return self.upCommingVisits.count
        }
        return self.pastVisits.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblPastVisits {
            let cell = tableView.dequeueReusableCell(withIdentifier: PastVisitCell.identifier) as! PastVisitCell
            cell.selectionStyle = .none
            if indexPath.row == self.pastVisits.count - 1 {
                cell.viewSeparator.isHidden = true
            } else {
                cell.viewSeparator.isHidden = false
            }
            cell.setAppointment(self.pastVisits[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: UpcommingVisitCell.identifier) as! UpcommingVisitCell
            cell.selectionStyle = .none
            if indexPath.row == self.upCommingVisits.count - 1 {
                cell.viewSeparator.isHidden = true
            } else {
                cell.viewSeparator.isHidden = false
            }
            cell.btnCancel.tag = indexPath.row
            cell.btnCancel.addTarget(self, action: #selector(self.actionCancel(_:)), for: .touchUpInside)
            cell.btnReschedule.addTarget(self, action: #selector(self.actionReschedule(_:)), for: .touchUpInside)
            cell.btnCheckIn.tag = indexPath.row
            cell.btnCheckIn.addTarget(self, action: #selector(self.actionCheckIn(_:)), for: .touchUpInside)
            cell.setAppointment(self.upCommingVisits[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblUpcommingVisits {
            let upcoming = self.upCommingVisits[indexPath.row]
            if upcoming.hasPatientCheckedIn.value() == "1" {
                return 295
            } else {
                return 258
            }
        }
        return 165
    }
}


extension TelehealthViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Description" {
            textView.text = ""
            textView.textColor = .label
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) == "" {
            textView.text = "Description"
            textView.textColor = UIColor(hex: "BEBEBE")
        }
    }
}
