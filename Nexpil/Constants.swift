//
//  Constants.swift
//  Nexpil
//
//  Created by Golder on 4/29/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

enum TaskType: Int {
    case simple = 1
    case code = 2
    case url = 3
}
