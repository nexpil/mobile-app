//
//  TaskCell.swift
//  Nexpil
//
//  Created by Arif on 3/22/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class TaskCell: UICollectionViewCell {

    @IBOutlet weak var eventText: UILabel!
    @IBOutlet weak var eventSubTitle: UILabel!
    @IBOutlet weak var eventBg: GradientView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
