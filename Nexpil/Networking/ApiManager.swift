//
//  ApiManager.swift
//  Nexpil
//
//  Created by Arif on 9/10/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class ApiManager {
    
    class func request<T: Codable>(router: Router, success: @escaping (T)->(), failure: @escaping (Error)->()) {
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path    
        if !router.queryParameters.isEmpty {
            components.queryItems = router.queryParameters
        }
        
        let session = URLSession(configuration: .default)
        guard let url = components.url else { return }
        var urlRequest = URLRequest(url: url)
        urlRequest.allHTTPHeaderFields = router.headers
        urlRequest.httpMethod = router.method
//        print(url)
        if !router.postParameters.isEmpty {
            if let contentType = router.headers["Content-Type"], contentType == "application/json" {
                urlRequest.httpBody = try! JSONSerialization.data(withJSONObject: router.postParameters, options: [])
            } else {
                let formUrlString = router.postParameters.queryParameters
                urlRequest.httpBody = formUrlString.data(using: String.Encoding.utf8)
            }
        }
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            DispatchQueue.main.async {
                if (error != nil) {
                    failure(error!)
                } else {
                    do {
                        let tResponse = try JSONDecoder().decode(T.self, from: data!)
                        success(tResponse)
                    } catch let parseError {
                        failure(parseError)
                    }
                }
            }
        }
        dataTask.resume()
    }
}


protocol URLQueryParameterStringConvertible {
    var queryParameters: String {get}
}

extension Dictionary : URLQueryParameterStringConvertible {
    var queryParameters: String {
        var parts: [String] = []
        for (key, value) in self {
            let part = String(format: "%@=%@",
                String(describing: key).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!,
                String(describing: value).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
            parts.append(part as String)
        }
        return parts.joined(separator: "&")
    }
}
