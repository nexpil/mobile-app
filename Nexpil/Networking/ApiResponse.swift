//
//  ApiResponse.swift
//  Nexpil
//
//  Created by Arif on 9/10/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct APIResponse<T: Codable>: Codable {
    var status: ResponseStatus
    var data: T?
    var message: String?
}

enum ResponseStatus: String, Codable {
    case success = "true"
    case failure = "false"
}
