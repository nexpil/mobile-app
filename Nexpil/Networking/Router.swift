//
//  Router.swift
//  Nexpil
//
//  Created by Arif on 9/10/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

enum Router {
    case getPatientDetails(_ userCode: String)
    case getMyTasks(_ userId: String)
    case getCommunityUser(_ userId: String)
    case addCommunityUser(_ userId: String,_ userCode: String)
    case getConnectedPysicians(_ userId: String)
    case getAllAppointments(_ userId: String)
    case getAvailablePhysicianSlots(_ doctorId: String,_ date: String)
    case postAppointment(_ appointment: PostAppointment)
    case checkIntoAppointment(_ appointId: String)
    case CancelAppointment(_ appointId: String)
    case patientLoginFromCode(_ request: AuthRequest)
    case getHealthkitModules(_ userId: String)
    case getHealthData(_ userId: Int, _ moduleId: Int)
    case activeHealthModule(_ userId: Int, _ moduleId: Int)
    
    var scheme: String {
        return "https"
    }
    
    var host: String {
        return "twilio.nexp.xyz"
//        return "twilio.nexp.xyz"
    }

        
    
    var path: String {
        switch self {
        case .getMyTasks(_):
            return "/webapi/v1/tasks"
        case .getCommunityUser(userID: _), .addCommunityUser(_ , _):
            return "/nexpil/communityusers_information.php"
        case .getConnectedPysicians(_):
            return "/nexpil/doctor_connected_with_users.php"
        case .getAllAppointments(_):
            return "/webapi/v1/get-appointment"
        case .getAvailablePhysicianSlots(_, _):
            return "/webapi/v1/available-time-slots"
        case .postAppointment(_), .checkIntoAppointment(_), .CancelAppointment(_):
            return "/nexpil/new_appointment.php"
        case .getPatientDetails(_):
            return "/webapi/v1/patient-login"
        case .patientLoginFromCode(_):
            return "/webapi/v1/patient-login"
        case .getHealthkitModules(_):
            return "/webapi/v1/patient-healthkit-modules"
        case .getHealthData(_, _):
            return "/webapi/v1/patient-healthkit-history"
        case .activeHealthModule(_, _):
            return "/webapi/v1/authenticate-module"
        }
    }
    
    var queryParameters: [URLQueryItem] {
        switch self {
        case .getCommunityUser(_), .addCommunityUser(_, _), .getConnectedPysicians(_), .getAvailablePhysicianSlots(_, _), .postAppointment(_), .checkIntoAppointment(_), .CancelAppointment(_), .patientLoginFromCode(_), .getHealthData(_, _), .activeHealthModule(_, _):
            return []
        case .getAllAppointments(let userId):
            return [URLQueryItem(name: "patient_id", value: userId)]
        case .getMyTasks(let userId):
            return [URLQueryItem(name: "patient_id", value: userId)]
        case .getPatientDetails(let userCode):
            return [URLQueryItem(name: "user_code", value: userCode)]
        case .getHealthkitModules(let userId):
            return [URLQueryItem(name: "patient_id", value: userId)]
        }
    }
    
    var postParameters: [String: Any] {
        switch self {
        case .getCommunityUser(let userID):
            return ["userid": userID, "choice": 0]
        case .addCommunityUser(let userID, let userCode):
            return ["userid": userID, "choice": 2, "usercode": userCode]
        case .getConnectedPysicians(let userID):
            return ["userid": userID]
        case .getAvailablePhysicianSlots(let drId, let date):
            return ["doctor_id": drId, "appointment_date": date]
        case .postAppointment(let appointment):
            return ["choice": 0, "patient_id": appointment.patientId, "doctor_id": appointment.doctorId, "appointment_date": appointment.appointmentDate, "appointment_time": appointment.appointmentTime, "reason": appointment.reason, "description": appointment.description,"timeslotId":appointment.timeslotId]
        case .checkIntoAppointment(let id):
            return ["appointment_id": id, "hasPatientCheckedIn": 1, "choice": 4]
        case .CancelAppointment(let id):
            return ["appointment_id": id, "choice": 2]
        case .getMyTasks(_), .getPatientDetails(_), .getAllAppointments(_), .getHealthkitModules(_):
            return [:]
        case .patientLoginFromCode(let request):
            return [ "email" : request.email, "password" : request.password, "patient_id" : request.patientId, "first_name" : request.firstName, "last_name" : request.lastName, "timezone": request.timezone, "deviceToken": request.deviceToken]
        case .getHealthData(let userId, let moduleId):
            return ["patient_id": userId, "healthkit_module_id": moduleId]
        case .activeHealthModule(let userId, let moduleId):
            return ["patient_id": userId, "healthkit_module_id": moduleId]
        }
    }
    
    var method: String {
        switch self {
        case .getCommunityUser(_), .addCommunityUser(_, _), .getConnectedPysicians(_), .getAvailablePhysicianSlots(_, _), .postAppointment(_), .checkIntoAppointment(_), .CancelAppointment(_), .patientLoginFromCode(_), .getHealthData(_, _), .activeHealthModule(_, _):
            return "POST"
        case .getAllAppointments(_), .getMyTasks(_), .getPatientDetails(_), .getHealthkitModules(_):
            return "GET"
        }
    }
    
    var headers: [String: String] {
        switch self {
        case .getCommunityUser(_), .addCommunityUser(_, _), .getConnectedPysicians(_), .postAppointment(_), .checkIntoAppointment(_), .CancelAppointment(_), .patientLoginFromCode(_):
            return ["Content-Type": "application/x-www-form-urlencoded"]
        case .getAvailablePhysicianSlots(_, _), .getAllAppointments(_), .getMyTasks(_), .getPatientDetails(_), .getHealthkitModules(_), .getHealthData(_, _), .activeHealthModule(_, _):
            return ["Content-Type": "application/json"]
        }
    }
}
