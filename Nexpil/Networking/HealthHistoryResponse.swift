//
//  HealthHistoryResponse.swift
//  Nexpil
//
//  Created by Arif on 9/10/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct HealthHistoryResponse<T: Codable>: Codable {
    var status: ResponseStatus
    var history: T?
    var error: String?
}
