//
//  GlobalChatManager.swift
//  Nexpil
//
//  Created by Arif on 11/27/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import TwilioChatClient
import NotificationBannerSwift
class GlobalChatManager: NSObject {
    
    let TOKEN_URL = "https://twilio.nexp.xyz/nexpil/chat_access_token.php"
    var token = ""
    
    // MARK: Chat variables
    var client: TwilioChatClient?
    private var identity: String
    
    init(withIdentity identity: String) {
        self.identity = identity
        print(self.identity)
    }
}

extension GlobalChatManager {
    
    func login(_ completion: @escaping (Bool) -> Void) {
        if self.token == "" {
            let urlString = "\(TOKEN_URL)?identity=\(self.identity)".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            TokenUtils.retrieveToken(url: urlString!) { (token, error) in
                guard let token = token else {
                    print("Error retrieving token: \(error.debugDescription)")
                    completion(false)
                    return
                }
                self.login(withToken: token.accessToken) { finished in
                    completion(finished)
                }
            }
        } else {
            self.login(withToken: self.token) { finished in
                completion(finished)
            }
        }
    }
    
    func login(withToken token: String, completion: @escaping (Bool) -> Void) {
        TwilioChatClient.chatClient(withToken: token, properties: nil,
                                    delegate: self) { (result, chatClient) in
            self.token = token
            self.client = chatClient
            completion(result.isSuccessful())
        }
    }
    
    func shutdown() {
        print("Now Shutdown 1")
        if let client = client {
            print("Now Shutdown 2")
            client.delegate = nil
            client.shutdown()
            self.client = nil
        }
    }
    
    func getMyChannels(_ completion: @escaping (Bool, [TCHChannelDescriptor]) -> Void) {
        self.client?.channelsList()?.userChannelDescriptors(completion: { (result, paginator) in
            if result.isSuccessful() {
                completion(true, paginator!.items())
                return
            }
            completion(false, [])
        })
    }
    
    func getAllChannels(_ completion: @escaping (Bool, [TCHChannelDescriptor]) -> Void) {
        self.client?.channelsList()?.publicChannelDescriptors(completion: { (result, paginator) in
            if result.isSuccessful() {
                completion(true, paginator!.items())
                return
            }
            
            completion(false, [])
        })
    }
    
    private func refreshAccessToken() {
        let urlString = "\(TOKEN_URL)?identity=\(identity)"
        
        TokenUtils.retrieveToken(url: urlString) { (token, error) in
            guard let token = token else {
                runAfter(5) {
                    self.refreshAccessToken()
                }
                print("Error retrieving token: \(error.debugDescription)")
                return
            }
            self.token = token.accessToken
            self.client?.updateToken(token.accessToken, completion: { (result) in
                if (result.isSuccessful()) {
                    print("Access token refreshed")
                } else {
                    print("Unable to refresh access token")
                }
            })
        }
    }
}

extension GlobalChatManager: TwilioChatClientDelegate {
    func chatClientTokenWillExpire(_ client: TwilioChatClient) {
        print("Chat Client Token will expire.")
        refreshAccessToken()
    }
    
    func chatClient(_ client: TwilioChatClient, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        //        print(status)
        if status == TCHClientSynchronizationStatus.completed {
            //            NexpilManager.shared.channelsList = []
            self.client?.channelsList()!.userChannelDescriptors(completion: { (result, paginator) in
                if result.isSuccessful() {
                    if let items = paginator?.items() {
                        let group = DispatchGroup()
                        var channels = [TCHChannel]()
                        for chDescriptor in items {
                            group.enter()
                            chDescriptor.channel { (result, channel) in
                                if result.isSuccessful() {
                                    channels.append(channel!)
                                } else {
                                    print(result.error)
                                }
                                group.leave()
                            }
                        }
                        group.notify(queue: DispatchQueue.main) {
                            NexpilManager.shared.channels = channels
                        }
                    }
                }
            })
//            self.client?.channelsList()?.publicChannelDescriptors(completion: { (result, paginator) in
//                if result.isSuccessful() {
//                    print(paginator?.items().count)
//                    let group = DispatchGroup()
//                    for item in paginator!.items() {
//                        group.enter()
//                        self.client?.channelsList()?.channel(withSidOrUniqueName: item.sid!, completion: { (result, channel) in
//                            channel?.destroy(completion: { (result1) in
//                                print(result1.isSuccessful())
//                                group.leave()
//                            })
//                        })
//                    }
//                }
//            })
        }
    }
    
    func chatClient(_ client: TwilioChatClient, channelAdded channel: TCHChannel) {
        self.client = client
        if !NexpilManager.shared.channels.contains(channel) {
            NexpilManager.shared.channels.append(channel)
        }
        //        print(channel.messages)
    }
    
    func chatClient(_ client: TwilioChatClient, channelDeleted channel: TCHChannel) {
        print("Channel Deleted")
        if let index = NexpilManager.shared.channels.firstIndex(of: channel) {
            NexpilManager.shared.channels.remove(at: index)
        }
    }
    
    //    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, updated: TCHChannelUpdate) {
    //        print("Channel Updated")
    //                if let index = NexpilManager.shared.channels.firstIndex(of: channel) {
    //                    NexpilManager.shared.channels[index] = channel
    //                }
    //    }
    
    func chatClient(_ client: TwilioChatClient, channel: TCHChannel, messageAdded message: TCHMessage) {
        switch UIApplication.shared.applicationState {
        case .active:
            print("active")
        case .background:
            print("background")
        case .inactive:
            print("inactive")
        }
        
        if message.author != self.identity {
            print("Message Added")
            if channel.sid != NexpilManager.shared.activeChannel?.sid {
                let topController = UIApplication.shared.topMostViewController()
                let color = MyBannerColors()
                
                let banner = FloatingNotificationBanner(title: channel.friendlyName ?? "", subtitle: message.body ?? "", titleColor: .darkGray, subtitleColor: UIColor.darkGray, style: BannerStyle.info, colors: color, iconPosition: GrowingNotificationBanner.IconPosition.top)
                banner.onTap = {
                    NexpilManager.shared.activeChannel = channel
                    let chatVC = CommunityChatViewController(nibName: "CommunityChatViewController", bundle: nil)
                    let chatNavVC = UINavigationController(rootViewController: chatVC)
                    chatNavVC.modalPresentationStyle = .fullScreen
                    topController!.present(chatNavVC, animated: true)
                }
                runAfter(1) {
                    banner.show(queuePosition: QueuePosition.front, bannerPosition: BannerPosition.top, queue: NotificationBannerQueue.default, on: nil, edgeInsets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), cornerRadius: 10, shadowColor: UIColor(red:0.95, green:0.95, blue:0.95, alpha:1), shadowOpacity: 1, shadowBlurRadius: 5, shadowCornerRadius: 10, shadowOffset: UIOffset.zero)
                }
            }
        }
    }
}

public class MyBannerColors: BannerColorsProtocol {
    public func color(for style: BannerStyle) -> UIColor {
        switch style {
        case .danger:   return UIColor(red:0.90, green:0.31, blue:0.26, alpha:1.00)
        case .info:     return UIColor(red:0.95, green:0.95, blue:0.95, alpha:0.90)
        case .customView:     return UIColor(red:0.2, green:0.2, blue:0.2, alpha:1.00)
        case .success:  return UIColor(red:0.22, green:0.80, blue:0.46, alpha:1.00)
        case .warning:  return UIColor(red:1.00, green:0.66, blue:0.16, alpha:1.00)
        }
    }
}
