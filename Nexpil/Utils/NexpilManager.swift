//
//  NexpilManager.swift
//  Nexpil
//
//  Created by Arif on 11/27/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import TwilioChatClient

class NexpilManager {
    static let shared = NexpilManager()
    var chatManager = GlobalChatManager(withIdentity: String(PreferenceHelper().getId()))
    var channels = [TCHChannel]()
    var activeChannel: TCHChannel?    
}
