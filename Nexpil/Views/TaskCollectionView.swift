//
//  TaskCollectionView.swift
//  Nexpil
//
//  Created by Arif on 3/22/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

protocol CollectionViewDelegate {
    func didSelectItemAt(_ indexPath: IndexPath)
}

@IBDesignable class TaskCollectionView: UIView {
    @IBOutlet weak var collectionView: UICollectionView!
    var delegate: CollectionViewDelegate?
    
    var view: UIView!
    var taskList: [String] = [] {
        didSet {
            self.collectionView.reloadData()
        }
    }
    
    var isShown = false {
        didSet {
            if !isShown {
                if self.taskList.count > 1 {
                    self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: true)
                    self.collectionView.reloadData()
                }
            } else {
                if taskList.count > 1 {
                    collectionView.collectionViewLayout.invalidateLayout()
                    collectionView.performBatchUpdates({
                        for (index, _) in self.taskList.enumerated() {
                            let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0))
                            let width = (UIScreen.main.bounds.width - 80) + CGFloat(index * 10)
                            UIView.animate(withDuration: 0.3) {
                                if cell != nil {
                                cell!.frame = CGRect(x: CGFloat(index) * width, y: 0, width: UIScreen.main.bounds.width - 80, height: 60)
                                }
                            }
                        }
                    }) { (isFinished) in
                        print("Finished")
                        
                    }
                    self.collectionView.reloadData()
                }
            }
            
            
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
        self.collectionView.registerCellNib(TaskCell.self)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "TaskCollectionView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
}

extension TaskCollectionView: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return taskList.count
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TaskCell", for: indexPath) as! TaskCell
        cell.eventText.text = self.taskList[indexPath.row]
        cell.eventSubTitle.text = "Complete " + self.taskList[indexPath.row]
        cell.eventBg.topColor = NPColorScheme(rawValue: 0)!.color
        cell.eventBg.bottomColor = NPColorScheme(rawValue: 5)!.color
//        UIView.animate(withDuration: 0.3) {
//            cell.layoutIfNeeded()
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.taskList.count > 1 && isShown {
            return CGSize(width: UIScreen.main.bounds.width - 80, height: 60)
        }
        return CGSize(width: UIScreen.main.bounds.width - 40, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.didSelectItemAt(indexPath)
    }
}
