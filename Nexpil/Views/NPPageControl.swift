//
//  NPPageControl.swift
//  Nexpil
//
//  Created by Cagri Sahan on 9/22/18.
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class NPPageControl: UIPageControl {
    
    override var currentPage: Int {
        didSet {
            updateBorderColor()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.pageIndicatorTintColor = UIColor(hex: "333333", alpha: 0.2)
        self.currentPageIndicatorTintColor = NPColorScheme.purple.color
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.pageIndicatorTintColor = UIColor(hex: "333333", alpha: 0.2)
        self.currentPageIndicatorTintColor = NPColorScheme.purple.color
    }
    
    func updateBorderColor() {
        //var startPos: Int = 0
        if #available(iOS 14.0, *) {
            self.pageIndicatorTintColor = UIColor(hex: "333333", alpha: 0.2)
            self.currentPageIndicatorTintColor = NPColorScheme.purple.color
            let smallConfiguration = UIImage.SymbolConfiguration(pointSize: 15, weight: .bold)
            let circleFill = UIImage(systemName: "circle.fill", withConfiguration: smallConfiguration)
            let circle = UIImage(systemName: "circle", withConfiguration: smallConfiguration)
            for i in 0..<numberOfPages {
                if i == currentPage {
                    setIndicatorImage(circleFill, forPage: i)
                } else {
                    setIndicatorImage(circle, forPage: i)
                }
            }
            
        } else {
            subviews.enumerated().forEach { index, subview in
                if index != currentPage {
                    subview.layer.borderColor = NPColorScheme.purple.color.cgColor
                    subview.layer.borderWidth = 1
                } else {
                    subview.layer.borderWidth = 0
                }
            }
        }
    }
    
    /*override func draw(_ rect: CGRect) {
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        var startPos: Int = 0
        if #available(iOS 14.0, *) {
            for i in 0..<self.numberOfPages {
                self.pageIndicatorTintColor = UIColor(hex: "333333", alpha: 0.2)
                self.currentPageIndicatorTintColor = NPColorScheme.purple.color
                if self.subviews.count > 0, i < self.subviews[0].subviews[0].subviews.count {
                    let subView = self.subviews[0].subviews[0]
                    print(subView.subviews.count)
                    let viewDot = subView.subviews[i]
                    print(viewDot.subviews.count)
                    if i == currentPage {
                        viewDot.frame = CGRect(x: startPos, y: 0, width: 25, height: 10)
                        viewDot.backgroundColor = currentPageIndicatorTintColor
                        viewDot.layer.cornerRadius = 5
                        startPos += 30
                    } else {
                        viewDot.frame = CGRect(x: startPos, y: 0, width: 10, height: 10)
                        viewDot.backgroundColor = pageIndicatorTintColor
                        viewDot.layer.cornerRadius = 5
                        startPos += 15
                    }
                }
            }
        } else {
            for i in 0..<self.numberOfPages {
                if i < self.subviews.count {
                    self.subviews[i].layer.cornerRadius = 5
                    if i == currentPage {
                        self.subviews[i].frame = CGRect(x: startPos, y: 0, width: 25, height: 10)
                        self.subviews[i].backgroundColor = currentPageIndicatorTintColor
                        self.subviews[i].layer.cornerRadius = 5
                        startPos += 30
                    } else {
                        self.subviews[i].frame = CGRect(x: startPos, y: 0, width: 10, height: 10)
                        self.subviews[i].backgroundColor = pageIndicatorTintColor
                        self.subviews[i].layer.cornerRadius = 5
                        startPos += 15
                    }
                }
            }
        }
    }*/
}
