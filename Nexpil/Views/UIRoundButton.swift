//
//  UIRoundButton.swift
//  Nexpil
//
//  Created by Golder on 3/13/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class UIRoundButton: UIButton {

    @IBInspectable public var radius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = self.radius
        }
    }
    
    @IBInspectable public var hasShadow: Bool = false {
        didSet {
            if self.hasShadow {
                self.layer.shadowColor = UIColor.gray.cgColor
                self.layer.shadowRadius = 3
                self.layer.shadowOpacity = 0.4
                self.layer.shadowOffset = CGSize(width: 0, height: 2)
            }
        }
    }
}
