//
//  ExtendedPageControl.swift
//  Nexpil
//
//  Copyright © 2018 Admin. All rights reserved.
//

import UIKit

class ExtendedPageControl: UIView {
    
    var numberOfPage: Int = 0
    var currentOfPage: Int = 0
    var f_start_point: CGFloat = 0.0
    
    var indicatorColor = UIColor(hex: "333333", alpha: 0.2)
    var currentIndicatorColor = NPColorScheme.purple.color
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    // ## view init method ##
    func set_view(_ page: Int, current: Int) {
        
        numberOfPage = page
        currentOfPage = current
        
        let f_all_width: CGFloat = CGFloat((numberOfPage-1)*20 + 25)
        
        guard f_all_width < self.frame.size.width else {
            print("frame.Width over Number Of Page")
            return
        }
        
        var f_width: CGFloat = 12, f_height: CGFloat = 12
        var f_x: CGFloat = (self.frame.size.width-f_all_width)/2.0, f_y: CGFloat = (self.frame.size.height-f_height)/2.0
        
        f_start_point = f_x
        
        for i in 0 ..< numberOfPage {
            let img_page = UIImageView()
            
            if i == currentOfPage {
                f_width = 30
                img_page.backgroundColor = currentIndicatorColor
            } else {
                f_width = 12
                img_page.backgroundColor = indicatorColor
            }
            
            img_page.frame = CGRect(x: f_x, y: f_y, width: f_width, height: f_height)
            img_page.layer.cornerRadius = img_page.frame.size.height / 2.0
            
            img_page.tag = i + 10
            self.addSubview(img_page)
            
            f_x += f_width + 5
        }
    }
    
    // ## Call the move page in scrollView ##
    func scroll_did(_ scrollView: UIScrollView, _ isRight: Bool = true) {
        let f_page = scrollView.contentOffset.x / scrollView.frame.size.width
        let f_move: CGFloat = (15*(-scrollView.contentOffset.x)/scrollView.frame.size.width)
        
        if isRight {
            print("left")
            
            let tag_value = Int(f_page + 0.02) + 10
            print(tag_value)
            
            if let iv_page: UIImageView = self.viewWithTag(tag_value) as? UIImageView,
                tag_value >= 10 && tag_value+1 < 10+numberOfPage {
                
                iv_page.frame = CGRect(x: f_start_point+((CGFloat(tag_value)-10)*20),
                                       y: iv_page.frame.origin.y,
                                       width: 30+(f_move+((CGFloat(tag_value)-10)*15)),
                                       height: iv_page.frame.size.height)
                iv_page.backgroundColor = currentIndicatorColor
                
                if let iv_page_next: UIImageView = self.viewWithTag(tag_value+1) as? UIImageView {
                    let f_page_next_x: CGFloat = ((f_start_point+35)+((CGFloat(tag_value)-10)*20))
                    iv_page_next.frame = CGRect(x: f_page_next_x+(f_move+((CGFloat(tag_value)-10)*15)),
                                                y: iv_page_next.frame.origin.y,
                                                width: 12-(f_move+((CGFloat(tag_value)-10)*15)),
                                                height: iv_page_next.frame.size.height)
                    
                    iv_page_next.backgroundColor = indicatorColor
                }
            }
        } else {
            print("right")
            let tag_value = Int(f_page - 0.02) + 10
            print(tag_value)
            
            if let iv_page: UIImageView = self.viewWithTag(tag_value) as? UIImageView,
                tag_value >= 10 && tag_value+1 < 10+numberOfPage {
                
                iv_page.frame = CGRect(x: f_start_point+((CGFloat(tag_value)-10)*20),
                                       y: iv_page.frame.origin.y,
                                       width: 30+(f_move+((CGFloat(tag_value)-10)*15)),
                                       height: iv_page.frame.size.height)
                iv_page.backgroundColor = indicatorColor
                
                if let iv_page_next: UIImageView = self.viewWithTag(tag_value+1) as? UIImageView {
                    let f_page_next_x: CGFloat = ((f_start_point+35)+((CGFloat(tag_value)-10)*20))
                    iv_page_next.frame = CGRect(x: f_page_next_x+(f_move+((CGFloat(tag_value)-10)*15)),
                                                y: iv_page_next.frame.origin.y,
                                                width: 12-(f_move+((CGFloat(tag_value)-10)*15)),
                                                height: iv_page_next.frame.size.height)
                    
                    iv_page_next.backgroundColor = currentIndicatorColor
                }
            }
        }
    }
}
