//
//  HealthWeightGraphView.swift
//  Nexpil
//
//  Created by Shine on 2/19/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class HealthWeightGraphView: UIView {
    
    private let standardAfter   : CGFloat = 95

    private let lineColorAfter  = UIColor(hex: "877cec")
    private let dotColorAfter   = UIColor(hex: "877cec")
    private let dotColorBefore  = UIColor(hex: "877cec")
    
    private let colorDashLine   = UIColor.lightGray
    private let colorLightGray  = UIColor(hex: "f0f0f0")
    
    private let dotRadius       : CGFloat = 5.8
    private let lineWidth       : CGFloat = 5.5
    
    private let preHorzMargin   : CGFloat = 30
    private let horzMargin      : CGFloat = 22
    private let vertMargin      : CGFloat = 30
    
    var yValues: [CGFloat] = [60, 100, 140, 180, 220]
    var xValues: [String] = ["Nov 17", "Nov 19", "Nov 21", "Nov 23"]
    
    var dataAfter: [CGFloat] = [140, 180, 160, 140, 160, 180, 150]
    var dataBefore: [CGFloat] = [100, 140, 120, 100, 120, 140, 110]
    var indexsOfNoAfter: [Int] = []

    private var indexOfLatest = -1
    
    func setData(dataAfter: [CGFloat],
                 dataBefore: [CGFloat],
                 yValues: [CGFloat],
                 xValues: [String]) {
        
        indexOfLatest = -1
        
        self.yValues = yValues
        self.xValues = xValues
        self.dataAfter = dataAfter
        self.dataBefore = dataBefore
        
        self.filterData()
        
        setNeedsDisplay()
    }
    
    private func filterData() {
        indexsOfNoAfter.removeAll()
        
        for (index, value) in dataAfter.enumerated() {
            if value > 0 {
                indexOfLatest = index
            } else {
                indexsOfNoAfter.append(index)
                dataAfter[index] = standardAfter
            }

            if dataAfter[index] < yValues[0] {
                dataAfter[index] = yValues[0]
            } else if dataAfter[index] > yValues.last! {
                dataAfter[index] = yValues.last!
            }
        }
        
        for (index, value) in dataBefore.enumerated() {
            if value > 0 {
                indexOfLatest = index
            }

            if value < yValues[0] {
                dataBefore[index] = yValues[0]
            } else if value > yValues.last! {
                dataBefore[index] = yValues.last!
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        drawDashLinesAndValues()
        
        //drawConnectLines()
        drawLineOfAfter()
        
        drawTheLatestInfo()
    }

    private func drawText(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat, text: String, color: UIColor = .lightGray, alignment: NSTextAlignment = .center) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        let attrs = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.paragraphStyle: paragraphStyle]
        text.draw(with: CGRect(x: x, y: y - 10, width: width, height: height), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
    }

    private func drawDashLinesAndValues() {

        let path = UIBezierPath()

        if yValues.count > 0 {
            let height = self.bounds.maxY - self.vertMargin * 2
            let h = self.yValues.last! - self.yValues.first!
            let min = self.yValues.first!
            for yValue in yValues {
                let y = self.bounds.maxY - (self.vertMargin + (yValue - min) / h * height)
                let  p0 = CGPoint(x: self.preHorzMargin + self.bounds.minX, y: y)
                path.move(to: p0)
                let  p1 = CGPoint(x: self.preHorzMargin + self.bounds.maxX, y: y)
                path.addLine(to: p1)
                
                drawText(x: 0, y: y, width: self.preHorzMargin, height: 50, text: String(format: "%.0f", yValue), color: .lightGray, alignment: .left)
            }
        }
        
        if xValues.count > 0 {
            let step = xValues.count > 1 ? (self.bounds.width - self.preHorzMargin - self.horzMargin * 2) / CGFloat(xValues.count - 1) : 0
            for i in 0 ..< xValues.count {
                if xValues.count > 10 && (self.dataAfter[i] == self.yValues[0]) && (self.dataBefore[i] == self.yValues[0]) {
                    
                } else {
                    let x = self.preHorzMargin + self.horzMargin + CGFloat(i) * step
                    drawText(x: x - step / 2, y: self.bounds.maxY - 10, width: step, height: 30, text: xValues[i])
                }
            }
        }

        let dashes: [CGFloat] = [8.0, 10.0]
        path.setLineDash(dashes, count: dashes.count, phase: 0.0)

        path.lineWidth = 0.5
        path.lineCapStyle = .butt
        UIColor.lightGray.set()
        path.stroke()
    }

    private func drawConnectLines() {
        if dataAfter.count == 0 || dataBefore.count == 0 {
            return
        }
        
        let path = UIBezierPath()

        let height = self.bounds.maxY - self.vertMargin * 2
        let h = self.yValues.last! - self.yValues.first!
        let min = self.yValues.first!

        let step = dataAfter.count > 1 ? (self.bounds.width - self.preHorzMargin - self.horzMargin * 2) / CGFloat(dataAfter.count - 1) : 0
        for i in 0 ..< dataAfter.count {
            let x = self.preHorzMargin + self.horzMargin + CGFloat(i) * step
            let y0 = self.bounds.maxY - (self.vertMargin + (dataAfter[i] - min) / h * height)
            let  p0 = CGPoint(x: x, y: y0)
            path.move(to: p0)
            let y1 = self.bounds.maxY - (self.vertMargin + (dataBefore[i] - min) / h * height)
            let  p1 = CGPoint(x: x, y: y1)
            path.addLine(to: p1)
        }
        
        colorLightGray.setStroke()
        path.lineWidth = lineWidth
        path.stroke()
    }
    
    private func drawTheLatestInfo() {
        
        if indexOfLatest == -1 {
            return
        }

        let height = self.bounds.maxY - self.vertMargin * 2
        let h = self.yValues.last! - self.yValues.first!
        let min = self.yValues.first!
        
        let step = dataAfter.count > 1 ? (self.bounds.width - self.preHorzMargin - self.horzMargin * 2) / CGFloat(dataAfter.count - 1) : 0
        
        let x = self.preHorzMargin + self.horzMargin + CGFloat(indexOfLatest) * step
        let y0 = self.bounds.maxY - (self.vertMargin + (dataAfter[indexOfLatest] - min) / h * height)
     
        drawPoint(point: CGPoint(x: x, y: y0), color: dotColorBefore, radius: dotRadius)
        
        drawText(x: x - 20, y: y0 + 25, width: 40, height: 30, text: String(format: "%.0f", dataAfter[indexOfLatest]), color: dotColorBefore)
    }
    
    private func drawLineOfAfter() {
        if dataAfter.count == 0 {
            return
        }

        let path = quadCurvedPath(data: dataAfter, dotColor: dotColorAfter)
        lineColorAfter.setStroke()
        path.lineWidth = lineWidth
        path.stroke()
    }

    private func coordYFor(index: Int, data: [CGFloat]) -> CGFloat {
        let height = self.bounds.maxY - self.vertMargin * 2
        let h = self.yValues.last! - self.yValues.first!
        let min = self.yValues.first!
        return bounds.maxY - (vertMargin + (data[index] - min) / h * height)
    }

    private func quadCurvedPath(data: [CGFloat], dotColor: UIColor) -> UIBezierPath {
        
        let path = UIBezierPath()
        let step = data.count > 1 ? (bounds.width - preHorzMargin - horzMargin * 2) / CGFloat(data.count - 1) : 0

        var p1 = CGPoint(x: preHorzMargin + horzMargin, y: coordYFor(index: 0, data: data))
        path.move(to: p1)

        if !indexsOfNoAfter.contains(0) {
            drawPoint(point: p1, color: dotColor, radius: dotRadius)
        }

        if (data.count == 2) {
            if !indexsOfNoAfter.contains(1) {//if no data, no line
                path.addLine(to: CGPoint(x: preHorzMargin + horzMargin + step, y: coordYFor(index: 1, data: data)))
            }
            return path
        }

        var oldControlP: CGPoint?

        for i in 1 ..< data.count {
            let p2 = CGPoint(x: preHorzMargin + horzMargin + step * CGFloat(i), y: coordYFor(index: i, data: data))
            
            if !indexsOfNoAfter.contains(i) {
                drawPoint(point: p2, color: dotColor, radius: dotRadius)
            }
            var p3: CGPoint?
            if i < data.count - 1 {
                p3 = CGPoint(x: preHorzMargin + horzMargin + step * CGFloat(i + 1), y: coordYFor(index: i + 1, data: data))
            }

            let newControlP = controlPointForPoints(p1: p1, p2: p2, next: p3)

            if !indexsOfNoAfter.contains(i) {//if no data, no line
                path.addCurve(to: p2, controlPoint1: oldControlP ?? p1, controlPoint2: newControlP ?? p2)
            }
            path.move(to: p2)//if no data, just move to point

            p1 = p2
            oldControlP = antipodalFor(point: newControlP, center: p2)
        }
        return path
    }

    /// located on the opposite side from the center point
    private func antipodalFor(point: CGPoint?, center: CGPoint?) -> CGPoint? {
        guard let p1 = point, let center = center else {
            return nil
        }
        let newX = 2 * center.x - p1.x
        let diffY = abs(p1.y - center.y)
        let newY = center.y + diffY * (p1.y < center.y ? 1 : -1)

        return CGPoint(x: newX, y: newY)
    }

    /// halfway of two points
    private func midPointForPoints(p1: CGPoint, p2: CGPoint) -> CGPoint {
        return CGPoint(x: (p1.x + p2.x) / 2, y: (p1.y + p2.y) / 2);
    }

    /// Find controlPoint2 for addCurve
    /// - Parameters:
    ///   - p1: first point of curve
    ///   - p2: second point of curve whose control point we are looking for
    ///   - next: predicted next point which will use antipodal control point for finded
    private func controlPointForPoints(p1: CGPoint, p2: CGPoint, next p3: CGPoint?) -> CGPoint? {
        guard let p3 = p3 else {
            return nil
        }

        let leftMidPoint  = midPointForPoints(p1: p1, p2: p2)
        let rightMidPoint = midPointForPoints(p1: p2, p2: p3)

        var controlPoint = midPointForPoints(p1: leftMidPoint, p2: antipodalFor(point: rightMidPoint, center: p2)!)

        if p1.y.between(a: p2.y, b: controlPoint.y) {
            controlPoint.y = p1.y
        } else if p2.y.between(a: p1.y, b: controlPoint.y) {
            controlPoint.y = p2.y
        }


        let imaginContol = antipodalFor(point: controlPoint, center: p2)!
        if p2.y.between(a: p3.y, b: imaginContol.y) {
            controlPoint.y = p2.y
        }
        if p3.y.between(a: p2.y, b: imaginContol.y) {
            let diffY = abs(p2.y - p3.y)
            controlPoint.y = p2.y + diffY * (p3.y < p2.y ? 1 : -1)
        }

        // make lines easier
        controlPoint.x += (p2.x - p1.x) * 0.1

        return controlPoint
    }

    private func drawPoint(point: CGPoint, color: UIColor, radius: CGFloat) {
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: point.x - radius, y: point.y - radius, width: radius * 2, height: radius * 2))
        
        color.setFill()
        ovalPath.fill()
    }
}
