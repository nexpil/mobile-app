//
//  HealthBloodPressureGraphView.swift
//  Nexpil
//
//  Created by Shine on 2/14/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class HealthBloodPressureGraphView: UIView {

    private let standardBefore  : CGFloat = 80
    private let standardAfter   : CGFloat = 120

    private let lineColorAfter  = UIColor(hex: "7ce2ec")
    private let lineColorBefore = UIColor(hex: "877cec")
    private let dotColorAfter   = UIColor(hex: "7ce2ec")
    private let dotColorBefore  = UIColor(hex: "877cec")
    
    private let colorDashLine   = UIColor.lightGray
    private let colorLightGray  = UIColor(hex: "f0f0f0")
    
    private let dotRadius       : CGFloat = 4
    private let lineWidth       : CGFloat = 8
    
    private let preHorzMargin   : CGFloat = 30
    private let horzMargin      : CGFloat = 22
    private let vertMargin      : CGFloat = 30
    
    var yValues: [CGFloat] = [60, 100, 140, 180, 220]
    var xValues: [String] = ["Nov 17", "Nov 19", "Nov 21", "Nov 23"]
    
    var dataAfter: [CGFloat] = [140, 180, 160, 140, 160, 180, 150]
    var dataBefore: [CGFloat] = [100, 140, 120, 100, 120, 140, 110]
    var indexsOfNoAfter: [Int] = []
    var indexsOfNoBefore: [Int] = []

    private var indexOfLatest = -1
    
    func setData(dataAfter: [CGFloat],
                 dataBefore: [CGFloat],
                 yValues: [CGFloat],
                 xValues: [String]) {

        DispatchQueue.main.async {
            self.indexOfLatest = -1
            
            self.yValues = yValues
            self.xValues = xValues
            self.dataAfter = dataAfter
            self.dataBefore = dataBefore
            
            self.filterData()
            
            self.setNeedsDisplay()
        }
    }
    
    private func filterData() {
        indexsOfNoAfter.removeAll()
        indexsOfNoBefore.removeAll()

        for (index, value) in dataAfter.enumerated() {
            if value > 0 {
                indexOfLatest = index
            } else {
                indexsOfNoAfter.append(index)
                dataAfter[index] = standardAfter
            }

            if dataAfter[index] < yValues[0] {
                dataAfter[index] = yValues[0]
            } else if dataAfter[index] > yValues.last! {
                dataAfter[index] = yValues.last!
            }
        }
        
        for (index, value) in dataBefore.enumerated() {
            if value > 0 {
                indexOfLatest = index
            } else {
                indexsOfNoBefore.append(index)
                dataBefore[index] = standardBefore
            }

            if dataBefore[index] < yValues[0] {
                dataBefore[index] = yValues[0]
            } else if dataBefore[index] > yValues.last! {
                dataBefore[index] = yValues.last!
            }
        }
    }
    
    override func draw(_ rect: CGRect) {
        
        if let sublayers = self.layer.sublayers {
            for layer in sublayers {
                layer.removeFromSuperlayer()
            }
        }

        drawDashLinesAndValues()
        
        drawConnectLines()
        drawTheLatestInfo()
        
        if dataAfter.count > 0 {
            drawDots(data: dataAfter, dotColor: dotColorAfter)
        }
        if dataBefore.count > 0 {
            drawDots(data: dataBefore, dotColor: dotColorBefore)
        }
    }

    private func drawText(x: CGFloat, y: CGFloat, width: CGFloat, height: CGFloat, text: String, color: UIColor = .lightGray, alignment: NSTextAlignment = .center) {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alignment
        let attrs = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.paragraphStyle: paragraphStyle]
        text.draw(with: CGRect(x: x, y: y - 10, width: width, height: height), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
    }

    private func drawDashLinesAndValues() {

        let path = UIBezierPath()

        if yValues.count > 0 {
            let height = self.bounds.maxY - self.vertMargin * 2
            let h = self.yValues.last! - self.yValues.first!
            let min = self.yValues.first!
            for yValue in yValues {
                let y = self.bounds.maxY - (self.vertMargin + (yValue - min) / h * height)
                let  p0 = CGPoint(x: self.preHorzMargin + self.bounds.minX, y: y)
                path.move(to: p0)
                let  p1 = CGPoint(x: self.preHorzMargin + self.bounds.maxX, y: y)
                path.addLine(to: p1)
                
                drawText(x: 0, y: y, width: self.preHorzMargin, height: 50, text: String(format: "%.0f", yValue), color: .lightGray, alignment: .left)
            }
        }
        
        if xValues.count > 0 {
            let step = xValues.count > 1 ? (self.bounds.width - self.preHorzMargin - self.horzMargin * 2) / CGFloat(xValues.count - 1) : 0
            for i in 0 ..< xValues.count {
                if xValues.count > 10 && (self.dataAfter[i] == self.yValues[0]) && (self.dataBefore[i] == self.yValues[0]) {
                    
                } else {
                    let x = self.preHorzMargin + self.horzMargin + CGFloat(i) * step
                    drawText(x: x - step / 2, y: self.bounds.maxY - 10, width: step, height: 30, text: xValues[i])
                }
            }
        }

        let dashes: [CGFloat] = [8.0, 10.0]
        path.setLineDash(dashes, count: dashes.count, phase: 0.0)

        path.lineWidth = 0.5
        path.lineCapStyle = .butt
        UIColor.lightGray.set()
        path.stroke()
    }

    private func drawConnectLines() {
        if dataAfter.count == 0 || dataBefore.count == 0 {
            return
        }
        
        let path = UIBezierPath()

        let height = self.bounds.maxY - self.vertMargin * 2
        let h = self.yValues.last! - self.yValues.first!
        let min = self.yValues.first!

        let step = dataAfter.count > 1 ? (self.bounds.width - self.preHorzMargin - self.horzMargin * 2) / CGFloat(dataAfter.count - 1) : 0
        for i in 0 ..< dataAfter.count {
            let x = self.preHorzMargin + self.horzMargin + CGFloat(i) * step
            let y0 = self.bounds.maxY - (self.vertMargin + (dataAfter[i] - min) / h * height)
            let  p0 = CGPoint(x: x, y: y0)
            path.move(to: p0)
            let y1 = self.bounds.maxY - (self.vertMargin + (dataBefore[i] - min) / h * height)
            let  p1 = CGPoint(x: x, y: y1)
            path.addLine(to: p1)
        }
        
        colorLightGray.setStroke()
        path.lineWidth = lineWidth
        path.stroke()
    }
    
    private func drawTheLatestInfo() {
        
        if indexOfLatest == -1 {
            return
        }

        let height = self.bounds.maxY - self.vertMargin * 2
        let h = self.yValues.last! - self.yValues.first!
        let min = self.yValues.first!

        let step = dataAfter.count > 1 ? (self.bounds.width - self.preHorzMargin - self.horzMargin * 2) / CGFloat(dataAfter.count - 1) : 0
        let x = self.preHorzMargin + self.horzMargin + CGFloat(indexOfLatest) * step
        let y0 = self.bounds.maxY - (self.vertMargin + (dataAfter[indexOfLatest] - min) / h * height)
        let y1 = self.bounds.maxY - (self.vertMargin + (dataBefore[indexOfLatest] - min) / h * height)

        let layer = CAGradientLayer()
        layer.frame = CGRect(x: x - lineWidth * 0.5, y: fmin(y0, y1), width: lineWidth, height: fabs(y1 - y0))
        layer.colors = [dotColorAfter.cgColor, dotColorBefore.cgColor]
        self.layer.addSublayer(layer)
        
        drawText(x: x - 20, y: y0 - 15, width: 40, height: 30, text: String(format: "%.0f", dataAfter[indexOfLatest]), color: .systemBlue)
        
        drawText(x: x - 20, y: y1 + 25, width: 40, height: 30, text: String(format: "%.0f", dataBefore[indexOfLatest]), color: .systemBlue)
    }
    
    private func coordYFor(index: Int, data: [CGFloat]) -> CGFloat {
        let height = self.bounds.maxY - self.vertMargin * 2
        let h = self.yValues.last! - self.yValues.first!
        let min = self.yValues.first!
        return bounds.maxY - (vertMargin + (data[index] - min) / h * height)
    }

    private func drawDots(data: [CGFloat], dotColor: UIColor) {
        
        let step = data.count > 1 ? (bounds.width - preHorzMargin - horzMargin * 2) / CGFloat(data.count - 1) : 0

        let p1 = CGPoint(x: preHorzMargin + horzMargin, y: coordYFor(index: 0, data: data))

        if !(indexsOfNoAfter.contains(0) && indexsOfNoBefore.contains(0)) {
            drawPoint(point: p1, color: dotColor, radius: dotRadius)
        }

        for i in 1 ..< data.count {
            let p2 = CGPoint(x: preHorzMargin + horzMargin + step * CGFloat(i), y: coordYFor(index: i, data: data))
            
            if !(indexsOfNoAfter.contains(i) && indexsOfNoBefore.contains(i)) {
                drawPoint(point: p2, color: dotColor, radius: dotRadius)
            }
        }
    }

    private func drawPoint(point: CGPoint, color: UIColor, radius: CGFloat) {
        let ovalPath = UIBezierPath(ovalIn: CGRect(x: point.x - radius, y: point.y - radius, width: radius * 2, height: radius * 2))
        
        color.setFill()
        ovalPath.fill()
    }
}
