//
//  UIRoundView.swift
//  Nexpil
//
//  Created by Shine on 2/12/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class UIRoundView: UIView {

    @IBInspectable public var radius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = self.radius
        }
    }
}
