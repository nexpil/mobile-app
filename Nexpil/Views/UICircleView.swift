//
//  UICircleView.swift
//  Nexpil
//
//  Created by Shine on 2/11/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit

class UICircleView: UIView {
    
    @IBInspectable public var bgColor: UIColor = .white {
        didSet {
            self.backgroundColor = bgColor
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.height * 0.5
    }
}
