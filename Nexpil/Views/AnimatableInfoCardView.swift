//
//  AnimatableInfoCardView.swift
//  Nexpil
//
//  Created by Arif on 5/26/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import UIKit
import DWAnimatedLabel

class AnimatableInfoCardView: UIView {

    @IBOutlet var identifier: DWAnimatedLabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var containerView: GradientView!
    
    var parent: InformationCardEditableHolder?
    var view: UIView!
    var width: CGFloat = 150 {
        didSet {
            self.view.frame.size.width = self.width
        }
    }
    
    @IBInspectable var identifierText: String = "Full Name" {
        didSet {
            self.identifier.text = identifierText
        }
    }
    
    @IBInspectable var valueText: String = "" {
        didSet {
            self.value.text = valueText
        }
    }
    
    @IBInspectable var identifierColor: UIColor = #colorLiteral(red: 0.2235294118, green: 0.8274509804, blue: 0.8901960784, alpha: 1) {
        didSet {
            self.identifier.textColor = identifierColor
        }
    }
    
    func setValueText(_ text: String) {
        if text != "" {
            self.valueText = text
            hideError()
        } else {
            showError()
        }
    }
    
    func animateAndSetValueText(_ text: String, unhideAfter seconds: TimeInterval, runAfterTime: TimeInterval) {
        self.identifier.alpha = 0
        self.value.alpha = 0
        self.valueText = text
        let viewIdentifier = createGradientView(frame: identifier.frame, cornerRadius: 9.5, topColor: #colorLiteral(red: 0.2235294118, green: 0.8274509804, blue: 0.8901960784, alpha: 1), bottomColor: #colorLiteral(red: 0.2235294118, green: 0.8274509804, blue: 0.8901960784, alpha: 1))
        let viewValue = createGradientView(frame: value.frame, cornerRadius: 12, topColor: UIColor(hex: "d7d7d7"), bottomColor: UIColor(hex: "646464"))
        containerView.addSubview(viewIdentifier)
        containerView.addSubview(viewValue)
            
        let width = self.value.intrinsicContentSize.width > self.containerView.frame.size.width ? self.containerView.frame.size.width - 30 : self.value.intrinsicContentSize.width
        UIView.animate(withDuration: 1, delay: runAfterTime, options: UIViewAnimationOptions.curveEaseIn, animations: {
            viewIdentifier.frame.size.width = self.identifier.intrinsicContentSize.width
            viewValue.frame.size.width = width
        }) { (finished) in
            runAfter(seconds) {
                self.identifier.alpha = 1
                UIView.animate(withDuration: 0.7, delay: 0, options: UIViewAnimationOptions.curveEaseIn, animations: {
                    viewIdentifier.frame.origin.x = self.identifier.intrinsicContentSize.width + self.identifier.frame.origin.x
                    viewIdentifier.frame.size.width = 0
                }) { (hasFinished) in
                    self.value.alpha = 1
                    UIView.animate(withDuration: 0.7, delay: 0.2, options: UIViewAnimationOptions.curveEaseIn, animations: {
                        if text == "                    " {
                            viewValue.alpha = 0
                            self.showError()
                        } else {
                            viewValue.frame.origin.x = width + self.value.frame.origin.x
                            viewValue.frame.size.width = 0
                        }
                    }) { (isFinished) in
                        viewValue.removeFromSuperview()
                        viewIdentifier.removeFromSuperview()
                        if text == "                    " {
                            self.valueText = ""
                        }
                    }
                }
            }
        }
    }
    
    func showError() {
        self.containerView.layer.borderColor = UIColor.systemRed.cgColor
        self.containerView.layer.borderWidth = 1
    }
    
    func hideError() {
        self.containerView.layer.borderWidth = 0
    }
    
    func createGradientView(frame: CGRect, cornerRadius: CGFloat, topColor: UIColor, bottomColor: UIColor) -> GradientView {
        let gradientView = GradientView(frame: frame)
        gradientView.cornerRadius = cornerRadius
        gradientView.topColor = topColor
        gradientView.bottomColor = bottomColor
        gradientView.frame.size.width = 20
        return gradientView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
        setup()
    }
    
    func xibSetup() {
        self.view = loadViewFromNib()
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(view)
        view.frame = self.bounds
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "AnimatableInfoCardView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    func setup() {
        self.identifier.text = identifierText
        self.value.text = valueText
        self.identifier.textColor = identifierColor
        self.backgroundColor = nil
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.hideShadow()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.showShadow()
        
        guard let touchPoint = touches.first?.location(in: self) else { return }
        if self.bounds.contains(touchPoint) {
            parent?.cardTapped(withIdentifier: identifierText)
        }
        super.touchesEnded(touches, with: event)
    }
    
    private func sizeHeaderToFit(headerView: UIView?) -> CGFloat {
        guard let headerView = headerView else {
            return 0.0
        }

        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()

        let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height

        return height
    }
}
