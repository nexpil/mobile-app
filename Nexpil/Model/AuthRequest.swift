//
//  AuthRequest.swift
//  Nexpil
//
//  Created by Arif on 12/4/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

struct AuthRequest: Codable {
    var email, password, firstName, lastName: String
    var timezone, deviceToken: String
    var patientId: Int

    enum CodingKeys: String, CodingKey {
        case email, password
        case firstName = "first_name"
        case lastName = "last_name"
        case timezone, deviceToken
        case patientId = "patient_id"
    }
}
