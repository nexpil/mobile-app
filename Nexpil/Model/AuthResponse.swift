//
//  AuthResponse.swift
//  Nexpil
//
//  Created by Arif on 12/4/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

// MARK: - AuthResponse
struct AuthResponse: Codable {
    var status: String?
    var userinfo: Patient?
    var message: String?
    var error: String?
}

// MARK: - Userinfo
struct Patient: Codable {
    var id, userId: Int?
    var usertype, language, phoneNumber, email: String?
    var street, city, state: String?
    var address: JSONNull?
    var zipcode, firstName, lastName, userName: String?
    var dob: String?
    var age: Int?
    var diagnosis: JSONNull?
    var diagnosisDate, applicationLocation, location, sex: String?
    var usercode, userimage, active: String?
    var taskcode: JSONNull?
    var createdAt, updatedAt, timezone, deviceToken: String?
    var morningStart, morningEnd, middayStart, middayEnd: Int?
    var eveningStart, eveningEnd, nightStart, nightEnd: Int?
    var patientGroupId: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case userId = "user_id"
        case usertype, language
        case phoneNumber = "phone_number"
        case email, street, city, state, address, zipcode
        case firstName = "first_name"
        case lastName = "last_name"
        case userName = "user_name"
        case dob = "DOB"
        case age, diagnosis
        case diagnosisDate = "diagnosis_date"
        case applicationLocation = "application_location"
        case location, sex, usercode, userimage, active, taskcode
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case timezone, deviceToken
        case morningStart = "morning_start"
        case morningEnd = "morning_end"
        case middayStart = "midday_start"
        case middayEnd = "midday_end"
        case eveningStart = "evening_start"
        case eveningEnd = "evening_end"
        case nightStart = "night_start"
        case nightEnd = "night_end"
        case patientGroupId = "patientGroupID"
    }
}
