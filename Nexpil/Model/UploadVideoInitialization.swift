//
//  UploadVideoInitialization.swift
//  Nexpil
//
//  Created by Arif on 5/28/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation
import Alamofire

// MARK: - UploadVideoInitializationResponse
struct UploadVideoInitializationResponse: Codable {
    var url: String?
    var fields: Fields?
}

// MARK: - Fields
struct Fields: Codable {
    var key, xAmzAlgorithm, xAmzCredential, xAmzDate: String?
    var xAmzSecurityToken, policy, xAmzSignature: String?

    enum CodingKeys: String, CodingKey {
        case key
        case xAmzAlgorithm = "x-amz-algorithm"
        case xAmzCredential = "x-amz-credential"
        case xAmzDate = "x-amz-date"
        case xAmzSecurityToken = "x-amz-security-token"
        case policy
        case xAmzSignature = "x-amz-signature"
    }
}

// MARK: - Helper functions for creating encoders and decoders

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

// MARK: - Alamofire response handlers

extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }

            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }

    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }

    @discardableResult
    func responseUploadVideoInitializationResponse(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<UploadVideoInitializationResponse>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}


extension DataRequest {
    @discardableResult
    func responseScanMedicationResponse(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<ScanMedicationResponse>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}
