//
//  NexpilTask.swift
//  Nexpil
//
//  Created by Golder on 4/28/20.
//  Copyright © 2020 Admin. All rights reserved.
//

import Foundation

class NexpilTask {
    var id          : String!
    var typeId      : String!
    var name        : String!
    var description : String!
    var frequency   : String!
    var location    : String!
    var phoneNo     : String!
    var faxNo       : String!
    var email       : String!
    var website     : String!
    var triggers    : String!
    var strStartDate: String!
    var strEndDate  : String!
    var addedDate   : String!
    var ownerId     : String!
    var physicianId : String!
    var code        : String!
    
    init (json: [String: Any]) {
        self.id             = json["id"]            as? String
        self.typeId         = json["type_id"]       as? String
        self.name           = json["name"]          as? String
        self.description    = json["description"]   as? String
        self.frequency      = json["frequency"]     as? String
        self.location       = json["location"]      as? String
        self.phoneNo        = json["phone_no"]      as? String
        self.faxNo          = json["fax_no"]        as? String
        self.email          = json["email"]         as? String
        self.website        = json["website"]       as? String
        self.triggers       = json["triggers"]      as? String
        self.strStartDate   = json["start_date"]    as? String
        self.strEndDate     = json["end_date"]      as? String
        self.addedDate      = json["added_date"]    as? String
        self.ownerId        = json["owner_id"]      as? String
        self.physicianId    = json["physician_id"]  as? String
        self.code           = json["code"]          as? String
    }
}
